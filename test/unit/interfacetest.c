/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  interfacetest.c  TESTING interface.c                                     */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>		/* exit() */
#include "print.h"
#include "compile.h"
#include "common.h"

#define YES 1
#define NO 0

#define ANONYMOUS 0
#define NOT_RECURSIVE 0
#define RECURSIVE 1

#define HAS_CAPTURES 1
#define HEADFAIL 1
#define NULLABLE 1
#define NOFAIL 1
#define FIRSTSET 1
#define NEEDFOLLOW 1
#define HEADFAIL 1

#define NO_CAPTURES 0
#define NOT_HEADFAIL 0
#define NOT_NULLABLE 0
#define CAN_FAIL 0
#define NO_FIRSTSET 0
#define NO_NEEDFOLLOW 0
#define NO_HEADFAIL 0

/* Borrowed from analyze.c */
/* Follow ref, extract binding field, return NULL if it is not a Pattern. */
static Pattern *binding_pattern_value (Ref ref) {
  Binding *b = env_get_binding(ref);
  if (!b) return NULL;		/* invalid ref */
  if (b->val.type != Epattern_t) return NULL;
  return (Pattern *) b->val.ptr;
}

#define failif(test, msg)						\
  do { if (test) { fprintf(stderr, "Failed %s:%d: %s\n", __FILE__,	\
			   __LINE__, msg);				\
      fflush(stderr);							\
      exit(-1);								\
    }									\
  } while (0)

int main(void) {

  int stat;
  char name[2];
  Context *C;
  Index id, index;
  Package *pkg, *other_package;
  SymbolTableEntry *entry;
  Error err;
  Binding *b;
  
  Pattern *p; 
  Env *toplevel, *genv; 

  Expression *exp, *A, *B, *S;

  Env *env, *a_random_env;
  Ref ref, refA, refB, refC, refD, refS, ref_anon;
  XRef xref;

  printf("$$$ interfacetest\n");

  /* Testing the error-checking */
  toplevel = pexlc_make_env(NULL);
  failif(!toplevel, "new toplevel should succeed");
  pexlc_env_free(toplevel);
  toplevel = pexlc_make_recursive_env(NULL);
  failif(!toplevel, "new toplevel should succeed");
  pexlc_env_free(toplevel);

  C = pexlc_make_context(NULL);
  failif(C, "should return null because arg cannot be null");
  
  toplevel = pexlc_make_env(NULL);
  failif(!toplevel, "new toplevel should succeed");
  
  C = pexlc_make_context(toplevel);
  failif(!C, "expected new context");

  ref = pexlc_make_binding(NULL, toplevel, "number 1");
  failif(!ref_is_error(ref), "context arg cannot be null");
  failif(ref.index != EXP_ERR_NULL, "error code should be indicated");
  ref = pexlc_make_binding(C, NULL, "number 2");
  failif(!ref_is_error(ref), "env arg cannot be null");
  failif(ref.index != EXP_ERR_NULL, "error code should be indicated");  
  
  ref = pexlc_make_binding(C, toplevel, NULL);
  printf("ref.index = %d\n", ref.index);
  failif(ref_is_error(ref), "should succeed and create an anonymous binding");
  failif(ref.env != toplevel, "checking env field");
  failif(ref.index != 0, "first actual binding is in slot 0");

  exp = pexle_from_boolean(C, 1);
  pexlc_bind_expression(NULL, exp, ref, &err);
  failif(err.value != EXP_ERR_NULL, "should catch null context arg");
  pexlc_bind_expression(C, NULL, ref, &err);
  failif(err.value != EXP_ERR_NULL, "should catch null exp arg");
  ref.env = NULL;
  ref.index = -1;		/* negative value indicates error */
  pexlc_bind_expression(C, exp, ref, &err);
  failif(err.value != ERR_BADREF, "should catch null env in ref");
  ref.env = NULL;
  ref.index = 0;		/* index == 0 and NULL env is "NOT FOUND" */
  pexlc_bind_expression(C, exp, ref, &err);
  failif(err.value != ERR_BADREF, "should catch NOT FOUND ref");
  ref.env = NULL;
  ref.index = 1;		/* index > 0 and NULL env corrupts ref */
  pexlc_bind_expression(C, exp, ref, &err);
  failif(err.value != ERR_INTERNAL, "should catch corrupt ref");

  pexle_free(exp);
  
  /* ----------------------------------------------------------------------------- */
  printf("********************** Non-recursive block using anonymous exps \n");

  env = pexlc_make_env(toplevel);
  failif(!env, "should succeed");

  refS = pexlc_make_binding(C, env, NULL); /* NULL == anonymous binding */
  failif(ref_is_error(ref), "bind should work");

  A = pexle_call(C, refS);			     /* A -> S */
  S = pexle_capture_f(C, "foo", pexle_from_boolean(C, 1)); /* S -> capture("foo", true) */

  /* First a test for error-checking  */
  a_random_env = env_new(NULL, NO);
  failif(!a_random_env, "expected a fresh env, as prereq for next test");
  pkg = pexlc_compile_expression(C, A, a_random_env, &err, NO_OPTIM);
  failif(pkg, "should catch error");
  failif(err.value != EXP_ERR_SCOPE, "should be a scope error");

  pkg = pexlc_compile_expression(C, A, env, &err, NO_OPTIM);
  failif(pkg, "should catch that refS does not contain a pattern");
  failif(err.value != EXP_ERR_NO_PATTERN, "should indicate error");

  pexlc_bind_expression(C, S, refS, &err);
  failif(err.value != OK, "should succeed");
  b = env_get_binding(refS);
  failif(!b, "should get a binding");
  failif(b->val.type != Epattern_t, "expression S converts into pattern type");

  pkg = pexlc_compile_expression(C, A, env, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  failif(err.value != OK, "should indicate ok");

  print_package(pkg);
  print_symboltable_stats(pkg->symtab);
  print_symboltable(pkg->symtab);

  failif(pkg->symtab->next < 1, "'foo' should be in slot 0 of the table");
  failif(strcmp(symboltable_get_name(pkg->symtab, 0), "foo") != 0,
	 "checking for 'foo', which should be in slot 0 of the table");

  package_free(pkg); pkg = NULL;
  printf("-----\n");

  pexle_free(A);
  
  /* ----------------------------------------------------------------------------- */
  printf("********************** Recursive block using anonymous exps \n");

  printf("Trying a single rule that is left recursive due to a self loop\n"); 
  genv = pexlc_make_recursive_env(toplevel);
  failif(!genv, "should succeed");
  refA = pexlc_make_binding(C, genv, "A");
  failif(ref_is_error(refA), "bind of unspecified value should work");
  A = pexle_call(C, refA);		/* A -> A */
  pexlc_bind_expression(C, A, refA, &err);
  failif(err.value != OK, "should succeed");

  /* First try compiling in env, which should give scope error (cannot call into child, genv) */
  pkg = pexlc_compile_expression(C, A, env, &err, NO_OPTIM);
  failif(pkg, "should fail");
  failif(err.value != EXP_ERR_SCOPE, "should indicate scope error");

  pkg = pexlc_compile_expression(C, A, genv, &err, NO_OPTIM);
  failif(pkg, "should fail due to left recursion");
  failif(err.value != EXP_ERR_LEFT_RECURSIVE, "should indicate error");

  printf("-----\n");

  printf("Trying mutually left-recursive rules\n"); 
  refA = pexlc_make_binding(C, genv, "A");
  failif(!ref_is_error(refA), "binding with name A already exists");
  failif(refA.index != ENV_ERR_EXISTS, "binding with name A already exists");

  refA = pexlc_make_binding(C, genv, ""); /* Anonymous is ok */
  failif(ref_is_error(refA), "bind should work");
  refB = pexlc_make_binding(C, genv, ""); /* Anonymous is ok */
  failif(ref_is_error(refB), "bind should work");
  A = pexle_call(C, refB);		/* A -> B */
  B = pexle_call(C, refA);		/* B -> A */

  pexlc_bind_expression(C, A, refA, &err);
  failif(err.value != OK, "rebind of A should work");

  pexlc_bind_expression(C, B, refB, &err);
  failif(err.value != OK, "rebind of B should work");

  print_env(genv, C);

  /* Compile B */
  pkg = pexlc_compile_expression(C, B, genv, &err, NO_OPTIM); 
  failif(pkg, "should catch left recursion"); 
  failif(err.value != EXP_ERR_LEFT_RECURSIVE, "should detect left recursion"); 
  
  printf("-----\n");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Grammar with realistic recursion \n");

  /* New env for grammar */
  pexlc_env_free(genv);
  genv = pexlc_make_recursive_env(env);
  failif(!genv, "failed to create new env");

  id = context_intern(C, "foo"); /* for later use */
  failif(id < 0, "symbol add should succeed");

  ref = pexlc_make_binding(C, genv, "A");
  failif(ref_is_error(ref), "bind should work");

  A = pexle_choice_f(C,		/* A -> "a" A "b" / epsilon */
		 pexle_seq_f(C,
			 pexle_from_string(C, "a"),
			 pexle_seq_f(C, 
				 pexle_call(C, ref),
				 pexle_from_string(C, "b"))),
		 pexle_from_boolean(C, 1));

  failif(!A, "A is a valid expression");
  printf("A is: \n"); 
  print_exp(A, 0, C); 

  /* Error check: if we try to compile A, we'll get an error because A calls a binding to Unspecified */
  pkg = pexlc_compile_expression(C, A, genv, &err, NO_OPTIM);
  failif(pkg, "should catch error");
  failif(err.value != EXP_ERR_NO_PATTERN, "should catch error");
  
  /* Now modify the "A" binding's value to the pattern based on the expression A */
  pexlc_bind_expression(C, A, ref, &err);
  failif(err.value != OK, "bind should work");

  pkg = pexlc_compile_expression(C, A, genv, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  failif(err.value != OK, "bind should work");

  print_package(pkg);
  package_free(pkg);
  pkg = NULL;

  printf("-----\n");

  /* ----------------------------------------------------------------------------- */
  printf("********************** No mutual left recursion in this example \n");

  pexlc_env_free(genv);
  genv = pexlc_make_recursive_env(env);
  failif(!genv, "failed to create new env");
  
  refA = pexlc_make_binding(C, genv, "A");
  failif(ref_is_error(refA), "bind should work");

  /* Here we use the variable B to hold the anonymous expression */
  /* <anon> -> "x" / A */
  B = pexle_choice_f(C,
		 pexle_from_string(C, "x"),
		 pexle_call(C, refA));

  failif(!B, "expected new expression");

  ref_anon = pexlc_make_binding(C, genv, NULL);
  failif(ref_is_error(ref_anon), "expected success");
  pexlc_bind_expression(C, B, ref_anon, &err);
  failif(err.value != OK, "bind should work");

  /* Here we reuse the variable B for no good reason */
  /* B -> "a"+ <anon> */
  B = pexle_seq_f(C, 
		pexle_repeat_f(C, pexle_from_string(C, "a"), 1), /* "a"+ */
		pexle_call(C, ref_anon)			 /* anon */
		);
  failif(!B, "expected new expression");

  refB = pexlc_make_binding(C, genv, "B");
  failif(ref_is_error(refB), "expected success");
  pexlc_bind_expression(C, B, refB, &err);
  failif(err.value != 0, "expected success");

  A = pexle_choice_f(C,		/* A -> B "b" / epsilon */
		 pexle_seq_f(C, 
			 pexle_call(C, refB),
			 pexle_from_string(C, "b")),
		 pexle_from_boolean(C, 1));

  failif(!A, "A is a valid expression");

  /* Now modify the "A" binding's value to the pattern based on the expression A */
  pexlc_bind_expression(C, A, refA, &err);
  failif(err.value != 0, "expected success");

  printf("<anon> is: \n");  
  p = binding_pattern_value(ref_anon); 
  failif(!p, "expected a pattern"); 
  print_pattern(p, 0, C);  
  printf("B is: \n");  
  p = binding_pattern_value(refB); 
  failif(!p, "expected a pattern"); 
  print_pattern(p, 0, C);  
  printf("A is: \n");  
  p = binding_pattern_value(refA); 
  failif(!p, "expected a pattern"); 
  print_pattern(p, 0, C);  

  pkg = pexlc_compile_bound_expression(C, refA, &err, NO_OPTIM);
  failif(!pkg, "expected success");
  failif(err.value != 0, "expected success");
  print_env(genv, C);
  print_package(pkg);

  printf("Pattern at refB:\n");
  print_pattern(binding_pattern_value(refB), 4, C);

  package_free(pkg);
  pkg = NULL;

  printf("-----\n");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Grammar with chain of recursive rules \n");

  /* New env for grammar */
  pexlc_env_free(genv);
  genv = pexlc_make_recursive_env(env);
  failif(!genv, "failed to create new env");

  id = context_intern(C, "foo"); /* for later use */
  failif(id < 0, "symbol add should succeed");

  refA = pexlc_make_binding(C, genv, "A");
  failif(ref_is_error(ref), "bind should work");
  refB = pexlc_make_binding(C, genv, "B");
  failif(ref_is_error(ref), "bind should work");
  refC = pexlc_make_binding(C, genv, "C");
  failif(ref_is_error(ref), "bind should work");
  refD = pexlc_make_binding(C, genv, "D");
  failif(ref_is_error(ref), "bind should work");

  exp = pexle_call(C, refB);		/* A -> B */
  pexlc_bind_expression(C, exp, refA, &err);
  failif(err.value != OK, "bind should work");
  
  exp = pexle_call(C, refC);		/* B -> C */
  pexlc_bind_expression(C, exp, refB, &err);
  failif(err.value != OK, "bind should work");

  exp = pexle_call(C, refD);		/* C -> D */
  pexlc_bind_expression(C, exp, refC, &err);
  failif(err.value != OK, "bind should work");

  exp = pexle_seq_f(C,		/* D -> "hi" A */
		pexle_from_string(C, "hi"),
		pexle_call(C, refA));
  pexlc_bind_expression(C, exp, refD, &err);
  failif(err.value != OK, "bind should work");

  pkg = pexlc_compile_bound_expression(C, refA, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  failif(err.value != OK, "bind should work");

  print_package(pkg);
  print_symboltable(pkg->symtab);
  package_free(pkg);
  pkg = NULL;

  printf("-----\n");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Misc patterns \n");

  /* New env for grammar */
  pexlc_env_free(genv);
  genv = pexlc_make_recursive_env(env);
  failif(!genv, "failed to create new env");

  exp = pexle_from_set(C, "abcABC", 6);
  failif(!exp, "exp should be a valid expression");
  pkg = pexlc_compile_expression(C, exp, genv, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  pexle_free(exp);

  print_package(pkg);
  print_symboltable(pkg->symtab);
  package_free(pkg);

  exp = pexle_from_bytes(C, "AB\0", 3);
  failif(!exp, "exp should be a valid expression");
  pkg = pexlc_compile_expression(C, exp, genv, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  pexle_free(exp);
  
  print_package(pkg);
  print_symboltable(pkg->symtab);
  package_free(pkg);

  exp = pexle_from_number(C, 3);	/* any 3 bytes */
  failif(!exp, "exp should be a valid expression");
  pkg = pexlc_compile_expression(C, exp, genv, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  pexle_free(exp);

  print_package(pkg);
  print_symboltable(pkg->symtab);
  package_free(pkg);

  exp = pexle_from_number(C, -3); /* NOT 3 bytes */
  failif(!exp, "exp should be a valid expression");
  pkg = pexlc_compile_expression(C, exp, genv, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  pexle_free(exp);
  
  print_package(pkg);
  print_symboltable(pkg->symtab);
  package_free(pkg);

  exp = pexle_from_number(C, 0);	/* same as TRUE (== epsilon)*/
  failif(!exp, "exp should be a valid expression");
  pkg = pexlc_compile_expression(C, exp, genv, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  pexle_free(exp);

  print_package(pkg);
  print_symboltable(pkg->symtab);
  package_free(pkg);

  exp = pexle_from_range(C, 65, 64);  /* Error checking */
  failif(exp, "error when from > to");
  exp = pexle_from_range(C, 65, 64 + 26);	/* [A-Z] */
  failif(!exp, "exp should be a valid expression");
  pkg = pexlc_compile_expression(C, exp, genv, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  pexle_free(exp);

  print_package(pkg);
  print_symboltable(pkg->symtab);
  package_free(pkg);

  exp = pexle_halt(C);
  failif(!exp, "exp should be a valid expression");
  pkg = pexlc_compile_expression(C, exp, genv, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  pexle_free(exp);

  print_package(pkg);
  print_symboltable(pkg->symtab);
  package_free(pkg);

  exp = pexle_not_f(C, NULL);
  failif(exp, "error, second arg should be an expression");
  exp = pexle_not_f(C, pexle_from_string(C, "JAJ"));
  failif(!exp, "exp should be a valid expression");
  pkg = pexlc_compile_expression(C, exp, genv, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  pexle_free(exp);

  print_package(pkg);
  print_symboltable(pkg->symtab);
  package_free(pkg);

  exp = pexle_lookahead_f(C, NULL);
  failif(exp, "error, second arg should be an expression");
  exp = pexle_lookahead_f(C, pexle_from_string(C, "JAJ"));
  failif(!exp, "exp should be a valid expression");
  pkg = pexlc_compile_expression(C, exp, genv, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  pexle_free(exp);
  
  print_package(pkg);
  print_symboltable(pkg->symtab);
  package_free(pkg);

/*   exp = pexle_lookbehind(C, NULL); */
/*   failif(exp, "error, second arg should be an expression"); */
/*   exp = pexle_lookbehind(C, pexle_from_string(C, "JAJ")); */
/*   failif(!exp, "exp should be a valid expression"); */
  printf("TODO: NOT COMPILING lookbehind right now.  Re-enable when ready.\n");
/*   pkg = pexlc_compile_expression(C, exp, genv, &err); */
/*   failif(!pkg, "should succeed"); */
  
/*   print_package(pkg); */
/*   print_symboltable(pkg->symtab); */
/*   package_free(pkg); */

  exp = pexle_subtract_f(C, pexle_from_range(C, 'x', 'z'), NULL);
  failif(exp, "error, second arg should be an expression");
  exp = pexle_subtract_f(C, NULL, pexle_from_string(C, "y"));
  failif(exp, "error, second arg should be an expression");
  exp = pexle_subtract_f(C, pexle_from_range(C, 'x', 'z'), pexle_from_string(C, "y"));
  failif(!exp, "exp should be a valid expression");
  pkg = pexlc_compile_expression(C, exp, genv, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  pexle_free(exp);

  print_package(pkg);
  print_symboltable(pkg->symtab);
  package_free(pkg);

  exp = pexle_repeat_f(C, NULL, 0);
  failif(exp, "error, second arg should be an expression");
  exp = pexle_repeat_f(C, pexle_from_string(C, "a"), 0);
  failif(!exp, "exp should be a valid expression");
  pkg = pexlc_compile_expression(C, exp, genv, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  pexle_free(exp);

  print_package(pkg);
  print_symboltable(pkg->symtab);
  package_free(pkg);

  exp = pexle_repeat_f(C, pexle_from_string(C, "a"), 4);
  failif(!exp, "exp should be a valid expression");
  pkg = pexlc_compile_expression(C, exp, genv, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  pexle_free(exp);
  
  print_package(pkg);
  print_symboltable(pkg->symtab);
  package_free(pkg);

  exp = pexle_repeat_f(C, pexle_from_string(C, "a"), -4);
  failif(!exp, "exp should be a valid expression");
  pkg = pexlc_compile_expression(C, exp, genv, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  pexle_free(exp);
  
  print_package(pkg);
  print_symboltable(pkg->symtab);
  package_free(pkg);

  exp = pexle_capture_f(C, "This is a long capture name", NULL);
  failif(exp, "error, second arg should be an expression");
  exp = pexle_capture_f(C, NULL, pexle_from_string(C, "x"));
  failif(exp, "error, second arg should be an expression");
  exp = pexle_capture_f(C, "This is a long capture name", pexle_from_string(C, "a"));
  failif(!exp, "exp should be a valid expression");
  pkg = pexlc_compile_expression(C, exp, genv, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  pexle_free(exp);
  
  print_package(pkg);
  print_symboltable(pkg->symtab);
  package_free(pkg);

  exp = pexle_constant(C, "foo", NULL);
  failif(exp, "error, second arg should be a string");
  exp = pexle_constant(C, NULL, "foo");
  failif(exp, "error, second arg should be a string");
  exp = pexle_constant(C, "This is a long capture name", "And a long value name");
  failif(!exp, "exp should be a valid expression");
  pkg = pexlc_compile_expression(C, exp, genv, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  pexle_free(exp);

  print_package(pkg);
  print_symboltable(pkg->symtab);

  package_free(pkg);
  pkg = NULL;

  printf("-----\n");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Using compile_environment \n");

#define bind(exp, env, name) do {	 \
    ref = pexlc_make_binding(C, (env), (name));	   \
    failif(ref_is_error(ref), "bind should work"); \
    name[0]++; \
    pexlc_bind_expression(C, (exp), ref, &err);		\
    failif(err.value != OK, "rebind should work");	\
  } while (0)

  /* New env */
  pexlc_env_free(genv);
  genv = pexlc_make_env(env);
  failif(!genv, "failed to create new env");

  name[0] = 'a';
  name[1] = '\0';
  exp = pexle_from_set(C, "abcABC", 6);
  failif(!exp, "exp should be a valid expression");
  bind(exp, genv, name);

  /* b */
  exp = pexle_from_bytes(C, "AB\0", 3);
  failif(!exp, "exp should be a valid expression");
  bind(exp, genv, name);

  /* c */
  exp = pexle_from_number(C, 3);	/* any 3 bytes */
  failif(!exp, "exp should be a valid expression");
  bind(exp, genv, name);
  
  /* d */
  exp = pexle_from_number(C, -3); /* NOT 3 bytes */
  failif(!exp, "exp should be a valid expression");
  bind(exp, genv, name);

  /* e */
  exp = pexle_from_number(C, 0);	/* same as TRUE (== epsilon)*/
  failif(!exp, "exp should be a valid expression");
  bind(exp, genv, name);

  /* f */
  exp = pexle_from_range(C, 65, 64);  /* Error checking */
  failif(exp, "error when from > to");
  exp = pexle_from_range(C, 65, 64 + 26);	/* [A-Z] */
  failif(!exp, "exp should be a valid expression");
  bind(exp, genv, name);
/*   pexle_free(exp); */

  /* g */
  exp = pexle_halt(C);
  failif(!exp, "exp should be a valid expression");
  bind(exp, genv, name);
/*   pexle_free(exp); */

  /* h */
  exp = pexle_not_f(C, NULL);
  failif(exp, "error, second arg should be an expression");
  exp = pexle_not_f(C, pexle_from_string(C, "JAJ"));
  failif(!exp, "exp should be a valid expression");
  bind(exp, genv, name);
/*   pexle_free(exp); */

  /* i */
  exp = pexle_lookahead_f(C, NULL);
  failif(exp, "error, second arg should be an expression");
  exp = pexle_lookahead_f(C, pexle_from_string(C, "JAJ"));
  failif(!exp, "exp should be a valid expression");
  bind(exp, genv, name);
/*   pexle_free(exp); */

  /* j */
  exp = pexle_subtract_f(C, pexle_from_range(C, 'x', 'z'), NULL);
  failif(exp, "error, second arg should be an expression");
  exp = pexle_subtract_f(C, NULL, pexle_from_string(C, "y"));
  failif(exp, "error, second arg should be an expression");
  exp = pexle_subtract_f(C, pexle_from_range(C, 'x', 'z'), pexle_from_string(C, "y"));
  failif(!exp, "exp should be a valid expression");
  bind(exp, genv, name);
/*   pexle_free(exp); */

  /* k */
  exp = pexle_repeat_f(C, NULL, 0);
  failif(exp, "error, second arg should be an expression");
  exp = pexle_repeat_f(C, pexle_from_string(C, "a"), 0);
  failif(!exp, "exp should be a valid expression");
  bind(exp, genv, name);
/*   pexle_free(exp); */

  /* l */
  exp = pexle_repeat_f(C, pexle_from_string(C, "a"), 4);
  failif(!exp, "exp should be a valid expression");
  bind(exp, genv, name);
/*   pexle_free(exp); */

  /* m */
  exp = pexle_repeat_f(C, pexle_from_string(C, "a"), -4);
  failif(!exp, "exp should be a valid expression");
  bind(exp, genv, name);
/*   pexle_free(exp); */

  /* n */
  exp = pexle_capture_f(C, "This is a long capture name", NULL);
  failif(exp, "error, second arg should be an expression");
  exp = pexle_capture_f(C, NULL, pexle_from_string(C, "x"));
  failif(exp, "error, second arg should be an expression");
  exp = pexle_capture_f(C, "This is a long capture name", pexle_from_string(C, "a"));
  failif(!exp, "exp should be a valid expression");
  bind(exp, genv, name);
/*   pexle_free(exp); */

  /* o */
  exp = pexle_constant(C, "foo", NULL);
  failif(exp, "error, second arg should be a string");
  exp = pexle_constant(C, NULL, "foo");
  failif(exp, "error, second arg should be a string");
  exp = pexle_constant(C, "This is a long capture name", "And a long value name");
  failif(!exp, "exp should be a valid expression");
  bind(exp, genv, name);
/*   pexle_free(exp); */
  
  /* SKIPPING LOOKBEHIND FOR NOW */
/*   exp = pexle_lookbehind(C, NULL); */
/*   failif(exp, "error, second arg should be an expression"); */
/*   exp = pexle_lookbehind(C, pexle_from_string(C, "JAJ")); */
/*   failif(!exp, "exp should be a valid expression"); */
  printf("TODO: NOT COMPILING lookbehind right now.  Re-enable when ready.\n");

  print_env(genv, C);

  pkg = pexlc_compile_environment(C, genv, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  failif(err.value != OK, "should work");
  
  print_package(pkg);
  print_symboltable(pkg->symtab);

  other_package = pkg;

  printf("-----\n");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Using xcall into a compiled package \n");

  failif(!other_package, "pre-requisite for this test");
  entry = symboltable_search(other_package->symtab, "l", &index);
  failif(!entry, "should find pattern named 'l' which was created in prior test");
  failif(index < 0, "should set the return value to index of symbol table entry");

  xref.pkg = other_package;
  xref.index = index;
  exp = pexle_xcall(C, xref);
  failif(!exp, "should succeed");
  printf("exp is xcall(%p, %d)\n", (void *) xref.pkg, xref.index);

  /* Error check */
  pkg = pexlc_compile_expression(C, exp, env, &err, NO_OPTIM);
  failif(pkg, "should fail because other_package is not in the package table");
  failif(err.value != CODEGEN_ERR_NO_PACKAGE, "should fail because other_package is not in the package table");

  package_set_importpath(other_package, "abc");
  stat = packagetable_add(C->packages, other_package);
  failif(stat < 0, "should succeed");
  index = packagetable_getnum(C->packages, other_package);
  failif(index < 0, "should find the package");

  pkg = pexlc_compile_expression(C, exp, env, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  pexle_free(exp);

  print_package(pkg);
  print_symboltable(pkg->symtab);

  package_free(other_package);
  
  pexlc_env_free(genv);
  package_free(pkg);
  pexlc_env_free(env);
  pexlc_env_free(a_random_env);
  pexlc_env_free(toplevel);
  context_free(C);
  
  /* ----------------------------------------------------------------------------- */
  printf("\nDone.\n");
  printf("Don't forget to run vmtest next.\n");

  return 0;
}

