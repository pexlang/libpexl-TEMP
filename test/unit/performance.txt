- In the VM, we should be able to predict the next instruction type in some
  cases.  If we finish instruction with opcode X and we know that opcode Y often
  follows, we can peek ahead to see if the next instruction is Y.  If so, we can
  jump directly to its stanza in the VM.  This will be particularly advantageous
  if it lets us bypass any ceremony associated with the fetch/decode/execute
  loop (of which there is almost nothing today).  Apart from that, the expected
  gain is from avoiding the switch (or computed goto), where branch prediction
  is limited or useless.  Instead, a simple conditional jump allows the branch
  predictor to start executing the likely path ahead of knowing whether the next
  opcode was the predicted one. 

- See Agner Fog's "manuals": https://www.agner.org/optimize/

- Consider passing pointers to Values (but keep the actual Value in the Binding
  as we do now -- Sunday, March 8, 2020).  Also, consider passing these pointers
  around as const when possible.

- Do not use static data frequently, as it is slow on Linux and OpenBSD.
  E.g. when calling toposort, pass in a local (stack allocated) instance of the
  null coordinates.

- Use alloca for stack allocation of initial (SMALL) stacks, allowing realloc to
  move them if they ever need to expand.

- Look into SSE4.x string instructions (accessed via intrinsics).
- Look into AVX / AVX2 instructions for comparing, say, 32 chars at a time.
- Use -msse4.1, -mAVX, -mAVX2 to enable these instruction sets.

- IMPLEMENT TWO SEARCH INSTRUCTIONS in the PEXL vm: Isrchset (finds first byte
  of input that is in a given character set) and Isrchstr (substring search).
  SSE INSTRUCTION:
  From https://www.strchr.com/strcmp_and_strlen_using_sse_4.2:
    MovDqU    xmm0, dqword[str1]
    PcmpIstrI xmm0, dqword[str2], imm

    PcmpIstrI is one of the new string-handling instructions comparing their
    operands. The first operand is always an SSE register (typically loaded with
    MovDqU). The second operand can be a memory location. Note the immediate
    operand, which consists of several bit fields controlling operation modes.

    Equal any (imm[3:2] = 00). The first operand is a character set, the second
    is a string (think of strspn or strcspn). The bit mask includes 1 if the
    character belongs to a set, 0 if not:

    operand2 = "You Drive Me Mad", operand1 = "aeiouy"
    IntRes1  =  0110001010010010

    Ranges (imm[3:2] = 01). The first operand consists of ranges, for example,
    "azAZ" means "all characters from a to z and all characters from A to Z":

    operand2 = "I'm here because", operand1 = "azAZ"
    IntRes1  =  1010111101111111

    Equal ordered (imm[3:2] = 11). Substring search (strstr). The first operand
    contains a string to search for, the second is a string to search in. The
    bit mask includes 1 if the substring is found at the corresponding position:

    operand2 = "WhenWeWillBeWed!", operand1 = "We"
    IntRes1  =  000010000000100

- Change buf.c to copy numbers into the buffer directly (memcpy, which will be
  optimized by the compiler) instead of processing one byte at a time.  If
  compiled for a big-endian machine, will need to reverse the bytes though.
    #include <stdint .h>
    #define IS_BIG_ENDIAN (*(uint16_t *)"\0\xff" < 0x100)
  OR
    __BYTE_ORDER__
    __ORDER_LITTLE_ENDIAN__
    __ORDER_BIG_ENDIAN__
    __ORDER_PDP_ENDIAN__

- Use 'restrict' keyword when we can ensure no pointer aliasing.

- Keep initial table sizes large enough to avoid resizing them, because realloc
  is expensive.

- Consider linking in a better implementation of malloc.

- COUNT operations (of some kind) in the vm instead of reporting the real time
  clock to users who want to measure pattern performance.  POSSIBLY use a CPU
  counter?  The time will vary with clock speed, but it would give a good
  picture at the CPU level of work done in matching.

- In the string hashcode function, replace the modulo operation with something
  that knows the tablesize is a power of 2.  (Also, then, start storing the
  tablesize as the exponent.)  THE COMPILER MAY OPTIMIZE THIS ALREADY.

- Use -fomit-frame-pointer (/Oy on Windows) but make sure that all program
  components were compiled with the same compiler.

- Opposite of EXPORT is __attribute__((visibility("internal"))).  Use this.

- Investigate Fog's Asmlib.

- "Accessing a variable or object through a pointer or reference may be just as
  fast as accessing it directly [due to modern processor design]."  -- Agner Fog

- Consider "computed goto" instead of (or for portability, along with) the
  switch statement in the vm.  Because this avoids bounds checking, we should
  try to arrange things so that we can jump based on k bits of the opcode, and
  provide all 2^k possible destination addresses.
  See e.g. https://eli.thegreenplace.net/2012/07/12/computed-goto-for-efficient-dispatch-tables
           https://bullno1.com/blog/switched-goto
	   
- Encode instructions using the low 3 or 4 bits as the "primary opcode" which
  determine the target of the computed goto.  Then, Call and Jump become a
  1-word instruction in which the target address is obtained by right shifting.
  Relative jumps are shifted using sign extension (arithmetic shift) and the
  target of absolute calls are shifted via logical shift.
    - Limiting the number of instructions to, say, 28 bits, means an instruction
      vector can be at most 256 million instructions, which take up 1Gb.  This
      seems enough for a single instruction vector, no?
    - How many instructions can we make into 1-word instructions?

- Keep the vm loop small by calling functions for infrequently used
  instructions.  Put those functions near the vm loop in the source file, so
  they end up near the vm loop in the binary (better cache coherence).
- BUT avoid nested function calls here.  The vm loop should call a function when
  needed, but ideally that function will not call other functions.  (Try to
  eliminate or inline those other functions.)

- MEASURE the performance difference between:
  (1) always testing s < e to ensure we are not at the end of input
  (2) instead test the current char for NUL, and if NUL, then check s < e
  Option (2) requires a NUL at the end of the input, which (a) might be there
  already, or (b) we can put there if we can write to the input buffer.  In
  approach (b), we can save the last byte in a variable, and write a NUL there.
  In some cases, perhaps for short enough strings, we could allocate a longer
  buffer and copy the input there.  In that case, we can get (i) guaranteed NUL
  at the end, and (ii) a buffer size that is a multiple of 16 or 32 to
  facilitate using vector operations in the vm.
  - E.g. memcpy 16Kb with aligned operands takes < 3,000 cycles.
  - E.g. strlen 128 bytes takes < 80 cycles.
  - Both of the above are faster with Fog's Asmlib.

- Look for "dependency chains" in loops, where the result of an operation is
  needed to do the next operation.  This inhibits the benefits of out-of-order
  execution.  Separate the calculations when possible.

- Table lookups are really fast when the table is on the stack (not declared
  'static').

- CONSIDER whether implementing tail call optimization in the PEXL compiler would
  be helpful.  (So that call chains are reduced to a single call/return with
  jumps from pattern to pattern.  Clearly this can only work when the
  intermediate patterns are not called in any other sequence.)  But inlining is
  better, because it avoids the jumps as well, so do that first.

- Would RECURSIVE patterns of fixed size benefit from tail call elimination?  We
  can maintain a counter of the number of matches instead of that many frames on
  the backtrack stack.

- CONSIDER a version of the vm loop that is optimized for input lengths up to
  64Kb (so that positions can be stored in 2 bytes instead of the 8-byte
  pointers we use today).  That reduces the size of the capture stack (CS) frame
  and the backtrack stack (BT) frame by a considerable amount:

  - A capture today is a pointer and an int32 (total 12 bytes, but will be
    aligned to 16).  Keeping a 16-bit index instead of the pointer and another
    16-bit data value will reduce storage by a factor of 4.  Need 2 bits min for
    capkind, leaving 14 bits for capidx (string table index).  Can convert
    capidx into an index into a table holding the actual offset in the string
    table block to make this work.  But would only 16,384 strings max be ok? and
    that would be 4096 if we used 4 bits for capkind.  YES, because this is on a
    per-package basis.
    ** OR: 32-bit position index and 32-bit data, to reduce storage by factor of 2.

  - A backtrack frame today is a pointer (to either a saved position or saved
    instruction vector), another pointer (to next instruction), and an int
    (total of 20 bytes).  First pointer could be reduced to a 16-bit index
    (into either the input position or package table).  Second pointer must
    encode next instruction (offset), so must be 32 bits.  Can the int
    (caplevel) be reduced to 16 bits?  Limiting capture depth to 65,535 seems
    reasonable, BUT caplevel is not that!  It is an index into the capture
    stack, which may have a long sequence of captures on it.  24 bits should be
    plenty, allowing the cap stack to have over 16M (16,777,216) entries.
    That gives a 10 byte total (with 4 byte alignment), reducing data used by
    factor of 2, but total storage by 20/12 = 1.67.

    It would be nice to have an 8-byte backtrack frame, to save both data size
    and storage size (assuming 4-byte alignment).  But in typical usage, the
    backtrack stack has at most dozens of frames, while the capture stack can
    have 1000+.
    Achieving an 8-byte backtrack frame would put an artificially low limit on
    the number of captures (65536/2 specifically, since a capture needs and
    open/close pair). 
    ******* TODO: Also, what do we do when we reach the limit??? *******
    
    A 32-bit caplevel allows 2^32 - 1 captures max (if we change the current
    code that uses -1 as a null value to use 0 instead).  At 12 bytes each (10
    bytes used, 4 byte alignment), that's 49.2Mb of raw capture data.
    
    ** OR: 32-bit position index, 32-bit instruction reference, 32 bits for
    caplevel, giving 12 bytes and a storage reduction factor of 1.67. 

  - Taking the 32-bit position approach, we get a storage reduction by half.
    And a vm loop that is optimized for up to 2^32 byte inputs, or 4Gb.

    Along with this, we could either:
    - Have a "full size" copy of the vm loop that uses pointers and can handle
      up to 2^64 data.
    - OR, use the 32-bit loop but push "new chunk" indicators onto CS stack
      whenever we cross a 4Gb input chunk boundary.  
      What to do about the BT stack, though?  Currently we simply move a pointer
      into that stack to pop entries, so how will we know to back up to an
      earlier chunk unless we examine all the BT stack frames before popping?

    ** PROPOSAL:
       - 16-bit and 64-bit versions of the vm loop, capture, and backtrack.
       - 16-bit version:
       	 - Capture is 4 bytes:
	   2 bytes input position
       	   2 bytes capture data (4 bits capkind, 12 bits revised capidx)
       	 - BT stack frame is 12 bytes (4 byte alignment, so 4 bytes unused):
       	   2 bytes input position (union with package table index)
	   4 bytes instruction offset
	   4 bytes capture level
       - The 64-bit version will lift 2 key limitations:
         (1) Input size can be up to size_t (64 bits)
	 (2) The number of capture names in a package can exceed 4096 (12 bits)
       - 64-bit version:
       	 - Capture is 12 bytes (8 byte alignment, so 4 bytes unused):
	   8 bytes input POINTER
       	   4 bytes capture data (3 bits capkind, 29 bits revised capidx)
       	 - BT stack frame is 16 bytes:
       	   8 bytes input POINTER (union with package table POINTER)
	   4 bytes instruction offset
	   4 bytes capture level

****
    ** REVISION: 16-bit (64KB) and 48-bit (256TB) versions would do.  TODO:
       Update the text and calculations below for this change.
****

    A question:  Would a 64-bit version be practical?  And would it ever be
    needed?  If our use cases included parsing programs (source code), then
    sure, parsing more than 64Kb at once might be a requirement.  But surely
    parsing 4Gb at once would be sufficient, in which case we'd implement a
    32-bit solution.

    What is the upper limit for a single match to a pattern?  If it's 64Kb, then
    our 16-bit variant is sized just right, and we can optimize the hell out of
    it. 

    The other use case for large inputs is streaming.  Streaming input would be
    possible if we dropped the outer capture and the pattern had the form '(foo
    / .)*'.  Then we could discard input that did not match 'foo' and every time
    'foo' matched, we could emit a capture (to disk) AND discard the input.

    Streaming would change how we record and report matches, because there is no
    static input string relative to which our match positions would be
    meaningful.  We could solve this by recording the actual substring of the
    input stream that is the entire match, along with the same match data
    (position-based) that we output now.

    How would this work?
    1 - Read a chunk of input < 64Kb into buf.  Let i = 0.
    2 - Search starting at pos i for a possible start to 'foo'.
    3 - If not found, flush buffer and start again (goto 1 above).
    4 - If we start matching 'foo' at pos i, then either:
      - We finish matching at j without reading more input.
      	- Write buf[i:j] and the match of 'foo'.
	- Start matching again at pos i = j+1 (goto 2 above).
      - We reach the end and are still matching.
        5 - Read another chunk to try to fill buf; if full, start filling buf2.
	6 - Continue matching (at i in buf, else at i2 = 0 in buf2).
	7 - If we have examined more than 64Kb in total, *BAIL*.
	8 - Else read another chunk (goto 5).

    By cycling between the two 64Kb buffers, we use 128Kb throughout the
    streaming process.

    But what about *BAIL*?  What is the right action to take at that point?

    For non-recursive patterns, we can guarantee a match in 64Kb by limiting
    repetitions.  For recursive patterns, could we implement the "Atharva
    feature" (named after my student who proposed a way of limiting recursive
    depth)?  Suppose we could do that efficiently, which seems reasonable even
    though it is a run-time (dynamic) calculation.

    If a single 'recursive_foo' matched 2 chars, we could limit the recursive
    depth to 32,768.  But what if 'recursive_foo' could match anywhere from 2 to
    32,768 chars?  We would have to limit the recursive depth to 2, and miss out
    on matching a 6 char string like '((()))'.

    The same effect happens with repeated sequences.  Must we limit the number
    of matches to 'foo{1,2}' if 'foo' can match from 2 to 32,768 chars?  (Here,
    assume 'foo' is inside a larger pattern, else we would just "stream match
    'foo'".) 
    
    The user might simply want to say, "match any 'bar' that will fit into
    64Kb", knowing that 'bar' has either 'foo' or 'recursive_foo' as a
    sub-pattern, and that due to those, it's possible the input contains a match
    to 'bar' that won't fit in 64Kb.

    WHAT TO DO IN THIS CASE?

    Could we keep matching 'bar' but discard data (and input)?  That might be
    necessary to "reset" the matching process relative to the input stream.  But
    suppose in the end, 'bar' failed to match?  Then we discarded input that we
    now need to back up and process.

    Could we instead allocate additional 64Kb buffers and try to match 'bar
    fully?  Not with 16-bit positions -- we would need more space to keep track
    (in a capture frame) of which buffer the position referred to.  (This
    defeats the 16-bit position size.)  Also, we must have some limit, e.g. 2^16
    buffers gives a 4Gb limit, and what if we are still not done matching 'bar'?

    It seems that the only reasonable approach is to set some limit, whether it
    is 64Kb or 4Gb or something else.  Then the problem of "stream matching"
    becomes "How will *BAIL* reset the matching process to avoid false matches
    that could occur if matching starts WITHIN an occurrence of 'bar'?"

    For recursively structured matches, we might match an inner set of
    'recursive_foo' instances, while missing an outer one(s).

    Should the user provide a 'reset' pattern to help avoid this?  E.g. for
    JSON, 'reset' might be to discard closing braces and brackets...  But this
    won't do what the user wants, because they really need to match complete
    'recursive_foo' instances and discard only extraneous closing braces and
    brackets.

    Better to emit a ERR_SIZE message into the output (match) stream when *BAIL*
    occurs.  When interpreting the output, we have two cases:
    (1) If 'bar' is well-conditioned, then it starts with a prefix that will not
    be matched by any sub-pattern of 'bar'.  The matching is self-resetting.
    (2) Otherwise, as in the JSON example, some number of matches after ERR_SIZE
    may be incomplete, in the sense that they match inner parts of a larger
    structure.

    Could we have a mechanism for recognizing the closing fragments of those
    outer structures that we were not able to match?  That would enable either
    skipping the inner matches or writing them to the output stream followed by
    some indicators of missing context (i.e. the closing fragments).

    ????




    Maybe, in time, we need both a 64-bit version for large data AND a streaming
    version for infinite data?  The 16-bit version would likely remain as a
    very common use case for which we want the fastest possible operation.
    Furthermore, if we knew that 'foo' would consume < 64Kb, then the 16-bit
    version would be used while streaming for 'foo'.


    Note on code vector size:
    Limiting the PC to 24 bits (indexing into an array of 4-byte words) gives a
    limit of 16,777,216 instructions, taking up 64Mb.  Since this is on a
    per-package basis, this seems more than enough.
  
    Note on caplevel size:
    Limiting caplevel to 24 bits (indexing into an array of 4-byte words in the
    16-bit proposal above) gives a limit of 16,777,216 capture stack entries
    (corresponding to 8,388,608 captures), taking up 64Mb.  This seems more than
    enough for a 64Kb input size.

    Reducing the PC and caplevel to 3 bytes each for the 16-bit proposal would
    shrink the BT stack entry size to 8 bytes (2 + 3 + 3) at the cost of storing
    the 3-byte fields into a 4-byte and a 2-byte.  Since the BT stack does not
    grow much, shrinking the BT frame this way does not appear useful.


** When an expression cannot capture, do not allocate the capture stack.
** When an expression cannot backtrack, do not allocate the backtrack stack.
** Re-use these stacks to avoid re-allocating them each time the vm starts.
   Provide a "free_buffers" API that frees them, in case the user wants to do
   this, e.g. because they have grown large.  Maybe later add a heuristic for
   when to free these stacks, such as when they have grown large but the last k
   invocations of the vm have used only a fraction (half?) of the space.

- In the "byte-coded capture" encoding, we take apart each 32-bit integer and
  encode each byte separately.  If we choose little-endian encoding, we should
  be able to do this faster on little-endian machines.

- During capture processing, the generation of JSON output would benefit from a
  faster integer to string (itoa) conversion.  Same for any output for humans,
  even if not JSON.
  https://github.com/appnexus/acf/blob/master/src/an_itoa.c
  Apache 2.0 license (any use, requires inclusion of license itself and
  indication in any modified file that we modified it)

- CONSIDER using the C stack instead of our custom stack for backtracking.  The
  top of the stack is in a local (stack allocated) var.  Do we need to examine
  previous entries, or just pop them?


- During CODE GENERATION, we call exp_patlen() and maybe other similar
  functions.  Look at codeseq1() for an example.  If we have a sequence of n
  items, we will end up calling exp_patlen() n^2 times, because we call it at
  the head of the sequence, and again when generating code for the 2nd item in
  the sequence, etc.
  DYNAMIC PROGRAMMING should fix this inefficiency.

- Optimizing lookbehind (if that proves necessary): When SEARCHing for a
  pattern, we currently have pattern min/max and the UNBOUNDED flag to limit the
  set of starting positions from which to try a match.  But if the pattern is a
  choice between two fixed-length sub-patterns, it would be more efficient to
  store those two lengths and try them.  We could extend this idea to
  accommodate a variety of strategies that mix matching attempts at discrete
  starting positions with match attempts at each position in a range.  E.g. we
  could store 4 (or more) "lengths" and flags that indicate which are fixed
  positions to try and which represent a range.

- Store strings (that are longer than a threshold) in the block storage area,
  and have an instruction that will match them from there.  This will save
  enormously on instruction vector space when looking for large/many strings.

- Consider avoiding branches by "predicating", i.e. using a flag that
  effectively guards an operation without branching.  E.g.
    push(item, stack, guardflag) could contain: stack.top += guardflag
  In this way, we avoid a branch and possibly gain a performance increase as a
  result.  Unless the situation is one in which branch prediction will win the
  day, such as when the branch usually resolves one way (taken, or not).


  
