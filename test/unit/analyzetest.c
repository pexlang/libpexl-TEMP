/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  analyzetest.c  TESTING analyze.c                                         */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>		/* exit() */
#include "print.h"
#include "analyze.h"

#define YES 1
#define NO 0

#define failif(test, msg)						\
  do { if (test) { fprintf(stderr, "Failed %s:%d: %s\n", __FILE__,	\
			   __LINE__, msg);				\
      fflush(stderr);							\
      exit(-1);								\
    }									\
  } while (0)

int main(void) {

  int nullable;
  Context *C = NULL;
  Index id;

  int stat;
  Charset cs;
  Node *node;
  unsigned char c;
  uint32_t min, max;

/*   Value val; */
  Expression *tstr, *calltree;
  Expression *consttree; 
  Expression *t1, *t2, *t3, *t4; 
  Expression *A, *S;

  PackageTable *m; 
  Package *pkg;
  Env *toplevel, *env, *g;
  SymbolTable *st;
  Ref ref, refA, refS;

/*   size_t bufsize = 16;	/\* 15 chars and a NULL byte *\/ */
/*   char *buf = (char *)alloca(bufsize); */


  printf("$$$ analyzetest\n");
  printf("analyzetest: Computing expression properties \n\n");

  /* Error checking */

  stat = exp_hascaptures(NULL);
  failif(stat != EXP_ERR_NULL, "should catch null arg error");

  stat = exp_itself_hascaptures(NULL);
  failif(stat != EXP_ERR_NULL, "should catch null arg error");
  
  stat = exp_patlen(NULL, &min, &max);
  failif(stat != EXP_ERR_NULL, "should catch null arg error");

  stat = exp_nofail(NULL);
  failif(stat != EXP_ERR_NULL, "should catch null arg error");

  stat = exp_nullable(NULL);
  failif(stat != EXP_ERR_NULL, "should catch null arg error");

  stat = exp_headfail(NULL);
  failif(stat != EXP_ERR_NULL, "should catch null arg error");

  stat = exp_needfollow(NULL);
  failif(stat != EXP_ERR_NULL, "should catch null arg error");

  stat = exp_getfirst(NULL, NULL, NULL);
  failif(stat != EXP_ERR_NULL, "should catch null arg error");
  stat = exp_getfirst(NULL, NULL, &cs);
  failif(stat != EXP_ERR_NULL, "should catch null arg error");
  stat = exp_getfirst(NULL, fullset, NULL);
  failif(stat != EXP_ERR_NULL, "should catch null arg error");
  stat = exp_getfirst(NULL, fullset, &cs);
  failif(stat != EXP_ERR_NULL, "should catch null arg error");

  tstr = pexle_from_string(C, "hello");
  failif(tstr->len != 9, "should produce 9-node tree");
  node = tstr->node;
  failif(!node, "precondition for next tests");

  stat = exp_getfirst(node, NULL, NULL);
  failif(stat != EXP_ERR_NULL, "should catch null arg error");
  stat = exp_getfirst(node, NULL, &cs);
  failif(stat != EXP_ERR_NULL, "should catch null arg error");
  stat = exp_getfirst(node, fullset, NULL);
  failif(stat != EXP_ERR_NULL, "should catch null arg error");


  /* ----------------------------------------------------------------------------- */
  printf("********************** Analyzing primitive patterns \n");

  printf("Checking literal string 'hello'\n");
  failif(exp_hascaptures(node), "this exp does not capture");
  failif(exp_itself_hascaptures(node), "this exp does not capture");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_BOUNDED, "expected exp_patlen to return bounded");
  failif((min != 5) || (max != 5), "wrong min/max");
  nullable = exp_nullable(node) || (node->tag == TFalse);
  failif(nullable, "literal non-empty string not nullable");
  failif((nullable && (min != 0)) || (!nullable && (min == 0)), "nullable disagrees with patlen");
  failif(exp_nofail(node), "literal non-empty string can fail");
  failif(exp_headfail(node) != 0, "literal multi-char string is not headfail");
  failif(exp_needfollow(node), "literal non-empty string does not need follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 0, "firstset can be used as test, does not accept epsilon");
  /* Now check the contents of cs for 'h' */
  for (c = 0; c < 255; c++)
    if (c == 'h')
      failif(!testchar(cs.cs, c), "cs should contain only 'h'");
    else
      failif(testchar(cs.cs, c), "cs should contain only 'h'");

  printf("Checking TTrue created via empty literal string\n");
  pexle_free(tstr);
  tstr = pexle_from_string(C, "");
  failif(tstr->len != 1, "should produce 1-node tree");
  failif(tstr->node->tag != TTrue, "the 1 node is TTrue, which matches epsilon");
  node = tstr->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp does not capture");
  failif(exp_itself_hascaptures(node), "this exp does not capture");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_BOUNDED, "expected exp_patlen to return bounded");
  failif((min != 0) || (max != 0), "wrong min/max");
  nullable = exp_nullable(node) || (node->tag == TFalse);
  failif(!nullable, "TTrue is nullable");
  failif((nullable && (min != 0)) || (!nullable && (min == 0)), "nullable disagrees with patlen");
  failif(!exp_nofail(node), "TTrue is nofail");
  failif(exp_headfail(node) != 0, "TTrue is not headfail");
  failif(exp_needfollow(node), "TTrue does not need follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 1, "firstset cannot be used as test, exp accepts epsilon");
  for (c = 0; c < 255; c++)
    failif(!testchar(cs.cs, c), "cs should contain every char");

  printf("Checking TAny\n");
  pexle_free(tstr);
  tstr = pexle_from_number(C, 1);
  failif(tstr->len != 1, "should produce 1-node tree");
  failif(tstr->node->tag != TAny, "the node is TAny, which matches 1 byte");
  node = tstr->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp does not capture");
  failif(exp_itself_hascaptures(node), "this exp does not capture");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_BOUNDED, "expected exp_patlen to return bounded");
  failif((min != 1) || (max != 1), "wrong min/max");
  nullable = exp_nullable(node) || (node->tag == TFalse);
  failif(nullable, "TAny is not nullable");
  failif((nullable && (min != 0)) || (!nullable && (min == 0)), "nullable disagrees with patlen");
  failif(exp_nofail(node), "TAny is not nofail");
  failif(exp_headfail(node) != 1, "TAny is headfail");
  failif(exp_needfollow(node), "TAny does not need follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 0, "firstset can be used as test, does not accept epsilon");
  for (c = 0; c < 255; c++)
    failif(!testchar(cs.cs, c), "cs should contain every char");

  printf("Checking TSeq of TAny\n");
  pexle_free(tstr);
  tstr = pexle_from_number(C, 10000);
  failif(tstr->len != 19999, "should produce this size tree");
  failif(tstr->node->tag != TSeq, "the root node is Tseq");
  node = tstr->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp does not capture");
  failif(exp_itself_hascaptures(node), "this exp does not capture");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_BOUNDED, "expected exp_patlen to return bounded");
  failif((min != 10000) || (max != 10000), "wrong min/max");
  nullable = exp_nullable(node) || (node->tag == TFalse);
  failif(nullable, "exp is not nullable");
  failif((nullable && (min != 0)) || (!nullable && (min == 0)), "nullable disagrees with patlen");
  failif(exp_nofail(node), "exp is not nofail");
  failif(exp_headfail(node) != 0, "exp is not headfail");
  failif(exp_needfollow(node), "exp does not need follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 0, "firstset can be used as test, does not accept epsilon");
  for (c = 0; c < 255; c++)
    failif(!testchar(cs.cs, c), "cs should contain every char");

  printf("Checking TNot of TAny\n");
  pexle_free(tstr);
  tstr = pexle_from_number(C, -1);
  failif(tstr->len != 2, "should produce this size tree");
  failif(tstr->node->tag != TNot, "the root node is TNot");
  node = tstr->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp does not capture");
  failif(exp_itself_hascaptures(node), "this exp does not capture");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_BOUNDED, "expected exp_patlen to return bounded");
  failif((min != 0) || (max != 0), "wrong min/max");
  nullable = exp_nullable(node) || (node->tag == TFalse);
  failif(nullable != 1, "exp is nullable");
  failif((nullable && (min != 0)) || (!nullable && (min == 0)), "nullable disagrees with patlen");
  failif(exp_nofail(node) != 0, "exp is not nofail");
  failif(exp_headfail(node) != 0, "exp is not headfail");
  failif(exp_needfollow(node), "exp does not need follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 1, "firstset cannot be used as test, exp accepts epsilon");
  for (c = 0; c < 255; c++)
    failif(testchar(cs.cs, c), "cs should contain no chars");

  printf("Checking TFalse\n");
  pexle_free(tstr);
  tstr = pexle_from_boolean(C, 0);
  failif(tstr->len != 1, "should produce this size tree");
  failif(tstr->node->tag != TFalse, "the root node is TFalse");
  node = tstr->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp does not capture");
  failif(exp_itself_hascaptures(node), "this exp does not capture");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_BOUNDED, "expected exp_patlen to return bounded");
  failif((min != 0) || (max != 0), "wrong min/max");
  failif(exp_nullable(node), "exp is not nullable"); 
  nullable = exp_nullable(node) || (node->tag == TFalse);
  failif((nullable && (min != 0)) || (!nullable && (min == 0)), "nullable disagrees with patlen"); 
  failif(exp_nofail(node) != 0, "exp is not nofail");
  failif(exp_headfail(node) != 1, "exp is headfail");
  failif(exp_needfollow(node), "exp does not need follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 0, "firstset can be used as test, does not accept epsilon");
  for (c = 0; c < 255; c++)
    failif(testchar(cs.cs, c), "cs should contain no chars");

  printf("Checking TSet\n");
  t3 = pexle_from_set(C, "abcdef", 6);
  failif(t3->len != 3, "a single charset occupies size of 3 nodes");
  failif(t3->node->tag != TSet, "tag should be TSet");
  node = t3->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp does not capture");
  failif(exp_itself_hascaptures(node), "this exp does not capture");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_BOUNDED, "expected exp_patlen to return bounded");
  failif((min != 1) || (max != 1), "wrong min/max");
  nullable = exp_nullable(node) || (node->tag == TFalse);
  failif(nullable != 0, "exp is not nullable");
  failif((nullable && (min != 0)) || (!nullable && (min == 0)), "nullable disagrees with patlen");
  failif(exp_nofail(node) != 0, "exp is not nofail");
  failif(exp_headfail(node) != 1, "exp is headfail");
  failif(exp_needfollow(node), "exp does not need follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 0, "firstset can be used as test, does not accept epsilon");
  for (c = 0; c < 255; c++)
    if (( c >= 'a') && (c <= 'f'))
      failif(!testchar(cs.cs, c), "cs should contain only a-f");
    else
      failif(testchar(cs.cs, c), "cs should contain only a-f");
	
  t4 = pexle_from_set(C, "", 0);
  failif(t4->len != 3, "a single charset occupies size of 3 nodes");
  failif(t4->node->tag != TSet, "tag should be TSet");
  node = t4->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp does not capture");
  failif(exp_itself_hascaptures(node), "this exp does not capture");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_BOUNDED, "expected exp_patlen to return bounded");
  failif((min != 1) || (max != 1), "wrong min/max");
  nullable = exp_nullable(node) || (node->tag == TFalse);
  failif(nullable != 0, "exp is not nullable");
  failif((nullable && (min != 0)) || (!nullable && (min == 0)), "nullable disagrees with patlen");
  failif(exp_nofail(node) != 0, "exp is not nofail");
  failif(exp_headfail(node) != 1, "exp is headfail");
  failif(exp_needfollow(node), "exp does not need follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 0, "firstset can be used as test, does not accept epsilon");
  for (c = 0; c < 255; c++)
    failif(testchar(cs.cs, c), "cs should contain no chars");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Using primitive 'halt' \n");

  t1 = pexle_halt(C);
  failif(t1->len != 1, "wrong size tree");
  failif(t1->node->tag != THalt, "THalt");
  node = t1->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp does not capture");
  failif(exp_itself_hascaptures(node), "this exp does not capture");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_BOUNDED, "expected exp_patlen to return bounded");
  failif((min != 0) || (max != 0), "wrong min/max");
  nullable = exp_nullable(node) || (node->tag == TFalse);
  failif(nullable != 1, "exp is nullable");
  failif((nullable && (min != 0)) || (!nullable && (min == 0)), "nullable disagrees with patlen");
  failif(exp_nofail(node) != 1, "exp is nofail");
  failif(exp_headfail(node) != 0, "exp is not headfail");
  failif(exp_needfollow(node), "exp does not need follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 0, "firstset can be used as test, does not accept epsilon");
  for (c = 0; c < 255; c++)
    failif(testchar(cs.cs, c), "cs should contain no chars");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Using combinator 'seq' \n");

  pexle_free(t1); 
  t1 = pexle_from_string(C, "a");		  /* headfail */
  t2 = pexle_repeat_f(C, pexle_from_number(C, 1), 0); /* .* is nofail */
  pexle_free(tstr);
  tstr = pexle_seq(C, t1, t2);		  /* "a" .* */
  failif(tstr->len != 4, "should succeed, and tree should have this many nodes");
  node = tstr->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp does not capture");
  failif(exp_itself_hascaptures(node), "this exp does not capture");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_UNBOUNDED, "expected exp_patlen to return UNBOUNDED");
  failif((min != 1), "wrong min");
  nullable = exp_nullable(node) || (node->tag == TFalse);
  failif(nullable != 0, "exp is NOT nullable");
  failif((nullable && (min != 0)) || (!nullable && (min == 0)), "nullable disagrees with patlen");
  failif(exp_nofail(node) != 0, "exp is NOT nofail");
  failif(exp_headfail(node) != 1, "exp is headfail");
  failif(!exp_needfollow(node), "exp would benefit from follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 0, "firstset can be used as test, does not accept epsilon");
  for (c = 0; c < 255; c++)
    if (c == 'a')
      failif(!testchar(cs.cs, c), "cs should contain only a");
    else
      failif(testchar(cs.cs, c), "cs should contain only a");

  pexle_free(tstr);

  /* ----------------------------------------------------------------------------- */
  printf("********************** Using combinator 'choice' \n");

  pexle_free(t1); pexle_free(t2); pexle_free(t3); pexle_free(t4);

  t1 = pexle_from_string(C, "ab");
  t2 = pexle_from_string(C, "c");
  t3 = pexle_choice(C, t1, t2);
  failif(t3->len != 5, "wrong size tree");
  failif(t3->node->tag != TChoice, "should get TChoice");
  node = t3->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp does not capture");
  failif(exp_itself_hascaptures(node), "this exp does not capture");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_BOUNDED, "expected exp_patlen to return bounded");
  failif((min != 1) || (max != 2), "wrong min/max");
  nullable = exp_nullable(node) || (node->tag == TFalse);
  failif(nullable != 0, "exp is NOT nullable");
  failif((nullable && (min != 0)) || (!nullable && (min == 0)), "nullable disagrees with patlen");
  failif(exp_nofail(node) != 0, "exp is NOT nofail");
  failif(exp_headfail(node) != 0, "exp is NOT headfail");
  failif(!exp_needfollow(node), "exp would benefit from follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 0, "firstset can be used as test, does not accept epsilon");
  for (c = 0; c < 255; c++)
    if (c == 'a' || c == 'c')
      failif(!testchar(cs.cs, c), "cs should contain only a, c");
    else
      failif(testchar(cs.cs, c), "cs should contain only a, c");


  /* ----------------------------------------------------------------------------- */
  printf("********************** Using combinator 'not' \n");

  pexle_free(t1); pexle_free(t2);
  t1 = pexle_from_string(C, "A");
  t2 = pexle_not(C, t1);
  failif(t2->len != 1 + t1->len, "wrong size tree");
  failif(t2->node->tag != TNot, "Tnot");
  node = t2->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp does not capture");
  failif(exp_itself_hascaptures(node), "this exp does not capture");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_BOUNDED, "expected exp_patlen to return bounded");
  failif((min != 0) || (max != 0), "wrong min/max");
  nullable = exp_nullable(node) || (node->tag == TFalse);
  failif(nullable != 1, "exp is nullable");
  failif((nullable && (min != 0)) || (!nullable && (min == 0)), "nullable disagrees with patlen");
  failif(exp_nofail(node) != 0, "exp is NOT nofail");
  failif(exp_headfail(node) != 0, "exp is NOT headfail");
  failif(exp_needfollow(node), "exp would NOT benefit from follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 1, "exp accepts epsilon");
  for (c = 0; c < 255; c++)
    if (c == 'A')
      failif(testchar(cs.cs, c), "cs should contain all but A");
    else
      failif(!testchar(cs.cs, c), "cs should contain all but A");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Using combinator 'lookahead' \n");

  pexle_free(t1); pexle_free(t2);
  t1 = pexle_from_string(C, "A");
  t2 = pexle_lookahead(C, t1);
  failif(t2->len != 1 + t1->len, "wrong size tree");
  failif(t2->node->tag != TAhead, "TAhead");
  node = t2->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp does not capture");
  failif(exp_itself_hascaptures(node), "this exp does not capture");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_BOUNDED, "expected exp_patlen to return bounded");
  failif((min != 0) || (max != 0), "wrong min/max");
  nullable = exp_nullable(node) || (node->tag == TFalse);
  failif(nullable != 1, "exp is nullable");
  failif((nullable && (min != 0)) || (!nullable && (min == 0)), "nullable disagrees with patlen");
  failif(exp_nofail(node) != 0, "exp is NOT nofail");
  failif(exp_headfail(node) != 1, "exp is headfail");
  failif(exp_needfollow(node), "exp would NOT benefit from follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 0, "firstset can be used as test, does not accept epsilon");
  for (c = 0; c < 255; c++)
    if (c == 'A')
      failif(!testchar(cs.cs, c), "cs should contain only A");
    else
      failif(testchar(cs.cs, c), "cs should contain only A");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Using combinator 'rep' \n");

  pexle_free(t1); pexle_free(t2);
  t1 = pexle_from_string(C, "A");
  t2 = pexle_repeat(C, t1, 0);
  failif(t2->len != 1 + t1->len, "wrong size tree");
  failif(t2->node->tag != TRep, "TRep");
  node = t2->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp does not capture");
  failif(exp_itself_hascaptures(node), "this exp does not capture");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_UNBOUNDED, "expected exp_patlen to return UNBOUNDED");
  failif((min != 0), "wrong min");
  nullable = exp_nullable(node) || (node->tag == TFalse);
  failif(nullable != 1, "exp is nullable");
  failif((nullable && (min != 0)) || (!nullable && (min == 0)), "nullable disagrees with patlen");
  failif(exp_nofail(node) != 1, "exp is nofail");
  failif(exp_headfail(node) != 0, "exp is NOT headfail");
  failif(!exp_needfollow(node), "exp would benefit from follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 1, "firstset cannot be used as test, exp accepts epsilon");
  /* Now check the contents of cs, which should be a full set */
  for (c = 0; c < 255; c++)
    failif(!testchar(cs.cs, c), "cs should contain all chars");

  pexle_free(t3);
  t3 = pexle_repeat(C, t1, 3);	/* "A"{3,} */
  failif(t3->len != 7 + t1->len, "wrong size tree");
  failif(t3->node->tag != TSeq, "TSeq");
  node = t3->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp does not capture");
  failif(exp_itself_hascaptures(node), "this exp does not capture");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_UNBOUNDED, "expected exp_patlen to return UNBOUNDED");
  failif((min != 3), "wrong min");
  nullable = exp_nullable(node) || (node->tag == TFalse);
  failif(nullable != 0, "exp is NOT nullable");
  failif((nullable && (min != 0)) || (!nullable && (min == 0)), "nullable disagrees with patlen");
  failif(exp_nofail(node) != 0, "exp is NOT nofail");
  failif(exp_headfail(node) != 0, "exp is NOT headfail");
  failif(!exp_needfollow(node), "exp would benefit from follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 0, "firstset can be used as test, does not accept epsilon");
  /* Now check the contents of cs */
  for (c = 0; c < 255; c++)
    if (c == 'A')
      failif(!testchar(cs.cs, c), "cs should contain only A");
    else
      failif(testchar(cs.cs, c), "cs should contain only A");

  pexle_free(t1); pexle_free(t2);
  t1 = pexle_from_string(C, "ABC");
  t2 = pexle_repeat(C, t1, -1);	/* "ABC"? ==> "ABC" / TTrue */
  failif(t2->len != 2 + t1->len, "wrong size tree");
  failif(t2->node->tag != TChoice, "TChoice");
  node = t2->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp does not capture");
  failif(exp_itself_hascaptures(node), "this exp does not capture");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_BOUNDED, "expected exp_patlen to return bounded");
  failif((min != 0) || (max != 3), "wrong min/max");
  nullable = exp_nullable(node) || (node->tag == TFalse);
  failif(nullable != 1, "exp is nullable");
  failif((nullable && (min != 0)) || (!nullable && (min == 0)), "nullable disagrees with patlen");
  failif(exp_nofail(node) != 1, "exp is nofail");
  failif(exp_headfail(node) != 0, "exp is NOT headfail");
  failif(!exp_needfollow(node), "exp would benefit from follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 1, "firstset cannot be used as test, exp accepts epsilon");
  /* Now check the contents of cs */
  for (c = 0; c < 255; c++)
    failif(!testchar(cs.cs, c), "cs should contain all chars");

  pexle_free(t3);
  t3 = pexle_repeat(C, t1, -3);	/* "ABC"{0,3} */
  failif(t3->len != 18 + t1->len, "wrong size tree");
  failif(t3->node->tag != TChoice, "TChoice");
  node = t3->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp does not capture");
  failif(exp_itself_hascaptures(node), "this exp does not capture");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_BOUNDED, "expected exp_patlen to return bounded");
  failif((min != 0) || (max != 9), "wrong min/max");
  nullable = exp_nullable(node) || (node->tag == TFalse);
  failif(nullable != 1, "exp is nullable");
  failif((nullable && (min != 0)) || (!nullable && (min == 0)), "nullable disagrees with patlen");
  failif(exp_nofail(node) != 1, "exp is nofail");
  failif(exp_headfail(node) != 0, "exp is NOT headfail");
  failif(!exp_needfollow(node), "exp would benefit from follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 1, "firstset cannot be used as test, exp accepts epsilon");
  /* Now check the contents of cs */
  for (c = 0; c < 255; c++)
    failif(!testchar(cs.cs, c), "cs should contain all chars");


  /* ----------------------------------------------------------------------------- */
  printf("********************** Using combinator 'subtract' \n");

  /* "A"-"XYZ" ==> (seq (not "XYZ") "A") */
  pexle_free(t1); pexle_free(t2); pexle_free(t3);
  t1 = pexle_from_string(C, "A");
  t2 = pexle_from_string(C, "XYZ");
  t3 = pexle_subtract(C, t1, t2);
  failif(t3->len != t1->len + t2->len + 2, "wrong size tree");
  failif(t3->node->tag != TSeq, "TSeq");
  node = t3->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp does not capture");
  failif(exp_itself_hascaptures(node), "this exp does not capture");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_BOUNDED, "expected exp_patlen to return bounded");
  failif((min != 1) || (max != 1), "wrong min/max");
  nullable = exp_nullable(node) || (node->tag == TFalse);
  failif(nullable != 0, "exp is NOT nullable");
  failif((nullable && (min != 0)) || (!nullable && (min == 0)), "nullable disagrees with patlen");
  failif(exp_nofail(node) != 0, "exp is NOT nofail");
  failif(exp_headfail(node) != 0, "exp is NOT headfail");
  failif(exp_needfollow(node), "exp would NOT benefit from follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 0, "firstset can be used as test, does not accept epsilon");
  /* Now check the contents of cs */
  for (c = 0; c < 255; c++)
    if (c == 'A')
      failif(!testchar(cs.cs, c), "cs should contain A");
    else
      failif(testchar(cs.cs, c), "cs should contain only A");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Using combinator 'lookbehind' \n");

  printf("Look back at string literal\n");
  pexle_free(t1); pexle_free(t2);
  t1 = pexle_from_string(C, "A");
  t2 = pexle_lookbehind(C, t1);
  failif(t2->len != 2, "wrong size tree");
  failif(t2->node->tag != TBehind, "TBehind");
  node = t2->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp does not capture");
  failif(exp_itself_hascaptures(node), "this exp does not capture");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_BOUNDED, "expected exp_patlen to return bounded");
  failif((min != 0) || (max != 0), "wrong min/max");
  nullable = exp_nullable(node) || (node->tag == TFalse);
  failif(nullable != 1, "exp is nullable");
  failif((nullable && (min != 0)) || (!nullable && (min == 0)), "nullable disagrees with patlen");
  failif(exp_nofail(node) != 0, "exp is NOT nofail");
  failif(exp_headfail(node) != 0, "exp is NOT headfail");
  failif(exp_needfollow(node), "exp would NOT benefit from follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 1, "firstset cannot be used as test, exp accepts epsilon");
  /* Now check the contents of cs */
  for (c = 0; c < 255; c++)
    failif(!testchar(cs.cs, c), "cs should contain all chars");

  printf("Look back at \"A\"? which has variable length\n");
  pexle_free(t1); pexle_free(t2); pexle_free(t3);
  t1 = pexle_from_string(C, "A");
  t2 = pexle_repeat(C, t1, -1);
  t3 = pexle_lookbehind(C, t2);
/*   print_exp(t3, 0, NULL); */
  failif(!t3, "the check for fixedlen happens later");
  node = t3->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp does not capture");
  failif(exp_itself_hascaptures(node), "this exp does not capture");
  printf("This exp will fail to compile, because the target of the lookbehind has variable length.\n");

  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_BOUNDED, "expected exp_patlen to return bounded");
  failif((min != 0) || (max != 0), "wrong min/max");

  nullable = exp_nullable(node) || (node->tag == TFalse);
  failif(nullable != 1, "exp is nullable");
  failif((nullable && (min != 0)) || (!nullable && (min == 0)), "nullable disagrees with patlen");
  failif(exp_nofail(node) != 0, "exp is NOT nofail");
  failif(exp_headfail(node) != 0, "exp is NOT headfail");
  failif(exp_needfollow(node), "exp would NOT benefit from follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 1, "firstset cannot be used as test, exp accepts epsilon");
  /* Now check the contents of cs */
  for (c = 0; c < 255; c++)
    failif(!testchar(cs.cs, c), "cs should contain all chars");

  printf("Look back at \"AB\"/\"CD\" which has length 2\n");
  pexle_free(t1); pexle_free(t2); pexle_free(t3);
  t1 = pexle_from_string(C, "AB");
  t2 = pexle_from_string(C, "CD");
  t3 = pexle_choice(C, t1, t2);
  t4 = pexle_lookbehind(C, t3);
/*   print_exp(t4, 0, NULL); */
  failif(!t4, "we do not check (now) for the acceptable lookbehind targets until compilation");
  failif(t4->node->tag != TBehind, "TBehind");
  node = t4->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp does not capture");
  failif(exp_itself_hascaptures(node), "this exp does not capture");

  stat = exp_patlen(child1(node), &min, &max);
  failif(stat != EXP_BOUNDED, "expected exp_patlen to return bounded");
  failif((min != 2) || (max != 2), "wrong min/max");

  printf("This exp will compile, because the target of the lookbehind has fixed length.\n");

  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_BOUNDED, "expected exp_patlen to return bounded");
  failif((min != 0) || (max != 0), "wrong min/max");

  nullable = exp_nullable(node) || (node->tag == TFalse);
  failif(nullable != 1, "exp is nullable");
  failif((nullable && (min != 0)) || (!nullable && (min == 0)), "nullable disagrees with patlen");
  failif(exp_nofail(node) != 0, "exp is NOT nofail");
  failif(exp_headfail(node) != 0, "exp is NOT headfail");
  failif(exp_needfollow(node), "exp would NOT benefit from follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 1, "firstset cannot be used as test, exp accepts epsilon");
  /* Now check the contents of cs */
  for (c = 0; c < 255; c++)
    failif(!testchar(cs.cs, c), "cs should contain all chars");

  pexle_free(t4);
  
  /* ----------------------------------------------------------------------------- */
  printf("********************** Capture \n");

  toplevel = env_new(NULL, NO);
  failif(!toplevel, "new toplevel should succeed");
  C = context_new(toplevel);
  failif(!C, "expected new context");

  pexle_free(t1); pexle_free(t2);
  t1 = pexle_from_string(C, "AB");
  t2 = pexle_capture(C, "hi", t1);
  node = t2->node;
  failif(!node, "precondition for next tests");

  failif(!exp_hascaptures(node), "this exp captures");
  failif(!exp_itself_hascaptures(node), "this exp captures");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_BOUNDED, "expected exp_patlen to return bounded");
  failif((min != 2) || (max != 2), "wrong min/max");
  nullable = exp_nullable(node) || (node->tag == TFalse);
  failif(nullable != 0, "exp is NOT nullable");
  failif((nullable && (min != 0)) || (!nullable && (min == 0)), "nullable disagrees with patlen");
  failif(exp_nofail(node) != 0, "exp is NOT nofail");
  failif(exp_headfail(node) != 0, "exp is NOT headfail");
  failif(exp_needfollow(node), "exp would NOT benefit from follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 0, "firstset can be used as test, does not accept epsilon");
  /* Now check the contents of cs */
  for (c = 0; c < 255; c++)
    if (c == 'A')
      failif(!testchar(cs.cs, c), "cs should contain only A");
    else
      failif(testchar(cs.cs, c), "cs should contain only A");

  pexle_free(t3);
  t3 = pexle_seq(C, t2, t2);
/*   print_exp(t3, 0, NULL); */
  node = t3->node;
  failif(!node, "precondition for next tests");

  failif(!exp_hascaptures(node), "this exp captures");
  failif(!exp_itself_hascaptures(node), "this exp captures");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_BOUNDED, "expected exp_patlen to succeed");
  failif((min != 4) || (max != 4), "wrong min/max");
  nullable = exp_nullable(node) || (node->tag == TFalse);
  failif(nullable != 0, "exp is NOT nullable");
  failif((nullable && (min != 0)) || (!nullable && (min == 0)), "nullable disagrees with patlen");
  failif(exp_nofail(node) != 0, "exp is NOT nofail");
  failif(exp_headfail(node) != 0, "exp is NOT headfail");
  failif(exp_needfollow(node), "exp would NOT benefit from follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 0, "firstset can be used as test, does not accept epsilon");
  /* Now check the contents of cs */
  for (c = 0; c < 255; c++)
    if (c == 'A')
      failif(!testchar(cs.cs, c), "cs should contain only A");
    else
      failif(testchar(cs.cs, c), "cs should contain only A");

  pexle_free(t1);  pexle_free(t2);  pexle_free(t3);

  /* ----------------------------------------------------------------------------- */
  printf("********************** Constant captures \n");

  consttree = pexle_constant(C, "", "");
  failif(consttree, "expected failure because name cannot be empty string");

  consttree = pexle_constant(C, "x", "");
  failif(!consttree, "expected success");
  pexle_free(consttree);

  consttree = pexle_constant(C, "foobar", "");
  failif(!consttree, "expected success");

  printf("Printing with context:\n");
  print_exp(consttree, 0, C);
  printf("Printing without context:\n");
  print_exp(consttree, 0, NULL);
  pexle_free(consttree);

  /* ----------------------------------------------------------------------------- */
  printf("********************** References \n");

  m = packagetable_new(1);
  failif(!m, "should succeed");

  env_free(toplevel);
  toplevel = env_new(NULL, NO);
  failif(!toplevel, "new toplevel should succeed");

  env = env_new(toplevel, NO);
  failif(!env, "should succeed");

  context_free(C);
  C = context_new(env);
  failif(!C, "expected new context");

  st = symboltable_new(1, 1);
  failif(!st, "should succeed");
  stat = symboltable_add(st, NULL, 1, 1, 0, 0, NULL); /* add empty string (here represented by NULL */
  failif(stat != 0, "should succeed, returned index should be 0");
  stat = symboltable_add(st, "foo", 1, 1, 0, 0, NULL);
  failif(stat != 1, "should succeed, returned index should be 1");

  pkg = package_new();
  failif(!pkg, "should succeed, even with importpath, prefix, source as empty string");
  package_free(pkg);

  /* anonymous binding has string ID 0 */
  stat = env_bind(env, 0, env_new_value(Eunspecified_t, 0, NULL), &ref);
  failif(stat != 0, "successful env_bind should return code 0");

  id = context_intern(C, "foobar");
  failif(id < 0, "intern should succeed");
  stat = env_bind(env, id, env_new_value(Eunspecified_t, 0, NULL), &ref);
  failif(stat != 0, "successful env_bind should return code 0");
  failif(ref.env != env, "checking ref contents");
  failif(ref.index != 1, "checking ref contents (second binding is at index 1)");

  printf("ref = (%p, %d)\n", (void *) ref.env, ref.index);
  calltree = pexle_call(C, ref);
  failif(!calltree, "should succeed");
  failif(calltree->node->v.env != ref.env, "tree should contain env from ref");
  failif(calltree->node->u.index != ref.index, "tree should contain index from ref");
  pexle_free(calltree);

  /* ----------------------------------------------------------------------------- */
  printf("********************** Non-recursive block using anonymous exps \n");

  g = env_new(env, NO);
  stat = env_bind(g, 0, env_new_value(Eunspecified_t, 0, NULL), &refA);
  failif(stat < 0, "bind should work");
  stat = env_bind(g, 0, env_new_value(Eunspecified_t, 0, NULL), &refS);
  failif(stat < 0, "bind should work");
/*   print_env_stats(g); */
/*   print_env(g, C); */

  A = pexle_call(C, refS);	/* A -> S */
  S = pexle_from_boolean(C, 1);	/* S -> true */
  node = A->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp NOT captures");
  failif(exp_itself_hascaptures(node), "this exp NOT captures");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_ERR_OPENFAIL, "expected to catch openfail error");
  failif(exp_nullable(node) != EXP_ERR_OPENFAIL, "exp has an OPEN CALL");
  failif(exp_nofail(node) != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 
  failif(exp_headfail(node) != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 
  failif(exp_needfollow(node) != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 
  stat = exp_getfirst(node, fullset, &cs); 
  failif(stat != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 

  pexle_free(A); pexle_free(S);
  env_free(g);
  printf("-----\n");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Recursive block using anonymous exps \n");

  printf("Trying a single rule that is left recursive due to a self loop\n"); 
  g = env_new(env, YES);
  stat = env_bind(g, 0, env_new_value(Eunspecified_t, 0, NULL), &refA);
  failif(stat < 0, "bind to unspecified should work");
  A = pexle_call(C, ref);		/* A -> A */

  print_env(g, C); 
  printf("Expression at refA: ");
  
  pexle_free(A);
  printf("-----\n");
  
  /* ----------------------------------------------------------------------------- */
  printf("********************** Grammar with realistic recursion \n");

  /* New env for grammar */
  env_free(g);
  g = env_new(env, YES);
  failif(!g, "failed to create new env");

  id = context_intern(C, "A");
  failif(id < 0, "symbol add should succeed");
  stat = env_bind(g, id, env_new_value(Eunspecified_t, 0, NULL), &ref);
  failif(ref_not_found(ref), "bind should work");

  A = pexle_choice_f(C, 		/* A -> "a" A "b" / epsilon */
		 pexle_seq_f(C, 
			 pexle_from_string(C, "a"),
			 pexle_seq_f(C, 
				 pexle_call(C, ref),
				 pexle_from_string(C, "b"))),
		 pexle_from_boolean(C, 1));

  failif(!A, "A will contain a TOpenCall");
/*   printf("A is: \n"); */
/*   print_exp(A, 0, st); */

  node = A->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp NOT captures");
  failif(exp_itself_hascaptures(node), "this exp NOT captures");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_ERR_OPENFAIL, "expected to catch openfail error");
  failif(exp_nullable(node) != 1, "exp nullable, and we never get to the OPEN CALL");
  failif(exp_nofail(node) != 1, "exp is nofail, and we never get to the OPEN CALL"); 
  failif(exp_headfail(node) != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 
  failif(exp_needfollow(node) != 1, "exp benefits from needfollow, and we never get to the OPEN CALL"); 
  stat = exp_getfirst(node, fullset, &cs); 
  failif(stat != 1, "firstset cannot be used as test, exp accepts epsilon");

  pexle_free(A);

  /* ----------------------------------------------------------------------------- */
  printf("********************** Grammar with one binding to unspecified \n");

  /* New env for grammar */
  env_free(g);
  g = env_new(env, NO);
  failif(!g, "failed to create new env");

  id = context_intern(C, "A");
  failif(id < 0, "symbol add should succeed");
  stat = env_bind(g, id, env_new_value(Eunspecified_t, 0, NULL), &ref);
  failif(ref_not_found(ref), "bind of unspecified value should work");
  failif(stat < 0, "bind of unspecified value should work");

  id = context_intern(C, "A");
  failif(id < 0, "symbol add should succeed");
  S = pexle_call(C, ref);		/* S -> A */
  failif(!S, "should be able to create S, because value bound to A is never examined");

  node = S->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp NOT captures");
  failif(exp_itself_hascaptures(node), "this exp NOT captures");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_ERR_OPENFAIL, "expected to catch openfail error");
  failif(exp_nullable(node) != EXP_ERR_OPENFAIL, "exp has an OPEN CALL");
  failif(exp_nofail(node) != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 
  failif(exp_headfail(node) != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 
  failif(exp_needfollow(node) != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 
  stat = exp_getfirst(node, fullset, &cs); 
  failif(stat != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 

  pexle_free(S);

  env_free(g);
  env_free(env);
  env_free(toplevel);
  symboltable_free(st);
  packagetable_free(m);
  context_free(C);

  /* ----------------------------------------------------------------------------- */
  printf("\nDone.\n");
  printf("Don't forget to run analyzetest2 next.\n");

  return 0;
}

