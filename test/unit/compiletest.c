/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  compiletest.c  TESTING compile.c                                         */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#define _GNU_SOURCE		/* for asprintf() */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>		/* exit() */
#include "print.h"
#include "expression.h"
#include "compile.h"
#include "analyze.h"

#define YES 1
#define NO 0

#define ANONYMOUS 0
#define NOT_RECURSIVE 0
#define RECURSIVE 1

#define HAS_CAPTURES 1
#define HEADFAIL 1
#define NULLABLE 1
#define NOFAIL 1
#define FIRSTSET 1
#define NEEDFOLLOW 1
#define HEADFAIL 1

#define NO_CAPTURES 0
#define NOT_HEADFAIL 0
#define NOT_NULLABLE 0
#define CAN_FAIL 0
#define NO_FIRSTSET 0
#define NO_NEEDFOLLOW 0
#define NO_HEADFAIL 0

/* Borrowed from analyze.c */
/* Follow ref, extract binding field, return NULL if it is not a Pattern. */
static Pattern *binding_pattern_value (Ref ref) {
  Binding *b = env_get_binding(ref);
  if (!b) return NULL;		/* invalid ref */
  if (b->val.type != Epattern_t) return NULL;
  return (Pattern *) b->val.ptr;
}

#define failif(test, msg)						\
  do { if (test) { fprintf(stderr, "Failed %s:%d: %s\n", __FILE__,	\
			   __LINE__, msg);				\
      fflush(stderr);							\
      exit(-1);								\
    }									\
  } while (0)

#define free_pattern_at(ref)						\
  do {									\
    b999 = env_get_binding(ref);					\
    if(bindingtype(b999) == Epattern_t) {				\
      pattern_free((Pattern *) b999->val.ptr);				\
      stat = env_rebind(ref, env_new_value(Eunspecified_t, 0, NULL));	\
      failif(stat < 0, "bind should work");				\
    } else {								\
      printf("free_pattern_at: no pattern to free\n");			\
    }									\
  } while (0)

static void check_metadata (Pattern *p,
			    int hascaptures,
			    int itself_hascaptures,
			    int nullable,
			    int nofail,
			    int headfail,
			    int bounded,
			    uint32_t expected_min,
			    uint32_t expected_max,
			    int needfollowflag,
			    Charset firstset,
			    int getfirstYES,
			    size_t lineno) {
  uint32_t min, max;
  int stat;
  Charset cs;
  unsigned char c;
  Node *node;
  char *msg;
  if (!p->fixed) {
    asprintf(&msg, "Error in check_node: pattern %p has no 'fixed' expression tree\n", (void *) p);
    failif(!p->fixed, msg);
  }
  node = p->fixed->node;
  printf("check_metadata called on pattern %p from line %lu\n", (void *)p, lineno);
  failif((exp_hascaptures(node) ? 1 : 0) != hascaptures, "this exp captures or not");
  failif((exp_itself_hascaptures(node) ? 1 : 0) != itself_hascaptures, "this pattern itself captures or not (without following calls)");
  stat = exp_patlen(node, &min, &max);
  failif(stat != bounded, "wrong boundedness");
  failif(min != expected_min, "wrong min");
  if (stat == EXP_BOUNDED) failif(max != expected_max, "wrong max");
  failif((exp_nullable(node) ? 1 : 0) != nullable, "pattern nullable or not");
  failif((exp_nofail(node) ? 1 : 0) != nofail, "pattern can fail or not");
  failif((exp_headfail(node) ? 1 : 0) != headfail, "pattern is headfail or not");
  failif((exp_needfollow(node) ? 1 : 0) != needfollowflag, "does pattern need follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(((stat == 0) ? 0 : 1) == getfirstYES, "stat==0 means firstset can be used as test");
  if (stat == 0) {
    /* Now check the contents of cs for 'h' */
    for (c = 0; c < 255; c++)
      if (testchar(firstset.cs, c)) {
	if (!testchar(cs.cs, c)) printf("charset missing %c\n", c);
	failif(!testchar(cs.cs, c), "firstset should contain this char");
      } else {
	if (testchar(cs.cs, c)) printf("cs should not have %c", c);
	failif(testchar(cs.cs, c), "firstset should not contain this char");
      }
  } /* if stat==0 */
}

int main(void) {

  Binding *b999;
  
  int stat; 
  Context *C;
  Index id;
  Package *pkg;
  
  Pattern *p; 
  Env *toplevel, *genv; 

  Value val;
  const char *str;

  Charset cs;  

  Expression *A, *B, *S;

  Env *env, *a_random_env;
  Ref ref, refA, refB, refS, ref_anon;

  printf("$$$ compiletest\n");

  toplevel = env_new(NULL, NO);
  failif(!toplevel, "new toplevel should succeed");
  
  C = context_new(toplevel);
  failif(!C, "expected new context");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Non-recursive block using anonymous exps \n");

  env = env_new(toplevel, NOT_RECURSIVE);
  failif(!env, "should succeed");

  stat = env_bind(env, ANONYMOUS, env_new_value(Eunspecified_t, 0, NULL), &refS);
  failif(stat < 0, "bind should work");

  A = pexle_call(C, refS);			     /* A -> S */
  S = pexle_capture_f(C, "foo", pexle_from_boolean(C, 1)); /* S -> capture("foo", true) */

  /* First, test for error checking */
  stat = compile_expression(NULL, S, env, &val);
  failif(stat != EXP_ERR_NULL, "expected to catch error");
  stat = compile_expression(C, NULL, env, &val);
  failif(stat != EXP_ERR_NULL, "expected to catch error");
  stat = compile_expression(C, S, NULL, &val);
  failif(stat != EXP_ERR_NULL, "expected to catch error");
  stat = compile_expression(C, S, env, NULL);
  failif(stat != EXP_ERR_NULL, "expected to catch error");
  a_random_env = env_new(NULL, NO);
  failif(!a_random_env, "expected a fresh env, as prereq for next test");
  stat = compile_expression(C, S, a_random_env, &val);
  failif(stat != EXP_ERR_SCOPE, "expected to catch error");

  stat = compile_expression(C, S, env, &val);
  failif(stat, "expected successful compile");
  failif(val.type != Epattern_t, "expected pattern type");

  stat = env_rebind(refS, val);
  failif(stat != 0, "bind should work");
  
  stat = compile_expression(C, A, env, &val);
  failif(stat, "expected successful compile");
  print_value(val, 0, C);
  failif(val.type != Epattern_t, "expected pattern type");

  memset(&cs, 0, CHARSETSIZE);
  setchar(cs.cs, 'A');
  check_metadata((Pattern *)val.ptr, HAS_CAPTURES, NO_CAPTURES, NULLABLE, NOFAIL, NOT_HEADFAIL, EXP_BOUNDED, 0, 0, NO_NEEDFOLLOW, cs, NO_FIRSTSET, __LINE__);

  /* First, test for error checking */
  stat = link_pattern(C, (Pattern *) val.ptr, NULL, NO_OPTIM);
  failif(stat != EXP_ERR_NULL, "should catch null arg error");
  stat = link_pattern(C, (Pattern *) NULL, &pkg, NO_OPTIM);
  failif(stat != EXP_ERR_NULL, "should catch null arg error");
  stat = link_pattern(NULL, (Pattern *) val.ptr, &pkg, NO_OPTIM);
  failif(stat != EXP_ERR_NULL, "should catch null arg error");

  /* Now try calling link_pattern properly */
  pkg = NULL;
  stat = link_pattern(C, (Pattern *) val.ptr, &pkg, NO_OPTIM);
  failif(stat != 0, "should work");
  failif(pkg == NULL, "expected a package structure");

  print_package(pkg);
  print_symboltable_stats(pkg->symtab);
  print_symboltable(pkg->symtab);

  failif(strcmp(symboltable_get_name(pkg->symtab, 0), "foo") != 0,
	 "checking for 'foo', which should be in slot 0 of the table");

  printf("-----\n");

  pattern_free((Pattern *)val.ptr);
  package_free(pkg); pkg = NULL;

  /* ----------------------------------------------------------------------------- */
  printf("********************** Recursive block using anonymous exps \n");

  printf("Trying a single rule that is left recursive due to a self loop\n"); 
  genv = env_new(env, RECURSIVE);
  stat = env_bind(genv, ANONYMOUS, env_new_value(Eunspecified_t, 0, NULL), &refA);
  failif(stat < 0, "bind to unspecified should work");
  A = pexle_call(C, refA);		/* A -> A */

  p = pattern_new(A, &stat);
  failif(!p, "expected new pattern");
  p->env = genv;		/* compilation env */

  stat = env_rebind(refA, env_new_value(Epattern_t, 0, (void *)p));
  failif(stat != 0, "should work");

  stat = compile_expression(C, A, env, &val);
  failif(stat != EXP_ERR_SCOPE, "should detect scope violation (env vs genv");

  stat = compile_expression(C, A, genv, &val);
  failif(stat != EXP_ERR_LEFT_RECURSIVE, "should detect left recursion");

  printf("-----\n");

  pexlc_env_free(genv);
  
  printf("Trying mutually left-recursive rules\n"); 
  genv = env_new(env, RECURSIVE);
  stat = env_bind(genv, ANONYMOUS, env_new_value(Eunspecified_t, 0, NULL), &refA);
  failif(stat < 0, "bind of unspecified should work");
  stat = env_bind(genv, ANONYMOUS, env_new_value(Eunspecified_t, 0, NULL), &refB);
  failif(stat < 0, "bind of novalue should work");

  A = pexle_call(C, refB);		/* A -> B */
  B = pexle_call(C, refA);		/* B -> A */

/*   printf("Expression at refA: "); print_exp(A, 0, NULL); */
/*   node = A->node; */

  p = pattern_new(A, &stat);
  failif(!p, "expected new pattern");
  p->env = genv;		/* compilation env */
  stat = env_rebind(refA, env_new_value(Epattern_t, 0, (void *)p));
  failif(stat < 0, "rebind of A should work");
  printf("Pattern A is: "); print_pattern(p, 0, C);

  p = pattern_new(B, &stat);
  failif(!p, "expected new pattern");
  p->env = genv;		/* compilation env */
  stat = env_rebind(refB, env_new_value(Epattern_t, 0, (void *)p));
  failif(stat < 0, "rebind of B should work");
  printf("Pattern B is: "); print_pattern(p, 0, C);

  /* Compile B */
  stat = compile_expression(C, B, genv, &val);
  failif(stat != EXP_ERR_LEFT_RECURSIVE, "should detect left recursion");
  
  printf("-----\n");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Grammar with realistic recursion \n");

  /* New env for grammar */
  pexlc_env_free(genv);
  genv = env_new(env, RECURSIVE);
  failif(!genv, "failed to create new env");

  id = context_intern(C, "foo"); /* for later use */
  failif(id < 0, "symbol add should succeed");

  id = context_intern(C, "A");
  failif(id < 0, "symbol add should succeed");
  stat = env_bind(genv, id, env_new_value(Eunspecified_t, 0, NULL), &ref);
  failif(ref_not_found(ref), "bind should work");

  A = pexle_choice_f(C,		/* A -> "a" A "b" / epsilon */
		   pexle_seq_f(C,
			     pexle_from_string(C, "a"),
			     pexle_seq_f(C, 
				       pexle_call(C, ref),
				       pexle_from_string(C, "b"))),
		   pexle_from_boolean(C, 1));

  failif(!A, "A is a valid expression");
  printf("A is: \n"); 
  print_exp(A, 0, C); 

  /* Error check: if we try to compile A, we'll get an error because A calls a binding to Unspecified */
  stat = compile_expression(C, A, genv, &val);
  failif(stat != EXP_ERR_NO_PATTERN, "should catch error");
  
  /* Now modify the "A" binding's value to the pattern based on the expression A */
  /* OLD WAY: */
      p = pattern_new(A, &stat); 
      failif(!p, "expected new pattern"); 
      p->env = genv;		/* compilation env */ 
      stat = env_rebind(ref, env_new_value(Epattern_t, 0, (void *)p)); 
      failif(ref_not_found(ref), "bind should work"); 
      failif(stat != 0, "bind should work"); 

  /* NEW WAY: */
/*   pexlc_bind_expression(C, A, ref, &err); */
/*   failif(err.value != OK, "bind should work"); */

  /* First, test for error checking */
  stat = compile_pattern(NULL, p, genv, &val); 
  failif(stat != EXP_ERR_NULL, "expected to catch error"); 
  stat = compile_pattern(C, NULL, genv, &val); 
  failif(stat != EXP_ERR_NULL, "expected to catch error"); 
  stat = compile_pattern(C, p, NULL, &val); 
  failif(stat != EXP_ERR_NULL, "expected to catch error"); 
  stat = compile_pattern(C, p, genv, NULL); 
  failif(stat != EXP_ERR_NULL, "expected to catch error"); 
  failif(!a_random_env, "expected a fresh env, as prereq for next test"); 
  stat = compile_pattern(C, p, a_random_env, &val); 
  failif(stat != EXP_ERR_SCOPE, "expected to catch error"); 
/*   stat = pexlc_compile_bound_expression(NULL, ref, &err, NO_OPTIM); */
/*   failif(stat != EXP_ERR_NULL, "expected to catch error"); */
/*   ref_anon.env = NULL;		/\* Try an invalid ref *\/ */
/*   ref_anon.index = -11;		/\* error when index < 0  *\/ */
/*   stat = pexlc_compile_bound_expression(C, ref_anon, &err, NO_OPTIM); */
/*   failif(stat != EXP_ERR_NULL, "expected to catch error"); */
/*   ref_anon.env = NULL;		/\* Try an invalid ref *\/ */
/*   ref_anon.index = 0;		/\* "not found" when index is 0  *\/ */
/*   stat = pexlc_compile_bound_expression(C, ref_anon, &err, NO_OPTIM); */
/*   failif(stat != EXP_ERR_NULL, "expected to catch error"); */

/*   failif(!a_random_env, "expected a fresh env, as prereq for next test"); */
/*   ref_anon.env = a_random_env; */
/*   ref_anon.env = 1; */
/*   stat = pexlc_compile_bound_expression(C, ref_anon, &err, NO_OPTIM); */
/*   failif(stat != EXP_ERR_SCOPE, "expected to catch error"); */

/*   stat = pexlc_compile_bound_expression(C, ref, &err, NO_OPTIM);  */
/*   failif(stat, "expected successful compile"); */

  /* Old way: */
  stat = compile_pattern(C, p, genv, &val);  
  failif(stat, "expected successful compile"); 

/*   val = env_get_binding(genv, ref)->value; */
  failif(val.type != Epattern_t, "expected pattern type");
  print_value(val, 0, C);

  failif(val.type != Epattern_t, "wrong value type returned from compile_expression");
  
  memset(&cs, 0, CHARSETSIZE);
  setchar(cs.cs, 'A');
  check_metadata((Pattern *)val.ptr, NO_CAPTURES, NO_CAPTURES, NULLABLE, NOFAIL, NOT_HEADFAIL, EXP_UNBOUNDED, 0, 123, NEEDFOLLOW, cs, NO_FIRSTSET, __LINE__);

/*   print_sorted_cfgraph(C->cst->g); */

  stat = link_pattern(C, (Pattern *) val.ptr, &pkg, NO_OPTIM);
  failif(stat != 0, "should work");
  failif(pkg == NULL, "expected a package structure");

  print_package(pkg);

  printf("-----\n");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Grammar with left recursion with nullable sub-pattern \n");

  A = pexle_choice_f(C,		/* A -> "a"? A "b" / epsilon */
		   pexle_seq_f(C,
			     pexle_repeat_f(C, pexle_from_string(C, "a"), -1),
			     pexle_seq_f(C, 
				       pexle_call(C, ref),
				       pexle_from_string(C, "b"))),
		   pexle_from_boolean(C, 1));

  failif(!A, "A is a valid expression");
  printf("A is: \n"); 
  print_exp(A, 0, C); 

  /* Now modify the "A" binding's value to the pattern based on the expression A */
  p = pattern_new(A, &stat);
  failif(!p, "expected new pattern");
  p->env = genv;		/* compilation env */
  free_pattern_at(ref);
  stat = env_rebind(ref, env_new_value(Epattern_t, 0, (void *)p));
  failif(ref_not_found(ref), "bind should work");
  failif(stat != 0, "bind should work");

  stat = compile_expression(C, A, genv, &val);
  failif(stat != EXP_ERR_LEFT_RECURSIVE, "should detect left recursion");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Grammar with left recursion and CALLED nullable sub-pattern \n");

  str = context_retrieve(C, 1);
  failif(str[0] != 'f' || str[1] != 'o' || str[2] != 'o', "symbol table entry 1 is 'foo'");
  
  B = pexle_repeat_f(C, pexle_from_string(C, "a"), -1); /* B -> "a"? */
  failif(!B, "expected new expression");
  p = pattern_new(B, &stat);
  failif(!p, "expected new pattern");
  p->env = genv;
  stat = env_bind(genv, 1, env_new_value(Epattern_t, 0, (void *)p), &refB);
  failif(stat != 0, "expected success");

  printf("foo is: \n"); 
  print_pattern(p, 0, C); 
  
  A = pexle_choice_f(C,		/* A -> foo A "b" / epsilon */
		   pexle_seq_f(C,
			     pexle_call(C, refB),
			     pexle_seq_f(C, 
				       pexle_call(C, ref),
				       pexle_from_string(C, "b"))),
		   pexle_from_boolean(C, 1));

  failif(!A, "A is a valid expression");

  /* Now modify the "A" binding's value to the pattern based on the expression A */
  p = pattern_new(A, &stat);
  failif(!p, "expected new pattern");
  p->env = genv;		/* compilation env */
  free_pattern_at(ref);
  stat = env_rebind(ref, env_new_value(Epattern_t, 0, (void *)p));
  failif(ref_not_found(ref), "bind should work");
  failif(stat != 0, "bind should work");

  printf("A is: \n"); 
  print_pattern(p, 0, C); 

  stat = compile_expression(C, A, genv, &val);
  failif(stat != EXP_ERR_LEFT_RECURSIVE, "should detect left recursion");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Grammar with mutual left recursion and CALLED nullable sub-pattern \n");

  B = pexle_seq_f(C, 
		pexle_repeat_f(C, pexle_from_string(C, "a"), -1), /* "a"? */
		pexle_call(C, ref)			    /* A */
		);				    /* B -> "a"? A */
  failif(!B, "expected new expression");
  p = pattern_new(B, &stat);
  failif(!p, "expected new pattern");
  p->env = genv;
  free_pattern_at(refB);
  stat = env_rebind(refB, env_new_value(Epattern_t, 0, (void *)p));
  failif(stat != 0, "expected success");
  
  A = pexle_choice_f(C, 		/* A -> foo "b" / epsilon */
		   pexle_seq_f(C, 
			     pexle_call(C, refB),
			     pexle_from_string(C, "b")),
		   pexle_from_boolean(C, 1));

  failif(!A, "A is a valid expression");
  printf("A is: \n"); 
  print_exp(A, 0, C); 
  printf("foo is: \n"); 
  print_exp(B, 0, C); 

  /* Now modify the "A" binding's value to the pattern based on the expression A */
  p = pattern_new(A, &stat);
  failif(!p, "expected new pattern");
  p->env = genv;		/* compilation env */
  free_pattern_at(ref);
  stat = env_rebind(ref, env_new_value(Epattern_t, 0, (void *)p));
  failif(ref_not_found(ref), "bind should work");
  failif(stat != 0, "bind should work");

  stat = compile_expression(C, A, genv, &val);
  failif(stat != EXP_ERR_LEFT_RECURSIVE, "should detect left recursion");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Add another non-terminal, and remove the mutual left recursion \n");

  /* <anon> -> "x" / A */
  B = pexle_choice_f(C,
		   pexle_from_string(C, "x"),
		   pexle_call(C, ref));

  failif(!B, "expected new expression");
  p = pattern_new(B, &stat);
  failif(!p, "expected new pattern");
  p->env = genv;
  stat = env_bind(genv, 0, env_new_value(Epattern_t, 0, (void *)p), &ref_anon);
  failif(stat != 0, "expected success");

  /* B -> "a"+ <anon> */
  B = pexle_seq_f(C, 
		pexle_repeat_f(C, pexle_from_string(C, "a"), 1), /* "a"+ */
		pexle_call(C, ref_anon)		   /* A */
		);
  failif(!B, "expected new expression");
  p = pattern_new(B, &stat);
  failif(!p, "expected new pattern");
  p->env = genv;
  free_pattern_at(refB);
  stat = env_rebind(refB, env_new_value(Epattern_t, 0, (void *)p));
  failif(stat != 0, "expected success");

  printf("foo is: \n"); 
  print_pattern(p, 0, C); 

  A = pexle_choice_f(C,		/* A -> B "b" / epsilon */
		   pexle_seq_f(C, 
			     pexle_call(C, refB),
			     pexle_from_string(C, "b")),
		   pexle_from_boolean(C, 1));

  failif(!A, "A is a valid expression");

  printf("<anon> is: \n"); 
  p = binding_pattern_value(ref_anon);
  failif(!p, "expected a pattern");
  print_pattern(p, 0, C); 

  /* Now modify the "A" binding's value to the pattern based on the expression A */
  p = pattern_new(A, &stat);
  failif(!p, "expected new pattern");
  p->env = genv;		/* compilation env */
  free_pattern_at(ref);
  stat = env_rebind(ref, env_new_value(Epattern_t, 0, (void *)p));
  failif(ref_not_found(ref), "bind should work");
  failif(stat != 0, "bind should work");

  printf("A is: \n"); 
  print_pattern(p, 0, C); 

  stat = compile_pattern(C, p, genv, &val);
  failif(stat, "expected successful compile");
  print_value(val, 0, C);
  print_env(genv, C);

  failif(val.type != Epattern_t, "wrong value type returned from compile_expression");

  memset(&cs, 0, CHARSETSIZE);
  setchar(cs.cs, 'A');
  check_metadata((Pattern *)val.ptr, NO_CAPTURES, NO_CAPTURES, NULLABLE, NOFAIL, NOT_HEADFAIL, EXP_UNBOUNDED, 0, 123, NEEDFOLLOW, cs, NO_FIRSTSET, __LINE__);

  package_free(pkg); pkg = NULL;
  stat = link_pattern(C, (Pattern *) val.ptr, &pkg, NO_OPTIM);
  failif(stat != 0, "should work");
  failif(pkg == NULL, "expected a package structure");

  print_package(pkg);

  package_free(pkg);

  printf("-----\n");


  /* ----------------------------------------------------------------------------- */
  printf("********************** Make the previous grammar mutually left recursive \n");

  /* <anon> -> "x" / A */
  B = pexle_choice_f(C,
		   pexle_from_string(C, "x"),
		   pexle_call(C, ref));

  failif(!B, "expected new expression");
  p = pattern_new(B, &stat);
  failif(!p, "expected new pattern");
  p->env = genv;
  stat = env_bind(genv, ANONYMOUS, env_new_value(Epattern_t, 0, (void *)p), &ref_anon);
  failif(stat != 0, "expected success");

  printf("<anon> is: \n"); 
  p = binding_pattern_value(ref_anon);
  failif(!p, "expected a pattern");
  print_pattern(p, 0, C); 

  /* B -> "a"* <anon> */
  B = pexle_seq_f(C,
		pexle_repeat_f(C, pexle_from_string(C, "a"), 0), /* "a"* */
		pexle_call(C, ref_anon)		   /* A */
		);
  failif(!B, "expected new expression");
  p = pattern_new(B, &stat);
  failif(!p, "expected new pattern");
  p->env = genv;
  free_pattern_at(refB);
  stat = env_rebind(refB, env_new_value(Epattern_t, 0, (void *)p));
  failif(stat != 0, "expected success");

  printf("foo is: \n"); 
  print_pattern(p, 0, C); 
  
  A = pexle_choice_f(C,		/* A -> B "b" / epsilon */
		   pexle_seq_f(C,
			     pexle_call(C, refB),
			     pexle_from_string(C, "b")),
		   pexle_from_boolean(C, 1));
  
  failif(!A, "A is a valid expression");

  /* Now modify the "A" binding's value to the pattern based on the expression A */
  p = pattern_new(A, &stat);
  failif(!p, "expected new pattern");
  p->env = genv;		/* compilation env */
  free_pattern_at(ref);
  stat = env_rebind(ref, env_new_value(Epattern_t, 0, (void *)p));
  failif(ref_not_found(ref), "bind should work");
  failif(stat != 0, "bind should work");

  printf("THIS TEST SHOULD CORRESPOND TO:\n");
  printf("  A -> (B 'b') / true\n");
  printf("  B -> 'a'* <anon>\n");
  printf("  <anon> -> 'x' / A\n");

  stat = compile_expression(C, A, genv, &val);
  failif(stat != EXP_ERR_LEFT_RECURSIVE, "should detect left recursion");

  printf("At refB is: ");
  print_pattern(binding_pattern_value(refB), 0, C);
  printf("<anon> is: ");
  print_pattern(binding_pattern_value(ref_anon), 0, C);
  printf("A is: \n");
  print_pattern(binding_pattern_value(ref), 0, C);

  pexlc_env_free(genv);
  pexlc_env_free(env);
  pexlc_env_free(a_random_env);
  pexlc_env_free(toplevel);
  context_free(C);
  
  /* ----------------------------------------------------------------------------- */
  printf("\nDone.\n");
  printf("Don't forget to run interfacetest next.\n");

  return 0;
}

