/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  envtest.c  TESTING the new env data structures                           */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "env.h"
#include "print.h"

#define YES 1
#define NO 0

#define failif(test, msg)						\
  do { if (test) { fprintf(stderr, "Failed %s:%d: %s\n", __FILE__,	\
			   __LINE__, msg);				\
      fflush(stderr);							\
      exit(-1);								\
    }									\
  } while (0)


int main(void) {
  Env *envnr, *envr, *envr_child, *toplevel;
  Binding *binding; 
  Index i;
  Ref ref;
  int stat;
  
  printf("$$$ envtest\n");

  toplevel = env_new(NULL, NO);
  failif(!toplevel, "creation of a top level env, as for a new package, should succeed");

  envnr = env_new(toplevel, NO);
  failif(!envnr, "Should be able to create a block with a top level env as parent");
  failif(envnr->size != ENV_INIT_SIZE, "wrong initial size");
  failif(envnr->next != 0, "wrong initial value for 'next'");
  env_free(envnr);

  envr = env_new(toplevel, YES);
  failif(!envr, "Should be able to create a recursive block with top level env as parent");
  failif(envr->size != ENV_INIT_SIZE, "wrong initial size");
  failif(envr->next != 0, "wrong initial value for 'next'");
  env_free(envr);
  
  /* ----------------------------------------------------------------------------- */
  printf("Printing the empty top level environment: (should produce empty table)\n");
  print_env_stats(toplevel);
  print_env(toplevel, NULL);

  failif(toplevel->size != ENV_INIT_SIZE, "wrong initial size");
  failif(toplevel->next != 0, "wrong initial value for 'next'");

  i = ENV_ITER_START;
  binding = env_local_iter(toplevel, &i);
  failif(binding, "empty env should yield NULL on first iteration");

  /* ----------------------------------------------------------------------------- */
  printf("Creating some bindings in top level\n");



  stat = env_bind(toplevel, 1, env_new_value(Eunspecified_t, 0, NULL), &ref);
  failif(stat != 0, "successful env_bind should return 0");
  failif(ref.env != toplevel, "ref should point to this env");
  failif(ref.index != 0, "first binding should be at index 0 of bindings array");

  stat = env_bind(toplevel, 1, env_new_value(Eunspecified_t, 0, NULL), &ref);
  failif(stat != ENV_ERR_EXISTS, "cannot create another binding with same name");

  /* The next binding should end up in index 1 of binding table. */
  stat = env_bind(toplevel, 111, env_new_value(Eunspecified_t, 0, NULL), &ref);
  failif(stat != 0, "successful env_bind should return 0");
  failif(ref.env != toplevel, "ref should point to this env");
  failif(ref.index != 1, "second binding should be at index 1 of bindings array");

  stat = env_bind(toplevel, 2, env_new_value(Eunspecified_t, 0, NULL), &ref); 
  failif(stat != 0, "successful env_bind should return 0"); 
  failif(ref.env != toplevel, "ref should point to this env"); 

  stat = env_bind(toplevel, 3, env_new_value(Eunspecified_t, 0, NULL), &ref); 
  failif(stat != 0, "successful env_bind should return 0"); 
  failif(ref.env != toplevel, "ref should point to this env"); 

  print_env(toplevel, NULL);

  binding = env_get_binding(ref);
  failif(binding->val.type != Eunspecified_t, "checking the value we bound is correct"); 
  failif(binding->val.data != 0, "checking the value we bound is correct"); 
  failif(binding->val.ptr != NULL, "checking the value we bound is correct");

  /* ----------------------------------------------------------------------------- */
  printf("Creating some bindings in envnr (child of top level)\n");

  envnr = env_new(toplevel, NO);
  failif(!envnr, "Should be able to create a block with a top level env as parent");

  stat = env_bind(envnr, 100, env_new_value(Eunspecified_t, 0, NULL), &ref);
  failif(stat != 0, "successful env_bind should return 0");
  failif(ref.env != envnr, "ref should point to this env");
  failif(ref.index != 0, "first binding should be at index 0 of bindings array");

  stat = env_bind(envnr, 101, env_new_value(Eunspecified_t, 0, NULL), &ref); 
  failif(stat != 0, "successful env_bind should return 0"); 

  stat = env_bind(envnr, 102, env_new_value(Eunspecified_t, 0, NULL), &ref); 
  failif(stat != 0, "successful env_bind should return 0"); 
  
  stat = env_bind(envnr, 100, env_new_value(Eunspecified_t, 0, NULL), &ref); 
  failif(stat != ENV_ERR_EXISTS, "binding with this name exists"); 

  print_env_stats(envnr);
  print_env(envnr, NULL);

  failif(envnr->next != 3, "wrong number of bindings?");

  /* ----------------------------------------------------------------------------- */
  printf("Creating some bindings in envr (recursive, child of top level)\n");

  envr = env_new(toplevel, YES);
  failif(!envr, "Should be able to create a recursive block with top level env as parent");

  stat = env_bind(envr, 200, env_new_value(Eunspecified_t, 0, NULL), &ref); 
  failif(stat != 0, "successful env_bind should return 0"); 
  failif(ref.env != envr, "ref should point to this env"); 
  failif(ref.index != 0, "first binding should be at index 0 of bindings array"); 

  stat = env_bind(envr, 201, env_new_value(Eunspecified_t, 0, NULL), &ref); 
  failif(stat != 0, "successful env_bind should return 0"); 

  stat = env_bind(envr, 202, env_new_value(Eunspecified_t, 0, NULL), &ref); 
  failif(stat != 0, "successful env_bind should return 0"); 
  
  stat = env_bind(envr, 203, env_new_value(Eunspecified_t, 0, NULL), &ref); 
  failif(stat != 0, "successful env_bind should return 0"); 
  
  print_env_stats(envr);
  print_env(envr, NULL);

  failif(envr->next != 4, "wrong number of bindings?");
  
  /* ----------------------------------------------------------------------------- */
  printf("Creating some bindings in envr_child (recursive, child of envr)\n");

  envr_child = env_new(envr, YES);
  failif(!envr_child, "should succeed");
  
  stat = env_bind(envr_child, 300, env_new_value(Eunspecified_t, 0, NULL), &ref);
  failif(stat != 0, "successful env_bind should return 0");

  stat = env_bind(envr_child, 301, env_new_value(Eunspecified_t, 0, NULL), &ref);
  failif(stat != 0, "successful env_bind should return 0");

  print_env_stats(envr_child);
  print_env(envr_child, NULL);

  failif(envr_child->next != 2, "wrong number of bindings?");
  
  /* ----------------------------------------------------------------------------- */
  printf("Checking that the 'compiled' flag is reset with rebind, unbind\n");

/*   failif(env_has_attr(toplevel, Eattr_compiled), "before compilation, flag says NOT compiled"); */

  ref.env = toplevel;
  ref.index = 1;
  binding = env_get_binding(ref);
  failif(!binding, "should get the binding with name 111");
  failif(binding->name != 111, "wrong binding?");
  
  stat = env_rebind(ref, env_new_value(Eunspecified_t, 0, NULL));
  failif(stat, "should succeed");
  failif(binding->val.type != Eunspecified_t, "checking the value we bound is correct");
  failif(binding->val.data != 0, "checking the value we bound is correct");
  failif(binding->val.ptr != NULL, "checking the value we bound is correct");
  
/*   failif(env_has_attr(toplevel, Eattr_compiled), "before compilation, flag says NOT compiled"); */
/*   set_env_attr(toplevel, Eattr_compiled, 1); */
/*   failif(!env_has_attr(toplevel, Eattr_compiled), "checking set of env attribute"); */

  stat = env_rebind(ref, env_new_value(Eunspecified_t, 1234, NULL));
  failif(stat, "should succeed");
  failif(binding->val.type != Eunspecified_t, "checking the value we bound is correct");
  failif(binding->val.data != 1234, "checking the value we bound is correct");
  failif(binding->val.ptr != NULL, "checking the value we bound is correct");
  
/*   failif(env_has_attr(toplevel, Eattr_compiled), "after rebind, flag says NOT compiled"); */

  ref.env = envr;
  ref.index = 7;		/* DOES NOT EXIST */
  stat = env_rebind(ref, env_new_value(Eunspecified_t, 0, NULL));
  failif(stat != ENV_NOT_FOUND, "invalid index should be caught and an error returned");
  
  binding = env_get_binding(ref);
  failif(binding, "invalid index should be caught and an error returned");

  stat = env_unbind(ref);
  failif(stat != ENV_NOT_FOUND, "invalid index should be caught and an error returned");
  
  /* ----------------------------------------------------------------------------- */
  printf("Checking unbind\n");

  ref.env = toplevel;
  ref.index = 1;		/* index in binding table = 1 */
  binding = env_get_binding(ref);
  failif(!binding, "should find the binding we made earlier");
  failif(binding->name != 111, "should find the binding we made earlier");
  failif(binding->val.type != Eunspecified_t, "checking the value we bound is correct");
  failif(binding->val.data != 1234, "checking the value we bound is correct");
  failif(binding->val.ptr != NULL, "checking the value we bound is correct");

/*   failif(env_has_attr(toplevel, Eattr_compiled), "flag says NOT compiled"); */
  stat = env_unbind(ref);
  failif(stat != ENV_OK, "should be able to unbind this ref");
/*   failif(env_has_attr(toplevel, Eattr_compiled), "flag STILL says NOT compiled"); */
  stat = env_unbind(ref);
  failif(stat != ENV_NOT_FOUND, "should not be able to unbind this deleted ref");

/*   set_env_attr(toplevel, Eattr_compiled, 1); */
  stat = env_unbind(ref);
  failif(stat != ENV_NOT_FOUND, "should not be able to unbind this deleted ref");
/*   failif(!env_has_attr(toplevel, Eattr_compiled), "failed unbind should not change flag"); */
  ref.index = 2;
  stat = env_unbind(ref);
  failif(stat != ENV_OK, "should be able to unbind this ref");
/*   failif(env_has_attr(toplevel, Eattr_compiled), "flag should be cleared by unbind"); */

  /* Make sure that rebind fails on a deleted binding */
  binding = env_get_binding(ref);
  failif(binding, "binding should be null, since it has been deleted");
  stat = env_rebind(ref, env_new_value(Eunspecified_t, 0, NULL));
  failif(stat != ENV_NOT_FOUND, "should not be able to rebind this deleted ref");

  print_env_stats(toplevel);
  print_env(toplevel, NULL);

  /* ----------------------------------------------------------------------------- */
  printf("Testing env_lookup and env_local_lookup\n");

  /* envr_child */
  ref = env_local_lookup(envr_child, 300);
  failif(ref.env != envr_child, "block lookup failed");
  failif(ref.index != 0, "should find the first binding, at index 0");
  
  ref = env_local_lookup(envr_child, 301);
  failif(ref.env != envr_child, "block lookup failed");
  failif(ref.index != 1, "should find the binding at index 1");

  ref = env_local_lookup(envr_child, 0);
  failif(ref.env != NULL, "block lookup should fail when looking for anon binding");

  ref = env_local_lookup(envr_child, 333);
  failif(ref.env != NULL, "block lookup should fail because this name is not bound");

  /* envr */
/*   ref = env_local_lookup(envr, 200); */
/*   failif(ref.env != envr, "block lookup failed"); */
/*   failif(ref.index != 0, "should find the first binding, at index 0"); */

  ref = env_local_lookup(envr, 203);
  failif(ref.env != envr, "block lookup failed");

  ref = env_local_lookup(envr, 0);
  failif(ref.env != NULL, "block lookup should fail when looking for anon binding");

  ref = env_local_lookup(envr, 222);
  failif(ref.env != NULL, "block lookup should fail because this name is not bound");

  /* envnr */
  ref = env_local_lookup(envnr, 100);
  failif(ref.env != envnr, "block lookup failed");
  failif(ref.index != 0, "should find the first binding, at index 0");

  ref = env_local_lookup(envnr, 102);
  failif(ref.env != envnr, "block lookup failed");

  ref = env_local_lookup(envnr, 0);
  failif(ref.env != NULL, "block lookup should fail when looking for anon binding");

  ref = env_local_lookup(envnr, 111);
  failif(ref.env != NULL, "block lookup should fail because this name is not bound");

  /* top level */
  ref = env_local_lookup(toplevel, 1);
  failif(ref.env != toplevel, "block lookup failed");
  failif(ref.index != 0, "should find the first binding, at index 0");

  ref = env_local_lookup(toplevel, 3);
  failif(ref.env != toplevel, "block lookup failed");

  ref = env_local_lookup(toplevel, 0);
  failif(ref.env != NULL, "block lookup should fail when looking for anon binding");

  ref = env_local_lookup(toplevel, 999);
  failif(ref.env != NULL, "block lookup should fail because this name is not bound");

  ref = env_local_lookup(toplevel, 111);
  failif(ref.env != NULL, "block lookup should fail because this name has been deleted");

  /* env_lookup in envnr should find envnr and top bindings */

  ref = env_lookup(envnr, 3);
  failif(ref.env != toplevel, "block lookup failed");

  ref = env_local_lookup(envnr, 0);
  failif(ref.env != NULL, "block lookup should fail when looking for anon binding");

  ref = env_local_lookup(envnr, 999);
  failif(ref.env != NULL, "block lookup should fail because this name is not bound");

  /* env_lookup in envr should find envr and toplevel bindings */

  ref = env_lookup(envr, 1);
  failif(ref.env != toplevel, "block lookup failed");
  failif(ref.index != 0, "should find the first binding, at index 0");

  ref = env_lookup(envr, 0);
  failif(ref.env != NULL, "block lookup should fail when looking for anon binding");

  ref = env_lookup(envr, 2);
  failif(ref.env != NULL, "block lookup should fail because this name is deleted");

  ref = env_local_lookup(envr, 999);
  failif(ref.env != NULL, "block lookup should fail because this name is not bound");

  /* env_lookup in envr_child should find envr_child, envr, and toplevel bindings */

  ref = env_lookup(envr_child, 1);
  failif(ref.env != toplevel, "block lookup failed");
  failif(ref.index != 0, "should find the first binding, at index 0");

/*   ref = env_lookup(envr_child, 200); */
/*   failif(ref.env != envr, "block lookup failed"); */
/*   failif(ref.index != 0, "should find the first binding, at index 0"); */

  ref = env_lookup(envr_child, 0);
  failif(ref.env != NULL, "block lookup should fail when looking for anon binding");

  ref = env_lookup(envr_child, 2);
  failif(ref.env != NULL, "block lookup should fail because this name is deleted");

  ref = env_local_lookup(envr_child, 999);
  failif(ref.env != NULL, "block lookup should fail because this name is not bound");

  /* ----------------------------------------------------------------------------- */
  printf("Testing env_local_iter\n");

  i = ENV_ITER_START;
  while ((binding = env_local_iter(toplevel, &i)))
    printf("toplevel has binding named %d\n", binding->name);
  failif(i != 4, "last binding is at index 3");

  i = ENV_ITER_START;
  while ((binding = env_local_iter(envr_child, &i)))
    printf("envr_child has binding named %d\n", binding->name);
  failif(i != 2, "last binding is at index 1");
  
  env_free(envnr);
  envnr = env_new(toplevel, NO);

  i = ENV_ITER_START;
  while ((binding = env_local_iter(envnr, &i)))
    printf("THIS MESSAGE SHOULD NOT BE PRINTED!  %d\n", binding->name);
  failif(i != 0, "no bindings in this fresh env");

  stat = env_bind(envnr, 666, env_new_value(Eunspecified_t, 0, NULL), &ref);
  failif(stat != ENV_OK, "should succeed");
  failif(ref.env != envnr, "should succeed");

  stat = env_unbind(ref);
  failif(stat != ENV_OK, "should succeed");
  
  print_env(envnr, NULL);

  i = ENV_ITER_START;
  stat = 0;
  while ((binding = env_local_iter(envnr, &i))) {
    stat = 1;
    printf("THIS MESSAGE SHOULD NOT BE PRINTED!  %d\n", binding->name);
  }
  failif(stat, "no undeleted bindings in this env");
  failif(i != 1, "no undeleted bindings in this env");

  /* ----------------------------------------------------------------------------- */
  printf("Testing anon bindings\n");

  stat = env_bind(envnr, 0, env_new_value(Eunspecified_t, 0, NULL), &ref);
  failif(stat != ENV_OK, "should get anon binding");
  failif(ref.env != envnr, "should get anon binding");

  stat = env_bind(envnr, 0, env_new_value(Eunspecified_t, 0, NULL), &ref);
  failif(stat != ENV_OK, "should get anon binding");
  failif(ref.env != envnr, "should get anon binding");

  print_env(envnr, NULL);

  stat = env_bind(envr_child, 0, env_new_value(Eunspecified_t, 0, NULL), &ref);
  failif(stat != ENV_OK, "should get anon binding");
  failif(ref.env != envr_child, "should get anon binding");

  binding = env_get_binding(ref);
  failif(!binding, "should find the binding we made earlier");
  failif(binding->env != envr_child, "did we get the right anon binding");
  failif(binding->name != 0, "should be anon");
  failif(binding->val.type != Eunspecified_t, "did we get the right binding");
  failif(binding->val.data != 0, "did we get the right binding");

  ref.env = envnr;
  ref.index = 2;
  binding = env_get_binding(ref);
  failif(!binding, "should find the binding we made earlier");
  failif(binding->env != envnr, "did we get the right anon binding");
  failif(binding->name != 0, "should be anon");
  failif(binding->val.type != Eunspecified_t, "did we get the right binding");
  failif(binding->val.data != 0, "did we get the right binding");
  
  env_free(envr_child);
  
  /* ----------------------------------------------------------------------------- */
  printf("Testing dynamic expansion of binding table\n");
  printf("  Starting size of envnr is %u/%u\n", envnr->next, envnr->size);
  for (i = envnr->next; i < envnr->size; i++) {
    stat = env_bind(envnr, 0, env_new_value(Eunspecified_t, 0, NULL), &ref);
    failif(stat != 0, "bind should succeed");
  }
  printf("  Size of envnr is now %u/%u\n", envnr->next, envnr->size);
  failif(envnr->size != envnr->next, "table should be full");

  stat = env_bind(envnr, 0, env_new_value(Eunspecified_t, 0, NULL), &ref);
  failif(stat != 0, "bind should succeed");
  printf("  Size of envnr is now %u/%u\n", envnr->next, envnr->size);
  failif(envnr->size <= envnr->next, "table should have expanded");

  env_free(envr);
  env_free(envnr);
  env_free(toplevel);
  printf("Done.\n");
  return 0;
}

