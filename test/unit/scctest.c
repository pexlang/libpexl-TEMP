/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  SCCtest.c                                                             */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */


#define ANNOUNCE_COMPARISONS 0

#include <stdio.h>
#include <assert.h>
#include <string.h>

#include "scc.h"
#include "logging.h"

#define TESTMAXCHARS 10

#define TRUE 1
#define FALSE 0
#define UNUSED(x) (void)(x)

static void *nullptr = NULL;
static char nullchar = '\0';
static char nullchar7[7];
static char nullchar16[16];

#pragma GCC diagnostic push  /* GCC 4.6 and up */

#ifdef __clang__
/* nothing to do */
#elif __GNUC__
  #pragma GCC diagnostic ignored "-Wformat"
#elif _MSC_VER
  #error MS C compiler not supported
#elif __MINGW32__
/* nothing to do */
#endif

/* This comparator receives pointers to char pointers. */
static int cmp_ptr (void *context, const void *aa, const void *bb) {
  const char *const a = *(const char *const *) aa;
  const char *const b = *(const char *const *) bb;
  UNUSED(context);
  if (ANNOUNCE_COMPARISONS) printf("cmp_ptr(%s, %s)\n", a, b); 
  if (a && b) return strncmp(a, b, TESTMAXCHARS);
  if (!a && !b) return 0;	/* both null */
  return 1;			/* just one is null */
}

/* This printer expects pointers to char pointers. */
static void print_deplists_ptr (void **input, size_t nel) {
  size_t i;
  const char **deplist;  
  printf("Deplists: \n");
  for (i = 0; i < nel; i++) {
    printf("[%d] ", (int) i);
    deplist = input[i];
    while (*deplist) {
      printf(" %s ", (const char *) *deplist);
      deplist++;
    }
    printf("\n");
  }
}

/* This comparator receives values that are single bytes (each is a char). */
static int cmp_char (void *context, const void *aa, const void *bb) {
  const char a = *(const char *const) aa;
  const char b = *(const char *const) bb;
  UNUSED(context);
  if (ANNOUNCE_COMPARISONS) printf("cmp_char(%c, %c)\n", a, b);
  return (a < b) ? -1 : ((a > b) ? 1 : 0);
}


/* This printer expects single bytes (each is a char). */
static void print_deplists_char (void **input, size_t nel) {
  size_t i;
  const char *deplist;  
  printf("Deplists: \n");
  for (i = 0; i < nel; i++) {
    printf("[%d] ", (int) i);
    deplist = input[i];
    while (*deplist) {
      printf(" %c ", (const char) *deplist);
      deplist++;
    }
    printf("\n");
  }
}

static int cmp_char7 (void *context, const void *aa, const void *bb) {
  char a[7];
  char b[7];
  UNUSED(context);
  memcpy(a,  (const void *const)aa, 7);
  memcpy(b,  (const void *const)bb, 7);
  if (ANNOUNCE_COMPARISONS) printf("cmp_char7(%c, %c)\n", a[0], b[0]); 
  return (a[0] < b[0]) ? -1 : ((a[0] > b[0]) ? 1 : 0);
}

/* This printer expects values of 7 bytes (each is a char). */
static void print_deplists_char7 (void **input, size_t nel) {
  size_t i;
  const char *deplist;  
  printf("Deplists: \n");
  for (i = 0; i < nel; i++) {
    printf("[%d] ", (int) i);
    deplist = input[i];
    while (*deplist) {
      printf(" %.*s ", 7, (const char *) deplist);
      deplist += 7;
    }
    printf("\n");
  }
}

static int cmp_char16 (void *context, const void *aa, const void *bb) {
  char a[16];
  char b[16];
  UNUSED(context);
  memcpy(a,  (const void *const)aa, 16);
  memcpy(b,  (const void *const)bb, 16);
  if (ANNOUNCE_COMPARISONS) printf("cmp_char16(%c, %c)\n", a[0], b[0]); 
  return (a[0] < b[0]) ? -1 : ((a[0] > b[0]) ? 1 : 0);
}

/* This printer expects values of 16 bytes (each is a char). */
static void print_deplists_char16 (void **input, size_t nel) {
  size_t i;
  const char *deplist;  
  printf("Deplists: \n");
  for (i = 0; i < nel; i++) {
    printf("[%d] ", (int) i);
    deplist = input[i];
    while (*deplist) {
      printf(" %.*s ", 16, (const char *) deplist);
      deplist += 16;
    }
    printf("\n");
  }
}

static void print_output (SCC *output) {
  Index i;
  printf("Output: size = %d, top = %d\n", output->size, output->top);
  for (i = 0; i < output->size; i++) printf("%d %d\n", output->elements[i].vnum, output->elements[i].cnum);
}

#define ALERT(i) do { \
    LOGf("check_output", "mismatch at output element %d", (i));	\
    retval = FALSE; \
  } while(0)

/* IMPORTANT: Handles only graph sizes of 3 and 4 */
static int check_output (SCC *output,
			 int v0, int g0,
			 int v1, int g1,
			 int v2, int g2,
			 int v3, int g3) {
  int retval = TRUE;
  Index i = 0;
  SCC_Component el;
  assert( (output->size==3) || (output->size==4) );
  el = output->elements[i];
  if (el.vnum != v0) ALERT(i);
  if (el.cnum != g0) ALERT(i);
  i++; el = output->elements[i];
  if (el.vnum != v1) ALERT(i);
  if (el.cnum != g1) ALERT(i);
  i++; el = output->elements[i];
  if (el.vnum != v2) ALERT(i);
  if (el.cnum != g2) ALERT(i);
  if (output->size == 3) return retval;
  i++; el = output->elements[i];
  if (el.vnum != v3) ALERT(i);
  if (el.cnum != g3) ALERT(i);
  return retval;
}

int main(void) {

  SCC *output;
  int stat;
  
  void *input[4];
  size_t nel = 4;

  const char *A = "A";
  const char *B = "B";
  const char *C = "C";
  const char *D = "D";

  /* For Part 1 */
  const char *Adeps[4];
  const char *Bdeps[4];
  const char *Cdeps[4];
  const char *Ddeps[4];

  /* For Part 2 */
  char Adeps_char[4];
  char Bdeps_char[4];
  char Cdeps_char[4];
  char Ddeps_char[4];

  /* For Part 3: 4 values, 7 chars each */
  char Adeps_char7[28];
  char Bdeps_char7[28];
  char Cdeps_char7[28];
  char Ddeps_char7[28];
  
  /* For Part 4: 4 values, 16 chars each */
  char Adeps_char16[64];
  char Bdeps_char16[64];
  char Cdeps_char16[64];
  char Ddeps_char16[64];
  
  printf("$$$ SCCtest\n");

  printf("*********** Part 1: A deplist is a null-terminated sequence of pointers to strings\n");

  stat = cmp_ptr(NULL, (const void *) &A, (const void *) &A);
  printf("Comparing A to A (should be 0):  %d\n", stat);
  assert(stat == 0);
  stat = cmp_ptr(NULL, (const void *) &A, (const void *) &C);
  printf("Comparing A to C (should be -1): %d\n", stat);
  assert(stat < 0);
  stat = cmp_ptr(NULL, (const void *) &C, (const void *) &A);
  printf("Comparing C to A (should be 1):  %d\n", stat);
  assert(stat > 0);
  
  /* EXAMPLE 1: No cycles */
  Adeps[0] = A;
  Adeps[1] = B;
  Adeps[2] = C;
  Adeps[3] = NULL;
  Bdeps[0] = B;
  Bdeps[1] = C;
  Bdeps[2] = "X";		/* INTRODUCE GRAPH ERROR */
  Bdeps[3] = NULL;
  Cdeps[0] = C;
  Cdeps[1] = NULL;
  Ddeps[0] = D;
  Ddeps[1] = NULL;

  input[0] = (void *)Adeps;	/* c89 does not allow aggregates */
  input[1] = (void *)Cdeps;
  input[2] = (void *)Bdeps;
  input[3] = (void *)Ddeps;

  printf("\nChecking for SCC_ERR_GRAPH\n\n");
  print_deplists_ptr(input, nel);
  
  stat = SCC_run(input, nel, sizeof(void *), &nullptr, cmp_ptr, NULL, &output);
  printf("After SCC_run(), stat = %d\n", stat);
  assert( stat == SCC_ERR_GRAPH ); /* bad graph */

  Bdeps[2] = D;		/* Fix GRAPH ERROR */
  printf("\nEXAMPLE 1\n\n");
  print_deplists_ptr(input, nel);
  
  stat = SCC_run(input, nel, sizeof(void *), &nullptr, cmp_ptr, NULL, &output);
  printf("After SCC_run(), stat = %d\n", stat);
  print_deplists_ptr(input, nel);
  assert( stat == 0 );		/* all should be sorted */

  print_output(output);
  assert(check_output(output, 1, -2, 3, -4, 2, -3, 0, -1));
  SCC_free(output);

  /* EXAMPLE 2: With cycles */
  Adeps[0] = A;
  Adeps[1] = B;
  Adeps[2] = C;
  Adeps[3] = NULL;
  Bdeps[0] = B;
  Bdeps[1] = A;
  Bdeps[2] = NULL;
  Cdeps[0] = C;
  Cdeps[1] = NULL;
  Ddeps[0] = D;
  Ddeps[1] = D;
  Ddeps[2] = NULL;
  input[0] = (void *)Adeps;
  input[1] = (void *)Cdeps;
  input[2] = (void *)Ddeps;
  input[3] = (void *)Bdeps;
  nel = 4;

  printf("\nEXAMPLE 2\n\n");
  print_deplists_ptr(input, nel);
  
  stat = SCC_run(input, nel, sizeof(void *), &nullptr, cmp_ptr, NULL, &output);
  printf("After SCC_run(), stat = %d\n", stat);
  print_deplists_ptr(input, nel);
  assert(stat == 0);

  print_output(output);
  assert(check_output(output, 1, -2, 3, 1, 0, 1, 2, 4));
  SCC_free(output);

  printf("\nResetting EXAMPLE 2\n\n");
  Adeps[0] = A;
  Adeps[1] = B;
  Adeps[2] = C;
  Adeps[3] = NULL;
  Bdeps[0] = B;
  Bdeps[1] = A;
  Bdeps[2] = NULL;
  Cdeps[0] = C;
  Cdeps[1] = NULL;
  Ddeps[0] = D;
  Ddeps[1] = D;
  Ddeps[2] = NULL;

  input[0] = (void *)Adeps;
  input[1] = (void *)Cdeps;
  input[2] = (void *)Ddeps;
  input[3] = (void *)Bdeps;
  nel = 4;

  print_deplists_ptr(input, nel);
  
  stat = SCC_run(input, nel, sizeof(void *), &nullptr, cmp_ptr, NULL, &output);
  printf("After SCC_run(), stat = %d\n", stat);
  print_deplists_ptr(input, nel);
  assert(stat == 0);

  print_output(output);
  assert(check_output(output, 1, -2, 3, 1, 0, 1, 2, 4));
  SCC_free(output);

  printf("\nEXAMPLE 3   A->B->C->D->A \n\n");
  Adeps[0] = A;
  Adeps[1] = B;
  Adeps[2] = NULL;
  Cdeps[0] = C;
  Cdeps[1] = D;
  Cdeps[2] = NULL;
  Ddeps[0] = D;
  Ddeps[1] = A;
  Ddeps[2] = NULL;
  Bdeps[0] = B;
  Bdeps[1] = C;
  Bdeps[2] = NULL;
  input[0] = (void *)Adeps;
  input[1] = (void *)Cdeps;
  input[2] = (void *)Ddeps;
  input[3] = (void *)Bdeps;
  nel = 4;

  print_deplists_ptr(input, nel);
  
  stat = SCC_run(input, nel, sizeof(void *), &nullptr, cmp_ptr, NULL, &output);
  printf("After SCC_run(), stat = %d\n", stat);
  print_deplists_ptr(input, nel);
  assert(stat == 0);

  print_output(output);
  assert(check_output(output, 2, 1, 1, 1, 3, 1, 0, 1));
  SCC_free(output);
  
  printf("\nEXAMPLE 4   A->D; D->B, A; B->nothing \n\n");
  Adeps[0] = A;
  Adeps[1] = D;
  Adeps[2] = NULL;
  Bdeps[0] = B;
  Bdeps[1] = NULL;
  Ddeps[0] = D;
  Ddeps[1] = B;
  Ddeps[2] = A;
  Ddeps[3] = NULL;
  input[0] = (void *)Adeps;
  input[1] = (void *)Bdeps;
  input[2] = (void *)Ddeps;
  nel = 3;

  print_deplists_ptr(input, nel);
  
  stat = SCC_run(input, nel, sizeof(void *), &nullptr, cmp_ptr, NULL, &output);
  printf("After SCC_run(), stat = %d\n", stat);
  print_deplists_ptr(input, nel);
  assert(stat == 0);

  print_output(output);
  assert(check_output(output, 1, -3, 2, 1, 0, 1, 999, 999));
  SCC_free(output);

  printf("\n*********** Part 2: A deplist is a null-terminated sequence of chars\n");

  stat = cmp_char(NULL, (const void *) "A", (const void *) "A");
  printf("Comparing A to A (should be 0):  %d\n", stat);
  assert(stat == 0);
  stat = cmp_char(NULL, (const void *) "A", (const void *) "C");
  printf("Comparing A to C (should be -1): %d\n", stat);
  assert(stat < 0);
  stat = cmp_char(NULL, (const void *) "C", (const void *) "A");
  printf("Comparing C to A (should be 1):  %d\n", stat);
  assert(stat > 0);
  
  /* EXAMPLE 1: No cycles */
  Adeps_char[0] = *A;
  Adeps_char[1] = *B;
  Adeps_char[2] = *C;
  Adeps_char[3] = nullchar;
  Bdeps_char[0] = *B;
  Bdeps_char[1] = *C;
  Bdeps_char[2] = *D;
  Bdeps_char[3] = nullchar;
  Cdeps_char[0] = *C;
  Cdeps_char[1] = nullchar;
  Ddeps_char[0] = *D;
  Ddeps_char[1] = nullchar;

  input[0] = (void *)&Adeps_char;	/* c89 does not allow aggregates */
  input[1] = (void *)&Cdeps_char;
  input[2] = (void *)&Bdeps_char;
  input[3] = (void *)&Ddeps_char;

  nel = 4;

  printf("\nEXAMPLE 1\n\n");
  print_deplists_char(input, nel);

  stat = SCC_run(input, nel, sizeof(char), &nullchar, cmp_char, NULL, &output);
  printf("After SCC_run(), stat = %d\n", stat);
  print_deplists_char(input, nel);
  assert( stat == 0 );		/* all should be sorted */

  print_output(output);
  assert(check_output(output, 1, -2, 3, -4, 2, -3, 0, -1));
  SCC_free(output);

  /* EXAMPLE 2: With cycles */
  Adeps_char[0] = *A;
  Adeps_char[1] = *B;
  Adeps_char[2] = *C;
  Adeps_char[3] = nullchar;
  Bdeps_char[0] = *B;
  Bdeps_char[1] = *A;
  Bdeps_char[2] = nullchar;
  Cdeps_char[0] = *C;
  Cdeps_char[1] = nullchar;
  Ddeps_char[0] = *D;
  Ddeps_char[1] = *D;
  Ddeps_char[2] = nullchar;
  input[0] = (void *)&Adeps_char;
  input[1] = (void *)&Cdeps_char;
  input[2] = (void *)&Ddeps_char;
  input[3] = (void *)&Bdeps_char;
  nel = 4;

  printf("\nEXAMPLE 2\n\n");
  print_deplists_char(input, nel);
  
  stat = SCC_run(input, nel, sizeof(char), &nullchar, cmp_char, NULL, &output);
  printf("After SCC_run(), stat = %d\n", stat);
  print_deplists_char(input, nel);
  assert(stat == 0);

  print_output(output);
  assert(check_output(output, 1, -2, 3, 1, 0, 1, 2, 4));
  SCC_free(output);

  printf("\nEXAMPLE 3   A->B->C->D->A \n\n");
  Adeps_char[0] = *A;
  Adeps_char[1] = *B;
  Adeps_char[2] = nullchar;
  Cdeps_char[0] = *C;
  Cdeps_char[1] = *D;
  Cdeps_char[2] = nullchar;
  Ddeps_char[0] = *D;
  Ddeps_char[1] = *A;
  Ddeps_char[2] = nullchar;
  Bdeps_char[0] = *B;
  Bdeps_char[1] = *C;
  Bdeps_char[2] = nullchar;
  input[0] = (void *)Adeps_char;
  input[1] = (void *)Cdeps_char;
  input[2] = (void *)Ddeps_char;
  input[3] = (void *)Bdeps_char;
  nel = 4;

  print_deplists_char(input, nel);
  
  stat = SCC_run(input, nel, sizeof(char), &nullchar, cmp_char, NULL, &output);
  printf("After SCC_run(), stat = %d\n", stat);
  print_deplists_char(input, nel);
  assert(stat == 0);

  print_output(output);
  assert(check_output(output, 2, 1, 1, 1, 3, 1, 0, 1)); 
  SCC_free(output);

  printf("\nEXAMPLE 4   A->D; D->B, A; B->nothing \n\n");
  Adeps_char[0] = *A;
  Adeps_char[1] = *D;
  Adeps_char[2] = nullchar;
  Bdeps_char[0] = *B;
  Bdeps_char[1] = nullchar;
  Ddeps_char[0] = *D;
  Ddeps_char[1] = *B;
  Ddeps_char[2] = *A;
  Ddeps_char[3] = nullchar;
  input[0] = (void *)Adeps_char;
  input[1] = (void *)Bdeps_char;
  input[2] = (void *)Ddeps_char;
  nel = 3;

  print_deplists_char(input, nel);
  
  stat = SCC_run(input, nel, sizeof(char), &nullchar, cmp_char, NULL, &output);
  printf("After SCC_run(), stat = %d\n", stat);
  print_deplists_char(input, nel);
  assert(stat == 0);

  print_output(output);
  assert(check_output(output, 1, -3, 2, 1, 0, 1, 999, 999));
  SCC_free(output);

  printf("\nEXAMPLE 5   A->C, B->C, C->B, D->nothing \n\n");
  Adeps_char[0] = *A;
  Adeps_char[1] = *C;
  Adeps_char[2] = nullchar;
  Bdeps_char[0] = *B;
  Bdeps_char[1] = *C;
  Bdeps_char[2] = nullchar;
  Cdeps_char[0] = *C;
  Cdeps_char[1] = *B;
  Cdeps_char[2] = nullchar;
  Ddeps_char[0] = *D;
  Ddeps_char[1] = nullchar;

  input[0] = (void *)Adeps_char;
  input[1] = (void *)Bdeps_char;
  input[2] = (void *)Ddeps_char;
  input[3] = (void *)Cdeps_char;
  nel = 4;

  print_deplists_char(input, nel);
  
  stat = SCC_run(input, nel, sizeof(char), &nullchar, cmp_char, NULL, &output);
  printf("After SCC_run(), stat = %d\n", stat);
  print_deplists_char(input, nel);
  assert(stat == 0);

  print_output(output);
  assert(check_output(output, 1, 2, 3, 2, 0, -1, 2, -4));
  SCC_free(output);

  printf("\n*********** Part 3: A deplist is a null-terminated sequence of 7-char strings\n");
  printf("Need to manually check that swapchar is called with 7 chars\n");

  stat = cmp_char7(NULL, (const void *) "A>>>>>>", (const void *) "A<<<<<<");
  printf("Comparing A to A (should be 0):  %d\n", stat);
  assert(stat == 0);
  stat = cmp_char7(NULL, (const void *) "A++++++", (const void *) "C------");
  printf("Comparing A to C (should be -1): %d\n", stat);
  assert(stat < 0);
  stat = cmp_char7(NULL, (const void *) "C------", (const void *) "A++++++");
  printf("Comparing C to A (should be 1):  %d\n", stat);
  assert(stat > 0);

  /* A depends on B, A */
  memcpy(Adeps_char7, "A234567B234567A234567\0\0\0\0\0\0\0", 28);
  /* B depends on nothing */
  memcpy(Bdeps_char7, "B------\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0", 28);
  /* C depends on B */
  memcpy(Cdeps_char7, "C++++++B......\0\0\0\0\0\0\0\0\0\0\0\0\0\0", 28); 
  /* D depends on A and C */
  memcpy(Ddeps_char7, "DxxxxxxAyyyyyyCzzzzzz\0\0\0\0\0\0\0", 28); 

  input[0] = (void *)Adeps_char7;
  input[1] = (void *)Bdeps_char7;
  input[2] = (void *)Cdeps_char7;
  input[3] = (void *)Ddeps_char7;
  nel = 4;

  memcpy(nullchar7, "\0\0\0\0\0\0\0", 7);

  print_deplists_char7(input, nel);

  stat = SCC_run(input, nel, 7 * sizeof(char), &nullchar7, cmp_char7, NULL, &output);
  printf("After SCC_run(), stat = %d\n", stat);
  print_deplists_char7(input, nel);
  assert(stat == 0);

  print_output(output);
  assert(check_output(output, 1, -2, 0, 1, 2, -3, 3, -4));
  SCC_free(output);

  printf("\n*********** Part 4: A deplist is a null-terminated sequence of 16-byte strings\n");
  printf("Need to manually check that swaplong is called with 2 longs\n");

  stat = cmp_char16(NULL, (const void *) "A>>>>>>><<<<<<<<", (const void *) "A<<<<<<<>>>>>>>>");
  printf("Comparing A to A (should be 0):  %d\n", stat);
  assert(stat == 0);
  stat = cmp_char16(NULL, (const void *) "A+++++++++++++++", (const void *) "C---------------");
  printf("Comparing A to C (should be -1): %d\n", stat);
  assert(stat < 0);
  printf("Comparing C to A (should be 1):  %d\n", stat);
  stat = cmp_char16(NULL, (const void *) "C---------------", (const void *) "A+++++++++++++++");
  assert(stat > 0);

  /* A depends on B */
  memcpy(Adeps_char16, "A234567812345678B234567812345678\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0", 64);
  /* B depends on nothing */
  memcpy(Bdeps_char16, "B---------------\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0", 64);
  /* C depends on B */
  memcpy(Cdeps_char16, "C+++++++++++++++B...............\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0", 64); 
  /* D depends on A and C */
  memcpy(Ddeps_char16, "DxxxxxxxxxxxxxxxAyyyyyyyyyyyyyyyCzzzzzzzzzzzzzzz\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0", 64); 

  input[0] = (void *)Adeps_char16;
  input[1] = (void *)Bdeps_char16;
  input[2] = (void *)Cdeps_char16;
  input[3] = (void *)Ddeps_char16;
  nel = 4;

  memcpy(nullchar16, "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0", 16);

  print_deplists_char16(input, nel);

  stat = SCC_run(input, nel, 16 * sizeof(char), &nullchar16, cmp_char16, NULL, &output);
  printf("After SCC_run(), stat = %d\n", stat);
  print_deplists_char16(input, nel);
  assert(stat == 0);

  print_output(output);
  assert(check_output(output, 1, -2, 0, -1, 2, -3, 3, -4));
  SCC_free(output);

  printf("Done.\n");
  return 0;
}

#pragma GCC diagnostic pop
