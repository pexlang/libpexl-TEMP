/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  symboltabletest.c  TESTS of a simple string table                        */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "symboltable.h"
#include "print.h"

#define MAXLEN 100

#define failif(test, msg)						\
  do { if (test) { fprintf(stderr, "Failed %s:%d: %s\n", __FILE__,	\
			   __LINE__, msg);				\
      fflush(stderr);							\
      exit(-1);								\
    }									\
  } while (0)


static void print_block (SymbolTable *st) {
  size_t i;
  for (i = 0; i < st->blocknext; i++) {
    printf("[%5ld] %s\n", i, &(st->block[i]));
    i += + strlen(&(st->block[i]));
  }
}

int main(void) {

  char *s;
  int stat;
  Index i, index, saveindex;
  SymbolTableEntry  *entry;
  long offset, rando, pos;
  const char *tmp;
  FILE *words;
  char buf[SYMBOLTABLE_MAXLEN + 2];
  int nwords;
  SymbolTable *st1;
  
  printf("$$$ symboltabletest\n");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Testing error checks\n");

  st1 = symboltable_new(0, 100);
  failif(!st1, "initial size <= 0, so will be set to default");
  failif(st1->size != SYMBOLTABLE_INITSIZE, "initial size <= 0, so will be set to default");
  symboltable_free(st1);

  st1 = symboltable_new(100, 0);
  failif(!st1, "initial block size <= 0, so will be set to default");
  failif(st1->blocksize != SYMBOLTABLE_INITBLOCKSIZE, "initial block size <= 0, so will be set to default");
  symboltable_free(st1);

  st1 = symboltable_new(SYMBOLTABLE_MAXSIZE + 1, 100);
  failif(!st1, "initial size > SYMBOLTABLE_MAXSIZE, so set to MAX");
  failif(st1->size != SYMBOLTABLE_MAXSIZE, "initial size > max, so will be set to max");
  symboltable_free(st1);

  st1 = symboltable_new(100, SYMBOLTABLE_MAXBLOCKSIZE + 1);
  failif(!st1, "initial block size > SYMBOLTABLE_MAXBLOCKSIZE, so set to MAX");
  failif(st1->blocksize != SYMBOLTABLE_MAXBLOCKSIZE, "initial block size > max, so will be set to max");
  symboltable_free(st1);

  st1 = symboltable_new(1, 1);
  failif(!st1, "size 1 is ok");
  failif(symboltable_len(st1) != 0, "len of empty table wrong");
  failif(symboltable_blocklen(st1) != 1, "len of empty block wrong");
  failif(st1->size != 1, "initial size wrong");
  failif(st1->blocksize != 1, "initial size wrong");

  stat = symboltable_add(NULL, NULL, 0, 0, 0, 0, NULL);
  failif(stat != SYMBOLTABLE_ERR_NULL, "st arg cannot be null");
  entry = symboltable_get(NULL, 0);
  failif(entry, "st arg cannot be null");
  tmp = symboltable_get_name(NULL, 0);
  failif(tmp, "st arg cannot be null");
  entry = symboltable_iter(NULL, &index);
  failif(entry, "st arg cannot be null");
  tmp = symboltable_block_iter(NULL, &index);
  failif(tmp, "st arg cannot be null");

  stat = symboltable_add(st1, NULL, 0, 0, 0, 0, NULL);
  failif(stat != 0, "able to not supply a string arg (same as empty string)");
  failif(symboltable_len(st1) != 1, "one symbol in table");

  memset(buf, 'A', SYMBOLTABLE_MAXLEN + 1);
  buf[SYMBOLTABLE_MAXLEN + 1] = '\0';
  failif(strlen(buf) != SYMBOLTABLE_MAXLEN + 1, "just checking the arg length");
  stat = symboltable_add(st1, buf, 0, 0, 0, 0, NULL);
  failif(stat != SYMBOLTABLE_ERR_STRINGLEN, "string must be less than max length");
  failif(symboltable_len(st1) != 1, "failed attempts to add strings should not affect table");
  failif(st1->blocksize != 1, "failed attempts to add strings should not affect table");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Storing a few strings \n");

  symboltable_free(st1);

  st1 = symboltable_new(1, 15);
  failif(!st1, "sizes 1, 15 ok");
  failif(symboltable_len(st1) != 0, "empty table has 0 entries");

  failif(symboltable_blocklen(st1) != 1, "empty string always in block");
  failif(st1->size != 1, "initial size wrong");
  failif(st1->blocksize != 15, "initial block size wrong");

  index = symboltable_add(st1, "foo", 1, 0, 0, -54321, NULL);
  printf("index = %d\n", index);
  failif(index != 0, "first entry in new table should be at index 0");

  entry = symboltable_get(st1, index);
  failif(entry == NULL, "get index 0 should succeed");
  failif(entry_name_offset(entry) != 1, "foo is first string, should start at block index 1");
  failif(entry_visibility(entry) != 1, "vis was set when this entry was added");
  failif(entry_type(entry) != 0, "stype not set when this entry was added");
  failif(entry->value != -54321, "value should be initialized correctly");

  index = symboltable_add(st1, "network", 0, 1, 0, 123, NULL);
  failif(index != 1, "2nd string should be at index 1");

  entry = symboltable_get(st1, index);
  failif(entry == NULL, "get index 0 should succeed");
  failif(entry_name_offset(entry) != 5, "network is after foo in block");
  failif(entry_visibility(entry) != 0, "vis not set when this entry was added");
  failif(entry_type(entry) != 1, "stype set when this entry was added"); 
  failif(entry->value != 123, "value should be initialized");

  index = symboltable_add(st1, "indefatigable", 0, 0, 1010, -1, NULL);
  failif(index != 2, "3rd string should be at index 2");
  entry = symboltable_get(st1, index);
  failif(entry == NULL, "get index 0 should succeed");
  failif(entry_name_offset(entry) != 13, "indefatigable is after network in block");
  failif(entry_visibility(entry) != 0, "vis not set when this entry was added");
  failif(entry_type(entry) != 0, "stype not set when this entry was added");
  failif(entry->value != -1, "value should be initialized");
  failif(entry->size != 1010, "size should be initialized");
  saveindex = index;

  index = symboltable_add(st1, NULL, 1, 1, 0, 0, NULL);
  failif(index != 3, "now at index 3");

  index = symboltable_add(st1, "", 1, 1, 0, 0, NULL);
  failif(index != 4, "now at index 4");

  printf("Block storage:\n");
  print_block(st1);

  print_symboltable_stats(st1);
  print_symboltable(st1);

  printf("Testing iterator for block storage:\n");
  tmp = symboltable_block_iter(st1, NULL);
  failif(tmp, "index arg cannot be null");
  i = 0;
  index = SYMBOLTABLE_ITER_START;
  while ((tmp = symboltable_block_iter(st1, &index))) { printf(">> %s\n", tmp); i++; }
  failif(i != 3, "incorrect number of strings (empty strings count)");

  printf("Testing iterator for symbol table entries:\n");
  entry = symboltable_iter(st1, NULL);
  failif(entry, "index arg cannot be null");
  i = 0;
  index = SYMBOLTABLE_ITER_START;
  while ((entry = symboltable_iter(st1, &index))) {
    printf(">> [%5d] %s\n", entry_name_offset(entry), symboltable_get_name(st1, index));
    i++;
  }
  failif(i != 5, "incorrect number of entries");


  entry = symboltable_get(st1, saveindex);
  failif(!entry, "expected an entry");
  failif(entry_name_offset(entry) != 13, "indefatigable is after network in block");
  failif(entry_visibility(entry) != 0, "vis not set when this entry was added");
  failif(entry_type(entry) != 0, "stype not set when this entry was added");
  failif(entry->value != -1, "value should be what was set earlier");
  failif(entry->size != 1010, "size should be what was set earlier");
  

  printf("Testing max string length\n");
  /* Now try storing a string that is exactly the longest length allowed */
  buf[SYMBOLTABLE_MAXLEN] = '\0';
  index = symboltable_add(st1, buf, 0, 0, 0, 0, NULL);
  printf("index = %d\n", index);
  failif(index <= 0, "should work");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Block Iterator \n");

  i = 0;
  index = SYMBOLTABLE_ITER_START;
  /* This is not a great test.  Does not ensure we get every index once. */
  while ((tmp = symboltable_block_iter(st1, &index))) {
    switch (index) {
    case 1:
      printf("found entry %d\n", index);
      failif(strcmp(tmp, "foo") != 0, "entry 1 wrong");
      break;
    case 5:
      printf("found entry %d\n", index);
      failif(strcmp(tmp, "network") != 0, "entry 2 wrong");
      break;
    case 13:
      printf("found entry %d\n", index);
      failif(strcmp(tmp, "indefatigable") != 0, "entry 3 wrong");
      break;
    case 27:
      printf("found entry %d\n", index);
      failif(strncmp(tmp, "AAAAA", 5) != 0, "entry 4 wrong");
      failif(strlen(tmp) != SYMBOLTABLE_MAXLEN, "last string should have max length");
      break;
    default:
      printf("index = %d\n", index);
      failif(1, "iterator stopped at invalid position");
    }
    i++;
  }
  failif(i != 4, "wrong number of strings");

  symboltable_free(st1);
  st1 = symboltable_new(1, 1);

  /* ----------------------------------------------------------------------------- */
#define NWORDS 7500
  printf("********************** Storing %d more strings\n", NWORDS);

  printf("Loading from /usr/dict/words\n");
  words = fopen("/usr/share/dict/words", "r");
  stat = fseek(words, 0, SEEK_END);
  offset = ftell(words);
  printf("stat is %d, last is %ld\n", stat, offset);

  rando = rand();		/* 32 bits */
  pos = ((rando >> 22) * offset) / (1<<10);
  if ((offset - pos) < (NWORDS * 10)) pos = offset - (NWORDS * 10);
  stat = fseek(words, pos, SEEK_SET);
  printf("%ld/%ld, random seek chose a place %ld%% into file\n", pos, offset, pos*100/offset);
  s = fgets(buf, MAXLEN, words); /* read past a partial word */
  for (nwords=0; nwords < NWORDS; nwords++) {
    s = fgets(buf, MAXLEN, words);
    if (!s) break;
    while((*s!='\0') && (*s!='\n')) s++;
    *s = '\0';			/* overwrite newline */
    index = symboltable_add(st1, buf, 0, 0, 0, 0, NULL);
    if (index <= 0) {
      printf("Error from symboltable_add(, 0, 0, NULL).  Stats are:\n");
      print_symboltable_stats(st1);
    }
  } /* for */
  printf("%d strings processed from /usr/dict/words\n", nwords);
  print_symboltable_stats(st1);

  i = 0;
  index = SYMBOLTABLE_ITER_START;
  while ((entry = symboltable_iter(st1, &index))) i++;
  failif(i != NWORDS, "wrong number of symbols in table");

  symboltable_free(st1);

  /* ----------------------------------------------------------------------------- */
  printf("********************** Filling the table to its block max of %d bytes\n", SYMBOLTABLE_MAXBLOCKSIZE);

  st1 = symboltable_new(1, 1);
  memset(buf, 'A', SYMBOLTABLE_MAXLEN);
  buf[SYMBOLTABLE_MAXLEN] = '\0';
  failif(strlen(buf) < SYMBOLTABLE_MAXLEN, "precondition");

  i = 0;
  while ((index = symboltable_add(st1, buf, 0, 0, 0, 0, NULL)) >= 0) {
    /* Modify the string so it won't match one already in the block */
    buf[i % SYMBOLTABLE_MAXLEN]++;
    i++;
  }
  printf("%d strings of length %ld added to new string table\n", i, strlen(buf));
  failif(index != SYMBOLTABLE_ERR_BLOCKFULL, "should get block full error");

  failif(st1->blocksize > SYMBOLTABLE_MAXBLOCKSIZE, "still under the max");
  /* Make a string that will just fit in the remaining space available */
  buf[st1->blocksize - st1->blocknext - 1] = '\0';
  /* Modify the string so it won't match one already in the block */
  buf[0]++;
  index = symboltable_add(st1, buf, 0, 0, 0, 0, NULL);
  printf("After adding a string of size %ld to fill the table, index = %d\n",
	 strlen(buf), index);
  printf("And the value of blocknext = %ld (MAX is %d)\n", st1->blocknext, SYMBOLTABLE_MAXBLOCKSIZE);
  failif(st1->blocknext != SYMBOLTABLE_MAXBLOCKSIZE, "table size should be at maximum");
  failif(st1->block[st1->blocknext - 1] != '\0', "the terminal NUL byte is not there?!?");

  printf("********************** Filling the table to its max of %d entries\n", SYMBOLTABLE_MAXSIZE);

  failif(st1->size >= SYMBOLTABLE_MAXSIZE, "test pre-requisite not met");
  
  i = st1->next;			/* current number of entries */
  while ((index = symboltable_add(st1, NULL, 0, 0, 0, 0, NULL)) >= 0) {
    i++;
  }
  printf("%d entries in symbol table\n", i);
  failif(index != SYMBOLTABLE_ERR_FULL, "should get full error");

  failif(st1->next != st1->size, "should be the max now");
  failif(st1->size != SYMBOLTABLE_MAXSIZE, "should be the max now");

  print_symboltable_stats(st1);
  symboltable_free(st1);

  /* ----------------------------------------------------------------------------- */
  printf("\nDone.\n");
  
  return 0;
}

