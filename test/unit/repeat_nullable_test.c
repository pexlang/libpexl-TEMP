/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  repeat_nullable_test.c                                                   */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>		/* exit() */
#include "print.h"
#include "compile.h"
#include "vm.h"
#include "common.h"

#define YES 1
#define NO 0

#define TRUE 1
#define FALSE 0

#define ANNOUNCE(...) do {				\
  printf("In %s at line %d:\n", __FILE__, __LINE__);	\
  printf(__VA_ARGS__);					\
  } while (0)

#define failif(test, msg)						\
  do { if (test) { fprintf(stderr, "Failed %s:%d: %s\n", __FILE__,	\
			   __LINE__, msg);				\
      fflush(stderr);							\
      exit(-1);								\
    }									\
  } while (0)

/* Return 0 if match components agree with expected values */
static int check_match(Match* m, int matched, size_t leftover, int abend, 
		       int match_data_expected, const char* input) {
    int result = 0;
    if (m->matched != matched) {
        fprintf(stderr, "Error: expected match=%s, found match=%s, for input: %s\n",
		(matched ? "TRUE" : "FALSE"), (m->matched ? "TRUE" : "FALSE"), input);
        fflush(stderr);
        result = 1;	
    }
    if (m->leftover != leftover) {
        fprintf(stderr, "Error: expected leftover=%lu, found leftover=%lu, for input: %s\n",
		leftover, m->leftover, input);
        fflush(stderr);	
        result = 2;	
    }
    if (m->abend != abend) {
        fprintf(stderr, "Error: expected abend=%s, found abend=%s, for input: %s\n",
		(abend ? "TRUE" : "FALSE"), (m->abend ? "TRUE" : "FALSE"), input);
        fflush(stderr);	
        result = 3;	
    }
    if ((match_data_expected >= 0) && (m->data->next != ((size_t) match_data_expected))) {
        fprintf(stderr, "Error: expected %d bytes of match data, found %lu, for input: %s\n",
		match_data_expected, m->data->next, input);
        fflush(stderr);	
        result = 4;	
    }
    return result;
}

static void free_stuff (Package *pkg, Buffer *buf, Match *match){
    if (buf) buf_free(buf);
    if (pkg) package_free(pkg);
    if (match && match->data) buf_free(match->data);
    if (match) match_free(match);
}

static void print_match_data (char *s, size_t n) {
  uint8_t c;
  size_t i;
  for (i = 0; i < n; i++) {
    c = *s++;
    if ((c < 33) || (c > 126)) printf("0x%02d ", c);
    else printf("%c ", c);
  }
  printf("\n");
}

static int test (Context *C, Expression *exp, const char *input, 
		 int match_expected, size_t leftover_expected, int match_data_expected) {
  Package *pkg;
  Error err;
  Buffer *buf;
  Env *env = C->env;
  PackageTable *pt = C->packages;
  int stat;
  Match *match = match_new();
  if (!match) {
    fprintf(stderr, "expected new match structure\n");
    return 1;
  }

  pkg = pexlc_compile_expression(C, exp, env, &err, DEFAULT_OPTIM);
  if (!pkg) {
    fprintf(stderr, "compile expression should succeed\n");
    return 1;
  }
  if (err.value != OK) {
    fprintf(stderr, "error value from compile expression should indicate ok\n");
    return err.value;
  }

/*   print_package(pkg); */

  stat = packagetable_set_user(pt, pkg);
  if (stat < 0) {
    fprintf(stderr, "expected successful set of user package\n");
    return stat;
  }

  buf = buf_new_wrap(input, strlen(input));
  if (!buf) {
    fprintf(stderr, "expected string buffer\n");
    return 1;
  }

  printf("INPUT: \"%s\"\n", input);

  /* First run with debug encoder, so we can what captures were generated  */
  stat = vm_start(pt, pkg->ep, buf, 0, strlen(input), debug_encoder, NULL, match);
  if (stat != 0) {
    fprintf(stderr, "with debug encoder: expected successful execution of vm, but received stat = %d\n", stat);
    return stat;
  }

  /* Run again with byte encoder, so we can debug it */
  stat = vm_start(pt, pkg->ep, buf, 0, strlen(input), byte_encoder, NULL, match);
  if (stat != 0) {
    fprintf(stderr, "with byte encoder: expected successful execution of vm, but received stat = %d\n", stat);
    return stat;
  }

  print_match(match);
  print_match_data(match->data->data, match->data->next);
  printf("\n");
  stat = check_match(match, match_expected, leftover_expected, FALSE, match_data_expected, input);
  if (stat) {
    fprintf(stderr, "Wrong match result: stat = %d\n", stat);
    return stat;
  }

  free_stuff(pkg, buf, match);
  return OK;
}


int main(void) {

  int n;
  Context *C;
  Env *toplevel, *env; 

  Expression *exp;
  Ref ref;
  Error err;

  printf("$$$ repeat_nullable\n");

  toplevel = pexlc_make_env(NULL);
  failif(!toplevel, "new toplevel should succeed");
  
  C = pexlc_make_context(toplevel);
  failif(!C, "expected new context");

  /* First let's test repeat of a non-nullable exp */

  ANNOUNCE("CHECKING repetitions of 0 or more occurrences, i.e. star\n\n");

  printf("--- Case (0): repeated exp converts to charset; ISpan issued\n\n");
  printf("--- Test case \"X\"*\n");
  exp = pexle_repeat_f(C, pexle_from_string(C, "X"), 0);
  failif(!exp, "expected expression");

  failif(test(C, exp, "", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "X", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "XXabc", TRUE, 3, -1), "test failed");
  failif(test(C, exp, "XXXXXXXX", TRUE, 0, -1), "test failed");

  printf("--- Test case CAPTURE(\"X\"*)\n");
  pexle_free(exp);
  exp = pexle_capture_f(C, "exes", pexle_repeat_f(C, pexle_from_string(C, "X"), 0));
  failif(!exp, "expected expression");

  failif(test(C, exp, "", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "X", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "XXabc", TRUE, 3, -1), "test failed");
  failif(test(C, exp, "XXXXXXXX", TRUE, 0, -1), "test failed");

  /* Now try nullable expressions */

  printf("--- Case (1): special case does not apply because exp is neither headfail nor non-nullable\n\n");
  printf("--- Case (2): general case, no optimizations\n\n");
  printf("--- Test case (\"X\"/epsilon)*\n");
  pexle_free(exp);
  exp = pexle_repeat_f(C, pexle_choice_f(C, pexle_from_string(C, "X"), pexle_from_boolean(C, TRUE)), 0);
  failif(!exp, "expected expression");

  failif(test(C, exp, "", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "X", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "XXabc", TRUE, 3, -1), "test failed");
  failif(test(C, exp, "XXXXXXXX", TRUE, 0, -1), "test failed");

  printf("--- Test case CAPTURE((\"X\"/epsilon)*)\n");
  pexle_free(exp);
  exp = pexle_capture_f(C, "exes", pexle_repeat_f(C, pexle_choice_f(C, pexle_from_string(C, "X"), pexle_from_boolean(C, TRUE)), 0));
  failif(!exp, "expected expression");

  failif(test(C, exp, "", TRUE, 0, 13), "test failed");
  failif(test(C, exp, "X", TRUE, 0, 13), "test failed");
  failif(test(C, exp, "XXabc", TRUE, 3, 13), "test failed");
  failif(test(C, exp, "XXXXXXXX", TRUE, 0, 13), "test failed");

  printf("--- Case (3): general case, optimization due to choice on stack\n\n");
  printf("--- Test case (\"X\"/epsilon)* / epsilon \n");
  pexle_free(exp);
  exp = pexle_choice_f(C,
		     pexle_repeat_f(C,
				  pexle_choice_f(C,
					       pexle_from_string(C, "X"),
					       pexle_from_boolean(C, TRUE)),
				  0),
		     pexle_from_boolean(C, TRUE));
  failif(!exp, "expected expression");

  failif(test(C, exp, "", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "X", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "XXabc", TRUE, 3, -1), "test failed");
  failif(test(C, exp, "XXXXXXXX", TRUE, 0, -1), "test failed");

  printf("--- Test case CAPTURE((\"X\"/epsilon)* / epsilon)\n");
  pexle_free(exp);
  exp = pexle_capture_f(C, "exes",
		      pexle_choice_f(C,
				   pexle_repeat_f(C,
						pexle_choice_f(C,
							     pexle_from_string(C, "X"),
							     pexle_from_boolean(C, TRUE)),
						0),
				   pexle_from_boolean(C, TRUE)));
  failif(!exp, "expected expression");

  failif(test(C, exp, "", TRUE, 0, 13), "test failed");
  failif(test(C, exp, "X", TRUE, 0, 13), "test failed");
  failif(test(C, exp, "XXabc", TRUE, 3, 13), "test failed");
  failif(test(C, exp, "XXXXXXXX", TRUE, 0, 13), "test failed");

  /*************************************************************************/
  /* Now with repetitions of "up to n occurrences" of nullable expressions */
  /*************************************************************************/

  ANNOUNCE("CHECKING repetitions of at most n occurrences\n\n");

  printf("--- Case (0): repeated exp converts to charset; ITestChar issued\n\n");
  printf("--- Test case \"X\"{0,2}\n");
  exp = pexle_repeat_f(C, pexle_from_string(C, "X"), -2);
  failif(!exp, "expected expression");

  failif(test(C, exp, "", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "X", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "XXabc", TRUE, 3, -1), "test failed");
  failif(test(C, exp, "XXXX", TRUE, 2, -1), "test failed");

  printf("--- Test case CAPTURE(\"X\"{0,2})\n");
  pexle_free(exp);
  exp = pexle_capture_f(C, "exes", pexle_repeat_f(C, pexle_from_string(C, "X"), -2));
  failif(!exp, "expected expression");

  failif(test(C, exp, "", TRUE, 0, 13), "test failed");
  failif(test(C, exp, "X", TRUE, 0, 13), "test failed");
  failif(test(C, exp, "XXabc", TRUE, 3, 13), "test failed");
  failif(test(C, exp, "XXXX", TRUE, 2, 13), "test failed");

  printf("--- Case (1): special case does not apply because exp is neither headfail nor non-nullable\n\n");
  printf("--- Case (2): general case, no optimizations\n\n");
  printf("--- Test case (\"X\"/epsilon){0,2}\n");
  pexle_free(exp);
  exp = pexle_repeat_f(C, pexle_choice_f(C, pexle_from_string(C, "X"), pexle_from_boolean(C, TRUE)), -2);
  failif(!exp, "expected expression");

  failif(test(C, exp, "", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "X", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "XXabc", TRUE, 3, -1), "test failed");
  failif(test(C, exp, "XXXX", TRUE, 2, -1), "test failed");

  printf("--- Test case CAPTURE((\"X\"/epsilon){0,2})\n");
  pexle_free(exp);
  exp = pexle_capture_f(C, "exes", pexle_repeat_f(C, pexle_choice_f(C, pexle_from_string(C, "X"), pexle_from_boolean(C, TRUE)), -2));
  failif(!exp, "expected expression");

  failif(test(C, exp, "", TRUE, 0, 13), "test failed");
  failif(test(C, exp, "X", TRUE, 0, 13), "test failed");
  failif(test(C, exp, "XXabc", TRUE, 3, 13), "test failed");
  failif(test(C, exp, "XXXX", TRUE, 2, 13), "test failed");

  printf("--- Case (3): general case, optimization due to choice on stack\n\n");
  printf("--- Test case (\"X\"/epsilon){0,2} / epsilon \n");
  pexle_free(exp);
  exp = pexle_choice_f(C,
		     pexle_repeat_f(C,
				  pexle_choice_f(C,
					       pexle_from_string(C, "X"),
					       pexle_from_boolean(C, TRUE)),
				  -2),
		     pexle_from_boolean(C, TRUE));
  failif(!exp, "expected expression");

  failif(test(C, exp, "", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "X", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "XXabc", TRUE, 3, -1), "test failed");
  failif(test(C, exp, "XXXX", TRUE, 2, -1), "test failed");

  printf("--- Test case CAPTURE((\"X\"/epsilon){0,2} / epsilon)\n");
  pexle_free(exp);
  exp = pexle_capture_f(C, "exes",
		      pexle_choice_f(C,
				   pexle_repeat_f(C,
						pexle_choice_f(C,
							     pexle_from_string(C, "X"),
							     pexle_from_boolean(C, TRUE)),
						-2),
				   pexle_from_boolean(C, TRUE)));
  failif(!exp, "expected expression");

  failif(test(C, exp, "", TRUE, 0, 13), "test failed");
  failif(test(C, exp, "X", TRUE, 0, 13), "test failed");
  failif(test(C, exp, "XXabc", TRUE, 3, 13), "test failed");
  failif(test(C, exp, "XXXX", TRUE, 2, 13), "test failed");

  /************************************************************************/
  /* Now with repetitions of "up to 1 occurrence" of nullable expressions */
  /************************************************************************/

  ANNOUNCE("CHECKING repetitions of at most 1 occurrence\n\n");

  printf("--- Case (0): repeated exp converts to charset; ITestChar issued\n\n");
  printf("--- Test case \"X\"?\n");
  exp = pexle_repeat_f(C, pexle_from_string(C, "X"), -1);
  failif(!exp, "expected expression");

  failif(test(C, exp, "", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "X", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "XXabc", TRUE, 4, -1), "test failed");
  failif(test(C, exp, "XXXX", TRUE, 3, -1), "test failed");
  failif(test(C, exp, "a", TRUE, 1, -1), "test failed");

  printf("--- Test case CAPTURE(\"X\")?\n");
  pexle_free(exp);
  exp = pexle_capture_f(C, "exes", pexle_repeat_f(C, pexle_from_string(C, "X"), -1));
  failif(!exp, "expected expression");

  failif(test(C, exp, "", TRUE, 0, 13), "test failed");
  failif(test(C, exp, "X", TRUE, 0, 13), "test failed");
  failif(test(C, exp, "XXabc", TRUE, 4, 13), "test failed");
  failif(test(C, exp, "XXXX", TRUE, 3, 13), "test failed");
  failif(test(C, exp, "a", TRUE, 1, 13), "test failed");

  printf("--- Case (1): special case does not apply because exp is neither headfail nor non-nullable\n\n");
  printf("--- Case (2): general case, no optimizations\n\n");
  printf("--- Test case (\"X\"/epsilon)?\n");
  pexle_free(exp);
  exp = pexle_repeat_f(C, pexle_choice_f(C, pexle_from_string(C, "X"), pexle_from_boolean(C, TRUE)), -1);
  failif(!exp, "expected expression");

  failif(test(C, exp, "", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "X", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "XXabc", TRUE, 4, -1), "test failed");
  failif(test(C, exp, "XXXX", TRUE, 3, -1), "test failed");
  failif(test(C, exp, "a", TRUE, 1, -1), "test failed");

  printf("--- Test case CAPTURE((\"X\"/epsilon))?\n");
  pexle_free(exp);
  exp = pexle_capture_f(C, "exes", pexle_repeat_f(C, pexle_choice_f(C, pexle_from_string(C, "X"), pexle_from_boolean(C, TRUE)), -1));
  failif(!exp, "expected expression");

  failif(test(C, exp, "", TRUE, 0, 13), "test failed");
  failif(test(C, exp, "X", TRUE, 0, 13), "test failed");
  failif(test(C, exp, "XXabc", TRUE, 4, 13), "test failed");
  failif(test(C, exp, "XXXX", TRUE, 3, 13), "test failed");
  failif(test(C, exp, "a", TRUE, 1, 13), "test failed");

  printf("--- Case (3): general case, optimization due to choice on stack\n\n");
  printf("--- Test case (\"X\"/epsilon)? / epsilon \n");
  pexle_free(exp);
  exp = pexle_choice_f(C,
		     pexle_repeat_f(C,
				  pexle_choice_f(C,
					       pexle_from_string(C, "X"),
					       pexle_from_boolean(C, TRUE)),
				  -1),
		     pexle_from_boolean(C, TRUE));
  failif(!exp, "expected expression");

  failif(test(C, exp, "", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "X", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "XXabc", TRUE, 4, -1), "test failed");
  failif(test(C, exp, "XXXX", TRUE, 3, -1), "test failed");
  failif(test(C, exp, "a", TRUE, 1, -1), "test failed");

  printf("--- Test case CAPTURE((\"X\"/epsilon) / epsilon)\n");
  pexle_free(exp);
  exp = pexle_capture_f(C, "exes",
		      pexle_choice_f(C,
				   pexle_repeat_f(C,
						pexle_choice_f(C,
							     pexle_from_string(C, "X"),
							     pexle_from_boolean(C, TRUE)),
						-1),
				   pexle_from_boolean(C, TRUE)));
  failif(!exp, "expected expression");

  failif(test(C, exp, "", TRUE, 0, 13), "test failed");
  failif(test(C, exp, "X", TRUE, 0, 13), "test failed");
  failif(test(C, exp, "XXabc", TRUE, 4, 13), "test failed");
  failif(test(C, exp, "XXXX", TRUE, 3, 13), "test failed");
  failif(test(C, exp, "a", TRUE, 1, 13), "test failed");

  /***************************************************************************/
  /* Now with repetitions of "n or more occurrences" of nullable expressions */
  /***************************************************************************/

  ANNOUNCE("CHECKING repetitions of n or more occurrences\n\n");

  n = 3;
  printf("--- Case (0): repeated exp converts to charset; ITestChar issued\n\n");
  printf("--- Test case \"X\"{n,}\n");
  exp = pexle_repeat_f(C, pexle_from_string(C, "X"), n);
  failif(!exp, "expected expression");

  failif(test(C, exp, "", FALSE, 0, -1), "test failed");
  failif(test(C, exp, "X", FALSE, 1, -1), "test failed");
  failif(test(C, exp, "XXabc", FALSE, 5, -1), "test failed");
  failif(test(C, exp, "XXXX", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "a", FALSE, 1, -1), "test failed");

  printf("--- Test case CAPTURE(\"X\"){n,}\n");
  pexle_free(exp);
  exp = pexle_capture_f(C, "exes", pexle_repeat_f(C, pexle_from_string(C, "X"), n));
  failif(!exp, "expected expression");

  failif(test(C, exp, "", FALSE, 0, 0), "test failed");
  failif(test(C, exp, "X", FALSE, 1, 0), "test failed");
  failif(test(C, exp, "XXabc", FALSE, 5, 0), "test failed");
  failif(test(C, exp, "XXXX", TRUE, 0, 13), "test failed");
  failif(test(C, exp, "a", FALSE, 1, 0), "test failed");

  printf("--- Case (1): special case does not apply because exp is neither headfail nor non-nullable\n\n");
  printf("--- Case (2): general case, no optimizations\n\n");
  printf("--- Test case (\"X\"/epsilon){n,}\n");
  pexle_free(exp);
  exp = pexle_repeat_f(C, pexle_choice_f(C, pexle_from_string(C, "X"), pexle_from_boolean(C, TRUE)), n);
  failif(!exp, "expected expression");

  failif(test(C, exp, "", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "X", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "XXabc", TRUE, 3, -1), "test failed");
  failif(test(C, exp, "XXXX", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "a", TRUE, 1, -1), "test failed");

  printf("--- Test case CAPTURE((\"X\"/epsilon)){n,}\n");
  pexle_free(exp);
  exp = pexle_capture_f(C, "exes", pexle_repeat_f(C, pexle_choice_f(C, pexle_from_string(C, "X"), pexle_from_boolean(C, TRUE)), n));
  failif(!exp, "expected expression");

  failif(test(C, exp, "", TRUE, 0, 13), "test failed");
  failif(test(C, exp, "X", TRUE, 0, 13), "test failed");
  failif(test(C, exp, "XXabc", TRUE, 3, 13), "test failed");
  failif(test(C, exp, "XXXX", TRUE, 0, 13), "test failed");
  failif(test(C, exp, "a", TRUE, 1, 13), "test failed");

  printf("--- Case (3): general case, optimization due to choice on stack\n\n");
  printf("--- Test case (\"X\"/epsilon){n,} / epsilon \n");
  pexle_free(exp);
  exp = pexle_choice_f(C,
		     pexle_repeat_f(C,
				  pexle_choice_f(C,
					       pexle_from_string(C, "X"),
					       pexle_from_boolean(C, TRUE)),
				  n),
		     pexle_from_boolean(C, TRUE));
  failif(!exp, "expected expression");

  failif(test(C, exp, "", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "X", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "XXabc", TRUE, 3, -1), "test failed");
  failif(test(C, exp, "XXXX", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "a", TRUE, 1, -1), "test failed");

  printf("--- Test case CAPTURE((\"X\"/epsilon) / epsilon)\n");
  pexle_free(exp);
  exp = pexle_capture_f(C, "exes",
		      pexle_choice_f(C,
				   pexle_repeat_f(C,
						pexle_choice_f(C,
							     pexle_from_string(C, "X"),
							     pexle_from_boolean(C, TRUE)),
						n),
				   pexle_from_boolean(C, TRUE)));
  failif(!exp, "expected expression");

  failif(test(C, exp, "", TRUE, 0, 13), "test failed");
  failif(test(C, exp, "X", TRUE, 0, 13), "test failed");
  failif(test(C, exp, "XXabc", TRUE, 3, 13), "test failed");
  failif(test(C, exp, "XXXX", TRUE, 0, 13), "test failed");
  failif(test(C, exp, "a", TRUE, 1, 13), "test failed");

  /***************************************/
  /* Now with repetitions of repetitions */
  /***************************************/

  ANNOUNCE("CHECKING repetitions of repetitions\n\n");

  n = 3;
  printf("--- Case (0): repeated exp converts to charset; ITestChar issued\n\n");
  printf("--- Test case ('X'?)*\n");
  exp = pexle_repeat_f(C,
		     pexle_repeat_f(C, pexle_from_string(C, "X"), -1),
		     0);
  failif(!exp, "expected expression");

  failif(test(C, exp, "", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "X", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "XXabc", TRUE, 3, -1), "test failed");
  failif(test(C, exp, "XXXX", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "a", TRUE, 1, -1), "test failed");

  printf("--- Test case CAPTURE(('X'?)*)\n");
  pexle_free(exp);
  exp = pexle_capture_f(C, "exes", 
		      pexle_repeat_f(C,
				   pexle_repeat_f(C, pexle_from_string(C, "X"), -1),
				   0));
  failif(!exp, "expected expression");

  failif(test(C, exp, "", TRUE, 0, 13), "test failed");
  failif(test(C, exp, "X", TRUE, 0, 13), "test failed");
  failif(test(C, exp, "XXabc", TRUE, 3, 13), "test failed");
  failif(test(C, exp, "XXXX", TRUE, 0, 13), "test failed");
  failif(test(C, exp, "a", TRUE, 1, 13), "test failed");

  printf("--- Case (1): special case does not apply because exp is neither headfail nor non-nullable\n\n");
  printf("--- Case (2): general case, no optimizations\n\n");
  printf("--- Test case (('X'/epsilon){n,})*\n");
  pexle_free(exp);
  exp = pexle_repeat_f(C, 
		     pexle_repeat_f(C, pexle_choice_f(C, pexle_from_string(C, "X"), pexle_from_boolean(C, TRUE)), n),
		     0);
  failif(!exp, "expected expression");

  failif(test(C, exp, "", TRUE, 0, 1), "test failed");
  failif(test(C, exp, "X", TRUE, 0, 1), "test failed");
  failif(test(C, exp, "XXabc", TRUE, 3, 1), "test failed");
  failif(test(C, exp, "XXXX", TRUE, 0, 1), "test failed");
  failif(test(C, exp, "a", TRUE, 1, 1), "test failed");

  printf("--- Test case CAPTURE(\"exes\", ('X'/epsilon){n,})*\n");
  pexle_free(exp);
  exp = pexle_capture_f(C, "exes", 
		      pexle_repeat_f(C, 
				   pexle_repeat_f(C, pexle_choice_f(C, pexle_from_string(C, "X"), pexle_from_boolean(C, TRUE)), n),
				   0));
  failif(!exp, "expected expression");

  failif(test(C, exp, "", TRUE, 0, 13), "test failed");
  failif(test(C, exp, "X", TRUE, 0, 13), "test failed");
  failif(test(C, exp, "XXabc", TRUE, 3, 13), "test failed");
  failif(test(C, exp, "XXXX", TRUE, 0, 13), "test failed");
  failif(test(C, exp, "a", TRUE, 1, 13), "test failed");

  printf("--- Case (3): general case, optimization due to choice on stack\n\n");
  printf("--- Test case (('X'/epsilon){n,})* / epsilon \n");
  pexle_free(exp);
  exp = pexle_choice_f(C, 
		     pexle_repeat_f(C, 
				  pexle_repeat_f(C, 
					       pexle_choice_f(C, pexle_from_string(C, "X"), 
							    pexle_from_boolean(C, TRUE)), 
					       n),
				  0),
		     pexle_from_boolean(C, TRUE));
  failif(!exp, "expected expression");

  failif(test(C, exp, "", TRUE, 0, 1), "test failed");
  failif(test(C, exp, "X", TRUE, 0, 1), "test failed");
  failif(test(C, exp, "XXabc", TRUE, 3, 1), "test failed");
  failif(test(C, exp, "XXXX", TRUE, 0, 1), "test failed");
  failif(test(C, exp, "a", TRUE, 1, 1), "test failed");

  printf("--- Test case CAPTURE(\"exes\", (('X'/epsilon){n,})* / epsilon)\n");
  pexle_free(exp);
  exp = pexle_capture_f(C, "exes",
		      pexle_choice_f(C, 
				   pexle_repeat_f(C, 
						pexle_repeat_f(C, 
							     pexle_choice_f(C, pexle_from_string(C, "X"), 
									  pexle_from_boolean(C, TRUE)), 
							     n),
						0),
				   pexle_from_boolean(C, TRUE)));
  failif(!exp, "expected expression");

  failif(test(C, exp, "", TRUE, 0, 13), "test failed");
  failif(test(C, exp, "X", TRUE, 0, 13), "test failed");
  failif(test(C, exp, "XXabc", TRUE, 3, 13), "test failed");
  failif(test(C, exp, "XXXX", TRUE, 0, 13), "test failed");
  failif(test(C, exp, "a", TRUE, 1, 13), "test failed");

  /********************************/
  /* Now with a recursive grammar */
  /********************************/

  ANNOUNCE("CHECKING a grammar that repeats a nullable using recursion\n\n");

  env = pexlc_make_recursive_env(toplevel);
  C->env = env;

  printf("--- Test case ('A -> {\"a\" A}/epsilon; expression = A*\n");

  ref = pexlc_make_binding(C, env, NULL);
  failif(ref_is_error(ref), "should succeed and create an anonymous binding");
  exp = pexle_choice_f(C, 
		     pexle_seq_f(C, pexle_from_string(C, "a"), pexle_call(C, ref)),
		     pexle_from_boolean(C, TRUE));
  pexlc_bind_expression(C, exp, ref, &err);
  failif(err.value != OK, "should succeed");

  exp = pexle_repeat(C, exp, 0);
  failif(!exp, "expected expression");

  failif(test(C, exp, "", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "a", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "aaXYZ", TRUE, 3, -1), "test failed");
  failif(test(C, exp, "aaaa", TRUE, 0, -1), "test failed");
  failif(test(C, exp, "aaaa ", TRUE, 1, -1), "test failed");

  printf("--- Test case CAPTURE('foo', A*)\n");
  exp = pexle_capture(C, "foo", exp);
  failif(!exp, "expected expression");

  failif(test(C, exp, "", TRUE, 0, 12), "test failed");
  failif(test(C, exp, "a", TRUE, 0, 12), "test failed");
  failif(test(C, exp, "aaXYZ", TRUE, 3, 12), "test failed");
  failif(test(C, exp, "aaaa", TRUE, 0, 12), "test failed");
  failif(test(C, exp, "aaaa ", TRUE, 1, 12), "test failed");

  /* ----------------------------------------------------------------------------- */
  printf("\nDone.\n");
  printf("Don't forget to run patlentest next.\n");
  return 0;
}

