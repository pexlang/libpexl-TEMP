/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  hashtabletest.c  tests for the string-keyed hash table                   */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#define _GNU_SOURCE		/* for asprintf() */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "hashtable.h"

/* #ifdef __APPLE__  */
/* /\* Nothing to do *\/  */
/* #else  */
/*   #ifdef __linux__  */
/*     #include <foo.h>  */
/*   #endif  */
/* #endif  */


static void print_hashtable_stats(HashTable *ht) {
  printf("Hash table %p: %lu/%lu used, load factor %ld%%.  Block %lu/%lu bytes used.\n",
	 (void *)ht, ht->count, ht->size, ht->count*100/ht->size,
	 ht->blockcount, ht->blocksize);
}

static void print_hashtable(HashTable *ht) {
  size_t i, count = 0;
  int j;
  const char *name;
  HashTableEntry entry;
  printf("Hash table:\n");
  for (i = 0; i < ht->size; i++) {
    entry = ht->entries[i];
    if (entry.block_offset >= 0) {
      name = &(ht->block[entry.block_offset]);
      printf("%3lu -> [%9d]  ", i, entry.block_offset);
      for (j = 0; j < 8; j++) printf("%d ", entry.data.chars[j]); 
      printf("  %s\n", name);
      count++;
    }
  } /* for */
  printf("TOTAL %lu strings found\n", count);
  if (ht->count != count)
    printf("ERROR in string table count, which is %lu\n", ht->count);
}

#define MAXLEN 100

#define failif(test, msg)						\
  do { if (test) { fprintf(stderr, "Failed %s:%d: %s\n", __FILE__,	\
			   __LINE__, msg);				\
      fflush(stderr);							\
      exit(-1);								\
    }									\
  } while (0)



int main(void) {

  fpos_t startpos;
  
  int i;
  size_t expected_max;
  const char *probe;
  char *s;
  
  int stat, index;
  long offset, rando, pos;

  size_t len;

  HashTableEntry entry, entry2, saveentry, tmp;
  HashTableData data;
  HashTable *st1;
  
  FILE *words;

  char buf[MAXLEN];
  int nwords;
  
  printf("$$$ hashtabletest\n");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Storing a few strings \n");


  st1 = hashtable_new(0, 0);	/* Request default sizes */
  failif(!st1, "expected new string table");
  printf("entries @ %p\n", (void *) st1->entries);
  printf("entries[0,1,2] = %d, %d, %d\n",
	 st1->entries[0].block_offset, st1->entries[1].block_offset, st1->entries[2].block_offset);
  print_hashtable_stats(st1);

  for (i = 0; i < 8; i++) data.chars[i] = i + 65; /* "ABCDEFGH" */
  stat = hashtable_add(st1, "foo", data);
  failif(stat < 0, "add should succeed");
  print_hashtable(st1);

  stat = hashtable_add(st1, "network", data);
  failif(stat < 0, "add should succeed");
  stat = hashtable_add(st1, "indefatigable", data);
  failif(stat < 0, "add should succeed");
  saveentry = hashtable_search(st1, "indefatigable"); /* Use this in later tests */
  failif(saveentry.block_offset < 0, "should get valid entry");
  
  stat = hashtable_add(st1, NULL, data);
  failif(stat >= 0, "this add should not succeed because string arg cannot be NULL");

  stat = hashtable_add(st1, "", data);
  failif(stat < 0, "should be able to put empty string in table");

  print_hashtable_stats(st1);
  print_hashtable(st1);
    
  i = 0;
  index = HASHTABLE_ITER_START;
  while ((tmp = hashtable_iter(st1, &index)), tmp.block_offset >= 0) {
    printf(">> %s\n", hashtable_get_key(st1, tmp));
    i++;
  }
  failif(i != 4, "incorrect number of strings? empty string should count.");

  probe = hashtable_get_key(st1, saveentry);
  failif(!probe, "expected a string");
  printf("GET on saved entry with offset %d produced: %s\n", saveentry.block_offset, probe);

  /* ----------------------------------------------------------------------------- */
  printf("********************** Storing a string that is already there \n");

  print_hashtable_stats(st1);
  print_hashtable(st1);
  
  stat = hashtable_add(st1, "indefatigable", data);
  entry2 = hashtable_search(st1, "indefatigable");
  printf("saved entry offset = %d, new entry offset = %d\n", saveentry.block_offset, entry2.block_offset);
  failif(entry2.block_offset != saveentry.block_offset, "should find same string again");
  failif(entry2.data.ptr != saveentry.data.ptr, "should find same data again");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Storing a string that is too long \n");

  s = malloc(sizeof(char) * (HASHTABLE_MAX_KEYLEN+2));
  failif(!s, "OOM?");
  memset(s, 'a', HASHTABLE_MAX_KEYLEN + 1);
  s[HASHTABLE_MAX_KEYLEN + 1] = '\0';
  failif(strlen(s) != HASHTABLE_MAX_KEYLEN + 1, "checking that string exceeds max");
  
  stat = hashtable_add(st1, s, data);
  failif(stat != HASHTABLE_ERR_KEYLEN, "should catch this specific error");

  /* Now try storing a string that is exactly the longest length allowed */
  s[HASHTABLE_MAX_KEYLEN] = '\0';
  stat = hashtable_add(st1, s, data);
  failif(stat < 0, "add should succeed");
  entry = hashtable_search(st1, s);
  failif(entry.block_offset <= 0, "should work");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Searching the table \n");

  s[HASHTABLE_MAX_KEYLEN] = 'a'; /* Now the string is one char too long */
  entry = hashtable_search(st1, s);
  failif(entry.block_offset > 0, "should NOT find this too-long string");

  s[HASHTABLE_MAX_KEYLEN] = '\0';
  entry = hashtable_search(st1, s);
  failif(entry.block_offset <= 0, "should find that longest string");
  probe = hashtable_get_key(st1, entry);
  failif(!probe, "should get string");
  len = 0;
  while (len++ < HASHTABLE_MAX_KEYLEN)
    failif(s[len] != probe[len], "did not find exactly the right string?!?!?");

  entry = hashtable_search(st1, "indefatigable");
  failif(entry.block_offset != saveentry.block_offset, "should find same string again");
  failif(entry.data.ptr != saveentry.data.ptr, "should find same data again");
  entry = hashtable_search(st1, "foo");
  failif(entry.block_offset <= 0, "should find this string");
  entry = hashtable_search(st1, "foobar");
  failif(entry.block_offset != HASHTABLE_NOT_FOUND, "should not find this string");

  entry = hashtable_search(st1, NULL);
  failif(entry.block_offset >= 0, "string cannot be NULL");

  free(s);

  /* ----------------------------------------------------------------------------- */
#define NWORDS 7500
  printf("********************** Storing %d more strings\n", NWORDS);

  printf("Current hash table status:\n");
  print_hashtable_stats(st1);
  printf("Loading from /usr/dict/words\n");
  words = fopen("/usr/share/dict/words", "r");
  stat = fseek(words, 0, SEEK_END);
  offset = ftell(words);
  printf("stat is %d, last is %ld\n", stat, offset);

  rando = rand();		/* 32 bits */
  pos = ((rando >> 22) * offset) / (1<<10);
  if ((offset - pos) < (NWORDS * 10)) pos = offset - (NWORDS * 10);
  stat = fseek(words, pos, SEEK_SET);
  printf("%ld/%ld, random seek chose a place %ld%% into file\n", pos, offset, pos*100/offset);
  s = fgets(buf, MAXLEN, words); /* read past a partial word */
  stat = fgetpos(words, &startpos);
  failif(stat != 0, "fgetpos failed!?");

  for (nwords=0; nwords < NWORDS; nwords++) {
    s = fgets(buf, MAXLEN, words);
    if (!s) break;
    while((*s!='\0') && (*s!='\n')) s++;
    *s = '\0';			/* overwrite newline */
    stat = hashtable_add(st1, buf, data);
    if (stat < 0) {
      printf("Error %d from hashtable_add.  Stats are:\n", stat);
      print_hashtable_stats(st1);
    }
  } /* for */
  printf("%d strings read and added from /usr/dict/words\n", nwords);
  print_hashtable_stats(st1);

  /* ----------------------------------------------------------------------------- */
  printf("********************** Retrieving the same %d strings\n", NWORDS);

  stat = fsetpos(words, &startpos);
  failif(stat != 0, "fsetpos failed!?");
  for (nwords=0; nwords < NWORDS; nwords++) {
    s = fgets(buf, MAXLEN, words);
    if (!s) break;
    while((*s!='\0') && (*s!='\n')) s++;
    *s = '\0';			/* overwrite newline */
    entry = hashtable_search(st1, buf);
    if (entry.block_offset < 0) {
      printf("Error %d from hashtable_search.  Stats are:\n", entry.block_offset);
      print_hashtable_stats(st1);
    }
    failif(strcmp(buf, hashtable_get_key(st1, entry)) != 0, "get or search failed"); 
  } /* for */
  printf("%d strings verified from /usr/dict/words\n", nwords);
  print_hashtable_stats(st1);

  hashtable_free(st1);

  /* ----------------------------------------------------------------------------- */
  printf("********************** Testing max number of entries with good load factor\n");

  if ((HASHTABLE_MAX_BLOCKSIZE - HASHTABLE_MAX_SLOTS) < (4)) {
    printf("SKIPPING test of max number of entries because max block size not large enough to store expected number of strings\n");
    printf("HASHTABLE_MAX_SLOTS is 2^%d\n", HASHTABLE_MAX_SLOTS);
    printf("HASHTABLE_MAX_BLOCKSIZE is 2^%d\n", HASHTABLE_MAX_BLOCKSIZE);
    printf("Therefore, average string size must be less than %d chars (including NUL terminator)\n",
	   1 << (HASHTABLE_MAX_BLOCKSIZE - HASHTABLE_MAX_SLOTS));
  } else {
    st1 = hashtable_new(10, 20);
    failif(!st1, "hashtable_new failed");
    expected_max = ((size_t) 1 << HASHTABLE_MAX_SLOTS) * HASHTABLE_EXPAND_LOAD / 100;
    printf("Expected maximum number of entries is %lu\n", expected_max);
    for (nwords=0; ((size_t) nwords) < expected_max; nwords++) {
      stat = asprintf(&s, "%x", nwords);
      stat = hashtable_add(st1, s, data);
      free(s);
      if (stat < 0) {
	print_hashtable_stats(st1);
	printf("add failed with error code %d at word %d\n", stat, nwords);
	failif(stat < 0, "add should succeed");
      }
      if (stat == HASHTABLE_ERR_BLOCKFULL) {
	printf("String table (block) is full\n");
	break;
      }
      failif(stat < 0, "add failed");
    }
    printf("%d entries added\n", nwords);
    print_hashtable_stats(st1);

    /* ----------------------------------------------------------------------------- */
    printf("********************** Testing max number of entries with high load factor\n");

    expected_max = (((size_t) 1) << HASHTABLE_MAX_SLOTS) * (HASHTABLE_MAX_LOAD + 1) / 100;
    printf("Expected additional number of entries is %lu\n", expected_max - nwords);
    i = nwords;
    for (; ((size_t) nwords) < expected_max; nwords++) {
      stat = asprintf(&s, "%x", nwords);
      stat = hashtable_add(st1, s, data);
      free(s);
      if (stat < 0) {
	print_hashtable_stats(st1);
	printf("add failed with error code %d at word %d\n", stat, nwords);
      }
      if (stat == HASHTABLE_ERR_BLOCKFULL) {
	printf("String table (block) is full\n");
	break;
      }
      failif(stat < 0, "add failed");
    }
    printf("%d entries added\n", nwords - i);
    print_hashtable_stats(st1);

    printf("Counting how many additional entries before FULL error\n");
    i = 0;
    while (1) {
      stat = asprintf(&s, "%x", nwords);
      nwords++;			/* must put unique strings into table! */
      stat = hashtable_add(st1, s, data);
      free(s);
      if (stat < 0) break;
      i++;
    }
    printf("%d additional entries added\n", i);
    print_hashtable_stats(st1);

    if (stat == HASHTABLE_ERR_BLOCKFULL) {
      printf("String table (block) is full\n");
    } else {
      failif(i != 1, "only one more entry should be able to be added");
      failif(stat != HASHTABLE_ERR_SIZE, "wrong error code");
    }
  }
  hashtable_free(st1);

  /* ----------------------------------------------------------------------------- */
  printf("\nDone.\n");

  return 0;
}

