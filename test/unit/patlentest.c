/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  patlentest.c  TESTING the new patlen code in analyze.c                 */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#define _GNU_SOURCE		/* for asprintf() */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>		/* exit() */
#include "print.h"
#include "analyze.h"
#include "compile.h"		/* pexlc_env_free() */

#define YES 1
#define NO 0

#define TRUE 1
#define FALSE 0

#define failif(test, msg)						\
  do { if (test) { fprintf(stderr, "Failed %s:%d: %s\n", __FILE__,	\
			   __LINE__, msg);				\
      fflush(stderr);							\
      exit(-1);								\
    }									\
  } while (0)

static int check_patlen (Package *pkg,
			 Index symtab_index,
			 int nullable,
			 int unbounded,
			 uint32_t expected_min,
			 uint32_t expected_max) {
  SymbolTableEntry *stentry;
  uint32_t min, max;
  int stat;

  failif(!pkg, "Error in check_patlen: null package\n");
  failif(!pkg->symtab, "Error in check_patlen: package has no symbol table\n");
  
  stentry = symboltable_get(pkg->symtab, (Index) symtab_index);
  if (stentry == NULL) {
    printf("failed to get the symtab entry for index %u", symtab_index);
    return 1;
  }
  stat = has_flag(metadata_flags(stentry->meta), PATTERN_NULLABLE);
  if ((stat==0) != (nullable==0)) {
    printf("error: expected %snullable pattern\n", nullable ? "" : "NON-");
    return 2;
  }
  stat = has_flag(metadata_flags(stentry->meta), PATTERN_UNBOUNDED);
  if ((stat == 0) != (unbounded == 0)) {
    printf("pattern is %s, which was not expected\n", stat ? "unbounded" : "bounded");
    return 4;
  }
  min = metadata_minlen(stentry->meta);
  max = metadata_maxlen(stentry->meta);
  if ((min != expected_min) || ((stat == EXP_BOUNDED) && (max != expected_max))) {
    if (min != expected_min)
      printf("min len = %u, expected = %u\n", min, expected_min);
    if ((stat == EXP_BOUNDED) && (max != expected_max))
      printf("max len = %u, expected = %u\n", max, expected_max);
    return 5;
  } 
  return OK;
}

int main(void) {

  size_t n;
  char *str;
  uint32_t j, min;
  
  Context *C;
  Package *pkg;
  Error err;

  Env *toplevel, *env; 
  Expression *A, *B, *Choice;
  
  int i;
  int unbounded;
  uint32_t sizes[] = {1000,
                      1024,
		      1024 + 511,
		      1024 + 512,
		      64 * 1024 - 1,
		      1024 * 1024,
		      PATTERN_MAXLEN,
		      PATTERN_MAXLEN+1,
		      0};	/* 0 is END MARKER */

  printf("$$$ patlentest\n");

  toplevel = env_new(NULL, NO);
  failif(!toplevel, "new toplevel should succeed");
  
  C = context_new(toplevel);
  failif(!C, "expected new context");

  /* ----------------------------------------------------------------------------- */

  env = env_new(toplevel, NO);
  failif(!env, "should succeed");

  printf("patlentest: Testing patlen on fixed-lengths \n\n");

  i = 0;
  while ((n = sizes[i++])) {
    printf("\nTesting size %lu\n", n);
    str = malloc(n * sizeof(char));
    A = pexle_from_bytes(C, str, n);
    
    pkg = pexlc_compile_expression(C, A, env, &err, DEFAULT_OPTIM);
    failif(!pkg, "should succeed");
    failif(err.value != OK, "should have no error");

    /* Check to see if the min/max lengths and the fixedlen flag are
       printing correctly
    */
    print_symboltable(pkg->symtab);
    /* patterns longer than this are simply marked UNBOUNDED */
    unbounded = (n > PATTERN_MAXLEN);
    failif(check_patlen(pkg, 0, FALSE, unbounded, unbounded ? PATTERN_MAXLEN : n, n), "test failed"); 

    for (j = 0; j < 101; j = (j==0) ? j+1 : j * 10) {
      printf("\nTesting CHOICE of size %lu and size %lu\n", n-j, n);
      A = pexle_from_bytes(C, str, n);
      B = pexle_from_bytes(C, str, n-j);
      Choice = pexle_choice(C, A, B); /* Do not free A, B */
      pkg = pexlc_compile_expression(C, Choice, env, &err, DEFAULT_OPTIM);
      failif(!pkg, "should succeed");
      failif(err.value != OK, "should have no error");

      print_symboltable(pkg->symtab);
      /* patterns longer than this are simply marked UNBOUNDED */
      unbounded = (n > PATTERN_MAXLEN);
      min = ((n - j) > PATTERN_MAXLEN) ? PATTERN_MAXLEN : (n - j);
      failif(check_patlen(pkg, 0, FALSE, unbounded, min, n), "test failed"); 

      /* Same as above, but choice is reversed */
      printf("\nTesting CHOICE of size %lu and size %lu\n", n, n-j);
      Choice = pexle_choice(C, B, A); /* Do not free A, B */
      pkg = pexlc_compile_expression(C, Choice, env, &err, DEFAULT_OPTIM);
      failif(!pkg, "should succeed");
      failif(err.value != OK, "should have no error");

      print_symboltable(pkg->symtab);
      /* patterns longer than this are simply marked UNBOUNDED */
      unbounded = (n > PATTERN_MAXLEN);
      min = ((n - j) > PATTERN_MAXLEN) ? PATTERN_MAXLEN : (n - j);
      failif(check_patlen(pkg, 0, FALSE, unbounded, min, n), "test failed"); 
      
      /* Same as above, but an additional choice with epsilon to make it nullable */
      printf("\nTesting CHOICE of size %lu and size %lu and EPSILON\n", n, n-j);
      Choice = pexle_choice_f(C, B, A); /* Free A, B */
      Choice = pexle_choice_f(C, Choice, pexle_from_boolean(C, TRUE));
      pkg = pexlc_compile_expression(C, Choice, env, &err, DEFAULT_OPTIM);
      failif(!pkg, "should succeed");
      failif(err.value != OK, "should have no error");

      print_symboltable(pkg->symtab);
      /* patterns longer than this are simply marked UNBOUNDED */
      unbounded = (n > PATTERN_MAXLEN);
      failif(check_patlen(pkg, 0, TRUE, unbounded, 0, n), "test failed"); 
      

    } /* for */

    free(str);

  } /* while */



  pexlc_env_free(env);
  pexlc_env_free(toplevel);

  context_free(C);

  /* ----------------------------------------------------------------------------- */
  printf("\nDone.\n");
  printf("Don't forget to run lookaroundtest next.\n");

  return 0;
}

