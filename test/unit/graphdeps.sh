#!/bin/bash

sourcedir=`cd ../../src && pwd`
system_include=/Library//Developer/CommandLineTools/SDKs/MacOSX10.15.sdk/usr/include/
includedirs="$sourcedir,$system_include"
excluderegex="print\.[ch]"

perl cinclude2dot --include $includedirs --exclude $excluderegex --src $sourcedir --quotetypes quote > "$TMPDIR/deps.dot"

# dot is part of the graphviz package
dot -Tpdf "$TMPDIR/deps.dot" >"deps.pdf"
open "deps.pdf"
