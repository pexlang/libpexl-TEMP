/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  expressiontest.c  TESTING expression.c                                   */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>		/* exit() */
#include "print.h"

#define YES 1
#define NO 0

#define failif(test, msg)						\
  do { if (test) { fprintf(stderr, "Failed %s:%d: %s\n", __FILE__,	\
			   __LINE__, msg);				\
      fflush(stderr);							\
      exit(-1);								\
    }									\
  } while (0)

int main(void) {

  Context *C = NULL;
  
  char *toolong;
  
  int stat;
/*   Value val; */
  Charset cs;
  Expression *exp, *tstr, *calltree;
/*   Expression *consttree; */
  Expression *t1, *t2, *t3, *t4; 
  Expression *A, *B, *S;

  PackageTable *m; 
  Package *pkg;
  Env *toplevel, *env, *g;
  SymbolTable *st;
  Ref ref, refA, refB, refS;

/*   size_t bufsize = 16;	/\* 15 chars and a NULL byte *\/ */
/*   char *buf = (char *)alloca(bufsize); */


  printf("$$$ expressiontest\n");
  printf("expressiontest: Building pegs using expression.c \n\n");

  /* Error checking */

/*   val = pexle_new_value_expression(NULL); */
/*   failif(val.ptr, "the null arg should be logged, but the value created anyway"); */
/*   failif(val.type != Eexpression_t, "value should have the right type"); */

  exp = pexle_from_set(C, NULL, 6);
  failif(exp, "the string arg is required, so this call should fail");

  exp = pexle_from_range(C, 'B','A');
  failif(exp, "args must be in order");

  toolong = malloc(EXP_MAXSTRING + 2);
  memset(toolong, 'A', EXP_MAXSTRING + 2);
  toolong[EXP_MAXSTRING + 1] = '\0';
  failif(strlen(toolong) != EXP_MAXSTRING + 1, "precondition for next test");
  tstr = pexle_from_string(C, toolong);
  failif(tstr, "should catch that this string is too long");

  /* Now make it exactly EXP_MAXSTRING long */
  toolong[EXP_MAXSTRING] = '\0';
  tstr = pexle_from_string(C, toolong);
  failif(!tstr, "should work, since this is the longest acceptable string");
  pexle_free(tstr);
  free(toolong);

  tstr = pexle_from_string(C, "X");
  failif(tstr->len != 1, "should produce 1-node tree");

  exp = pexle_seq(C, NULL, tstr);
  failif(exp, "both tree args are required, so this call should fail");
  exp = pexle_seq(C, tstr, NULL);
  failif(exp, "both tree args are required, so this call should fail");
  exp = pexle_seq(C, NULL, NULL);
  failif(exp, "both tree args are required, so this call should fail");
  
  exp = pexle_choice(C, NULL, tstr);
  failif(exp, "both tree args are required, so this call should fail");
  exp = pexle_choice(C, tstr, NULL);
  failif(exp, "both tree args are required, so this call should fail");
  exp = pexle_choice(C, NULL, NULL);
  failif(exp, "both tree args are required, so this call should fail");

  exp = pexle_not(C, NULL);
  failif(exp, "tree arg is required, so this call should fail");

  exp = pexle_lookahead(C, NULL);
  failif(exp, "tree arg is required, so this call should fail");

  exp = pexle_lookbehind(C, NULL);
  failif(exp, "tree arg is required, so this call should fail");

  exp = pexle_subtract(C, tstr, NULL);
  failif(exp, "both tree args are required, so this call should fail");
  exp = pexle_subtract(C, NULL, tstr);
  failif(exp, "both tree args are required, so this call should fail");
  exp = pexle_subtract(C, NULL, NULL);
  failif(exp, "both tree args are required, so this call should fail");

  exp = pexle_repeat(C, NULL, 1);
  failif(exp, "tree arg is required, so this call should fail");

  exp = pexle_capture(C, "foo", NULL);
  failif(exp, "tree arg is required, so this call should fail");
  exp = pexle_capture(C, NULL, tstr);
  failif(exp, "name arg is required, so this call should fail");
  exp = pexle_capture(C, NULL, NULL);
  failif(exp, "both args are required, so this call should fail");

  ref.env = NULL;
  exp = pexle_call(C, ref);
  failif(exp, "ref env cannot be NULL, so this call should fail");

  stat = to_charset(tstr->node, NULL);
  failif(stat != 0, "the charset arg is required, so this call should fail");

  stat = to_charset(NULL, &cs);
  failif(stat != 0, "the node arg is required, so this call should fail");
  
  stat = to_charset(tstr->node, &cs);
  failif(stat != 1, "one TChar node should be convertible to a character set");
  
  t1 = pexle_from_number(C, 1);	/* TAny */
  stat = to_charset(t1->node, &cs);
  failif(stat != 1, "one TAny node should be convertible to a character set");

  pexle_free(t1);
  pexle_free(tstr);

  /* ----------------------------------------------------------------------------- */
  printf("********************** Building primitive patterns \n");

  tstr = pexle_from_string(C, "hello");
  print_exp(tstr, 0, NULL);
  failif(tstr->len != 9, "should produce 9-node tree");
  pexle_free(tstr);

  tstr = pexle_from_string(C, "");
  print_exp(tstr, 0, NULL);
  failif(tstr->len != 1, "should produce 1-node tree");
  failif(tstr->node->tag != TTrue, "the 1 node is TTrue, which matches epsilon");
  pexle_free(tstr);

  tstr = pexle_from_number(C, 1);
  print_exp(tstr, 0, NULL);
  failif(tstr->len != 1, "should produce 1-node tree");
  failif(tstr->node->tag != TAny, "the node is TAny, which matches 1 byte");
  pexle_free(tstr);

  tstr = pexle_from_number(C, 0);
  print_exp(tstr, 0, NULL);
  failif(tstr->len != 1, "should produce 1-node tree");
  failif(tstr->node->tag != TTrue, "the 1 node is TTrue, which matches epsilon");
  pexle_free(tstr);

  tstr = pexle_from_number(C, 4);
  print_exp(tstr, 0, NULL);
  failif(tstr->len != 7, "should produce 7-node tree");
  failif(tstr->node->tag != TSeq, "the root node is Tseq");
  pexle_free(tstr);

  tstr = pexle_from_number(C, -2);
  print_exp(tstr, 0, NULL);
  failif(tstr->len != 4, "should produce this size tree");
  failif(tstr->node->tag != TNot, "the root node is TNot");
  pexle_free(tstr);

  tstr = pexle_from_boolean(C, 0);
  print_exp(tstr, 0, NULL);
  failif(tstr->len != 1, "should produce this size tree");
  failif(tstr->node->tag != TFalse, "the root node is TFalse");
  pexle_free(tstr);

  tstr = pexle_from_boolean(C, 1);
  print_exp(tstr, 0, NULL);
  failif(tstr->len != 1, "should produce this size tree");
  failif(tstr->node->tag != TTrue, "the root node is TTrue");
  pexle_free(tstr);

  tstr = pexle_from_boolean(C, 9000);
  print_exp(tstr, 0, NULL);
  failif(tstr->len != 1, "should produce this size tree");
  failif(tstr->node->tag != TTrue, "the root node is TTrue");
  pexle_free(tstr);

  tstr = pexle_from_boolean(C, -1);
  print_exp(tstr, 0, NULL);
  failif(tstr->len != 1, "should produce this size tree");
  failif(tstr->node->tag != TTrue, "the root node is TTrue");
  pexle_free(tstr);

  t3 = pexle_from_set(C, "abcdef", 6);
  print_exp(t3, 0, NULL);
  failif(t3->len != 3, "a single charset occupies size of 3 nodes");
  failif(t3->node->tag != TSet, "tag should be TSet");
  pexle_free(t3);

  t4 = pexle_from_set(C, "", 0);
  print_exp(t4, 0, NULL);
  failif(t4->len != 3, "a single charset occupies size of 3 nodes");
  failif(t4->node->tag != TSet, "tag should be TSet");
  pexle_free(t4);

  t3 = pexle_from_range(C, 'A','Z');
  print_exp(t3, 0, NULL);
  failif(t3->len != 3, "a single charset occupies size of 3 nodes");
  failif(t3->node->tag != TSet, "tag should be TSet");
  pexle_free(t3);

  printf("*** Testing range(0, 255)\n");
  t4 = pexle_from_range(C, 0, 255);
  failif(!t4, "should succeed");
  print_exp(t4, 0, NULL);
  failif(t4->len != 3, "a single charset occupies size of 3 nodes");
  failif(t4->node->tag != TSet, "tag should be TSet");
  pexle_free(t4);

  t4 = pexle_from_range(C, 10, 10);
  print_exp(t4, 0, NULL);
  failif(t4->len != 3, "a single charset occupies size of 3 nodes");
  failif(t4->node->tag != TSet, "tag should be TSet");
  pexle_free(t4);

  /* ----------------------------------------------------------------------------- */
  printf("\n********************** Using primitive 'halt' \n");

  t1 = pexle_halt(C);
  print_exp(t1, 0, NULL);
  failif(t1->len != 1, "wrong size tree");
  failif(t1->node->tag != THalt, "THalt");
  pexle_free(t1);

  /* ----------------------------------------------------------------------------- */
  printf("\n********************** Using combinator 'seq' \n");

  t1 = pexle_from_string(C, "ab");
  t2 = pexle_from_string(C, "c");
  tstr = pexle_seq(C, t1, t2);
  print_exp(tstr, 0, NULL);
  failif(tstr->len != 5, "wrong size tree");

  t3 = pexle_from_number(C, 2);
  t4 = pexle_seq(C, t3, tstr);
  print_exp(t4, 0, NULL);
  failif(t4->len != 9, "wrong size tree");

  printf("Testing boolean optimizations\n");

  pexle_free(t3); pexle_free(t4);
  t3 = pexle_from_boolean(C, 0);		/* False */
  t4 = pexle_seq(C, t3, t1);
  print_exp(t4, 0, NULL);
  failif(t4->len != 1, "wrong size tree");
  failif(t4->node->tag != TFalse, "optimization produces TFalse");
  pexle_free(t3); pexle_free(t4);

  t3 = pexle_from_boolean(C, 1);		/* True */
  t4 = pexle_seq(C, t3, t1);
  print_exp(t4, 0, NULL);
  failif(t4->len != 3, "wrong size tree");
  pexle_free(t3); pexle_free(t4);

  t3 = pexle_from_boolean(C, 1);		/* True */
  t4 = pexle_seq(C, t1, t3);
  print_exp(t4, 0, NULL);
  failif(t4->len != 3, "wrong size tree");
  pexle_free(t3); pexle_free(t4);

  pexle_free(t1); pexle_free(t2);

  /* ----------------------------------------------------------------------------- */
  printf("\n********************** Using combinator 'choice' \n");

  t1 = pexle_from_string(C, "ab");
  t2 = pexle_from_string(C, "c");
  t3 = pexle_choice(C, t1, t2);
  print_exp(t3, 0, NULL);
  failif(t3->len != 5, "wrong size tree");
  failif(t3->node->tag != TChoice, "should get TChoice");
  pexle_free(t3);

  t3 = pexle_from_number(C, 2);
  t4 = pexle_choice(C, t3, tstr);
  print_exp(t4, 0, NULL);
  failif(t4->len != 9, "wrong size tree");
  failif(t4->node->tag != TChoice, "should get TChoice");
  pexle_free(t1); pexle_free(t2); pexle_free(t3); pexle_free(t4); pexle_free(tstr);

  printf("Testing charset optimizations\n");

  t1 = pexle_from_set(C, "A", 1);
  t2 = pexle_from_set(C, "B", 1);
  t3 = pexle_choice(C, t1, t2);
  print_exp(t3, 0, NULL);
  failif(t3->len != 3, "wrong size tree");
  failif(t3->node->tag != TSet, "optimization produces another TSet");
  pexle_free(t1); pexle_free(t2); pexle_free(t3); 

  t1 = pexle_from_set(C, "ABC", 1);
  t2 = pexle_from_set(C, "B", 1);
  t3 = pexle_choice(C, t1, t2);
  print_exp(t3, 0, NULL);
  failif(t3->len != 3, "wrong size tree");
  failif(t3->node->tag != TSet, "optimization produces another TSet");
  pexle_free(t1); pexle_free(t2); pexle_free(t3); 

  /* The nofail optimization is now delayed until compilation */
/*   printf("Testing nofail optimizations\n"); */

/*   t1 = pexle_from_boolean(C, 1);		/\* "Be true; you need not fail." *\/ */
/*   t2 = pexle_from_string(C, "Hello"); */
/*   t3 = pexle_choice(C, t1, t2); */
/*   print_exp(t3, 0, NULL); */
/*   failif(t3->len != 1, "wrong size tree"); */
/*   failif(t3->node->tag != TTrue, "optimization produces TTrue"); */
/*   pexle_free(t1); pexle_free(t2); pexle_free(t3);  */

/*   t1 = pexle_from_string(C, "");		/\* "Be true; you need not fail." *\/ */
/*   t2 = pexle_from_boolean(C, 0); */
/*   t3 = pexle_choice(C, t1, t2); */
/*   print_exp(t3, 0, NULL); */
/*   failif(t3->len != 1, "wrong size tree"); */
/*   failif(t3->node->tag != TTrue, "optimization produces TTrue"); */
/*   pexle_free(t1); pexle_free(t2); pexle_free(t3);  */

/*   t1 = pexle_from_string(C, "A"); */
/*   t2 = pexle_from_boolean(C, 0); */
/*   t3 = pexle_choice(C, t1, t2); */
/*   print_exp(t3, 0, NULL); */
/*   failif(t3->len != 1, "wrong size tree"); */
/*   failif(t3->node->tag != TChar, "optimization produces TChar, not TChoice"); */
/*   pexle_free(t1); pexle_free(t2); pexle_free(t3); */

  /* ----------------------------------------------------------------------------- */
  printf("\n********************** Using combinator 'not' \n");

  t1 = pexle_from_string(C, "A");
  t2 = pexle_not(C, t1);
  print_exp(t2, 0, NULL);
  failif(t2->len != 1 + t1->len, "wrong size tree");
  failif(t2->node->tag != TNot, "Tnot");
  pexle_free(t1); pexle_free(t2);

  /* No optimizations here */
  t1 = pexle_from_boolean(C, 1);
  t2 = pexle_not(C, t1);
  print_exp(t2, 0, NULL);
  failif(t2->len != 1 + t1->len, "wrong size tree");
  failif(t2->node->tag != TNot, "Tnot");
  pexle_free(t1); pexle_free(t2);

  /* ----------------------------------------------------------------------------- */
  printf("\n********************** Using combinator 'lookahead' \n");

  t1 = pexle_from_string(C, "A");
  t2 = pexle_lookahead(C, t1);
  print_exp(t2, 0, NULL);
  failif(t2->len != 1 + t1->len, "wrong size tree");
  failif(t2->node->tag != TAhead, "TAhead");
  pexle_free(t1); pexle_free(t2);

  /* No optimizations here */
  t1 = pexle_from_boolean(C, 1);
  t2 = pexle_lookahead(C, t1);
  print_exp(t2, 0, NULL);
  failif(t2->len != 1 + t1->len, "wrong size tree");
  failif(t2->node->tag != TAhead, "TAhead");
  pexle_free(t1); pexle_free(t2);

  /* ----------------------------------------------------------------------------- */
  printf("\n********************** Using combinator 'rep' \n");

  t1 = pexle_from_string(C, "A");
  t2 = pexle_repeat(C, t1, 0);
  print_exp(t2, 0, NULL);
  failif(t2->len != 1 + t1->len, "wrong size tree");
  failif(t2->node->tag != TRep, "TRep");
  t3 = pexle_repeat(C, t1, 3);
  print_exp(t3, 0, NULL);
  failif(t3->len != 7 + t1->len, "wrong size tree");
  failif(t3->node->tag != TSeq, "TSeq");
  pexle_free(t1); pexle_free(t2); pexle_free(t3);
  
  t1 = pexle_from_string(C, "A");
  t2 = pexle_repeat(C, t1, -1);
  print_exp(t2, 0, NULL);
  failif(t2->len != 2 + t1->len, "wrong size tree");
  failif(t2->node->tag != TChoice, "TChoice");
  t3 = pexle_repeat(C, t1, -3);
  print_exp(t3, 0, NULL);
  failif(t3->len != 10 + t1->len, "wrong size tree");
  failif(t3->node->tag != TChoice, "TChoice");
  pexle_free(t1); pexle_free(t2); pexle_free(t3);

  /* ----------------------------------------------------------------------------- */
  printf("\n********************** Using combinator 'subtract' \n");

  /* "A"-"XYZ" ==> (seq (not "XYZ") "A") */
  t1 = pexle_from_string(C, "A");
  t2 = pexle_from_string(C, "XYZ");
  t3 = pexle_subtract(C, t1, t2);
  print_exp(t3, 0, NULL);
  failif(t3->len != t1->len + t2->len + 2, "wrong size tree");
  failif(t3->node->tag != TSeq, "TSeq");
  pexle_free(t1); pexle_free(t2); pexle_free(t3);

  /* Charsets combine, so [ABC]-[CDE] ==> [AB] */
  t1 = pexle_from_set(C, "ABC", 3);
  t2 = pexle_from_set(C, "CDE", 3);
  t3 = pexle_subtract(C, t1, t2);
  print_exp(t3, 0, NULL);
  failif(t3->len != 3, "wrong size tree");
  failif(t3->node->tag != TSet, "TSet");
  pexle_free(t1); pexle_free(t2); pexle_free(t3);

  /* ----------------------------------------------------------------------------- */
  printf("\n********************** Using combinator 'lookbehind' \n");

  printf("Look back at string literal\n");
  t1 = pexle_from_string(C, "A");
  t2 = pexle_lookbehind(C, t1);
  failif(t2->len != 2, "wrong size tree");
  failif(t2->node->tag != TBehind, "TBehind");
  print_exp(t2, 0, NULL);
  t3 = pexle_lookbehind(C, t1);
  failif(t3->len != 2, "wrong size tree");
  failif(t3->node->tag != TBehind, "TBehind");
/*   failif(len != 1, "wrong length returned"); */
  pexle_free(t1); pexle_free(t2); pexle_free(t3);

  printf("Look back at \"A\"? which has variable length\n");
  t1 = pexle_from_string(C, "A");
  t2 = pexle_repeat(C, t1, -1);
  print_exp(t2, 0, NULL);
  t3 = pexle_lookbehind(C, t2);
  failif(!t3, "the check for fixedlen happens later");
  pexle_free(t1); pexle_free(t2); pexle_free(t3);

  printf("TODO: test lookbehind at a bound pattern\n");
/*   stat = bind_entrypoint(env, "LB_TEST", vis, t3, NULL); */
/*   failif(stat != 0, "bind should work"); */
/*   failif(env_len(env) != 1, "first binding made"); */

/*   printf("This tree has a lookbehind to a variable-length target, which will fail during compilation: "); */
/*   print_exp(t3, 0, NULL); */

  printf("Look back at [A-Z]+ which has variable length\n");
  t1 = pexle_from_range(C, 'A', 'Z');
  t2 = pexle_repeat(C, t1, 1);
  t3 = pexle_lookbehind(C, t2);
  print_exp(t3, 0, NULL);
  failif(!t3, "we do not check (now) for the acceptable lookbehind targets until compilation");
  pexle_free(t1); pexle_free(t2); pexle_free(t3);

  printf("Look back at \"AB\"/\"CD\" which has length 2\n");
  t1 = pexle_from_string(C, "AB");
  t2 = pexle_from_string(C, "CD");
  t3 = pexle_choice(C, t1, t2);
  t4 = pexle_lookbehind(C, t3);
  print_exp(t4, 0, NULL);
  failif(!t4, "we do not check (now) for the acceptable lookbehind targets until compilation");
  failif(t4->node->tag != TBehind, "TBehind");
  pexle_free(t1); pexle_free(t2); pexle_free(t3); pexle_free(t4);

  /* ----------------------------------------------------------------------------- */
  printf("\n********************** Capture \n");

  t1 = pexle_from_string(C, "AB");
  t2 = pexle_capture(C, "hi", t1);
  print_exp(t2, 0, NULL);

  t3 = pexle_seq(C, t2, t2);
  print_exp(t3, 0, NULL);

  t4 = pexle_capture(C, "foo", t3);
  print_exp(t4, 0, NULL);

  pexle_free(t1); pexle_free(t2); pexle_free(t3); pexle_free(t4);
  
  /* ----------------------------------------------------------------------------- */
  printf("\n********************** Constant captures \n");

  printf("TEMPORARILY BLOCKED until new constant capture impl is ready\n");
#if 0
  consttree = pexle_constant(C, 0, 0);
  failif(!consttree, "expected success");

  pexle_free(consttree);
  consttree = pexle_constant(1, 2);
  failif(!consttree, "expected success");

  printf("Printing without env:\n");
  print_exp(consttree, 0, NULL);
#endif

  /* ----------------------------------------------------------------------------- */
  printf("\n********************** References \n");

  m = packagetable_new(1);
  failif(!m, "should succeed");

  toplevel = env_new(NULL, NO);
  failif(!toplevel, "new toplevel should succeed");

  env = env_new(toplevel, NO);
  failif(!env, "should succeed");

  st = symboltable_new(1, 1);
  failif(!st, "should succeed");
  stat = symboltable_add(st, NULL, 1, 1, 0, 0, NULL); /* add empty string (here represented by NULL */
  failif(stat != 0, "should succeed, returned index should be 0");
  stat = symboltable_add(st, "foo", 1, 1, 0, 0, NULL);
  failif(stat != 1, "should succeed, returned index should be 1");

  pkg = package_new();
  failif(!pkg, "should succeed, even with importpath, prefix, source as empty string");
  package_free(pkg);

  stat = env_bind(env, 0, env_new_value(Eunspecified_t, 0, NULL), &ref);
  failif(stat != 0, "successful env_bind should return code 0");

  stat = env_bind(env, 1, env_new_value(Eunspecified_t, 0, NULL), &ref);
  failif(stat != 0, "successful env_bind should return code 0");
  failif(ref.env != env, "checking ref contents");
  failif(ref.index != 1, "checking ref contents (second binding is at index 1)");

  printf("ref = (%p, %d)\n", (void *) ref.env, ref.index);
  calltree = pexle_call(C, ref);
  print_exp(calltree, 0, NULL);
  failif(!calltree, "should succeed");
  failif(calltree->node->v.env != ref.env, "tree should contain env from ref");
  failif(calltree->node->u.index != ref.index, "tree should contain index from ref");
  pexle_free(calltree);

#if 0
  /* Re-enable backref tests after converting the impl to use TFunction node */


  /* ----------------------------------------------------------------------------- */
  printf("\n********************** Backreferences \n");

  backreftree = pexle_backreference(C, env, NULL);
  failif(backreftree, "cannot pass NULL");

  backreftree = pexle_backreference(C, env, "target");
  failif(!backreftree, "backreference should work");
  print_exp(backreftree, 0, env);

  backreftree = pexle_backreference(C, env, "foo");
  failif(!backreftree, "backreference should work; does not ensure existence of 'foo'");
  print_exp(backreftree, 0, env);
  
#endif

  /* ----------------------------------------------------------------------------- */
  printf("\n********************** Non-recursive block using anonymous exps \n");

  /* New env for grammar */
  g = env_new(env, NO);
  stat = env_bind(g, 0, env_new_value(Eunspecified_t, 0, NULL), &refA);
  failif(stat < 0, "bind should work");
  stat = env_bind(g, 0, env_new_value(Eunspecified_t, 0, NULL), &refS);
  failif(stat < 0, "bind should work");
  print_env_stats(g);
  print_env(g, C);

  A = pexle_call(C, refS);	/* A -> S */
  printf("A is: "); print_exp(A, 0, NULL);
  S = pexle_from_boolean(C, 1);	/* S -> true */
  printf("S is: "); print_exp(S, 0, NULL);

  print_env(g, C);

  pexle_free(A);
  pexle_free(S);
  
  env_free(g);

  printf("-----\n");

  /* ----------------------------------------------------------------------------- */
  printf("\n********************** Recursive block using anonymous exps \n");

  printf("Trying a single rule that is left recursive due to a self loop\n"); 
  g = env_new(env, YES);
  stat = env_bind(g, 0, env_new_value(Eunspecified_t, 0, NULL), &refA);
  failif(stat < 0, "bind to unspecified should work");
  A = pexle_call(C, ref);		/* A -> A */
  print_exp(A, 0, NULL);
  printf("Expression at refA: "); print_exp(A, 0, NULL);
  pexle_free(A);
  
  printf("-----\n");

  printf("Trying mutually left-recursive rules that are not accessible from start"); 
  env_free(g);
  g = env_new(env, YES);
  stat = env_bind(g, 0, env_new_value(Eunspecified_t, 0, NULL), &refA);
  failif(stat < 0, "bind of unspecified should work");
  stat = env_bind(g, 0, env_new_value(Eunspecified_t, 0, NULL), &refB);
  failif(stat < 0, "bind of novalue should work");
  A = pexle_call(C, refB);		/* A -> B */
  B = pexle_call(C, refA);		/* B -> A */

  printf("Reminder, there is no left recursion check yet.\n");
  
  printf("Expression at refA: "); print_exp(A, 0, NULL);
  printf("Expression at refB: "); print_exp(B, 0, NULL);

  pexle_free(A); pexle_free(B);
  
  env_free(g);
  env_free(env);
  env_free(toplevel);
  symboltable_free(st);
  packagetable_free(m);
  
  printf("-----\nDone.\n");

  return 0;
}

