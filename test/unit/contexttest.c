/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  contexttest.c  Testing compiler context                                  */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "env.h"
#include "compile.h"		/* pexlc_env_free() */

#define NO 0

#define failif(test, msg)						\
  do { if (test) { fprintf(stderr, "Failed %s:%d: %s\n", __FILE__,	\
			   __LINE__, msg);				\
      fflush(stderr);							\
      exit(-1);								\
    }									\
  } while (0)

static int next_power2 (int n) {
  int i = 0;
  failif( n <= 0, "bad arg to next_power2" );
  while((n = n >> 1)) i++;
  return 1 << (i+1);
}

int main(void) {

  Context *C;
  Env *toplevel;

  printf("$$$ contexttest\n");

  /* ----------------------------------------------------------------------------- */

  C = context_new(NULL);
  failif(C, "env arg not allowed to be null");

  toplevel = env_new(NULL, NO);	/* no parent, not recursive */
  failif(!toplevel, "should get fresh env");
  failif(toplevel->size != ENV_INIT_SIZE, "should get fresh env of default size");
  failif(env_has_attr(toplevel, Eattr_recursive), "top level env not recursive");

  C = context_new(toplevel);
  failif(!C, "expected success");
  failif(C->env != toplevel, "checking that env is set");

  failif(!C->stringtab, "should get fresh symbol table");
  failif(C->stringtab->size != (size_t) next_power2(HASHTABLE_MIN_ENTRIES * 100 / HASHTABLE_MAX_LOAD),
	 "should get fresh table of default size");
  failif(C->stringtab->blocksize != HASHTABLE_MIN_ENTRIES * HASHTABLE_AVG_LEN,
	 "block storage should have default size");

  failif(!C->packages, "should get fresh package table");
  failif(C->packages->size != PACKAGETABLE_INITSIZE, "should get fresh table of default size");

  pexlc_env_free(toplevel);
  context_free(C);

  /* ----------------------------------------------------------------------------- */
  printf("\nDone.\n");

  printf("Don't forget to run compiletest next.\n");

  return 0;
}

