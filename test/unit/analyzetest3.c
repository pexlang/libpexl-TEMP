/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  analyzetest3.c  TESTING analyze.c                                        */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#define _GNU_SOURCE		/* for asprintf() */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>		/* exit() */
#include "print.h"
#include "analyze.h"
#include "compile.h"		/* pexlc_env_free() */

#define YES 1
#define NO 0

#define CAPTURES 1
#define HEADFAIL 1
#define NULLABLE 1
#define NOFAIL 1
#define FIRSTSET 1
#define NEEDFOLLOW 1
#define HEADFAIL 1

#define NO_CAPTURES 0
#define NOT_HEADFAIL 0
#define NOT_NULLABLE 0
#define CAN_FAIL 0
#define NO_FIRSTSET 0
#define NO_NEEDFOLLOW 0
#define NO_HEADFAIL 0

#define failif(test, msg)						\
  do { if (test) { fprintf(stderr, "Failed %s:%d: %s\n", __FILE__,	\
			   __LINE__, msg);				\
      fflush(stderr);							\
      exit(-1);								\
    }									\
  } while (0)

#define assert_unspecified_value_at(ref)				\
  do {									\
    b999 = env_get_binding(ref);					\
    failif(bindingtype(b999) != Eunspecified_t, "how can this happen?"); \
  } while (0)

/* Borrowed from analyze.c */
/* Follow ref, extract binding field, return NULL if it is not a Pattern. */
static Pattern *binding_pattern_value (Ref ref) {
  Binding *b = env_get_binding(ref);
  if (!b) return NULL;		/* invalid ref */
  if (b->val.type != Epattern_t) return NULL;
  return (Pattern *) b->val.ptr;
}

static void check_node (Pattern *p,
			int hascaptures,
			int itself_hascaptures,
			int nullable,
			int nofail,
			int headfail,
			int unbounded,
			uint32_t expected_min,
			uint32_t expected_max,
			int needfollowflag,
			Charset firstset,
			int getfirstYES,
			size_t lineno) {
  uint32_t min, max;
  int stat;
  Charset cs;
  unsigned char c;
  Node *node;
  char *msg;
  if (!p->fixed) {
    asprintf(&msg, "Error in check_node: pattern %p has no 'fixed' expression tree\n", (void *) p);
    failif(!p->fixed, msg);
  }
  node = p->fixed->node;
  printf("check_node called on pattern %p from line %lu\n", (void *)p, lineno);
  failif((exp_hascaptures(node) ? 1 : 0) != hascaptures, "this exp captures or not");
  failif((exp_itself_hascaptures(node) ? 1 : 0) != itself_hascaptures, "this pattern itself captures or not (without following calls)");
  stat = exp_patlen(node, &min, &max);
  if (stat != unbounded) {
    if ((stat != EXP_BOUNDED) && (stat != EXP_UNBOUNDED))
      printf("return value from exp_patlen is error: %d\n", stat);
    else
      printf("pattern is %s, which was not expected", (stat==EXP_BOUNDED) ? "bounded" : "unbounded");
  }
  failif(stat != unbounded, "wrong boundedness result");
  if (min != expected_min)
    printf("min len = %u, expected = %u\n", min, expected_min);
  if ((stat == EXP_BOUNDED) && (max != expected_max))
    printf("max len = %u, expected = %u\n", max, expected_max);
  failif(min != expected_min, "wrong min value");
  failif((stat == EXP_BOUNDED) && (max != expected_max), "wrong max value");
  failif((exp_nullable(node) ? 1 : 0) != nullable, "pattern nullable or not");
  failif((exp_nofail(node) ? 1 : 0) != nofail, "pattern can fail or not");
  failif((exp_headfail(node) ? 1 : 0) != headfail, "pattern is headfail or not");
  failif((exp_needfollow(node) ? 1 : 0) != needfollowflag, "does pattern need follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(((stat == 0) ? 0 : 1) == getfirstYES, "stat==0 means firstset can be used as test");
  if (stat == 0) {
    /* Now check the contents of cs for 'h' */
    for (c = 0; c < 255; c++)
      if (testchar(firstset.cs, c)) {
	if (!testchar(cs.cs, c)) printf("charset missing %c\n", c);
	failif(!testchar(cs.cs, c), "firstset should contain this char");
      } else {
	if (testchar(cs.cs, c)) printf("cs should not have %c", c);
	failif(testchar(cs.cs, c), "firstset should not contain this char");
      }
  } /* if stat==0 */
}

int main(void) {

  uint32_t min, max;
  Binding *b999;
  Context *C;

  int stat; 
  CompState *cst; 
  Pattern *p;		  /* USE ONLY FOR REFS, not new allocations */
  Pattern *pstr, *p1, *p2, *p3, *p4, *p5; 
  Env *toplevel, *genv, *env;
  Node *node; 

  Index i; 

  Charset cs;  
  unsigned char c;  

  Expression *tstr; 
  Expression *t1, *t2, *t3, *t4, *t5;  
  Expression *A, *B, *S;

  PackageTable *m;  
  Expression *calltree;
  Ref ref, refKEEP, refA, refB, refS;

  printf("$$$ analyzetest3\n");
  printf("analyzetest3: Testing set_metadata \n\n");

  toplevel = env_new(NULL, NO);
  failif(!toplevel, "new toplevel should succeed");

  C = context_new(toplevel);
  failif(!C, "should succeed");

  /* Error checking */

  stat = set_metadata(NULL);
  failif(stat != EXP_ERR_NULL, "catch null arg error");
  tstr = pexle_from_string(C, "hello");
  failif(!tstr, "expected new expression");
  pstr = pattern_new(tstr, &stat); /* freed by pexlc_env_free(toplevel) */
  failif(!pstr, "expected new pattern");
  failif(stat != 0, "expected new pattern");
  cst = compstate_new();
  failif(!cst, "expected new compstate");
  cst->pat = pstr;
  stat = set_metadata(cst);
  failif(stat != EXP_ERR_INTERNAL, "catch null graph field");

  pstr->env = toplevel;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success, as no calls to fix");
  stat = build_cfgraph(pstr, &(cst->g));
  failif(stat != 0, "expected cfgraph");
  failif(!cst->g, "expected cfgraph");
  stat = set_metadata(cst);
  failif(stat != EXP_ERR_NULL, "catch null graph order field");
  stat = sort_cfgraph(cst->g);
  failif(stat != 0, "expected sorted cfgraph");

  pexlc_env_free(toplevel);

  /* ----------------------------------------------------------------------------- */
  printf("********************** Set non-recursive metadata \n");


  printf("Using literal string 'hello'\n");
  tstr = pexle_from_string(C, "hello");
  failif(!tstr, "expected new expression");
  pattern_free(pstr);
  pstr = pattern_new(tstr, &stat); /* freed later by pexlc_env_free(toplevel) */
  failif(!pstr, "expected new pattern");
  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = pstr;
  toplevel = env_new(NULL, NO);
  failif(!toplevel, "new toplevel should succeed");
  pstr->env = toplevel;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success, as no calls to fix");
  stat = build_cfgraph(pstr, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  failif(cst->g->next != 1, "size of dependency graph should be 1 vertex");

  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success, as no calls to fix");

  memset(&cs, 0, CHARSETSIZE);
  setchar(cs.cs, 'h');
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NOT_NULLABLE, CAN_FAIL, NO_HEADFAIL, EXP_BOUNDED, 5, 5, NO_NEEDFOLLOW, cs, FIRSTSET, __LINE__);

  stat = sort_cfgraph(cst->g);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");

  printf("Calling pattern for literal string 'hello'\n");
  failif(!pstr, "precondition for next test");
  stat = env_bind(toplevel, 123, env_new_value(Epattern_t, 0, (void *)pstr), &ref);
  failif(stat != 0, "expected bind to succeed");
  failif(ref_not_found(ref), "expected a valid ref");
  failif(ref.env != toplevel, "ref should point to toplevel where binding lives");

  t1 = pexle_call(C, ref);	/* call tstr */
  failif(!tstr, "expected new expression");

  refKEEP = ref;

  /* Wrap the call in a new pattern struct */
  p1 = pattern_new(t1, &stat);
  failif(!p1, "expected new pattern");
  failif(stat != 0, "expected new pattern");

  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p1;
  p1->env = toplevel;

  cfgraph_free(cst->g);

  printf("Ensuring that build_cfgraph catches certain errors\n");
  stat = build_cfgraph(p1, &(cst->g));
  failif(stat != EXP_ERR_INTERNAL, "have not called fix_open_calls() which sets p1->fixed"); 

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;
  printf("Fixing open calls\n");
  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");

/*   printf("Note: toplevel is %p\n", (void *) toplevel); */
/*   printf("Note: toplevel->package is %p\n", (void *) toplevel->package); */
/*   printf("Note: p1->env is %p\n", (void *) p1->env); */
/*   printf("Note: cst->pat->env is %p\n", (void *) cst->pat->env); */

  printf("Using build_cfgraph correctly to operate on TCall\n");
  stat = build_cfgraph(p1, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  failif(cst->g->next != 2, "size of dependency graph should be 2 (p1 and its call target)");
  /* Check the contents of the deplist for p1 */
  i = 0;
  do {
    p = cst->g->deplists[0][i++];
    printf("[0][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  stat = sort_cfgraph(cst->g);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");

  stat = set_metadata(cst);
  failif(stat != 0, "expected success");

  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NOT_NULLABLE, CAN_FAIL, NO_HEADFAIL, EXP_BOUNDED, 5, 5, NO_NEEDFOLLOW, cs, FIRSTSET, __LINE__);

  p = binding_pattern_value(refKEEP);
  failif(!p, "should get our call target pattern back");

  failif(cst->g->deplists[0][0] != p1, "vertex p1");
  failif(cst->g->deplists[0][1] != p, "call target");
  failif(cst->g->deplists[0][2] != NULL, "NULL terminator");

  /* Check the contents of the deplist for the call target, p */
  i = 0;
  do {
    p = cst->g->deplists[1][i++];
    printf("[1][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(refKEEP);
  failif(!p, "should get our call target pattern back");

  failif(cst->g->deplists[1][0] != p, "vertex p");
  failif(cst->g->deplists[1][1] != NULL, "NULL terminator");

  check_node(p, NO_CAPTURES, NO_CAPTURES, NOT_NULLABLE, CAN_FAIL, NO_HEADFAIL, EXP_BOUNDED, 5, 5, NO_NEEDFOLLOW, cs, FIRSTSET, __LINE__);

  pattern_free(p1);

  /* ----------------------------------------------------------------------------- */
  printf("********************** Using combinator 'seq' \n");

  t1 = pexle_call(C, refKEEP);		  /* "hello" */
  t2 = pexle_from_string(C, "a");		  /* "a" */
  t3 = pexle_seq(C, t1, t2);		  /* "hello" "a" */
  failif(!t3, "should succeed");

  /* Wrap the call in a new pattern struct */
  p3 = pattern_new(t3, &stat);
  failif(!p3, "expected new pattern");
  failif(stat != 0, "expected new pattern");
  p3->env = toplevel;

  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p3;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;
  printf("Fixing open calls\n");
  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");

  printf("Building dependency graph\n");
  stat = build_cfgraph(p3, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  failif(cst->g->next != 2, "size of dependency graph should be 2 (p3 and its call target)");
  /* Check the contents of the deplist for p3 */
  i = 0;
  do {
    p = cst->g->deplists[0][i++];
    printf("[0][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(refKEEP);
  failif(!p, "should get our call target pattern back");

  failif(cst->g->deplists[0][0] != p3, "vertex p3");
  failif(cst->g->deplists[0][1] != p, "call target");
  failif(cst->g->deplists[0][2] != NULL, "NULL terminator");

  /* Check the contents of the deplist for the call target, p */
  i = 0;
  do {
    p = cst->g->deplists[1][i++];
    printf("[1][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(refKEEP);
  failif(!p, "should get our call target pattern back");

  failif(cst->g->deplists[1][0] != p, "vertex p");
  failif(cst->g->deplists[1][1] != NULL, "NULL terminator");

  stat = sort_cfgraph(cst->g);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");

  stat = set_metadata(cst);
  failif(stat != 0, "expected success");

  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NOT_NULLABLE, CAN_FAIL, NO_HEADFAIL, EXP_BOUNDED, 6, 6, NO_NEEDFOLLOW, cs, FIRSTSET, __LINE__);

  /* ----------------------------------------------------------------------------- */
  printf("********************** Using combinator 'choice' \n");

  /* Choice of t2 and t3, where t3 is a seq that makes a call to p */
  t4 = pexle_choice(C, t2, t3);	/* "a" / {"hello" "a"} */
  failif(t4->node->tag != TChoice, "should get TChoice");
  node = t4->node;
  failif(!node, "precondition for next tests");

  /* Wrap the call in a new pattern struct */
  p4 = pattern_new(t4, &stat);
  failif(!p4, "expected new pattern");
  failif(stat != 0, "expected new pattern");
  p4->env = toplevel;

  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p4;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;
  printf("Fixing open calls\n");
  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");

  printf("Building dependency graph\n");
  stat = build_cfgraph(p4, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  failif(cst->g->next != 2, "size of dependency graph should be 2 (p4 and its call target)");
  /* Check the contents of the deplist */
  i = 0;
  do {
    p = cst->g->deplists[0][i++];
    printf("[0][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(refKEEP);
  failif(!p, "should get our call target pattern back");

  failif(cst->g->deplists[0][0] != p4, "vertex p4");
  failif(cst->g->deplists[0][1] != p, "call target");
  failif(cst->g->deplists[0][2] != NULL, "NULL terminator");

  /* Check the contents of the deplist for the call target, p */
  i = 0;
  do {
    p = cst->g->deplists[1][i++];
    printf("[1][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(refKEEP);
  failif(!p, "should get our call target pattern back");

  failif(cst->g->deplists[1][0] != p, "vertex p");
  failif(cst->g->deplists[1][1] != NULL, "NULL terminator");

  stat = sort_cfgraph(cst->g);
  failif(stat != 0, "expected success");
  stat = set_metadata(cst);
  failif(stat != 0, "expected success");

  setchar(cs.cs, 'a');
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NOT_NULLABLE, CAN_FAIL, NO_HEADFAIL, EXP_BOUNDED, 1, 6, NEEDFOLLOW, cs, FIRSTSET, __LINE__);

  /* Choice of t1 and t3, where t1 calls p and p3 is a seq that makes a call to p */
  t4 = pexle_choice(C, t1, t3);	/* "hello" / {"hello" "a"} */
  failif(t4->node->tag != TChoice, "should get TChoice");
  node = t4->node;
  failif(!node, "precondition for next tests");

  /* Wrap the call in a new pattern struct */
  pattern_free(p4);
  p4 = pattern_new(t4, &stat);
  failif(!p4, "expected new pattern");
  failif(stat != 0, "expected new pattern");
  p4->env = toplevel;

  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p4;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;
  printf("Fixing open calls\n");
  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");

  printf("Building dependency graph\n");
  stat = build_cfgraph(p4, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  failif(cst->g->next != 2, "size of dependency graph should be 2 (p4 and its call target)");
  /* Check the contents of the deplist for p3 */
  i = 0;
  do {
    p = cst->g->deplists[0][i++];
    printf("[0][%2d]  pattern %p\n", i, (void *) p);
/*     print_pattern(p); */
  } while (p);

  p = binding_pattern_value(refKEEP);
  failif(!p, "should get our call target pattern back");

  failif(cst->g->deplists[0][0] != p4, "vertex p4");
  failif(cst->g->deplists[0][1] != p, "call target");
  failif(cst->g->deplists[0][2] != p, "second instance of same call target");
  failif(cst->g->deplists[0][3] != NULL, "NULL terminator");

  /* Check the contents of the deplist for the call target, p */
  i = 0;
  do {
    p = cst->g->deplists[1][i++];
    printf("[1][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(refKEEP);
  failif(!p, "should get our call target pattern back");

  failif(cst->g->deplists[1][0] != p, "vertex p");
  failif(cst->g->deplists[1][1] != NULL, "NULL terminator");

  stat = sort_cfgraph(cst->g);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");

  stat = set_metadata(cst);
  failif(stat != 0, "expected success");

  memset(&cs, 0, CHARSETSIZE);
  setchar(cs.cs, 'h');
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NOT_NULLABLE, CAN_FAIL, NO_HEADFAIL, EXP_BOUNDED, 5, 6, NEEDFOLLOW, cs, FIRSTSET, __LINE__);

  /* ----------------------------------------------------------------------------- */
  printf("********************** Using combinator 'not' \n");

  /* t5 = not t4 */
  t5 = pexle_not(C, t4); 	/* !{"hello" / {"hello" "a"}} */
  failif(t5->node->tag != TNot, "should get TNot");
  node = t5->node;
  failif(!node, "precondition for next tests");

  /* Wrap the call in a new pattern struct */
  p5 = pattern_new(t5, &stat);
  failif(!p5, "expected new pattern");
  failif(stat != 0, "expected new pattern");
  p5->env = toplevel;

  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p5;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  printf("Fixing open calls\n");
  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");

  printf("Building dependency graph\n");
  stat = build_cfgraph(p5, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  failif(cst->g->next != 2, "size of dependency graph should be 2 (p4 and its call target)");
  /* Check the contents of the deplist for p3 */
  i = 0;
  do {
    p = cst->g->deplists[0][i++];
    printf("[0][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(refKEEP);
  failif(!p, "should get our call target pattern back");

  failif(cst->g->deplists[0][0] != p5, "vertex p5");
  failif(cst->g->deplists[0][1] != p, "call target");
  failif(cst->g->deplists[0][2] != p, "second instance of same call target");
  failif(cst->g->deplists[0][3] != NULL, "NULL terminator");

  /* Check the contents of the deplist for the call target, p */
  i = 0;
  do {
    p = cst->g->deplists[1][i++];
    printf("[1][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(refKEEP);
  failif(!p, "should get our call target pattern back");

  failif(cst->g->deplists[1][0] != p, "vertex p");
  failif(cst->g->deplists[1][1] != NULL, "NULL terminator");

  stat = sort_cfgraph(cst->g);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");

  stat = set_metadata(cst);
  failif(stat != 0, "expected success");

  print_pattern(cst->pat, 0, C);
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NULLABLE, CAN_FAIL, NO_HEADFAIL, EXP_BOUNDED, 0, 0, NO_NEEDFOLLOW, cs, NO_FIRSTSET, __LINE__);

  pattern_free(p3);
  pattern_free(p4);
  pattern_free(p5);
  pexlc_env_free(toplevel);

  /* ----------------------------------------------------------------------------- */
  printf("********************** Testing dynamic expansion of cfgraph (ensure_vertex_space)' \n");

  pexle_free(t1); pexle_free(t2);
  compstate_free(cst);
  cst = compstate_new();

  toplevel = env_new(NULL, NO); 
  failif(!toplevel, "new toplevel should succeed"); 

  /* Construct a long call chain that ends in a call to tstr */
  tstr = pexle_from_string(C, "hello");
  failif(!tstr, "expected new expression");
  t1 = tstr;
  for(i = 0; i < (3 * EXP_CFG_INITIAL_SIZE); i++) {
    p1 = pattern_new(t1, &stat);
    failif(!p1, "expected new pattern");
    p1->env = toplevel;
    stat = env_bind(toplevel, i+900, env_new_value(Epattern_t, 0, (void *)p1), &ref);
    failif(stat != 0, "expected bind to succeed");
    failif(ref_not_found(ref), "expected a valid ref");
    failif(ref.env != toplevel, "ref should point to toplevel where binding lives");
    t2 = pexle_call(C, ref);
    failif(!t2, "should succeed");
    t1 = t2;
  }
  printf("Toplevel env is: "); print_env(toplevel, C);
  printf("Expression is: "); print_exp(t2, 1, NULL);
  printf("Entire call chain is: \n");
  i = 1;
  t4 = t2;
  while (t4->node->tag == TOpenCall) {
    set_ref_from_node(ref, t4->node);
    p = binding_pattern_value(ref);
    printf("%5d: exp %p calls exp %p\n", i, (void *) t4, (void *) p->tree);
    t4 = p->tree; i++;
  }
  if (t4->node->tag == TSeq) 
    printf("%5d: exp %p has tag TSeq\n", i, (void *) t4);
  else {
    printf("%5d: exp %p has unexpected tag %d", i, (void *) t4, t4->node->tag);
    failif(0, "Unexpected tag at the first non-call");
  }
  failif(i != (3 * EXP_CFG_INITIAL_SIZE) + 1, "Wrong length of call chain");

  p2 = pattern_new(t2, &stat);
  failif(!p2, "expected new pattern");
  p2->env = toplevel;		/* compilation env */
/*   printf("+++ about to free cst->pat\n"); */
/*   pattern_free(cst->pat); */
  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p2;
  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;
  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");
  stat = build_cfgraph(p2, &(cst->g));
  printf("stat = %d\n", stat);
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  printf("size = %u\n", cst->g->next);
  failif(cst->g->next != (3 * EXP_CFG_INITIAL_SIZE) + 1, "wrong size of dependency graph");

  stat = sort_cfgraph(cst->g);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");

  stat = set_metadata(cst);
  failif(stat != 0, "expected success");

  print_pattern(cst->pat, 0, C);
  /* patlen 5 is length of "hello" */
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NOT_NULLABLE, CAN_FAIL, NOT_HEADFAIL, EXP_BOUNDED, 5, 5, NO_NEEDFOLLOW, cs, FIRSTSET, __LINE__);

  p1 = NULL; p2 = NULL; p3 = NULL; p4 = NULL; p5 = NULL;
  pstr = NULL; p = NULL;
  t1 = NULL; t2 = NULL; t3 = NULL; t4 = NULL; t5 = NULL;
  tstr = NULL; 

  /* ----------------------------------------------------------------------------- */
  printf("********************** Testing dynamic expansion of deplist (ensure_deplist_space)' \n");

  pexlc_env_free(toplevel); 
  toplevel = env_new(NULL, NO); 
  failif(!toplevel, "new toplevel should succeed"); 
  printf("+++ about to free cst->pat\n");
  pattern_free(cst->pat);
  compstate_free(cst);
  cst = compstate_new();

  /* Construct a pattern that calls tstr many times */
  tstr = pexle_from_string(C, "hello");
  failif(!tstr, "expected new expression");
  pstr = pattern_new(tstr, &stat); /* will be freed by pexlc_env_free(toplevel) */
  failif(!pstr, "expected new pattern");
  pstr->env = toplevel;
  stat = env_bind(toplevel, i+900, env_new_value(Epattern_t, 0, (void *)pstr), &ref);
  failif(stat != 0, "expected bind to succeed");
  failif(ref_not_found(ref), "expected a valid ref");
  failif(ref.env != toplevel, "ref should point to toplevel where binding lives");
  t2 = pexle_call(C, ref);
  failif(!t2, "should succeed");
  for(i = 0; i < (3 * EXP_DEPLIST_INITIAL_SIZE); i++) {
    t2 = pexle_seq_f(C, pexle_call(C, ref), t2);
    failif(!t2, "should succeed");
  }
  printf("Toplevel env is: "); print_env(toplevel, C);
  printf("Expression is: "); print_exp(t2, 1, NULL);

  p2 = pattern_new(t2, &stat);	/* will be freed explicitly */
  failif(!p2, "expected new pattern");
  p2->env = toplevel;		/* compilation env */
  printf("+++ about to free cst->pat\n");
  pattern_free(cst->pat);
  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p2;
  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;
  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");
  stat = build_cfgraph(p2, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  failif(cst->g->next != 2, "wrong size of dependency graph");
  i = 1;
  while (cst->g->deplists[0][i]) i++;
  i--;			   /* do not count dl[0], the vertex itself */
  failif(i != (3 * EXP_DEPLIST_INITIAL_SIZE) + 1, "wrong size of dependency list");

  stat = sort_cfgraph(cst->g);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");

  stat = set_metadata(cst);
  failif(stat != 0, "expected success");

  print_pattern(cst->pat, 0, C);
  /* patlen 5 is length of "hello" */
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NOT_NULLABLE, CAN_FAIL, NOT_HEADFAIL,
	     EXP_BOUNDED,
	     5 * (3 * EXP_DEPLIST_INITIAL_SIZE + 1), /* min */
	     5 * (3 * EXP_DEPLIST_INITIAL_SIZE + 1), /* max */
	     NO_NEEDFOLLOW, cs, FIRSTSET, __LINE__);

  pattern_free(p2);  
  
  /* ----------------------------------------------------------------------------- */
  printf("********************** Using combinator 'lookahead' \n");

  t1 = pexle_from_string(C, "A");
  t2 = pexle_lookahead(C, t1);
  failif(t2->len != 1 + t1->len, "wrong size tree");
  failif(t2->node->tag != TAhead, "TAhead");
  node = t2->node;
  failif(!node, "precondition for next tests");

  /* p2 is (lookahead (fromstring "A")) */
  p2 = pattern_new(t2, &stat);	/* will be freed by pexlc_env_free(toplevel) */
  failif(!p2, "expected new pattern");
  p2->env = toplevel;		/* compilation env */
  stat = env_bind(toplevel, 85000, env_new_value(Epattern_t, 0, (void *)p2), &ref);
  failif(ref_not_found(ref), "expected bind to succeed");

  failif(p2->fixed, "not yet run fix_open_calls, which is precondition for next test");

  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p2;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");

  memset(&cs, 0, CHARSETSIZE);
  setchar(cs.cs, 'A');
  check_node(p2, NO_CAPTURES, NO_CAPTURES, NULLABLE, CAN_FAIL, HEADFAIL, EXP_BOUNDED, 0, 0, NO_NEEDFOLLOW, cs, FIRSTSET, __LINE__);

  t3 = pexle_call(C, ref);
  failif(!t3, "expected success");
  
  p3 = pattern_new(t3, &stat);	/* will be freed explicitly */
  failif(!p3, "expected new pattern");
  p3->env = toplevel;		/* compilation env */

  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p3;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");
  stat = build_cfgraph(p3, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  failif(cst->g->next != 2, "wrong size of dependency graph");
  i = 1;
  while (cst->g->deplists[0][i]) i++;
  i--;			   /* do not count dl[0], the vertex itself */
  failif(i != 1, "wrong size of dependency list");

  stat = sort_cfgraph(cst->g);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");

  stat = set_metadata(cst);
  failif(stat != 0, "expected success");

  print_pattern(cst->pat, 0, C);
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NULLABLE, CAN_FAIL, HEADFAIL, EXP_BOUNDED, 0, 0, NO_NEEDFOLLOW, cs, NO_FIRSTSET, __LINE__);

  pattern_free(p3);

  /* ----------------------------------------------------------------------------- */
  printf("********************** Using combinator 'rep' \n");

  pexle_free(t1);

  t1 = pexle_from_string(C, "A");
  t2 = pexle_repeat(C, t1, 0);	/* "A"* */
  failif(t2->len != 1 + t1->len, "wrong size tree");
  failif(t2->node->tag != TRep, "TRep");
  node = t2->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp does not capture");
  failif(exp_itself_hascaptures(node), "this exp does not capture");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_UNBOUNDED, "expected UNBOUNDED");
  failif((min != 0), "wrong min");
  failif(exp_nullable(node) != 1, "exp is nullable");
  failif(exp_nofail(node) != 1, "exp is nofail");
  failif(exp_headfail(node) != 0, "exp is NOT headfail");
  failif(!exp_needfollow(node), "exp would benefit from follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 1, "firstset cannot be used as test, exp accepts epsilon");
  /* Now check the contents of cs, which should be a full set */
  for (c = 0; c < 255; c++) 
    failif(!testchar(cs.cs, c), "cs should contain all chars"); 

  p2 = pattern_new(t2, &stat);	/* will be freed explicitly */
  failif(!p2, "expected new pattern");
  p2->env = toplevel;		/* compilation env */
  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p2;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");
  stat = build_cfgraph(p2, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  
  stat = sort_cfgraph(cst->g);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");

  stat = set_metadata(cst);
  failif(stat != 0, "expected success");

  print_pattern(cst->pat, 0, C);
  memset(&cs, 0, CHARSETSIZE);
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NULLABLE, NOFAIL, NO_HEADFAIL, EXP_UNBOUNDED, 0, 123, NEEDFOLLOW, cs, NO_FIRSTSET, __LINE__);

  t3 = pexle_repeat(C, t1, 3);	/* "A"{3,} */
  failif(t3->len != 7 + t1->len, "wrong size tree");
  failif(t3->node->tag != TSeq, "TSeq");
  node = t3->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp does not capture");
  failif(exp_itself_hascaptures(node), "this exp does not capture");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_UNBOUNDED, "expected UNBOUNDED");
  failif((min != 3), "wrong min");
  failif(exp_nullable(node) != 0, "exp is NOT nullable");
  failif(exp_nofail(node) != 0, "exp is NOT nofail");
  failif(exp_headfail(node) != 0, "exp is NOT headfail");
  failif(!exp_needfollow(node), "exp would benefit from follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 0, "firstset can be used as test, does not accept epsilon");

  p3 = pattern_new(t3, &stat);	/* will be freed explicitly */
  failif(!p3, "expected new pattern");
  p3->env = toplevel;		/* compilation env */
  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p3;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");
  stat = build_cfgraph(p3, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  
  stat = sort_cfgraph(cst->g);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");

  stat = set_metadata(cst);
  failif(stat != 0, "expected success");

  print_pattern(cst->pat, 0, C);
  memset(&cs, 0, CHARSETSIZE);
  setchar(cs.cs, 'A');
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NOT_NULLABLE, CAN_FAIL, NOT_HEADFAIL, EXP_UNBOUNDED, 3, 123, NEEDFOLLOW, cs, FIRSTSET, __LINE__);

  pexle_free(t1);
  
  t1 = pexle_from_string(C, "A");
  t2 = pexle_repeat(C, t1, -1);	/* "A"? ==> "A" / TTrue */
  failif(t2->len != 2 + t1->len, "wrong size tree");
  failif(t2->node->tag != TChoice, "TChoice");
  node = t2->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp does not capture");
  failif(exp_itself_hascaptures(node), "this exp does not capture");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_BOUNDED, "expected BOUNDED");
  failif((min != 0) || (max != 1), "wrong min/max value");
  failif(exp_nullable(node) != 1, "exp is nullable");
  failif(exp_nofail(node) != 1, "exp is nofail");
  failif(exp_headfail(node) != 0, "exp is NOT headfail");
  failif(!exp_needfollow(node), "exp would benefit from follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 1, "firstset cannot be used as test, exp accepts epsilon");
  /* Now check the contents of cs */
  for (c = 0; c < 255; c++) 
    failif(!testchar(cs.cs, c), "cs should contain all chars"); 

  pattern_free(p2);
  pattern_free(p3);

  p2 = pattern_new(t2, &stat);	/* will be freed explicitly */
  failif(!p2, "expected new pattern");
  p2->env = toplevel;		/* compilation env */
  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p2;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");
  stat = build_cfgraph(p2, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  
  stat = sort_cfgraph(cst->g);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");

  stat = set_metadata(cst);
  failif(stat != 0, "expected success");

  print_pattern(cst->pat, 0, C);
  memset(&cs, 0, CHARSETSIZE);
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NULLABLE, NOFAIL, NOT_HEADFAIL, EXP_BOUNDED, 0, 1, NEEDFOLLOW, cs, NO_FIRSTSET, __LINE__);

  t3 = pexle_repeat(C, t1, -3);	/* "A"{0,3} */
  failif(t3->len != 10 + t1->len, "wrong size tree");
  failif(t3->node->tag != TChoice, "TChoice");
  node = t3->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp does not capture");
  failif(exp_itself_hascaptures(node), "this exp does not capture");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_BOUNDED, "expected BOUNDED");
  failif((min != 0) || (max != 3), "wrong min/max value");
  failif(exp_nullable(node) != 1, "exp is nullable");
  failif(exp_nofail(node) != 1, "exp is nofail");
  failif(exp_headfail(node) != 0, "exp is NOT headfail");
  failif(!exp_needfollow(node), "exp would benefit from follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 1, "firstset cannot be used as test, exp accepts epsilon");
  /* Now check the contents of cs */
  for (c = 0; c < 255; c++) 
    failif(!testchar(cs.cs, c), "cs should contain all chars"); 
  pattern_free(p2);

  p3 = pattern_new(t3, &stat);	/* will be freed explicitly */
  failif(!p3, "expected new pattern");
  p3->env = toplevel;		/* compilation env */
  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p3;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");
  stat = build_cfgraph(p3, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  
  stat = sort_cfgraph(cst->g);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");

  stat = set_metadata(cst);
  failif(stat != 0, "expected success");

  print_pattern(cst->pat, 0, C);
  memset(&cs, 0, CHARSETSIZE);
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NULLABLE, NOFAIL, NO_HEADFAIL, EXP_BOUNDED, 0, 3, NEEDFOLLOW, cs, NO_FIRSTSET, __LINE__);

  pattern_free(p3);
  
  /* ----------------------------------------------------------------------------- */
  printf("********************** Using combinator 'subtract' \n");

  pexle_free(t1);

  /* "A"-"XYZ" ==> (seq (not "XYZ") "A") */
  t1 = pexle_from_string(C, "A");
  t2 = pexle_from_string(C, "XYZ");
  t3 = pexle_subtract(C, t1, t2);
  failif(t3->len != t1->len + t2->len + 2, "wrong size tree");
  failif(t3->node->tag != TSeq, "TSeq");
  node = t3->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp does not capture");
  failif(exp_itself_hascaptures(node), "this exp does not capture");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_BOUNDED, "expected BOUNDED");
  failif((min != 1) || (max != 1), "wrong min/max value");
  failif(exp_nullable(node) != 0, "exp is NOT nullable");
  failif(exp_nofail(node) != 0, "exp is NOT nofail");
  failif(exp_headfail(node) != 0, "exp is NOT headfail");
  failif(exp_needfollow(node), "exp would NOT benefit from follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 0, "firstset can be used as test, does not accept epsilon");
  /* Now check the contents of cs */
  for (c = 0; c < 255; c++) 
    if (c == 'A') 
      failif(!testchar(cs.cs, c), "cs should contain A"); 
    else 
      failif(testchar(cs.cs, c), "cs should contain only A"); 

  p3 = pattern_new(t3, &stat);	/* will be freed explicitly */
  failif(!p3, "expected new pattern");
  p3->env = toplevel;		/* compilation env */
  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p3;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");
  stat = build_cfgraph(p3, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  
  stat = sort_cfgraph(cst->g);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");

  stat = set_metadata(cst);
  failif(stat != 0, "expected success");

  print_pattern(cst->pat, 0, C);
  memset(&cs, 0, CHARSETSIZE);
  setchar(cs.cs, 'A');
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NOT_NULLABLE, CAN_FAIL, NO_HEADFAIL, EXP_BOUNDED, 1, 1, NO_NEEDFOLLOW, cs, FIRSTSET, __LINE__);

  pattern_free(p3);
  
  /* ----------------------------------------------------------------------------- */
  printf("********************** Using combinator 'lookbehind' \n");

  pexle_free(t1); pexle_free(t2);

  printf("Look back at string literal\n");
  t1 = pexle_from_string(C, "A");
  t2 = pexle_lookbehind_f(C, t1);
  failif(t2->len != 2, "wrong size tree");
  failif(t2->node->tag != TBehind, "TBehind");
  node = t2->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp does not capture");
  failif(exp_itself_hascaptures(node), "this exp does not capture");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_BOUNDED, "expected BOUNDED");
  failif((min != 0) || (max != 0), "wrong min/max value");
  failif(exp_nullable(node) != 1, "exp is nullable");
  failif(exp_nofail(node) != 0, "exp is NOT nofail");
  failif(exp_headfail(node) != 0, "exp is NOT headfail");
  failif(exp_needfollow(node), "exp would NOT benefit from follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 1, "firstset cannot be used as test, exp accepts epsilon");
  /* Now check the contents of cs */
  for (c = 0; c < 255; c++) 
    failif(!testchar(cs.cs, c), "cs should contain all chars"); 

  p2 = pattern_new(t2, &stat);	/* will be freed explicitly */
  failif(!p2, "expected new pattern");
  p2->env = toplevel;		/* compilation env */
  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p2;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");
  stat = build_cfgraph(p2, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  
  stat = sort_cfgraph(cst->g);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");

  stat = set_metadata(cst);
  failif(stat != 0, "expected success");

  print_pattern(cst->pat, 0, C);
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NULLABLE, CAN_FAIL, NO_HEADFAIL, EXP_BOUNDED, 0, 0, NO_NEEDFOLLOW, cs, NO_FIRSTSET, __LINE__);

  printf("Look back at \"A\"? which has variable length\n");
  t3 = pexle_lookbehind_f(C, pexle_repeat_f(C, pexle_from_string(C, "A"), -1));
  failif(!t3, "the check for fixedlen happens later");
  node = t3->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp does not capture");
  failif(exp_itself_hascaptures(node), "this exp does not capture");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_BOUNDED, "expected BOUNDED");
  failif((min != 0) || (max != 0), "wrong min/max value");
  printf("This exp will fail to compile, because the target of the lookbehind has variable length.\n");

  failif(exp_nullable(node) != 1, "exp is nullable");
  failif(exp_nofail(node) != 0, "exp is NOT nofail");
  failif(exp_headfail(node) != 0, "exp is NOT headfail");
  failif(exp_needfollow(node), "exp would NOT benefit from follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 1, "firstset cannot be used as test, exp accepts epsilon");
  /* Now check the contents of cs */
  for (c = 0; c < 255; c++) 
    failif(!testchar(cs.cs, c), "cs should contain all chars"); 

  pattern_free(p2);
  
  p3 = pattern_new(t3, &stat);	/* will be freed explicitly */
  failif(!p3, "expected new pattern");
  p3->env = toplevel;		/* compilation env */
  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p3;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");
  stat = build_cfgraph(p3, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  
  stat = sort_cfgraph(cst->g);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");

  stat = set_metadata(cst);
  failif(stat != 0, "expected success");

  print_pattern(cst->pat, 0, C);
/*   memset(&cs, 0, CHARSETSIZE); */
  setchar(cs.cs, 'A');
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NULLABLE, CAN_FAIL, NO_HEADFAIL, EXP_BOUNDED, 0, 0, NO_NEEDFOLLOW, cs, NO_FIRSTSET, __LINE__);

  pattern_free(p3);

  printf("Look back at \"AB\"/\"CD\" which has length 2\n");
  t3 = pexle_choice_f(C, pexle_from_string(C, "AB"), pexle_from_string(C, "CD"));
  t4 = pexle_lookbehind_f(C, t3);
/*   print_exp(t4, 0, NULL); */
  failif(!t4, "we do not check (now) for the acceptable lookbehind targets until compilation");
  failif(t4->node->tag != TBehind, "TBehind");
  node = t4->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp does not capture");
  failif(exp_itself_hascaptures(node), "this exp does not capture");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_BOUNDED, "expected BOUNDED");
  failif((min != 0) || (max != 0), "wrong min/max value");

  printf("This exp will compile, because the target of the lookbehind has fixed length.\n");
  stat = exp_patlen(child1(node), &min, &max);
  failif(stat != EXP_BOUNDED, "expected BOUNDED");
  failif((min != 2) || (max != 2), "wrong min/max value");

  failif(exp_nullable(node) != 1, "exp is nullable");
  failif(exp_nofail(node) != 0, "exp is NOT nofail");
  failif(exp_headfail(node) != 0, "exp is NOT headfail");
  failif(exp_needfollow(node), "exp would NOT benefit from follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 1, "firstset cannot be used as test, exp accepts epsilon");
  /* Now check the contents of cs */
  for (c = 0; c < 255; c++) 
    failif(!testchar(cs.cs, c), "cs should contain all chars"); 

  p4 = pattern_new(t4, &stat);	/* will be freed explicitly */
  failif(!p4, "expected new pattern");
  p4->env = toplevel;		/* compilation env */
  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p4;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");
  stat = build_cfgraph(p4, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  
  stat = sort_cfgraph(cst->g);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");

  stat = set_metadata(cst);
  failif(stat != 0, "expected success");

  print_pattern(cst->pat, 0, C);
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NULLABLE, CAN_FAIL, NO_HEADFAIL, EXP_BOUNDED, 0, 0, NO_NEEDFOLLOW, cs, NO_FIRSTSET, __LINE__);
  
  pattern_free(p4);

  /* ----------------------------------------------------------------------------- */
  printf("********************** Capture \n");

  t1 = pexle_from_string(C, "AB");
  t2 = pexle_capture_f(C, "hi", t1);
  node = t2->node;
  failif(!node, "precondition for next tests");

  failif(!exp_hascaptures(node), "this exp captures");
  failif(!exp_itself_hascaptures(node), "this exp captures");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_BOUNDED, "expected BOUNDED");
  failif((min != 2) || (max != 2), "wrong min/max value");

  failif(exp_nullable(node) != 0, "exp is NOT nullable");
  failif(exp_nofail(node) != 0, "exp is NOT nofail");
  failif(exp_headfail(node) != 0, "exp is NOT headfail");
  failif(exp_needfollow(node), "exp would NOT benefit from follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 0, "firstset can be used as test, does not accept epsilon");
  /* Now check the contents of cs */
  for (c = 0; c < 255; c++) 
    if (c == 'A') 
      failif(!testchar(cs.cs, c), "cs should contain only A"); 
    else 
      failif(testchar(cs.cs, c), "cs should contain only A"); 

  p2 = pattern_new(t2, &stat);	/* will be freed explicitly */
  failif(!p2, "expected new pattern");
  p2->env = toplevel;		/* compilation env */
  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p2;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");
  stat = build_cfgraph(p2, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  
  stat = sort_cfgraph(cst->g);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");

  stat = set_metadata(cst);
  failif(stat != 0, "expected success");

  print_pattern(cst->pat, 0, C);
  memset(&cs, 0, CHARSETSIZE);
  setchar(cs.cs, 'A');
  check_node(cst->pat, CAPTURES, CAPTURES, NOT_NULLABLE, CAN_FAIL, NOT_HEADFAIL, EXP_BOUNDED, 2, 2, NO_NEEDFOLLOW, cs, FIRSTSET, __LINE__);

  t3 = pexle_seq(C, t2, t2);
  node = t3->node;
  failif(!node, "precondition for next tests");

  failif(!exp_hascaptures(node), "this exp captures");
  failif(!exp_itself_hascaptures(node), "this exp captures");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_BOUNDED, "expected BOUNDED");
  failif((min != 4) || (max != 4), "wrong min/max value");

  failif(exp_nullable(node) != 0, "exp is NOT nullable");
  failif(exp_nofail(node) != 0, "exp is NOT nofail");
  failif(exp_headfail(node) != 0, "exp is NOT headfail");
  failif(exp_needfollow(node), "exp would NOT benefit from follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 0, "firstset can be used as test, does not accept epsilon");
  /* Now check the contents of cs */
  for (c = 0; c < 255; c++) 
    if (c == 'A') 
      failif(!testchar(cs.cs, c), "cs should contain only A"); 
    else 
      failif(testchar(cs.cs, c), "cs should contain only A"); 

  pattern_free(p2);

  p3 = pattern_new(t3, &stat);	/* will be freed explicitly */
  failif(!p3, "expected new pattern");
  p3->env = toplevel;		/* compilation env */
  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p3;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");
  stat = build_cfgraph(p3, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  
  stat = sort_cfgraph(cst->g);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");

  stat = set_metadata(cst);
  failif(stat != 0, "expected success");

  print_pattern(cst->pat, 0, C);
  memset(&cs, 0, CHARSETSIZE);
  setchar(cs.cs, 'A');
  check_node(cst->pat, CAPTURES, CAPTURES, NOT_NULLABLE, CAN_FAIL, NOT_HEADFAIL, EXP_BOUNDED, 4, 4, NO_NEEDFOLLOW, cs, FIRSTSET, __LINE__);

  pattern_free(p3);

  /* ----------------------------------------------------------------------------- */
  printf("********************** Constant captures \n");
  printf("TEMPORARILY BLOCKED until new constant capture impl is ready\n");

#if 0
  consttree = pexle_constant(C, 0, 0);
  failif(!consttree, "expected success");

  expression_free(consttree);
  consttree = pexle_constant(1, 2);
  failif(!consttree, "expected success");

  printf("Printing without env:\n");
  print_exp(consttree, 0, NULL);
#endif

  /* BELOW HERE */

  /* ----------------------------------------------------------------------------- */
  printf("********************** References \n");

  m = packagetable_new(1);
  failif(!m, "should succeed");

  pexlc_env_free(toplevel);
  toplevel = env_new(NULL, NO);
  failif(!toplevel, "new toplevel should succeed");

  env = env_new(toplevel, NO);
  failif(!env, "should succeed");

  compstate_free(cst);
  cst = compstate_new();

  stat = env_bind(toplevel, 0, env_new_value(Eunspecified_t, 0, NULL), &ref);
  failif(stat != 0, "successful env_bind should return code 0");
  refKEEP = ref;

  stat = env_bind(toplevel, 1, env_new_value(Eunspecified_t, 0, NULL), &ref);
  failif(stat != 0, "successful env_bind should return code 0");
  failif(ref.env != toplevel, "checking ref contents");
  failif(ref.index != 1, "checking ref contents (second binding is at index 1)");

  calltree = pexle_call(C, ref);
  failif(!calltree, "should succeed");
  failif(calltree->node->v.env != ref.env, "tree should contain env from ref");
  failif(calltree->node->u.index != ref.index, "tree should contain index from ref");

  /* Check that we follow the reference correctly */
  p1 = pattern_new(calltree, &stat); /* will be freed explicitly */
  failif(!p1, "expected new pattern");
  p1->env = env;		/* compilation env is 'env', a child of 'toplevel' */

  printf("\nCreating pattern %p that references a binding in a parent env...\n",
	 (void *) p1);
  printf("ref = (%p, %d)\n", (void *) ref.env, ref.index);
  print_pattern(p1, 0, C); 
  
  failif(cst->pat, "should be NULL in a fresh compstate");
  cst->pat = p1;

  reset_compilation_state(cst->pat);  

  stat = fix_open_calls(cst);  
  failif(stat != EXP_ERR_NO_PATTERN, "should catch error that ref points to unspecified (not a pattern)");  

  printf("Target of call is %p:\n", (void *) binding_pattern_value(ref));
  print_pattern(binding_pattern_value(ref), 0, C);

  /* Change the target of the ref to a pattern (from an unspecified value) */
  tstr = pexle_from_string(C, "hello");
  pstr = pattern_new(tstr, &stat); /* will be freed by pexlc_env_free(toplevel) */
  failif(stat != 0, "should succeed");
  failif(!pstr, "expected new pattern");
  pstr->env = toplevel;
  assert_unspecified_value_at(ref);
  stat = env_rebind(ref, env_new_value(Epattern_t, 0, (void *)pstr));
  failif(stat != 0, "should succeed");

  printf("Target of call AFTER REBIND is %p:\n", (void *) binding_pattern_value(ref));
  print_pattern(binding_pattern_value(ref), 0, C);

  /* !@# */
  calltree = pexle_call(C, ref);
  failif(!calltree, "should succeed");
  /* !@# */
  p2 = pattern_new(calltree, &stat);
  failif(!p2, "expected pattern");
  p2->env = env;		/* compilation env */

  printf("New pattern in toplevel is %p:\n", (void *) p2);
  print_pattern(p2, 0, C);

  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");

  cst->pat = p2;
  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  printf("stat = %d\n", stat);
  failif(stat != 0, "ref points to a pattern, so the open call resolves OK");

  stat = build_cfgraph(p2, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  
  stat = sort_cfgraph(cst->g);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");

  printf("\nCompiling pattern %p in toplevel environment %p:\n", (void *) cst->pat, (void *) cst->pat->env);
  print_pattern(cst->pat, 0, C);
  printf("which calls this pattern %p (in environment %p):\n",
	 (void *) binding_pattern_value(ref), (void *) binding_pattern_value(ref)->env);
  print_pattern(binding_pattern_value(ref), 0, C);

  /* Should succeed because the tstr pattern has metadata set */
  stat = set_metadata(cst);
  failif(stat != 0, "expected success");

  pattern_free(p2);

  /* Now try calling a pattern that calls "hello" */
  t1 = pexle_call(C, ref); /* call "hello" (in other pkg) */
  failif(!t1, "should succeed");
  pattern_free(p1);
  p1 = pattern_new(t1, &stat);
  failif(stat != 0, "expected success");
  failif(!p1, "expected pattern");
  p1->env = toplevel;
  assert_unspecified_value_at(refKEEP); 
  stat = env_rebind(refKEEP, env_new_value(Epattern_t, 0, (void *)p1));
  failif(stat != 0, "expected success");

  /* p1 is a pattern (to compile in toplevel) that calls refKEEP (in pkg) which calls ref (in pkg) which is "hello" */
  calltree = pexle_call(C, refKEEP);
  print_exp(calltree, 0, NULL); 
  failif(!calltree, "should succeed");

  /* p1 is bound, and will be freed with toplevel, so we can reuse the p1 variable here */
  p1 = pattern_new(calltree, &stat);
  failif(stat != 0, "expected success");
  p1->env = env;			/* 'env' is child of 'toplevel' */

  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p1;

  printf("\nCompiling pattern %p in toplevel environment %p:\n", (void *) cst->pat, (void *) cst->pat->env);
  print_pattern(cst->pat, 0, C);
  printf("which calls this pattern %p (in environment %p):\n",
	 (void *) binding_pattern_value(refKEEP), (void *) binding_pattern_value(refKEEP)->env);
  print_pattern(binding_pattern_value(refKEEP), 0, C);
  printf("which calls this pattern %p (in environment %p):\n",
	 (void *) binding_pattern_value(ref), (void *) binding_pattern_value(ref)->env);
  print_pattern(binding_pattern_value(ref), 0, C);

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  printf("stat = %d\n", stat);
  failif(stat != 0, "ref points to a pattern, so the open call resolves OK");

  stat = build_cfgraph(p1, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  
  stat = sort_cfgraph(cst->g);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");

  /* Should succeed because the tstr pattern has metadata set */
  stat = set_metadata(cst);
  failif(stat != 0, "expected success"); 

  /* We are going to continue to use p1, below */

  /* ABOVE HERE */

  /* ----------------------------------------------------------------------------- */
  printf("********************** Scope test (reusing results of prior test)\n");

  /* Get a ref to the pattern in 'env' that calls a toplevel pattern */
  stat = env_bind(env, 0, env_new_value(Epattern_t, 0, (void *)p1), &ref);
  failif(ref_not_found(ref), "expected valid ref");
  calltree = pexle_call(C, ref);
  failif(!calltree, "should succeed");
  p1 = pattern_new(calltree, &stat);
  failif(!p1, "expected pattern");
  failif(stat != 0, "expected success");
  p1->env = toplevel;		/* Should cause an error, because 'env' is not in scope of toplevel! */

  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p1;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  failif(stat != EXP_ERR_SCOPE, "ref points to a child env, which is NOT in scope"); 

  pattern_free(p1);
  
  /* ----------------------------------------------------------------------------- */
  printf("********************** Non-recursive block using anonymous exps \n");

  genv = env_new(env, NO);
  stat = env_bind(genv, 0, env_new_value(Eunspecified_t, 0, NULL), &refA);
  failif(stat < 0, "bind should work");
  stat = env_bind(genv, 0, env_new_value(Eunspecified_t, 0, NULL), &refS);
  failif(stat < 0, "bind should work");
/*   print_env_stats(genv); */
/*   print_env(genv, C); */

  A = pexle_call(C, refS);	/* A -> S */
  S = pexle_from_boolean(C, 1);	/* S -> true */

  node = A->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp NOT captures");
  failif(exp_itself_hascaptures(node), "this exp NOT captures");
  failif(exp_patlen(node, &min, &max) != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 
  failif(exp_nullable(node) != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 
  failif(exp_nofail(node) != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 
  failif(exp_headfail(node) != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 
  failif(exp_needfollow(node) != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 
  stat = exp_getfirst(node, fullset, &cs); 
  failif(stat != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 

  p1 = pattern_new(A, &stat);
  failif(!p1, "expected new pattern");
  p1->env = toplevel;		/* compilation env */

  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p1;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  failif(stat != EXP_ERR_NO_PATTERN, "should catch error that ref points to unspecified (not a pattern");

  pexlc_env_free(genv);
  printf("-----\n");

  pattern_free(p1);

  /* ----------------------------------------------------------------------------- */
  printf("********************** Recursive block using anonymous exps \n");

  printf("Trying a single rule that is left recursive due to a self loop\n"); 
  genv = env_new(env, YES);
  stat = env_bind(genv, 0, env_new_value(Eunspecified_t, 0, NULL), &refA);
  failif(stat < 0, "bind to unspecified should work");
  A = pexle_call(C, refA);		/* A -> A */
  
  p1 = pattern_new(A, &stat);
  failif(!p1, "expected new pattern");
  p1->env = genv;		/* compilation env */

  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p1;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  printf("stat = %d\n", stat);
  failif(stat != EXP_ERR_NO_PATTERN, "should catch error that ref points to unspecified (not a pattern)");

  stat = build_cfgraph(p1, &(cst->g));
  failif(stat != EXP_ERR_OPENFAIL, "should catch error that there is an open call");
  
  pattern_free(p1);

  printf("-----\n");

  printf("Trying mutually left-recursive rules that are not accessible from start"); 
  pexlc_env_free(genv);
  genv = env_new(env, YES);
  stat = env_bind(genv, 0, env_new_value(Eunspecified_t, 0, NULL), &refA);
  failif(stat < 0, "bind of unspecified should work");
  stat = env_bind(genv, 0, env_new_value(Eunspecified_t, 0, NULL), &refB);
  failif(stat < 0, "bind of novalue should work");
  A = pexle_call(C, refB);		/* A -> B */
  B = pexle_call(C, refA);		/* B -> A */

  printf("Reminder, there is no left recursion check yet.\n");
  
  printf("Expression at refA: "); print_exp(A, 0, NULL);
  node = A->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp NOT captures");
  failif(exp_itself_hascaptures(node), "this exp NOT captures");
  failif(exp_patlen(node, &min, &max) != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 
  failif(exp_nullable(node) != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 
  failif(exp_nofail(node) != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 
  failif(exp_headfail(node) != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 
  failif(exp_needfollow(node) != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 
  stat = exp_getfirst(node, fullset, &cs); 
  failif(stat != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 
  
  printf("Expression at refB: "); print_exp(B, 0, NULL);
  node = B->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp NOT captures");
  failif(exp_itself_hascaptures(node), "this exp NOT captures");
  failif(exp_patlen(node, &min, &max) != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 
  failif(exp_nullable(node) != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 
  failif(exp_nofail(node) != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 
  failif(exp_headfail(node) != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 
  failif(exp_needfollow(node) != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 
  stat = exp_getfirst(node, fullset, &cs); 
  failif(stat != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 

  p1 = pattern_new(B, &stat);
  failif(!p1, "expected new pattern");
  p1->env = toplevel;		/* compilation env */

  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p1;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  failif(stat != EXP_ERR_NO_PATTERN, "should catch error that ref points to unspecified (not a pattern");

  stat = build_cfgraph(p1, &(cst->g));
  failif(stat != EXP_ERR_OPENFAIL, "exp has an OPEN CALL");
  
  stat = sort_cfgraph(cst->g);
  failif(stat != EXP_ERR_NULL, "should catch error that graph does not exist");

  stat = set_metadata(cst);
  failif(stat != EXP_ERR_NULL, "should catch error that graph does not exist");

  /* Bind an actual pattern at refB */
  assert_unspecified_value_at(refB);
  stat = env_rebind(refB, env_new_value(Epattern_t, 0, (void *)p1));
  failif(stat != 0, "expected success");

  printf("Now binding a pattern at A, so that we get mutual left recursion A->B and B->A\n");
  p2 = pattern_new(A, &stat);
  failif(!p2, "expected new pattern");
  p2->env = toplevel;
  assert_unspecified_value_at(refA);
  stat = env_rebind(refA, env_new_value(Epattern_t, 0, (void *)p2));
  failif(stat != 0, "expected success");

  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p2;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");

  stat = build_cfgraph(p2, &(cst->g));
  failif(stat != 0, "expected success");
  
  stat = sort_cfgraph(cst->g);
  failif(stat != 0, "expected success");

  stat = set_metadata(cst);
  failif(stat != EXP_ERR_LEFT_RECURSIVE, "this is the only left recursive result in this test file");

  /* p1, p2 are bound and will be freed with toplevel */

  printf("-----\n");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Grammar with realistic recursion \n");

  /* New env for grammar */
  pexlc_env_free(genv);
  genv = env_new(env, YES);
  failif(!genv, "failed to create new env");

  stat = context_intern(C, "A");
  failif(stat < 0, "symbol add should succeed");
  stat = env_bind(genv, stat, env_new_value(Eunspecified_t, 0, NULL), &ref);
  failif(ref_not_found(ref), "bind should work");

  A = pexle_choice_f(C, 		/* A -> "a" A "b" / epsilon */
		   pexle_seq_f(C, 
			     pexle_from_string(C, "a"),
			     pexle_seq_f(C, 
				       pexle_call(C, ref),
				       pexle_from_string(C, "b"))),
		   pexle_from_boolean(C, 1));

  failif(!A, "A is a valid expression");
  printf("A is: \n"); 
  print_exp(A, 0, C); 

  node = A->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp NOT captures");
  failif(exp_itself_hascaptures(node), "this exp NOT captures");
  failif(exp_patlen(node, &min, &max) != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 
  failif(exp_nullable(node) != 1, "exp nullable, and we never get to the OPEN CALL"); 
  failif(exp_nofail(node) != 1, "exp is nofail, and we never get to the OPEN CALL"); 
  failif(exp_headfail(node) != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 
  failif(exp_needfollow(node) != 1, "exp benefits from needfollow, and we never get to the OPEN CALL"); 
  stat = exp_getfirst(node, fullset, &cs); 
  failif(stat != 1, "firstset cannot be used as test, exp accepts epsilon");

/*   print_env_stats(genv); */
/*   print_env(genv, C); */

  /* Now modify the "A" binding's value to the pattern based on the expression A */
  p1 = pattern_new(A, &stat);
  failif(!p1, "expected new pattern");
  p1->env = genv;		/* compilation env */
  assert_unspecified_value_at(ref);
  stat = env_rebind(ref, env_new_value(Epattern_t, 0, (void *)p1));
  failif(ref_not_found(ref), "bind should work");
  failif(stat != 0, "bind should work");

  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p1;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");
  stat = build_cfgraph(p1, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  
  stat = sort_cfgraph(cst->g);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");

  print_pattern(cst->pat, 0, C);

  stat = set_metadata(cst);
  failif(stat != 0, "expected success");

  memset(&cs, 0, CHARSETSIZE);
  setchar(cs.cs, 'A');
  /* Recursive patterns automatically set to unbounded (conservative) and min of 0 (conservative) */
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NULLABLE, NOFAIL, NOT_HEADFAIL, EXP_UNBOUNDED, 0, 123, NEEDFOLLOW, cs, NO_FIRSTSET, __LINE__);

  /*   p1 will be freed with genv */

  /* ----------------------------------------------------------------------------- */
  printf("********************** Grammar with one binding to unspecified \n");

  /* New env for grammar */
  pexlc_env_free(genv);
  genv = env_new(env, NO);
  failif(!genv, "failed to create new env");

  stat = context_intern(C, "A");
  failif(stat < 0, "symbol add should succeed");
  stat = env_bind(genv, stat, env_new_value(Eunspecified_t, 0, NULL), &ref);
  failif(ref_not_found(ref), "bind of unspecified value should work");
  failif(stat < 0, "bind of unspecified value should work");

  stat = context_intern(C, "A");
  failif(stat < 0, "symbol add should succeed");
  pexle_free(S);
  S = pexle_call(C, ref);		/* S -> A */
  failif(!S, "should be able to create S, because value bound to A is never examined");

  node = S->node;
  failif(!node, "precondition for next tests");

  failif(exp_hascaptures(node), "this exp NOT captures");
  failif(exp_itself_hascaptures(node), "this exp NOT captures");
  failif(exp_patlen(node, &min, &max) != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 
  failif(exp_nullable(node) != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 
  failif(exp_nofail(node) != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 
  failif(exp_headfail(node) != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 
  failif(exp_needfollow(node) != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 
  stat = exp_getfirst(node, fullset, &cs); 
  failif(stat != EXP_ERR_OPENFAIL, "exp has an OPEN CALL"); 

  pexle_free(S);
  pexlc_env_free(genv);
  pexlc_env_free(env);
  pexlc_env_free(toplevel);

  packagetable_free(m); 
  context_free(C);
  compstate_free(cst);

  /* ----------------------------------------------------------------------------- */
  printf("\nDone.\n");
  printf("Don't forget to run analyzetest4 next.\n");

  printf("\n** TODO: Add a test that includes a call to another package, one which is already compiled\n");

  return 0;
}

