
/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  peepholetest.c  TESTING peephole optimizer                               */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Vivek Reddy                                                     */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>		/* exit() */
#include "print.h"
#include "compile.h"
#include "vm.h"
#include "testframework.h"

/* Helper Functions */
static int verify_match (pattern* (*generate_pt)(void), const char* input, const char *patname, short matched, size_t leftover) {
    
    size_t len;
    pattern *pt;
    vm_match *vm_m;
    int stat;
    short fail;
    SymbolTableEntry *entry;
    Index entrypoint;
    
    printf("----------------------------------- Test Case: %s ------------------------ \n", input);
    len = strlen(input);

    pt = generate_pt();
    compile_generic_pattern(pt, ONLY_PEEPHOLE_OPTIM); 
    vm_m = create_pkg_tbl_with_user_pkg(pt, input, len, NULL);

    /* Find entrypoint for 'patname' */
    entry = symboltable_search(pt->pkg->symtab, patname, NULL);
    if (!entry) {
      fprintf(stderr, "Search of symbol table for '%s' FAILED\n", patname);
      fail = 1;
      goto done;
    }
    assert(entry_type(entry) == SYMBOL_TYPE_PATTERN);
    entrypoint = entry->value;
    stat = vm_start(vm_m->pk_tble, entrypoint, vm_m->buf, 0, len, debug_encoder, NULL, vm_m->match);
    fail = 0;
    if (stat != 0){
        fprintf(stderr, "Failed: VM Execution for Input: %s\n", input);
        fflush(stderr);	
        fail = 1;							
	goto done;
    }
    if (vm_m->match->matched != matched){
        fprintf(stderr, "Failed: Match Failed for Input: %s \n\t Expected: %d, Found: %d\n", input, matched, vm_m->match->matched);
        fflush(stderr);	
        fail = 1;						
    }
    if (vm_m->match->leftover != leftover){
        fprintf(stderr, "Failed: Leftover Failed for Input: %s \n\t Expected: %lu, Found: %lu\n", input, leftover, vm_m->match->leftover);
        fflush(stderr);	
        fail = 1;						
    }

 done:
    vm_match_free(vm_m);
    generic_pattern_free(pt);
    if (fail) return !OK;
    printf("  ... ok. \n");
    return OK;
}

/* Helper Functions */
static int verify_compilation(pattern* (*generate_pt)(void)){
    pattern* pt = generate_pt();
    compile_generic_pattern(pt, ONLY_PEEPHOLE_OPTIM);
    print_symboltable(pt->pkg->symtab);
    generic_pattern_free(pt);
    printf("  ... ok. \n");
    return OK;
}



int main(void){

    printf("$$$ peepholetest\n");
    printf("-------------------------------------  Test optimizeICall method -------------------------------------\n");

    printf("-------------------------------------  Verifying for Call -> Ret Optimization\n");
    failif(verify_compilation(&recursive_pattern) != OK, "Unit Test Failed");

    printf("------------------------------------- Verifying for Call -> Call Optimization\n");
    failif(verify_compilation(&recursive_pattern2) != OK, "Unit Test Failed");

    printf("------------------------------------- Verifying for Call -> Jmp Optimization\n");
    failif(verify_compilation(&recursive_pattern3) != OK, "Unit Test Failed");
    failif(verify_compilation(&recursive_pattern4) != OK, "Unit Test Failed");
    
    printf("------------------------------------- Testing optimizer on Larger patterns --------------------------- \n");

    printf("------------------------- Testing Ipv6_Strict Pattern ----------------------------------- \n");
    failif(verify_match(&create_ipv6, "2001:0db8:0000:0000:0000:ff00:0042:8329", "ipv6_strict", 1, 0) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipv6, "2001:db8:0:0:0:ff00:42:8329", "ipv6_strict", 1, 0) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipv6, "2001:db8::ff00:42:8329", "ipv6_strict", 1, 0) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipv6, "FEDC:BA98:7654:3210:FEDC:BA98:7654:3210", "ipv6_strict", 1, 0) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipv6, "1080:0:0:0:8:800:200C:4171", "ipv6_strict", 1, 0) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipv6, "3ffe:2a00:100:7031::1", "ipv6_strict", 1, 0) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipv6, "::192.9.5.5", "ipv6_strict", 1, 9) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipv6, "FFFF:129.144.52.38", "ipv6_strict", 0, 18) != OK, "Unit Test Failed");
    

    printf("------------------------- Testing Ipv4 Pattern ----------------------------------- \n");
    failif(verify_match(&create_ipv4, "255.255.255.255", "ipv4", 1, 0) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipv4, "0.0.0.0", "ipv4", 1, 0) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipv4, "1.2.234.123", "ipv4", 1, 0) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipv4, "255.0.0.255", "ipv4", 1, 0) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipv4, "255.255.255.255.255.255.255.255", "ipv4", 1, 16) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipv4, "1.1.1.256", "ipv4", 1, 1) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipv4, "1234.1.2.3", "ipv4", 0, 10) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipv4, "111.222.333.", "ipv4", 0, 12) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipv4, "111.222.333..444", "ipv4", 0, 16) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipv4, "999.999.999.999", "ipv4", 0, 15) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipv4, "256.1.1.1", "ipv4", 0, 9) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipv4, "1.256.1.1", "ipv4", 0, 9) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipv4, "1.1.256.1", "ipv4", 0, 9) != OK, "Unit Test Failed");     


    printf("------------------------- Testing Ipv4 / Ipv6 Pattern ----------------------------------- \n");
    failif(verify_match(&create_ipaddr, "2001:0db8:0000:0000:0000:ff00:0042:8329", "ipv6_strict", 1, 0) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "2001:db8:0:0:0:ff00:42:8329", "ipv6_strict", 1, 0) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "2001:db8::ff00:42:8329", "ipv6_strict", 1, 0) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "FEDC:BA98:7654:3210:FEDC:BA98:7654:3210", "ipv6_strict", 1, 0) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "1080:0:0:0:8:800:200C:4171", "ipv6_strict", 1, 0) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "3ffe:2a00:100:7031::1", "ipv6_strict", 1, 0) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "::192.9.5.5", "ipv6_strict", 1, 9) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "FFFF:129.144.52.38", "ipv6_strict", 0, 18) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "255.255.255.255", "ipv4", 1, 0) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "0.0.0.0", "ipv4", 1, 0) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "1.2.234.123", "ipv4", 1, 0) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "255.0.0.255", "ipv4", 1, 0) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "255.255.255.255.255.255.255.255", "ipv4", 1, 16) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "1.1.1.256", "ipv4", 1, 1) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "1234.1.2.3", "ipv4", 0, 10) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "111.222.333.", "ipv4", 0, 12) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "111.222.333..444", "ipv4", 0, 16) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "999.999.999.999", "ipv4", 0, 15) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "256.1.1.1", "ipv4", 0, 9) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "1.256.1.1", "ipv4", 0, 9) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "1.1.256.1", "ipv4", 0, 9) != OK, "Unit Test Failed"); 

    printf("--------------------------- Testing the Common Entry Point for Ipv6 and IPv4 --------------\n "); 
    failif(verify_match(&create_ipaddr, "2001:0db8:0000:0000:0000:ff00:0042:8329", "ipv6 / ipv4", 1, 0) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "2001:db8:0:0:0:ff00:42:8329", "ipv6 / ipv4", 1, 0) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "2001:db8::ff00:42:8329", "ipv6 / ipv4", 1, 0) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "FEDC:BA98:7654:3210:FEDC:BA98:7654:3210", "ipv6 / ipv4", 1, 0) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "1080:0:0:0:8:800:200C:4171", "ipv6 / ipv4", 1, 0) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "3ffe:2a00:100:7031::1", "ipv6 / ipv4", 1, 0) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "::192.9.5.5", "ipv6 / ipv4", 1, 9) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "FFFF:129.144.52.38", "ipv6 / ipv4", 0, 18) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "255.255.255.255", "ipv6 / ipv4", 1, 0) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "0.0.0.0", "ipv6 / ipv4", 1, 0) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "1.2.234.123", "ipv6 / ipv4", 1, 0) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "255.0.0.255", "ipv6 / ipv4", 1, 0) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "255.255.255.255.255.255.255.255", "ipv6 / ipv4", 1, 16) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "1.1.1.256", "ipv6 / ipv4", 1, 1) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "1234.1.2.3", "ipv6 / ipv4", 0, 10) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "111.222.333.", "ipv6 / ipv4", 0, 12) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "111.222.333..444", "ipv6 / ipv4", 0, 16) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "999.999.999.999", "ipv6 / ipv4", 0, 15) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "256.1.1.1", "ipv6 / ipv4", 0, 9) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "1.256.1.1", "ipv6 / ipv4", 0, 9) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "1.1.256.1", "ipv6 / ipv4", 0, 9) != OK, "Unit Test Failed");

    /* The tests below make use of the negative lookahead for an ipv4 pattern in ipv6_rest */
    failif(verify_match(&create_ipaddr, "::192.9.5.5", "ipv6_strict", 1, 9) != OK, "Unit Test Failed");
    failif(verify_match(&create_ipaddr, "::FFFF:129.144.52.38", "ipv6_strict", 1, 18) != OK, "Unit Test Failed");

    printf("\n\n ...... Peephole Optimizer Tests Done\n\n");

    printf("\n\nTODO: Add tests from net.rpl that depend on having the negative lookahead in ipv6_component\n\n");

    return 0;
}
