/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  packagetest.c  TESTING the package and package table structures          */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#define _GNU_SOURCE		/* for asprintf() */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "package.h"
#include "print.h"

#define YES 1
#define NO 0

#define failif(test, msg)						\
  do { if (test) { fprintf(stderr, "Failed %s:%d: %s\n", __FILE__,	\
			   __LINE__, msg);				\
      fflush(stderr);							\
      exit(-1);								\
    }									\
  } while (0)

int main(void) {

  int stat;
  PackageTable *m;
  Package *p, *p2, *p3, *pkg, *user, *prelude, *p_null;
  Index probe;
  size_t i;
  char *str, *str1, *str2;
  Instruction *dummy_code;
  uint32_t dummy_codesize = 100;
  uint32_t oldsize;

  dummy_code = malloc(1);
  failif(!dummy_code, "dummy code must be malloc'd and cannot be NULL");
  
  /* ----------------------------------------------------------------------------- */
  printf("$$$ packagetest\n");

  m = packagetable_new(0);
  failif(!m, "initial package table size 0, so default will be used");
  failif(m->size != PACKAGETABLE_INITSIZE, "should be default size");
  packagetable_free(m);
  
  m = packagetable_new(PACKAGETABLE_MAXSIZE + 1);
  failif(!m, "initial package table size out of range");
  failif(m->size != PACKAGETABLE_MAXSIZE, "should be max size");
  packagetable_free(m);
  
  m = packagetable_new(PACKAGETABLE_MAXSIZE);
  failif(!m, "should make a max sized table");
  failif(m->size != PACKAGETABLE_MAXSIZE, "checking size field");
  failif(m->next != 1, "checking next field, which starts at 1 because user pkg is in slot 0");
  failif(m->packages == NULL, "checking packages field");
  packagetable_free(m);

  prelude = package_new();
  failif(!prelude, "should succeed");
  failif(!prelude->symtab, "checking symbol table field");
  package_free(prelude); prelude = NULL;

  user = package_new();
  failif(!user, "should succeed, even with importpath, prefix, source as empty string");
  failif(!user->symtab, "checking symbol table field");
  package_free(user); user = NULL;

  m = packagetable_new(1);
  failif(!m, "should make a min sized table");
  failif(m->size >= 2, "checking size field, which may jump from 1 to a much larger size");
  failif(m->next != 1, "checking next field, which starts at 1 because user pkg is in slot 0");
  failif(m->packages == NULL, "checking packages field");
  
  /* ----------------------------------------------------------------------------- */
  printf("********************** Package tests \n");

  str = malloc(SYMBOLTABLE_MAXLEN + 2);
  memset(str, 'A', SYMBOLTABLE_MAXLEN + 1);
  str[SYMBOLTABLE_MAXLEN + 1] = '\0';
  failif(strlen(str) != SYMBOLTABLE_MAXLEN + 1, "verifying the precondition for test");
  p = package_new();
  failif(!p, "should succeed");
  stat = package_set_importpath(p, str);
  failif(stat != PACKAGE_ERR_STRLEN, "fails due to importpath length; symbol table should log that importpath is too long");

  stat = package_set_importpath(p, "+p_importpath+"); /* set importpath to something unique */
  failif(stat != 0, "should succeed");

  failif(p->symtab == NULL, "checking st field");
  failif(p->code != NULL, "checking code field");
  failif(p->codesize != 0, "checking codesize field");
  failif(package_importpath(p) != 0, "checking importpath field (symtab index)");
  failif(package_default_prefix(p) != -1, "checking default_prefix field");
  failif(p->source != -1, "checking source field");
  failif(p->major != 0, "checking major field");
  failif(p->minor != 0, "checking minor field");

  stat = package_set_default_prefix(p, "pre");
  failif(stat != 0, "should succeed");
  failif(package_default_prefix(p) == -1, "checking default_prefix field");
  failif(p->source != -1, "checking source field");
  
  stat = package_set_source(p, "/path/to/source");
  failif(stat != 0, "should succeed");
  failif(p->source == -1, "checking source field");

  stat = package_set_lang_version(p, 99, 1);
  failif(stat != 0, "should succeed");
  failif(p->major != 99, "checking major field");
  failif(p->minor != 1, "checking minor field");

  package_set_code(p, dummy_code, dummy_codesize);
  failif(p->code != dummy_code, "checking code field");
  failif(p->codesize != dummy_codesize, "checking codesize field");

  /* Create a second package */

  p2 = package_new();
  failif(!p2, "should succeed");
  stat = package_set_importpath(p2, "+importpath+");
  failif(stat != 0, "should succeed");
  stat = package_set_default_prefix(p2, "+default_prefix+");
  failif(stat != 0, "should succeed");
  stat = package_set_source(p2, "+source+");
  failif(stat != 0, "should succeed");
  stat = package_set_lang_version(p2, 101, 255);
  failif(stat != 0, "should succeed");

  failif(p2->symtab == NULL, "checking st field");
  failif(p2->code != NULL, "checking code field");
  failif(p2->codesize != 0, "checking codesize field");
  failif(package_importpath(p2) == -1, "checking importpath field");
  failif(package_default_prefix(p2) == -1, "checking default_prefix field");
  failif(p2->source == -1, "checking source field");
  failif(p2->major != 101, "checking major field");
  failif(p2->minor != 255, "checking minor field");
  
  /* ----------------------------------------------------------------------------- */
  printf("********************** Import table tests \n");

  failif(p->import_size != IMPORTTABLE_INITSIZE, "should be original size");
  failif(p2->import_size != IMPORTTABLE_INITSIZE, "should be original size");
  failif(p->import_next != 1, "should start at 1");
  failif(p2->import_next != 1, "should start at 1");

  failif(!p->imports, "should always be non-NULL");
  failif(!p2->imports, "should always be non-NULL");
  failif(p->imports[0].pkg != NULL, "should be NULL");
  failif(p2->imports[0].pkg != NULL, "should be NULL");

  stat = package_add_import(NULL, NULL, NULL);
  failif(stat != PACKAGE_ERR_NULL, "expected to catch error");
  stat = package_add_import(p, NULL, NULL);
  failif(stat != PACKAGE_ERR_NULL, "expected to catch error");
  stat = package_add_import(p, "foo", NULL);
  printf("stat = %d\n", stat);
  failif(stat < 0, "should succeed");
  failif(p->import_next != 2, "next should have advanced");
  print_symboltable(p->symtab);
  failif(p->imports[1].importpath != 3, "incorrect symtab index");

  stat = package_add_import(p, "foo", NULL);
  failif(stat != PACKAGE_ERR_EXISTS, "should fail because foo is already in the table"); 
    
  /* Fill the import table until maximum */
  oldsize = p->import_size;
  for (i = 0; i < IMPORTTABLE_MAXSIZE - 2; i++) {
    asprintf(&str1, "i_%-lu", i);
    failif(!str1, "expected a new string");
    asprintf(&str2, "p_%-lu", i);
    failif(!str2, "expected a new string");
    stat = package_add_import(p, str1, str2);
    if (stat < 0) printf("stat = %d\n", stat);
    failif(stat < 0, "should succeed");
    free(str1);
    free(str2);
    if (p->import_size != oldsize) {
      printf("import table size increased to %d\n", p->import_size);
      oldsize = p->import_size;
    }
    if (i == 13) {
      print_importtable(p); /* quick visual check */
      print_symboltable(p->symtab);
    }
  }
  printf("Added %lu entries to the import table\n", i);
  
  stat = package_add_import(p, "lalalala", "hahahaha");
  printf("stat = %d\n", stat);
  failif(stat != PACKAGE_ERR_FULL, "should fail with table full");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Package table tests \n");
  
  print_packagetable_stats(m);
  print_packagetable(m);

  p_null = package_new();
  failif(!p_null, "should succeed");

  /* Attempt to add with no importpath set */
  stat = packagetable_add(m, p_null);
  failif(stat != PACKAGE_ERR_IMPORTPATH, "should catch error");

  stat = packagetable_set_user(m, p_null);
  failif(stat != PACKAGE_OK, "user package does not need an importpath");

  stat = package_set_importpath(p_null, "");
  failif(stat != 0, "should succeed");
  stat = packagetable_add(m, p_null);
  failif(stat != PACKAGE_ERR_INTERNAL, "attempt to add package that is already in table");
  stat = packagetable_add(m, NULL);
  failif(stat != PACKAGE_ERR_NULL, "null package arg");
  stat = packagetable_add(NULL, p);
  failif(stat != PACKAGE_ERR_NULL, "null machine arg");

  stat = package_set_importpath(p_null, "ABCDEFGHIJ"); /* p_null is now a bad name for this variable */
  failif(stat != 0, "should succeed");
  stat = packagetable_add(m, p_null);
  failif(stat != PACKAGE_ERR_INTERNAL, "attempt to add package that is already in table");

  failif(!symboltable_get_name(p2->symtab, package_importpath(p2)), "import path should exist in st");
  failif(str[0]=='\0', "verifying precondition: importpath not empty");
  stat = packagetable_add(m, p2);
  failif(stat < 0, "should succeed");
  failif(m->packages[1] != p2, "first package other than user package should be at index [1]");
  stat = packagetable_add(m, p);
  failif(stat < 0, "should succeed");
  failif(m->packages[2] != p, "package added after p2 should be at index 2");

  free(str); str = NULL;

  print_packagetable_stats(m);
  print_packagetable(m);

  failif(m->next != 3, "we added 2 packages");
  failif(m->size != 4, "previous size was doubled in table expansion");
  
  p3 = package_new();
  failif(!p3, "should succeed");
  stat = package_set_importpath(p3, "+importpath+");
  failif(stat != 0, "should succeed");
  stat = package_set_source(p3, "+p3_source+");
  failif(stat != 0, "should succeed");

  failif(p3->symtab == NULL, "checking st field");
  failif(p3->code != NULL, "checking code field");
  failif(p3->codesize != 0, "checking codesize field");
  print_symboltable(p3->symtab);
  print_importtable(p3);
  failif(package_importpath(p3) != 0, "checking importpath (symtab index)");
  failif(package_default_prefix(p3) != -1, "checking default_prefix field which is unset");
  failif(p3->source != 1, "checking source field (symtab index)");
  failif(p3->major != 0, "checking major field");
  failif(p3->minor != 0, "checking minor field");
  
  stat = packagetable_add(m, p3);
  failif(stat != PACKAGE_ERR_EXISTS, "should find package with same importpath");

  stat = symboltable_add(p3->symtab, "+p3_importpath+", SYMBOL_GLOBAL, SYMBOL_TYPE_STRING, 0, 0, NULL);
  failif(stat != 2, "precondition for next test");
  package_importpath(p3) = 2; /* importpath now unique */
  stat = packagetable_add(m, p3);
  failif(stat != PACKAGE_OK, "should succeed");

  print_packagetable_stats(m);
  print_packagetable(m);

  /* Test packagetable_lookup */

  probe = packagetable_lookup(m, "");
  failif(probe, "lookup does not examine user package, so should NOT find package");
  probe = packagetable_lookup(m, "+importpath+");
  failif(probe != 1, "should find package");
  pkg = packagetable_get(m, probe);
  failif(pkg != p2, "should find package");
  probe = packagetable_lookup(m, "+p3_importpath+");
  failif(probe != 3, "should find package");
  pkg = packagetable_get(m, probe);
  failif(pkg != p3, "should find package");
  probe = packagetable_lookup(m, "helloworld");
  failif(probe, "no package with this importpath");

  probe = packagetable_lookup(NULL, "");
  failif(probe != PACKAGE_ERR_NULL, "machine arg cannot be NULL");

  /* Fill the package table */

  for (i = m->next; i < PACKAGETABLE_MAXSIZE; i++) {
    stat = asprintf(&str, "%ld", i);
    failif(stat < 0, "asprintf failed!");
    pkg = package_new();
    failif(!pkg, "should succeed");
    stat = package_set_importpath(pkg, str);
    free(str);
    failif(stat != 0, "should succeed");
    stat = packagetable_add(m, pkg);
    if (stat < 0) {
      printf("packagetable_add returned %d for importpath %lu\n", stat, i);
    }
    failif(stat < 0, "package add failed!");
  }

  failif(m->next != PACKAGETABLE_MAXSIZE, "table should be full");
  failif(m->size != PACKAGETABLE_MAXSIZE, "table should be full");

  /* Try one more package to make sure that PACKAGETABLE_MAXSIZE is respected */
  stat = asprintf(&str, "%ld", i);
  failif(stat < 0, "asprintf failed!");
  pkg = package_new();
  failif(!pkg, "should succeed");
  stat = package_set_importpath(pkg, str);
  free(str);
  failif(stat != 0, "should succeed");
  stat = packagetable_add(m, pkg);
  failif(stat != PACKAGE_ERR_FULL, "should not be able to add any more");

  /* Test packagetable_remove */
  failif(!pkg, "pre-condition for next test");
  failif(pkg->number != 0, "pre-condition for next test");

  stat = packagetable_remove(m, NULL);
  failif(stat != PACKAGE_ERR_NULL, "package arg required");
  stat = packagetable_remove(NULL, pkg);
  failif(stat != PACKAGE_ERR_NULL, "machine arg required");
  stat = packagetable_remove(NULL, NULL);
  failif(stat != PACKAGE_ERR_NULL, "both args required");

  package_free(pkg);  

  failif(p3->number != 3, "pre-condition for next test");
  stat = packagetable_remove(m, p3);
  failif(stat != PACKAGE_OK, "should succeed in writing a NULL in the package table");
  probe = packagetable_lookup(m, "+default_prefix+");
  failif(probe != 0, "should not find the package at index 3, which we removed");

  package_free(p3);
  
  for (i = 0; i < m->next; i++)
    if (i == 3) failif(m->packages[i] != NULL, "checking the entry for p3 itself");
    else failif(m->packages[i] == NULL, "checking all other entries");

  print_packagetable_stats(m);

  printf("Freeing all the packages we created to put in the package table...\n");
  packagetable_free_packages(m);
  packagetable_free(m);

  /* ----------------------------------------------------------------------------- */
  
  printf("\nDone.\n");

  return 0;
}

