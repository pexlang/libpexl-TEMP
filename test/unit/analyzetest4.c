/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  analyzetest4.c  TESTING analyze.c                                        */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#define _GNU_SOURCE		/* for asprintf() */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>		/* exit() */
#include "print.h"
#include "analyze.h"
#include "compile.h"		/* pexlc_env_free() */

#define YES 1
#define NO 0

#define CAPTURES 1
#define HEADFAIL 1
#define NULLABLE 1
#define NOFAIL 1
#define FIRSTSET 1
#define NEEDFOLLOW 1
#define HEADFAIL 1

#define NO_CAPTURES 0
#define NOT_HEADFAIL 0
#define NOT_NULLABLE 0
#define CAN_FAIL 0
#define NO_FIRSTSET 0
#define NO_NEEDFOLLOW 0
#define NO_HEADFAIL 0

/* Borrowed from analyze.c */
/* Follow ref, extract binding field, return NULL if it is not a Pattern. */
static Pattern *binding_pattern_value (Ref ref) {
  Binding *b = env_get_binding(ref);
  if (!b) return NULL;		/* invalid ref */
  if (b->val.type != Epattern_t) return NULL;
  return (Pattern *) b->val.ptr;
}
#define failif(test, msg)						\
  do { if (test) { fprintf(stderr, "Failed %s:%d: %s\n", __FILE__,	\
			   __LINE__, msg);				\
      fflush(stderr);							\
      exit(-1);								\
    }									\
  } while (0)

#define assert_unspecified_value_at(ref)				\
  do {									\
    b999 = env_get_binding(ref);					\
    failif(bindingtype(b999) != Eunspecified_t, "how can this happen?"); \
  } while (0)

#define free_pattern_at(ref)						\
  do {									\
    b999 = env_get_binding(ref);					\
    if(bindingtype(b999) == Epattern_t) {				\
      pattern_free((Pattern *) b999->val.ptr);				\
      stat = env_rebind(ref, env_new_value(Eunspecified_t, 0, NULL));	\
      failif(stat < 0, "bind should work");				\
    } else {								\
      printf("free_pattern_at: no pattern to free\n");			\
    }									\
  } while (0)

static void check_node (Pattern *p,
			int hascaptures,
			int itself_hascaptures,
			int nullable,
			int nofail,
			int headfail,
			int unbounded,
			uint32_t expected_min,
			uint32_t expected_max,
			int needfollowflag,
			Charset firstset,
			int getfirstYES,
			size_t lineno) {
  uint32_t min, max;
  int stat;
  Charset cs;
  unsigned char c;
  Node *node;
  char *msg;
  if (!p->fixed) {
    asprintf(&msg, "Error in check_node: pattern %p has no 'fixed' expression tree\n", (void *) p);
    failif(!p->fixed, msg);
  }
  node = p->fixed->node;
  printf("check_node called on pattern %p from line %lu\n", (void *)p, lineno);
  failif((exp_hascaptures(node) ? 1 : 0) != hascaptures, "this exp captures or not");
  failif((exp_itself_hascaptures(node) ? 1 : 0) != itself_hascaptures, "this pattern itself captures or not (without following calls)");
  stat = exp_patlen(node, &min, &max);
  if (stat != unbounded) {
    if ((stat != EXP_BOUNDED) && (stat != EXP_UNBOUNDED))
      printf("return value from exp_patlen is error: %d\n", stat);
    else
      printf("pattern is %s, which was not expected", (stat==EXP_BOUNDED) ? "bounded" : "unbounded");
  }
  failif(stat != unbounded, "wrong boundedness result");
  if (min != expected_min)
    printf("min len = %u, expected = %u\n", min, expected_min);
  if ((stat == EXP_BOUNDED) && (max != expected_max))
    printf("max len = %u, expected = %u\n", max, expected_max);
  failif(min != expected_min, "wrong min value");
  failif((stat == EXP_BOUNDED) && (max != expected_max), "wrong max value");
  failif((exp_nullable(node) ? 1 : 0) != nullable, "pattern nullable or not");
  failif((exp_nofail(node) ? 1 : 0) != nofail, "pattern can fail or not");
  failif((exp_headfail(node) ? 1 : 0) != headfail, "pattern is headfail or not");
  failif((exp_needfollow(node) ? 1 : 0) != needfollowflag, "does pattern need follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(((stat == 0) ? 0 : 1) == getfirstYES, "stat==0 means firstset can be used as test");
  if (stat == 0) {
    /* Now check the contents of cs for 'h' */
    for (c = 0; c < 255; c++)
      if (testchar(firstset.cs, c)) {
	if (!testchar(cs.cs, c)) printf("charset missing %c\n", c);
	failif(!testchar(cs.cs, c), "firstset should contain this char");
      } else {
	if (testchar(cs.cs, c)) printf("cs should not have %c", c);
	failif(testchar(cs.cs, c), "firstset should not contain this char");
      }
  } /* if stat==0 */
}

int main(void) {

  Binding *b999;
  
  Context *C;

  int stat; 
  CompState *cst; 
  Pattern *p;
  Env *toplevel, *genv; 
  Node *node; 
  Charset cs;

  const char *str;

  Index vnum, i;

  Expression *A, *B, *S;

  Env *env;

  Ref ref, refA, refB, refS, ref_anon;

  printf("$$$ analyzetest4\n");

  printf("analyzetest4: Testing exp_nullable_rec inside set_recursive_metadata \n\n");

  toplevel = env_new(NULL, NO);
  failif(!toplevel, "new toplevel should succeed");
  
  C = context_new(toplevel);
  failif(!C, "expected new context");
  stat = context_intern(C, "foo");
  failif(stat != 1, "first interned string should be at offset 1 in the storage block");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Non-recursive block using anonymous exps \n");

  env = env_new(toplevel, NO);
  failif(!env, "should succeed");

  stat = env_bind(env, 0, env_new_value(Eunspecified_t, 0, NULL), &refA);
  failif(stat < 0, "bind should work");
  stat = env_bind(env, 0, env_new_value(Eunspecified_t, 0, NULL), &refS);
  failif(stat < 0, "bind should work");

  A = pexle_call(C, refS);		/* A -> S */
  S = pexle_from_boolean(C, 1);		/* S -> true */

  p = pattern_new(S, &stat);	/* bound */
  failif(!p, "expected new pattern");
  p->env = env;

  assert_unspecified_value_at(refS);
  stat = env_rebind(refS, env_new_value(Epattern_t, 0, (void *)p));
  failif(stat != 0, "should work");
  printf("S is: "); 
  print_exp(S, 0, NULL); 
  printf("A is: "); 
  print_exp(A, 0, NULL);  
  printf("refS points to: ");
  print_pattern(binding_pattern_value(refS), 0, C);
  
  node = A->node;
  failif(!node, "precondition for next tests");

  p = pattern_new(A, &stat);
  failif(!p, "expected new pattern");
  p->env = env;		/* compilation env */
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p;
  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;
  stat = fix_open_calls(cst);
  printf("stat = %d\n", stat);
  failif(stat != 0, "should work");

  stat = build_cfgraph(p, &(cst->g));
  failif(stat != 0, "expected cfgraph");
  failif(!cst->g, "expected cfgraph");

  stat = sort_cfgraph(cst->g);
  failif(stat != 0, "expected sorted cfgraph");

  stat = set_metadata(cst); 
  failif(stat != 0, "should work"); 

/*   stat = left_recursive(cst, 0, 1); */
/*   failif(stat != 0, "should work, and should report OK"); */

  for (i = 0; i < cst->g->order->top; i++) {
    vnum = cst->g->order->elements[i].vnum;
    p = cst->g->deplists[vnum][0];
    printf("CHECKING vnum %d = pattern %p: READY = %s\n",
	   vnum, (void *) p, pattern_has_flag(p, PATTERN_READY) ? "YES" : "NO");
  }

/*   stat = find_left_recursion(cst); */
/*   failif(stat != 0, "should work, and should report OK"); */

  pattern_free(p);
  printf("-----\n");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Recursive block using anonymous exps \n");

  printf("Trying a single rule that is left recursive due to a self loop\n"); 
  genv = env_new(env, YES);
  stat = env_bind(genv, 0, env_new_value(Eunspecified_t, 0, NULL), &refA);
  failif(stat < 0, "bind to unspecified should work");
  A = pexle_call(C, refA);		/* A -> A */

  p = pattern_new(A, &stat);	/* bound */
  failif(!p, "expected new pattern");
  p->env = genv;		/* compilation env */
  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p;

  assert_unspecified_value_at(refA);
  stat = env_rebind(refA, env_new_value(Epattern_t, 0, (void *)p));
  failif(stat != 0, "should work");

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;
  stat = fix_open_calls(cst);
  failif(stat != 0, "should work");

  stat = build_cfgraph(p, &(cst->g));
  failif(stat != 0, "should work");
  
  stat = sort_cfgraph(cst->g);
  failif(stat != 0, "expected sorted cfgraph");

  stat = set_metadata(cst); 
  failif(stat != EXP_ERR_LEFT_RECURSIVE, "should detect the immediate left recursion");

  printf("-----\n");

  printf("Trying mutually left-recursive rules\n"); 
  pexlc_env_free(genv);
  genv = env_new(env, YES);
  stat = env_bind(genv, 0, env_new_value(Eunspecified_t, 0, NULL), &refA);
  failif(stat < 0, "bind of unspecified should work");
  stat = env_bind(genv, 0, env_new_value(Eunspecified_t, 0, NULL), &refB);
  failif(stat < 0, "bind of novalue should work");

  A = pexle_call(C, refB);		/* A -> B */
  B = pexle_call(C, refA);		/* B -> A */

/*   printf("Expression at refA: "); print_exp(A, 0, NULL); */
/*   node = A->node; */

  p = pattern_new(A, &stat);	/* bound */
  failif(!p, "expected new pattern");
  p->env = genv;		/* compilation env */
  assert_unspecified_value_at(refA);
  stat = env_rebind(refA, env_new_value(Epattern_t, 0, (void *)p));
  failif(stat < 0, "rebind of A should work");
  printf("Pattern A is: "); print_pattern(p, 0, C);

  p = pattern_new(B, &stat);	/* bound */
  failif(!p, "expected new pattern");
  p->env = genv;		/* compilation env */
  assert_unspecified_value_at(refB);
  stat = env_rebind(refB, env_new_value(Epattern_t, 0, (void *)p));
  failif(stat < 0, "rebind of B should work");
  printf("Pattern B is: "); print_pattern(p, 0, C);

  /* Compile B */
  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;
  stat = fix_open_calls(cst);
  failif(stat != 0, "should work");

  stat = build_cfgraph(p, &(cst->g));
  failif(stat != 0, "should work");
  
  stat = sort_cfgraph(cst->g);
  failif(stat != 0, "expected sorted cfgraph");

  print_sorted_cfgraph(cst->g);

  stat = set_metadata(cst); 
  failif(stat != EXP_ERR_LEFT_RECURSIVE, "should detect the left recursion");

/*   stat = left_recursive(cst, 0, 2); */
/*   failif(stat != EXP_ERR_LEFT_RECURSIVE, "should detect the left recursion"); */

/*   stat = find_left_recursion(cst); */
/*   failif(stat != EXP_ERR_LEFT_RECURSIVE, "should detect the left recursion"); */
  
  printf("-----\n");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Grammar with realistic recursion \n");

  /* New env for grammar */
  pexlc_env_free(genv);
  genv = env_new(env, YES);
  failif(!genv, "failed to create new env");

  stat = context_intern(C, "A");
  failif(stat < 0, "symbol add should succeed");
  stat = env_bind(genv, stat, env_new_value(Eunspecified_t, 0, NULL), &ref);
  failif(ref_not_found(ref), "bind should work");

  A = pexle_choice_f(C, 		/* A -> "a" A "b" / epsilon */
		 pexle_seq_f(C, 
			   pexle_from_set(C, "a", 1),
			   pexle_seq_f(C, 
				     pexle_call(C, ref),
				     pexle_from_set(C, "b", 1))),
		   pexle_from_boolean(C, 1));

  failif(!A, "A is a valid expression");
  printf("A is: \n"); 
  print_exp(A, 0, C); 

  /* Now modify the "A" binding's value to the pattern based on the expression A */
  p = pattern_new(A, &stat);	/* bound */
  failif(!p, "expected new pattern");
  p->env = genv;		/* compilation env */
  assert_unspecified_value_at(ref);
  stat = env_rebind(ref, env_new_value(Epattern_t, 0, (void *)p));
  failif(ref_not_found(ref), "bind should work");
  failif(stat != 0, "bind should work");

  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p;
  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;
  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");
  stat = build_cfgraph(p, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  
  stat = sort_cfgraph(cst->g);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");

  print_pattern(cst->pat, 0, C);

  stat = set_metadata(cst);
  failif(stat != 0, "expected success");

  memset(&cs, 0, CHARSETSIZE); 
  setchar(cs.cs, 'A'); 
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NULLABLE, NOFAIL, NOT_HEADFAIL,
	     EXP_UNBOUNDED, 0, 123123, NEEDFOLLOW, cs, NO_FIRSTSET, __LINE__); 
  
  /* ----------------------------------------------------------------------------- */
  printf("********************** Grammar with left recursion with nullable sub-pattern \n");

  A = pexle_choice_f(C, 		/* A -> "a"? A "b" / epsilon */
		   pexle_seq_f(C, 
			     pexle_repeat_f(C, pexle_from_set(C, "a", 1), -1),
			     pexle_seq_f(C, 
				       pexle_call(C, ref),
				       pexle_from_set(C, "b", 1))),
		   pexle_from_boolean(C, 1));

  failif(!A, "A is a valid expression");
  printf("A is: \n"); 
  print_exp(A, 0, C); 

  /* Now modify the "A" binding's value to the pattern based on the expression A */
  p = pattern_new(A, &stat);	/* bound */
  failif(!p, "expected new pattern");
  p->env = genv;		/* compilation env */

  free_pattern_at(ref);
  assert_unspecified_value_at(ref);
  stat = env_rebind(ref, env_new_value(Epattern_t, 0, (void *)p));
  failif(ref_not_found(ref), "bind should work");
  failif(stat != 0, "bind should work");

  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p;
  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;
  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");
  stat = build_cfgraph(p, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  
  stat = sort_cfgraph(cst->g);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");

  print_pattern(cst->pat, 0, C);

  stat = set_metadata(cst);
  failif(stat != EXP_ERR_LEFT_RECURSIVE, "should detect the left recursion");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Grammar with left recursion and CALLED nullable sub-pattern \n");

  str = context_retrieve(C, 1);
  failif(str[0] != 'f' || str[1] != 'o' || str[2] != 'o', "symbol table entry 1 is 'foo'");
  
  B = pexle_repeat_f(C, pexle_from_string(C, "a"), -1); /* B -> "a"? */
  failif(!B, "expected new expression");
  p = pattern_new(B, &stat);	/* bound */
  failif(!p, "expected new pattern");
  p->env = genv;
  stat = env_bind(genv, 1, env_new_value(Epattern_t, 0, (void *)p), &refB);
  failif(stat != 0, "expected success");

  printf("foo is: \n"); 
  print_pattern(p, 0, C); 

  
  A = pexle_choice_f(C, 		/* A -> foo A "b" / epsilon */
		 pexle_seq_f(C, 
			   pexle_call(C, refB),
			   pexle_seq_f(C, 
				     pexle_call(C, ref),
				     pexle_from_string(C, "b"))),
		   pexle_from_boolean(C, 1));

  failif(!A, "A is a valid expression");

  /* Now modify the "A" binding's value to the pattern based on the expression A */
  p = pattern_new(A, &stat);	/* bound */
  failif(!p, "expected new pattern");
  p->env = genv;		/* compilation env */
  free_pattern_at(ref);
  assert_unspecified_value_at(ref);
  stat = env_rebind(ref, env_new_value(Epattern_t, 0, (void *)p));
  failif(ref_not_found(ref), "bind should work");
  failif(stat != 0, "bind should work");

  printf("A is: \n"); 
  print_pattern(p, 0, C); 

  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p;
  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;
  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");
  stat = build_cfgraph(p, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  
  stat = sort_cfgraph(cst->g);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");

  print_pattern(cst->pat, 0, C);

  stat = set_metadata(cst);
  failif(stat != EXP_ERR_LEFT_RECURSIVE, "should detect the left recursion"); 

  /* ----------------------------------------------------------------------------- */
  printf("********************** Grammar with mutual left recursion and CALLED nullable sub-pattern \n");

  B = pexle_seq_f(C, 
		pexle_repeat_f(C, pexle_from_string(C, "a"), -1), /* "a"? */
		pexle_call(C, ref)			    /* A */
		);				    /* B -> "a"? A */
  failif(!B, "expected new expression");
  p = pattern_new(B, &stat);	/* bound */
  failif(!p, "expected new pattern");
  p->env = genv;
  free_pattern_at(refB);
  assert_unspecified_value_at(refB);
  stat = env_rebind(refB, env_new_value(Epattern_t, 0, (void *)p));
  failif(stat != 0, "expected success");
  
  A = pexle_choice_f(C, 		/* A -> foo "b" / epsilon */
		 pexle_seq_f(C, 
			   pexle_call(C, refB),
			   pexle_from_range(C, 'b', 'b')),
		   pexle_from_boolean(C, 1)); 

  failif(!A, "A is a valid expression");
  printf("A is: \n"); 
  print_exp(A, 0, C); 
  printf("foo is: \n"); 
  print_exp(B, 0, C); 

  /* Now modify the "A" binding's value to the pattern based on the expression A */
  p = pattern_new(A, &stat);	/* bound */
  failif(!p, "expected new pattern");
  p->env = genv;		/* compilation env */
  free_pattern_at(ref);
  assert_unspecified_value_at(ref);
  stat = env_rebind(ref, env_new_value(Epattern_t, 0, (void *)p));
  failif(ref_not_found(ref), "bind should work");
  failif(stat != 0, "bind should work");

  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p;
  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;
  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");
  stat = build_cfgraph(p, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  
  stat = sort_cfgraph(cst->g);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");

  print_pattern(cst->pat, 0, C);

  stat = set_metadata(cst);
  failif(stat != EXP_ERR_LEFT_RECURSIVE, "should detect the left recursion");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Add another non-terminal, and remove the mutual left recursion \n");

  /* <anon> -> "x" / A */
  B = pexle_choice_f(C, pexle_from_range(C, 'x', 'x'),
		   pexle_call(C, ref));

  failif(!B, "expected new expression");
  p = pattern_new(B, &stat);	/* bound */
  failif(!p, "expected new pattern");
  p->env = genv;
  stat = env_bind(genv, 0, env_new_value(Epattern_t, 0, (void *)p), &ref_anon);
  failif(stat != 0, "expected success");

  /* B -> "a"+ <anon> */
  B = pexle_seq_f(C, 
		pexle_repeat_f(C, pexle_from_range(C, 'a', 'a'), 1), /* "a"+ */
		pexle_call(C, ref_anon)		   /* A */
		);
  failif(!B, "expected new expression");
  p = pattern_new(B, &stat);	/* bound */
  failif(!p, "expected new pattern");
  p->env = genv;
  free_pattern_at(refB);
  assert_unspecified_value_at(refB);
  stat = env_rebind(refB, env_new_value(Epattern_t, 0, (void *)p));
  failif(stat != 0, "expected success");

  printf("foo is: \n"); 
  print_pattern(p, 0, C); 

  A = pexle_choice_f(C, 		/* A -> B "b" / epsilon */
		   pexle_seq_f(C, 
			     pexle_call(C, refB),
			     pexle_from_range(C, 'b', 'b')),
		   pexle_from_boolean(C, 1));

  failif(!A, "A is a valid expression");

  printf("<anon> is: \n"); 
  p = binding_pattern_value(ref_anon);
  failif(!p, "expected a pattern");
  print_pattern(p, 0, C); 

  /* Now modify the "A" binding's value to the pattern based on the expression A */
  p = pattern_new(A, &stat);	/* bound */
  failif(!p, "expected new pattern");
  p->env = genv;		/* compilation env */
  free_pattern_at(ref);
  assert_unspecified_value_at(ref);
  stat = env_rebind(ref, env_new_value(Epattern_t, 0, (void *)p));
  failif(ref_not_found(ref), "bind should work");
  failif(stat != 0, "bind should work");

  printf("A is: \n"); 
  print_pattern(p, 0, C); 

  print_env(genv, C);

  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p;
  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;
  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");
  stat = build_cfgraph(p, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  
  stat = sort_cfgraph(cst->g);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");

  print_pattern(cst->pat, 0, C);

  stat = set_metadata(cst);
  failif(stat != 0, "expected success");

  memset(&cs, 0, CHARSETSIZE); 
  setchar(cs.cs, 'A'); 
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NULLABLE, NOFAIL, NOT_HEADFAIL,
	     EXP_UNBOUNDED, 0, 123123,
	     NEEDFOLLOW, cs, NO_FIRSTSET, __LINE__); 

  /* ----------------------------------------------------------------------------- */
  printf("********************** Make the previous grammar mutually left recursive \n");

  /* <anon> -> "x" / A */
  B = pexle_choice_f(C, pexle_from_range(C, 'x', 'x'),
		   pexle_call(C, ref));

  failif(!B, "expected new expression");
  p = pattern_new(B, &stat);	/* bound */
  failif(!p, "expected new pattern");
  p->env = genv;
  stat = env_bind(genv, 0, env_new_value(Epattern_t, 0, (void *)p), &ref_anon);
  failif(stat != 0, "expected success");

  printf("<anon> is: \n"); 
  p = binding_pattern_value(ref_anon);
  failif(!p, "expected a pattern");
  print_pattern(p, 0, C); 

  /* B -> "a"* <anon> */
  B = pexle_seq_f(C, 
		pexle_repeat_f(C, pexle_from_set(C, "a", 1), 0), /* "a"* */
		pexle_call(C, ref_anon)			 /* A */
		);
  failif(!B, "expected new expression");
  p = pattern_new(B, &stat);	/* bound */
  failif(!p, "expected new pattern");
  p->env = genv;
  free_pattern_at(refB);
  assert_unspecified_value_at(refB);
  stat = env_rebind(refB, env_new_value(Epattern_t, 0, (void *)p));
  failif(stat != 0, "expected success");

  printf("foo is: \n"); 
  print_pattern(p, 0, C); 
  
  A = pexle_choice_f(C, 		/* A -> B "b" / epsilon */
		 pexle_seq_f(C, 
			   pexle_call(C, refB),
			   pexle_from_set(C, "b", 1)),
		   pexle_from_boolean(C, 1));

  failif(!A, "A is a valid expression");

  /* Now modify the "A" binding's value to the pattern based on the expression A */
  p = pattern_new(A, &stat);	/* bound */
  failif(!p, "expected new pattern");
  p->env = genv;		/* compilation env */
  free_pattern_at(ref);
  assert_unspecified_value_at(ref);
  stat = env_rebind(ref, env_new_value(Epattern_t, 0, (void *)p));
  failif(ref_not_found(ref), "bind should work");
  failif(stat != 0, "bind should work");

/*   printf("A is: \n");  */
/*   print_pattern(p, 0, C);  */

  print_env(genv, C);

  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p;
  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;
  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");
  stat = build_cfgraph(p, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  
  stat = sort_cfgraph(cst->g);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");

  printf("At refB is: ");
  print_pattern(binding_pattern_value(refB), 0, C);
  printf("<anon> is: ");
  print_pattern(binding_pattern_value(ref_anon), 0, C);
  printf("A is: \n");
  print_pattern(binding_pattern_value(ref), 0, C);

  print_env(genv, C);
  print_sorted_cfgraph(cst->g);

  printf("THIS TEST SHOULD CORRESPOND TO:\n");
  printf("  A -> (B 'b') / true\n");
  printf("  B -> 'a'* <anon>\n");
  printf("  <anon> -> 'x' / A\n");

  stat = set_metadata(cst); 
  failif(stat != EXP_ERR_LEFT_RECURSIVE, "should detect the left recursion");

  pexlc_env_free(genv);
  compstate_free(cst);
  pexlc_env_free(env);
  pexlc_env_free(toplevel);

  context_free(C);

  /* ----------------------------------------------------------------------------- */
  printf("\nDone.\n");
  printf("Don't forget to run contexttest next.\n");

  return 0;
}

