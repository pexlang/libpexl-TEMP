/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  inlineoptimizertest.c  TESTING inlining optimizer                        */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Vivek Reddy                                                     */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>		/* exit() */
#include "print.h"
#include "compile.h"
#include "vm.h"
#include "testframework.h"

static int assert_match(Match* y, Match* y_pred, const char* input){
    int crct = OK;
    if (y->matched != y_pred->matched) {
        fprintf(stderr, "Error: Match Failed for Input: %s \n\t No inline: %d, inline: %d\n", input, y->matched, y_pred->matched);
        fflush(stderr);
        crct = !OK;	
    }
    if (y->leftover != y_pred->leftover){
        fprintf(stderr, "Error: Leftover Failed for Input: %s \n\t No inline: %lu, inline: %lu\n", input, y->leftover, y_pred->leftover);
        fflush(stderr);	
        crct = !OK;	
    }
    if (y->abend != y_pred->abend){
        fprintf(stderr, "Error: Abend Failed for Input: %s \n\t No inline: %d, inline: %d\n", input, y->abend, y_pred->abend);
        fflush(stderr);	
        crct = !OK;	
    }
    return crct;
}

static Index lookup_entrypoint (const char *patname, vm_match *vm_m) {
  SymbolTableEntry *entry;
  Package *pkg = vm_m->pk_tble->packages[0];
  assert(pkg);
/*   print_symboltable(pkg->symtab);  */
  /* Find entrypoint for 'patname' */
  entry = symboltable_search(pkg->symtab, patname, NULL);
  if (!entry) {
    fprintf(stderr, "Search of symbol table for '%s' FAILED\n", patname);
    assert(0);
  }
  assert(entry_type(entry) == SYMBOL_TYPE_PATTERN);
  printf("** found pattern '%s' at entrypoint %u\n", patname, entry->value);
  return entry->value;
}

static int check_inline_opt(const char *input, const char *patname, pattern* (*generate_pt)(void)){
    
    vm_match *vm_m, *vm_m_il;
    pattern *pt1, *pt2;
    int stat, stat1, ret;
    size_t len;
    
    printf("----------------------------------- Test Case: %s ------------------------ \n", input);
    len = strlen(input);

    /* Without Inlining */
    pt1 = generate_pt();
    compile_generic_pattern(pt1, ONLY_PEEPHOLE_OPTIM);
    vm_m = create_pkg_tbl_with_user_pkg(pt1, input, len, NULL);
    stat = vm_start(vm_m->pk_tble, lookup_entrypoint(patname, vm_m), vm_m->buf, 0, len, debug_encoder, NULL, vm_m->match);
    /* print_symboltable(pt1->pkg->symtab); */

    /* With Inlining */
    pt2 = generate_pt();
    compile_generic_pattern(pt2, (ONLY_PEEPHOLE_OPTIM | ONLY_INLINE_OPTIM)); 
    vm_m_il = create_pkg_tbl_with_user_pkg(pt2, input, len, NULL);
    stat1 = vm_start(vm_m_il->pk_tble, lookup_entrypoint(patname, vm_m_il), vm_m_il->buf, 0, len, debug_encoder, NULL, vm_m_il->match);

    if (stat != stat1) printf("vm returned %d with no inlining, and %d with inlining\n", stat, stat1);
    assert(stat == stat1);
    ret = assert_match(vm_m->match, vm_m_il->match, input);

    vm_match_free(vm_m);
    vm_match_free(vm_m_il);

    generic_pattern_free(pt1);
    generic_pattern_free(pt2);
    
    return ret;
}


int main(void){


    printf("$$$ inlineoptimizertest\n");
    printf("------------------------- Unit Tests inline_optimizer ----------------------------------- \n");

    /* Simple Recursive patterns */
    failif(check_inline_opt("aaabbb", "A", &recursive_pattern)!= OK, "Unit Test Failed");
    failif(check_inline_opt("aaacbbbb", "A", &recursive_pattern)!= OK, "Unit Test Failed");
    
    failif(check_inline_opt("aaabbb", "A", &recursive_pattern2)!= OK, "Unit Test Failed");
    failif(check_inline_opt("aaacbbbb", "A", &recursive_pattern2)!= OK, "Unit Test Failed");
    failif(check_inline_opt("aaacaabcbcbcbcbc123", "A", &recursive_pattern2)!= OK, "Unit Test Failed");
    /*
    failif(check_inline_opt("aaacaabcbcbcbcbc", 6, 6, &recursive_pattern3)!= OK, "Unit Test Failed");
    failif(check_inline_opt("aaacaabcbcbcbcbc123", 6, 6, &recursive_pattern3)!= OK, "Unit Test Failed");
    failif(check_inline_opt("aaacaabcbcbcbcbc13", 6, 6, &recursive_pattern3)!= OK, "Unit Test Failed");
    */

    /* Ipv6 Tests */
    failif(check_inline_opt("2001:0db8:0000:0000:0000:ff00:0042:8329", "ipv6_strict", &create_ipv6)!= OK, "Unit Test Failed");
    failif(check_inline_opt("2001:db8:0:0:0:ff00:42:8329", "ipv6_strict", &create_ipv6)!= OK, "Unit Test Failed");
    failif(check_inline_opt("2001:db8::ff00:42:8329", "ipv6_strict", &create_ipv6)!= OK, "Unit Test Failed");
    failif(check_inline_opt("FEDC:BA98:7654:3210:FEDC:BA98:7654:3210", "ipv6_strict", &create_ipv6)!= OK, "Unit Test Failed");
    failif(check_inline_opt("1080:0:0:0:8:800:200C:4171", "ipv6_strict", &create_ipv6)!= OK, "Unit Test Failed");
    failif(check_inline_opt("3ffe:2a00:100:7031::1", "ipv6_strict", &create_ipv6)!= OK, "Unit Test Failed");
    failif(check_inline_opt("::192.9.5.5", "ipv6_strict", &create_ipv6)!= OK, "Unit Test Failed");
    failif(check_inline_opt("FFFF:129.144.52.38", "ipv6_strict", &create_ipv6)!= OK, "Unit Test Failed");
    failif(check_inline_opt("A:AA:ABC:ABCD:B", "ipv6_strict", &create_ipv6)!= OK, "Unit Test Failed");

    /* Ipv4 Tests */
    failif(check_inline_opt("255.255.255.255", "ipv4", &create_ipv4)!= OK, "Unit Test Failed");
    failif(check_inline_opt("0.0.0.0", "ipv4", &create_ipv4)!= OK, "Unit Test Failed");
    failif(check_inline_opt("255.255.255.255", "ipv4", &create_ipv4)!= OK, "Unit Test Failed");
    failif(check_inline_opt("1.2.234.123", "ipv4", &create_ipv4)!= OK, "Unit Test Failed");
    failif(check_inline_opt("255.0.0.255", "ipv4", &create_ipv4)!= OK, "Unit Test Failed");
    failif(check_inline_opt("255.255.255.255.255.255.255.255", "ipv4", &create_ipv4)!= OK, "Unit Test Failed");
    failif(check_inline_opt("1.1.1.256", "ipv4", &create_ipv4)!= OK, "Unit Test Failed");
    failif(check_inline_opt("1234.1.2.3", "ipv4", &create_ipv4)!= OK, "Unit Test Failed");
    failif(check_inline_opt("111.222.333.", "ipv4", &create_ipv4)!= OK, "Unit Test Failed");
    failif(check_inline_opt("111.222.333..444", "ipv4", &create_ipv4)!= OK, "Unit Test Failed");
    failif(check_inline_opt("999.999.999.999", "ipv4", &create_ipv4)!= OK, "Unit Test Failed");
    failif(check_inline_opt("256.1.1.1", "ipv4", &create_ipv4)!= OK, "Unit Test Failed");
    failif(check_inline_opt("1.256.1.1", "ipv4", &create_ipv4)!= OK, "Unit Test Failed");
    failif(check_inline_opt("1.1.256.1", "ipv4", &create_ipv4)!= OK, "Unit Test Failed");

    /* Ipv4-Ipv6 Tests Different Entry Points */
    failif(check_inline_opt("255.255.255.255", "ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("0.0.0.0", "ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("255.255.255.255", "ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("1.2.234.123", "ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("255.0.0.255", "ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("255.255.255.255.255.255.255.255", "ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("1.1.1.256", "ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("1234.1.2.3", "ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("111.222.333.", "ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("111.222.333..444", "ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("999.999.999.999", "ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("256.1.1.1", "ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("1.256.1.1", "ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("1.1.256.1", "ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("2001:0db8:0000:0000:0000:ff00:0042:8329", "ipv6_strict", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("2001:db8:0:0:0:ff00:42:8329", "ipv6_strict", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("2001:db8::ff00:42:8329", "ipv6_strict", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("FEDC:BA98:7654:3210:FEDC:BA98:7654:3210", "ipv6_strict", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("1080:0:0:0:8:800:200C:4171", "ipv6_strict", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("3ffe:2a00:100:7031::1", "ipv6_strict", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("::192.9.5.5", "ipv6_strict", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("FFFF:129.144.52.38", "ipv6_strict", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("A:AA:ABC:ABCD:B", "ipv6_strict", &create_ipaddr)!= OK, "Unit Test Failed");

    /* Ipv4-Ipv6 Tests Same Entry Point */
    failif(check_inline_opt("2001:0db8:0000:0000:0000:ff00:0042:8329", "ipv6 / ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("2001:db8:0:0:0:ff00:42:8329", "ipv6 / ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("2001:db8::ff00:42:8329", "ipv6 / ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("FEDC:BA98:7654:3210:FEDC:BA98:7654:3210", "ipv6 / ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("1080:0:0:0:8:800:200C:4171", "ipv6 / ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("3ffe:2a00:100:7031::1", "ipv6 / ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("::192.9.5.5", "ipv6 / ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("FFFF:129.144.52.38", "ipv6 / ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("A:AA:ABC:ABCD:B", "ipv6 / ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("255.255.255.255", "ipv6 / ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("0.0.0.0", "ipv6 / ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("255.255.255.255", "ipv6 / ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("1.2.234.123", "ipv6 / ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("255.0.0.255", "ipv6 / ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("255.255.255.255.255.255.255.255", "ipv6 / ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("1.1.1.256", "ipv6 / ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("1234.1.2.3", "ipv6 / ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("111.222.333.", "ipv6 / ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("111.222.333..444", "ipv6 / ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("999.999.999.999", "ipv6 / ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("256.1.1.1", "ipv6 / ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("1.256.1.1", "ipv6 / ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("1.1.256.1", "ipv6 / ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("100.100.100.122", "ipv6 / ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("0.00.00.000", "ipv6 / ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("254.256.255.255", "ipv6 / ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("AB.2.1.AB", "ipv6 / ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("1001:db8:::::", "ipv6 / ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("0:0:0:0:8:800:", "ipv6 / ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("3ffe:2a00:::0:0", "ipv6 / ipv4", &create_ipaddr)!= OK, "Unit Test Failed");
    failif(check_inline_opt("ABCD:abcd:ABcd:abCD:00:00:00", "ipv6 / ipv4", &create_ipaddr)!= OK, "Unit Test Failed");

    /* The tests below make use of the negative lookahead for an ipv4 pattern in ipv6_rest */
    failif(check_inline_opt("::192.9.5.5", "ipv6_strict", &create_ipaddr) != OK, "Unit Test Failed");
    failif(check_inline_opt("::FFFF:129.144.52.38", "ipv6_strict", &create_ipaddr) != OK, "Unit Test Failed");

    printf("\n\n ...... Inline Optimizer Tests Done\n\n");

    return 0;
}
