/*  -*- Mode: C/l; -*-                                                       */
/*                                                                           */
/*  buftest.c                                                               */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */


#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include "buf.h"

#pragma GCC diagnostic push  /* GCC 4.6 and up */
#pragma GCC diagnostic ignored "-Wlong-long"
static int64_t int64_min = INT64_MIN;
static int64_t int64_max = INT64_MAX;
#pragma GCC diagnostic pop

static int32_t int32_min = INT32_MIN;
static int32_t int32_max = INT32_MAX;
static int16_t int16_min = INT16_MIN;
static int16_t int16_max = INT16_MAX;

static int64_t int48_min = BUF_INT48_MIN;
static int64_t int48_max = BUF_INT48_MAX;

#define failif(test, msg)						\
  do { if (test) { fprintf(stderr, "Failed %s:%d: %s\n", __FILE__,	\
			   __LINE__, msg);				\
      fflush(stderr);							\
      exit(-1);								\
    }									\
  } while (0)

#define RANDOS 51000

#pragma GCC diagnostic push  /* GCC 4.6 and up */
#ifdef __clang__
/* nothing to do */
#elif __GNUC__
  #pragma GCC diagnostic ignored "-Wformat"
#elif _MSC_VER
  #error MS C compiler not supported
#elif __MINGW32__
/* nothing to do */
#endif

/* make a 64-bit value */
static int64_t random64(void) {
  return (int64_t) ((((uint64_t) rand())<<32) | rand());
}

int main (void) {

  FILE *f;

  int64_t randos64[RANDOS];
  int32_t randos32[RANDOS];
  int16_t randos16[RANDOS];
  
  int stat;
  Buffer *b1, *b2, *b3;
  int i, j, k;
  short ii, jj;
  Buf_cursor p, save_p;
  size_t len;
  const char *s;

  int16_t an_int16;
  int32_t an_int32;
  int64_t an_int64;

  char silly[BUF_INITIAL_SIZE];
  
  printf("$$$ buftest\n");


  printf("Part 1: test all functions on one buffer\n");

  b1 = buf_new(0);
  assert(b1->size == BUF_INITIAL_SIZE);
  assert(b1->next == 0);
  
  s = "Hello, world!";
  len = strlen(s);
  buf_addlstring(b1, s, len);

  p = NULL;
  p = buf_peeklstring(b1, p, 5);
  failif(!p, "should pass");
  failif(*p != 'H', "H");
  failif(*(p+4) != 'o', "o");
  save_p = p;

  p = buf_peeklstring(b1, p, len);
  failif(!p, "should pass");
  failif(p != save_p, "peek should not change cursor");

  p = buf_peeklstring(b1, p, len + 1);
  failif(p, "should fail");

  assert(b1->next == 13);

  save_p = b1->data + b1->next;
  stat = buf_addlstring(b1, "foobar", 6);
  failif(stat, "should pass");
  assert( b1->next == 19 );

  p = buf_readlstring(b1, save_p, 6);
  assert(p == b1->data + b1->next);
  assert(p - save_p == 6);
  
  stat = buf_addint32(b1, 1234567890);
  failif(stat, "should pass");
  assert( b1->next == 19 + 4 );
  p = b1->data + b1->next - 4;
  p = buf_peekint32(b1, p, &i);
  failif(!p, "should pass");
  assert( i == 1234567890 );
  save_p = p;
  p  = buf_readint32(b1, p, &j);
  failif(!p, "should pass");
  assert( i == j );
  assert( p == save_p + 4 );

  /* now repeat that with SHORT */

  stat = buf_addint16(b1, 16385);
  failif(stat, "should pass");
  assert( b1->next == 19 + 4 + 2 );
  p = b1->data + b1->next - 2;
  save_p = p;
  p = buf_peekint16(b1, p, &ii);
  failif(!p, "should pass");
  failif(p != save_p, "peek should not change non-null cursor");
  assert( ii == 16385 );
  save_p = p;
  p = buf_readint16(b1, p, &jj);
  failif(!p, "should pass");
  failif(p == save_p, "read should advance cursor");
  assert( ii == jj );
  assert( p == save_p + 2 );
  
  /* ---------------------------------------------------------------------------------------- */
  printf("Part 2: growing the buffer\n");

  len = b1->size - b1->next;	/* amount available in current storage block */
  assert( len < BUF_INITIAL_SIZE );
  failif(buf_isdynamic(b1), "should still be static");

  failif(buf_isimmutable(b1), "this is NOT an immutable buffer");

  for (k=0; k < (int) BUF_INITIAL_SIZE; k++) silly[k] = (char)(k & 0xff);

  buf_addlstring(b1, silly, BUF_INITIAL_SIZE);
  /* capacity should have doubled */
  assert( b1->size == 2 * BUF_INITIAL_SIZE );
  assert( b1->next = 19 + 4 + 2 + BUF_INITIAL_SIZE );

  failif(!buf_isdynamic(b1), "should be dynamic now");
  
  /* capacity should increase by 3 * BUF_INITIAL_SIZE */
  buf_prepsize(b1, 3 * BUF_INITIAL_SIZE);
  assert( b1->size == 19 + 4 + 2 + BUF_INITIAL_SIZE + (3 * BUF_INITIAL_SIZE) );



  /* ---------------------------------------------------------------------------------------- */
  printf("Part 3a: wrapping existing data in a buffer\n");

  b2 = buf_new_wrap(silly, 100);
  failif(!b2, "should succeed");
  assert( b2->size == 100 );
  assert( b2->next == b2->size );
  failif(!buf_isimmutable(b2), "wrapped buffers are immutable");
  stat = buf_addlstring(b2, silly, BUF_INITIAL_SIZE);
  /* Cannot resize a wrapped buffer (error returned as NULL) */
  assert( stat < 0 );
  assert( b2->size == 100 );
  assert( b2->next = 100 );
  assert( b2->data[0] == '\0' );
  assert( b2->data[1] == '\1' );
	  
  /* ---------------------------------------------------------------------------------------- */
  printf("Part 3b: initializing a new buffer with existing data\n");

  buf_free(b2);
  b2 = buf_new_from(silly, 100);
  failif(!b2, "should succeed");
  assert( 100 < BUF_INITIAL_SIZE );
  assert( b2->size == BUF_INITIAL_SIZE );
  assert( b2->next == 100 );
  failif(buf_isimmutable(b2), "non-wrapped buffers are mutable");
  stat = buf_addlstring(b2, silly, BUF_INITIAL_SIZE);
  /* Resize should work */
  assert( stat == 0 );
  assert( b2->size == 2 * BUF_INITIAL_SIZE );
  assert( b2->next = 100 );
  assert( b2->data[0] == '\0' );
  assert( b2->data[1] == '\1' );

  /* ---------------------------------------------------------------------------------------- */
  printf("Part 4: numbers\n");

  buf_free(b1);
  b1 = buf_new(0);
  for (i = 0; i < RANDOS; i++) {
    randos64[i] = random64();
    stat = buf_addint64(b1, randos64[i]);
    failif(stat, "should succeed");
  }
  assert( ((RANDOS * 8) > BUF_INITIAL_SIZE) ^ (!buf_isdynamic(b1)) );

  /* Now read them back */
  p = NULL;
  for (i = 0; i < RANDOS; i++) {
    p = buf_readint64(b1, p, &an_int64);
    if (!p) printf("i = %d, rand = %lld\n", i, randos64[i]);
    failif(!p, "p should be a valid cursor");
    failif(an_int64 != randos64[i], "mismatch!");
  }

  /* Do the same as above, but with 32-bit data */
  buf_free(b1);
  b1 = buf_new(0);
  for (i = 0; i < RANDOS; i++) {
    randos32[i] = rand();
    stat = buf_addint32(b1, randos32[i]);
    failif(stat, "should succeed");
  }

  /* Now read them back */
  p = NULL;
  for (i = 0; i < RANDOS; i++) {
    p = buf_readint32(b1, p, &an_int32);
    if (!p) printf("i = %d, rand = %d\n", i, randos32[i]);
    failif(!p, "p should be a valid cursor");
    failif(an_int32 != randos32[i], "mismatch!");
  }

  /* Do the same as above, but with 16-bit data */
  buf_free(b1);
  b1 = buf_new(0);
  for (i = 0; i < RANDOS; i++) {
    randos16[i] = rand() & 0xFFFF;
    stat = buf_addint16(b1, randos16[i]);
    failif(stat, "should succeed");
  }

  /* Now read them back */
  p = NULL;
  for (i = 0; i < RANDOS; i++) {
    p = buf_readint16(b1, p, &an_int16);
    if (!p) printf("i = %d, rand = %d\n", i, randos16[i]);
    failif(!p, "p should be a valid cursor");
    if (an_int16 != randos16[i])
      printf("Mismatch: expected %d, found %d\n", randos32[i], an_int16);
    failif(an_int16 != randos16[i], "mismatch!");
  }


  /* ---------------------------------------------------------------------------------------- */
  printf("Part 5: edge case numbers\n");

  buf_free(b1);
  b1 = buf_new(BUF_INITIAL_SIZE + 99);
  /* Could have tested odd sizes in Part 1 above */
  failif(b1->size != (BUF_INITIAL_SIZE + 99), "this should work");

#define test(sz, buf, n) do {						\
    p = (buf)->data + (buf)->next;					\
    an_int##sz = (n);							\
    failif(buf_addint##sz((buf), an_int##sz), "add should succeed");	\
    failif(!buf_readint##sz((buf), p, &an_int##sz), "read should succeed"); \
    if (an_int##sz != n) printf("expected %lld, found %lld\n", (int64_t) n, (int64_t) an_int##sz); \
    failif(an_int##sz != (n), "mismatch!");				\
  } while (0)

  test(64, b1, (int64_t) 0);
  test(64, b1, (int64_t) 1);
  test(64, b1, (int64_t) -1);
  test(64, b1, int64_min);
  test(64, b1, int64_max);

  test(32, b1, (int32_t) 0);
  test(32, b1, (int32_t) 1);
  test(32, b1, (int32_t) -1);
  test(32, b1, int32_min);
  test(32, b1, int32_max);

  test(16, b1, (int16_t) 0);
  test(16, b1, (int16_t) 1);
  test(16, b1, (int16_t) -1);
  test(16, b1, int16_min);
  test(16, b1, int16_max);
  
#define testvar(buf, n, bytes) do {						\
    p = (buf)->data + (buf)->next;					\
    an_int64 = (n);							\
    failif(buf_addint_varsize((buf), an_int64, (bytes)), "add should succeed"); \
    failif(!buf_readint_varsize((buf), p, &an_int64, (bytes)), "read should succeed"); \
    if (an_int64 != n) printf("expected %lld, found %lld\n", (int64_t) n, (int64_t) an_int64); \
    failif(an_int64 != (n), "mismatch!");				\
  } while (0)

  printf("64 bit max %lld, min %lld\n", int64_max, int64_min);
  testvar(b1, (int64_t) 0, 8);
  testvar(b1, (int64_t) 1, 8);
  testvar(b1, (int64_t) -1, 8);
  testvar(b1, (int64_t) int64_min, 8);
  testvar(b1, (int64_t) int64_max, 8);

  printf("48 bit max %lld, min %lld\n", int48_max, int48_min); 
  testvar(b1, (int64_t) 0, 6); 
  testvar(b1, (int64_t) 1, 6); 
  testvar(b1, (int64_t) -1, 6);  
  testvar(b1, (int64_t) int48_min, 6); 
  testvar(b1, (int64_t) int48_max, 6); 

  printf("32 bit max %d, min %d\n", int32_max, int32_min);
  testvar(b1, (int64_t) 0, 4);
  testvar(b1, (int64_t) 1, 4);
  testvar(b1, (int64_t) -1, 4); 
  testvar(b1, (int64_t) int32_min, 4);
  testvar(b1, (int64_t) int32_max, 4);

  printf("16 bit max %d, min %d\n", int16_max, int16_min);
  testvar(b1, (int64_t) 0, 2);
  testvar(b1, (int64_t) 1, 2);
  testvar(b1, (int64_t) -1, 2);
  testvar(b1, (int64_t) int16_min, 2);
  testvar(b1, (int64_t) int16_max, 2);

  /* ---------------------------------------------------------------------------------------- */
  printf("Part 6: error cases for numbers (errors will be logged to stderr)\n");

  assert(b1->next >= 8);

#define testvarfail(buf, n, bytes) do {						\
    p = (buf)->data + (buf)->next;					\
    an_int64 = (n);							\
    failif(!buf_addint_varsize((buf), an_int64, (bytes)), "add should fail"); \
  } while (0)


  /* invalid number of bytes for encoding */
  testvarfail(b1, (int64_t) int64_max, 9);
  testvarfail(b1, (int64_t) int64_max, 7);
  testvarfail(b1, (int64_t) int64_max, 5);
  testvarfail(b1, (int64_t) int64_max, 0);
  testvarfail(b1, (int64_t) int64_max, -1);
  
  /* Now do the same for readint */ 
  failif(buf_readint_varsize(b1, NULL, &an_int64, 9), "read should fail");
  failif(buf_readint_varsize(b1, NULL, &an_int64, 7), "read should fail");
  failif(buf_readint_varsize(b1, NULL, &an_int64, 5), "read should fail");
  failif(buf_readint_varsize(b1, NULL, &an_int64, 3), "read should fail");
  failif(buf_readint_varsize(b1, NULL, &an_int64, 1), "read should fail");
  failif(buf_readint_varsize(b1, NULL, &an_int64, 0), "read should fail");
  failif(buf_readint_varsize(b1, NULL, &an_int64, -1), "read should fail");
  failif(buf_readint_varsize(b1, NULL, &an_int64, INT32_MAX), "read should fail");
  failif(buf_readint_varsize(b1, NULL, &an_int64, INT32_MIN), "read should fail");
  

  /* value exceeds encoding size */
  testvarfail(b1, (int64_t) int64_max, 6);
  testvarfail(b1, (int64_t) int64_max, 4);
  testvarfail(b1, (int64_t) int64_max, 2);

  /* ---------------------------------------------------------------------------------------- */
  printf("Part 7: Using prepsize and the unsafe add routines \n");

  len = BUF_INITIAL_SIZE + 733;
  p = buf_prepsize(b1, len);
  for (i=0; i<(int)len; i++) buf_addchar_UNSAFE(b1, (char) i);
  assert( (b1->data + b1->next) == (p + len) );
  for (i=0; i<(int)len; i++) failif(((char) *(p + i)) != (char) i, "mismatch!");

  s = "Hello, world!";
  p = buf_prepsize(b1, strlen(s));
  buf_addlstring_UNSAFE(b1, s, strlen(s));
  assert( (b1->data + b1->next) == (p + strlen(s)) );

  /* ---------------------------------------------------------------------------------------- */
  printf("Part 8: File i/o\n");

  /* Length only */
  f = fopen("/tmp/buftest-output", "w");
  assert(f);
  stat = buf_writelen(f, b1);
  assert(stat == 0);
  fclose(f);
  f = fopen("/tmp/buftest-output", "r");
  assert(f);
  stat = buf_readlen(f, &len);
  assert(stat == 0);
  assert( len == b1->next );
  fclose(f);

  /* Entire buffer */
  f = fopen("/tmp/buftest-output", "w");
  assert(f);
  stat = buf_writelen(f, b1);
  assert(stat == 0);
  stat = buf_write(f, b1);
  assert(stat == 0);
  fclose(f);
  f = fopen("/tmp/buftest-output", "r");
  assert(f);
  stat = buf_readlen(f, &len);
  assert(stat == 0);
  b3 = buf_read(f, len);
  assert(b3);
  fclose(f);
  assert( b3->next == b1->next );

  for (i=0; i < (int) len; i++)
    assert( (char) *(b3->data + i) == (char) *(b1->data + i));


  /* ---------------------------------------------------------------------------------------- */
  printf("Part the last: freeing buffers\n");

  buf_free(b3); 
  buf_free(b2); 
  buf_free(b1); 
  
  printf("Done.\n");
  return 0;
}
#pragma GCC diagnostic pop
