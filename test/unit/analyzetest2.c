/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  analyzetest2.c  TESTING analyze.c AND dependency.c                       */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>		/* exit() */
#include "print.h"
#include "analyze.h"
#include "compile.h"		/* pexlc_env_free() */

#define YES 1
#define NO 0

/* Borrowed from analyze.c */
/* Follow ref, extract binding field, return NULL if it is not a Pattern. */
static Pattern *binding_pattern_value (Ref ref) {
  Binding *b = env_get_binding(ref);
  if (!b) return NULL;		/* invalid ref */
  if (b->val.type != Epattern_t) return NULL;
  return (Pattern *) b->val.ptr;
}

#define failif(test, msg)						\
  do { if (test) { fprintf(stderr, "Failed %s:%d: %s\n", __FILE__,	\
			   __LINE__, msg);				\
      fflush(stderr);							\
      exit(-1);								\
    }									\
  } while (0)


int main(void) {

  uint32_t min, max;
  Context *C = NULL;
  
  int stat;
  CompState *cst;
  Pattern *p, *p1, *p3, *p4, *p5;
  Index i;
  CFgraph *graph;

  Charset cs;
  Node *node;
  unsigned char c;

  Expression *tstr, *tnum;
  Expression *t1, *t2, *t3, *t4, *t5; 
  Expression *A;

  Env *toplevel;
  Ref ref, refKEEP;

  printf("$$$ analyzetest2\n");
  printf("analyzetest2: fix open calls, compute metadata, deplists \n\n");

  /* Error checking */
  stat = fix_open_calls(NULL);
  failif(stat != EXP_ERR_NULL, "caught null compstate arg");

  p = pattern_new(NULL, &stat);
  failif(p, "caught null exp arg");
  failif(stat != EXP_ERR_NULL, "caught null exp arg");

  A = pexle_from_boolean(C, 1);
  failif(!A, "expected success");
  p = pattern_new(A, &stat);
  failif(!p, "expected success");
  failif(stat != 0, "expected success");

  stat = build_cfgraph(NULL, &graph);
  failif(stat != EXP_ERR_NULL, "expected to catch null arg error");
  tstr = pexle_from_string(C, "foo");
  pattern_free(p);

  p = pattern_new(tstr, &stat);
  failif(!p, "expected a pattern");
  stat = build_cfgraph(p, NULL);
  failif(stat != EXP_ERR_NULL, "expected to catch null arg error");
  stat = build_cfgraph(NULL, NULL);
  failif(stat != EXP_ERR_NULL, "expected to catch null arg error");

  graph = NULL;
  stat = sort_cfgraph(graph);
  failif(stat != EXP_ERR_NULL, "expected to catch null arg error");

  pattern_free(p);

  /* ----------------------------------------------------------------------------- */
  printf("********************** Fixing open calls \n");

  printf("Using literal string 'hello'\n");
  tstr = pexle_from_string(C, "hello");
  failif(!tstr, "expected new expression");
  p = pattern_new(tstr, &stat);
  failif(!p, "expected new pattern");
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p;
  toplevel = env_new(NULL, NO);
  failif(!toplevel, "new toplevel should succeed");
  p->env = toplevel;
  /* Error check */
  stat = build_cfgraph(p, &(cst->g));
  failif(stat != EXP_ERR_INTERNAL, "p->fixed is null because we have not set it yet");

  cfgraph_free(cst->g);

  p->fixed = pexle_copy(p->tree);
  stat = build_cfgraph(p, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  failif(cst->g->next != 1, "size of dependency graph should be 1 vertex");

  stat = sort_cfgraph(cst->g);
  printf("stat = %d\n", stat);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");
  failif(cst->g->order->size != 1, "size of toposort should be 1 vertex");
  print_sorted_cfgraph(cst->g);
  print_cfgraph(cst->g); 
  failif(cst->g->order->top != 1, "size of order array");
  failif(cst->g->order->elements[0].vnum != 0, "first component, one vertex");
  failif(cst->g->order->elements[0].cnum != -1, "correct component number");

  failif(!cst->pat, "precondition for next test");

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;
  
  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success, as no calls to fix");

  node = tstr->node;
  failif(exp_hascaptures(node), "this exp does not capture");
  failif(exp_itself_hascaptures(node), "this exp does not capture");
  stat = exp_patlen(node, &min, &max);
  failif(stat != EXP_BOUNDED, "expected patlen to return BOUNDED");
  failif((min != 5) || (max != 5), "wrong min/max");
  failif(exp_nullable(node), "literal non-empty string not nullable");
  failif(exp_nofail(node), "literal non-empty string can fail");
  failif(exp_headfail(node) != 0, "literal multi-char string is not headfail");
  failif(exp_needfollow(node), "literal non-empty string does not need follow set");
  stat = exp_getfirst(node, fullset, &cs);
  failif(stat != 0, "firstset can be used as test, does not accept epsilon");
  /* Now check the contents of cs for 'h' */
  for (c = 0; c < 255; c++)
    if (c == 'h')
      failif(!testchar(cs.cs, c), "cs should contain only 'h'");
    else
      failif(testchar(cs.cs, c), "cs should contain only 'h'");


  printf("Calling pattern for literal string 'hello'\n");
  failif(!p, "precondition for next test");
  stat = env_bind(toplevel, 123, env_new_value(Epattern_t, 0, (void *)p), &ref);
  failif(stat != 0, "expected bind to succeed");
  failif(ref_not_found(ref), "expected a valid ref");
  failif(ref.env != toplevel, "ref should point to toplevel where binding lives");

  t1 = pexle_call(C, ref);
  failif(!t1, "expected new expression");

  refKEEP = ref;

  /* Wrap the call in a new pattern struct */
  p1 = pattern_new(t1, &stat);
  failif(!p1, "expected new pattern");
  failif(stat != 0, "expected new pattern");

  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p1;
  p1->env = toplevel;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);

  printf("Ensuring that build_cfgraph catches certain errors\n");
  stat = build_cfgraph(p1, &(cst->g));
  printf("stat = %d\n", stat);
  failif(stat != EXP_ERR_OPENFAIL, "found TOpenCall");

  printf("Fixing open calls\n");
  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");

  printf("Note: toplevel is %p\n", (void *) toplevel);
  printf("Note: p1->env is %p\n", (void *) p1->env);
  printf("Note: cst->pat->env is %p\n", (void *) cst->pat->env);

  printf("Using build_cfgraph correctly to operate on TCall\n");
  stat = build_cfgraph(p1, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  failif(cst->g->next != 2, "size of dependency graph should be 2 (p1 and its call target)");
  /* Check the contents of the deplist for p1 */
  i = 0;
  do {
    p = cst->g->deplists[0][i++];
    printf("[0][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(refKEEP);
  failif(!p, "should get our call target pattern back");

  failif(cst->g->deplists[0][0] != p1, "vertex p1");
  failif(cst->g->deplists[0][1] != p, "call target");
  failif(cst->g->deplists[0][2] != NULL, "NULL terminator");

  /* Check the contents of the deplist for the call target, p */
  i = 0;
  do {
    p = cst->g->deplists[1][i++];
    printf("[1][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(refKEEP);
  failif(!p, "should get our call target pattern back");

  failif(cst->g->deplists[1][0] != p, "vertex p");
  failif(cst->g->deplists[1][1] != NULL, "NULL terminator");

  print_cfgraph(cst->g);  

  stat = sort_cfgraph(cst->g);
  printf("stat = %d\n", stat);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");
  failif(cst->g->order->size != 2, "wrong size of toposort result");
  print_sorted_cfgraph(cst->g);
  failif(cst->g->order->top != 2, "size of order array");
  failif(cst->g->order->elements[0].vnum != 1, "first component, one vertex");
  failif(cst->g->order->elements[0].cnum != -2, "correct component number");
  failif(cst->g->order->elements[1].vnum != 0, "first component, one vertex");
  failif(cst->g->order->elements[1].cnum != -1, "correct component number");

  pattern_free(p1);

  /* ----------------------------------------------------------------------------- */
  printf("********************** Using combinator 'seq' \n");

  t1 = pexle_call(C, refKEEP);		  /* "hello" */
  t2 = pexle_from_bytes(C, "a", 1);	  /* "a" */
  t3 = pexle_seq(C, t1, t2);
  failif(!t3, "should succeed");

  /* Wrap the call in a new pattern struct */
  p3 = pattern_new(t3, &stat);
  failif(!p3, "expected new pattern");
  failif(stat != 0, "expected new pattern");
  p3->env = toplevel;

  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p3;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);

  printf("Fixing open calls\n");
  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");

  printf("Building dependency graph\n");
  stat = build_cfgraph(p3, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  failif(cst->g->next != 2, "size of dependency graph should be 2 (p3 and its call target)");
  /* Check the contents of the deplist for p3 */
  i = 0;
  do {
    p = cst->g->deplists[0][i++];
    printf("[0][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(refKEEP);
  failif(!p, "should get our call target pattern back");

  failif(cst->g->deplists[0][0] != p3, "vertex p3");
  failif(cst->g->deplists[0][1] != p, "call target");
  failif(cst->g->deplists[0][2] != NULL, "NULL terminator");

  /* Check the contents of the deplist for the call target, p */
  i = 0;
  do {
    p = cst->g->deplists[1][i++];
    printf("[1][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(refKEEP);
  failif(!p, "should get our call target pattern back");

  failif(cst->g->deplists[1][0] != p, "vertex p");
  failif(cst->g->deplists[1][1] != NULL, "NULL terminator");

  print_cfgraph(cst->g);  

  stat = sort_cfgraph(cst->g);
  printf("stat = %d\n", stat);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");
  failif(cst->g->order->size != 2, "wrong size of toposort result");
  print_sorted_cfgraph(cst->g);
  failif(cst->g->order->top != 2, "size of order array");
  failif(cst->g->order->elements[0].vnum != 1, "first component, one vertex");
  failif(cst->g->order->elements[0].cnum != -2, "correct component number");
  failif(cst->g->order->elements[1].vnum != 0, "first component, one vertex");
  failif(cst->g->order->elements[1].cnum != -1, "correct component number");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Using combinator 'choice' \n");

  /* Choice of t2 and t3, where t3 is a seq that makes a call to p */
  t4 = pexle_choice(C, t2, t3);
  failif(t4->node->tag != TChoice, "should get TChoice");
  node = t4->node;
  failif(!node, "precondition for next tests");

  /* Wrap the call in a new pattern struct */
  p4 = pattern_new(t4, &stat);
  failif(!p4, "expected new pattern");
  failif(stat != 0, "expected new pattern");
  p4->env = toplevel;

  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p4;
  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g); 

  printf("Fixing open calls\n");
  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");

  printf("Building dependency graph\n");
  stat = build_cfgraph(p4, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  failif(cst->g->next != 2, "size of dependency graph should be 2 (p4 and its call target)");
  /* Check the contents of the deplist for p3 */
  i = 0;
  do {
    p = cst->g->deplists[0][i++];
    printf("[0][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(refKEEP);
  failif(!p, "should get our call target pattern back");

  failif(cst->g->deplists[0][0] != p4, "vertex p4");
  failif(cst->g->deplists[0][1] != p, "call target");
  failif(cst->g->deplists[0][2] != NULL, "NULL terminator");

  /* Check the contents of the deplist for the call target, p */
  i = 0;
  do {
    p = cst->g->deplists[1][i++];
    printf("[1][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(refKEEP);
  failif(!p, "should get our call target pattern back");

  failif(cst->g->deplists[1][0] != p, "vertex p");
  failif(cst->g->deplists[1][1] != NULL, "NULL terminator");

  print_cfgraph(cst->g);  

  stat = sort_cfgraph(cst->g);
  printf("stat = %d\n", stat);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");
  failif(cst->g->order->size != 2, "wrong size of toposort result");
  print_sorted_cfgraph(cst->g);
  failif(cst->g->order->top != 2, "size of order array");
  failif(cst->g->order->elements[0].vnum != 1, "first component, one vertex");
  failif(cst->g->order->elements[0].cnum != -2, "correct component number");
  failif(cst->g->order->elements[1].vnum != 0, "first component, one vertex");
  failif(cst->g->order->elements[1].cnum != -1, "correct component number");

  /* Choice of t1 and t3, where t1 calls p and p3 is a seq that makes a call to p */
  t4 = pexle_choice(C, t1, t3);
  failif(t4->node->tag != TChoice, "should get TChoice");
  node = t4->node;
  failif(!node, "precondition for next tests");

  /* Wrap the call in a new pattern struct */
  pattern_free(p4);
  p4 = pattern_new(t4, &stat);
  failif(!p4, "expected new pattern");
  failif(stat != 0, "expected new pattern");
  p4->env = toplevel;

  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p4;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);

  printf("Fixing open calls\n");
  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");

  printf("Building dependency graph\n");
  stat = build_cfgraph(p4, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  failif(cst->g->next != 2, "size of dependency graph should be 2 (p4 and its call target)");
  /* Check the contents of the deplist for p3 */
  i = 0;
  do {
    p = cst->g->deplists[0][i++];
    printf("[0][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(refKEEP);
  failif(!p, "should get our call target pattern back");

  failif(cst->g->deplists[0][0] != p4, "vertex p4");
  failif(cst->g->deplists[0][1] != p, "call target");
  failif(cst->g->deplists[0][2] != p, "second instance of same call target");
  failif(cst->g->deplists[0][3] != NULL, "NULL terminator");

  /* Check the contents of the deplist for the call target, p */
  i = 0;
  do {
    p = cst->g->deplists[1][i++];
    printf("[1][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(refKEEP);
  failif(!p, "should get our call target pattern back");

  failif(cst->g->deplists[1][0] != p, "vertex p");
  failif(cst->g->deplists[1][1] != NULL, "NULL terminator");

  print_cfgraph(cst->g);  

  stat = sort_cfgraph(cst->g);
  printf("stat = %d\n", stat);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");
  failif(cst->g->order->size != 2, "wrong size of toposort result");
  print_sorted_cfgraph(cst->g);
  failif(cst->g->order->top != 2, "size of order array");
  failif(cst->g->order->elements[0].vnum != 1, "first component, one vertex");
  failif(cst->g->order->elements[0].cnum != -2, "correct component number");
  failif(cst->g->order->elements[1].vnum != 0, "first component, one vertex");
  failif(cst->g->order->elements[1].cnum != -1, "correct component number");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Using combinator 'not' \n");

  /* t5 = not t4 */
  t5 = pexle_not(C, t4);
  failif(t5->node->tag != TNot, "should get TNot");
  node = t5->node;
  failif(!node, "precondition for next tests");

  /* Wrap the call in a new pattern struct */
  p5 = pattern_new(t5, &stat);
  failif(!p5, "expected new pattern");
  failif(stat != 0, "expected new pattern");
  p5->env = toplevel;

  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p5;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);

  printf("Fixing open calls\n");
  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");

  printf("Building dependency graph\n");
  stat = build_cfgraph(p5, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  failif(cst->g->next != 2, "size of dependency graph should be 2 (p5 and its call target)");
  /* Check the contents of the deplist for p3 */
  i = 0;
  do {
    p = cst->g->deplists[0][i++];
    printf("[0][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(refKEEP);
  failif(!p, "should get our call target pattern back");

  failif(cst->g->deplists[0][0] != p5, "vertex p5");
  failif(cst->g->deplists[0][1] != p, "call target");
  failif(cst->g->deplists[0][2] != p, "second instance of same call target");
  failif(cst->g->deplists[0][3] != NULL, "NULL terminator");

  /* Check the contents of the deplist for the call target, p */
  i = 0;
  do {
    p = cst->g->deplists[1][i++];
    printf("[1][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(refKEEP);
  failif(!p, "should get our call target pattern back");

  failif(cst->g->deplists[1][0] != p, "vertex p");
  failif(cst->g->deplists[1][1] != NULL, "NULL terminator");

  print_cfgraph(cst->g); 

  stat = sort_cfgraph(cst->g);
  printf("stat = %d\n", stat);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");
  failif(cst->g->order->size != 2, "wrong size of toposort result");
  print_sorted_cfgraph(cst->g);
  failif(cst->g->order->top != 2, "size of order array");
  failif(cst->g->order->elements[0].vnum != 1, "first component, one vertex");
  failif(cst->g->order->elements[0].cnum != -2, "correct component number");
  failif(cst->g->order->elements[1].vnum != 0, "first component, one vertex");
  failif(cst->g->order->elements[1].cnum != -1, "correct component number");

  pattern_free(p3);
  pattern_free(p4);
  pattern_free(p5);
  pexlc_env_free(toplevel);

  /* ----------------------------------------------------------------------------- */
  printf("********************** Testing dynamic expansion of cfgraph (ensure_vertex_space)' \n");

  toplevel = env_new(NULL, NO); 
  failif(!toplevel, "new toplevel should succeed"); 

  pexle_free(t1); pexle_free(t2);
  
  /* Construct a long call chain that ends in a call to tnum */
  tnum = pexle_from_number(C, 3);
  failif(!tnum, "expected new expression");
  t1 = tnum;
  for(i = 0; i < (3 * EXP_CFG_INITIAL_SIZE); i++) {
    p = pattern_new(t1, &stat);
    failif(!p, "expected new pattern");
    p->env = toplevel;
    stat = env_bind(toplevel, i+900, env_new_value(Epattern_t, 0, (void *)p), &ref);
    failif(stat != 0, "expected bind to succeed");
    failif(ref_not_found(ref), "expected a valid ref");
    failif(ref.env != toplevel, "ref should point to toplevel where binding lives");
    t2 = pexle_call(C, ref);
    failif(!t2, "should succeed");
    t1 = t2;
  }
  printf("Toplevel env is: "); print_env(toplevel, C);
  printf("Expression is: "); print_exp(t2, 1, NULL);
  printf("Entire call chain is: \n");
  i = 1;
  t4 = t2;
  while (t4->node->tag == TOpenCall) {
    set_ref_from_node(ref, t4->node);
    p = binding_pattern_value(ref);
    printf("%5d: exp %p calls exp %p\n", i, (void *) t4, (void *) p->tree);
    t4 = p->tree; i++;
  }
  if (t4->node->tag == TSeq) 
    printf("%5d: exp %p has tag TSeq\n", i, (void *) t4);
  else {
    printf("%5d: exp %p has unexpected tag %d", i, (void *) t4, t4->node->tag);
    failif(0, "Unexpected tag at the first non-call");
  }
  failif(i != (3 * EXP_CFG_INITIAL_SIZE) + 1, "Wrong length of call chain");

  p = pattern_new(t2, &stat);
  failif(!p, "expected new pattern");
  p->env = toplevel;		/* compilation env */
  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);

  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");
  stat = build_cfgraph(p, &(cst->g));
  printf("stat = %d\n", stat);
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  printf("size = %u\n", cst->g->next);
  failif(cst->g->next != (3 * EXP_CFG_INITIAL_SIZE) + 1, "wrong size of dependency graph");

  print_cfgraph(cst->g);

  stat = sort_cfgraph(cst->g);
  printf("stat = %d\n", stat);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");
  failif(cst->g->order->size != (3 * EXP_CFG_INITIAL_SIZE) + 1, "wrong size of toposort result");
  print_sorted_cfgraph(cst->g);
  failif(cst->g->order->top != (3 * EXP_CFG_INITIAL_SIZE) + 1, "size of order array");
  failif(cst->g->order->elements[0].vnum != (3 * EXP_CFG_INITIAL_SIZE), "first vertex in first component");
  failif(cst->g->order->elements[0].cnum != - ((3 * EXP_CFG_INITIAL_SIZE) + 1), "correct component number");
  failif(cst->g->order->elements[3 * EXP_CFG_INITIAL_SIZE].vnum != 0, "first component, one vertex");
  failif(cst->g->order->elements[3 * EXP_CFG_INITIAL_SIZE].cnum != -1, "correct component number");

  pattern_free(p);

  /* ----------------------------------------------------------------------------- */
  printf("********************** Testing dynamic expansion of deplist (ensure_deplist_space)' \n");

  pexlc_env_free(toplevel);
  toplevel = env_new(NULL, NO); 
  failif(!toplevel, "new toplevel should succeed"); 

  /* Construct a pattern that calls tstr many times */
  tstr = pexle_from_string(C, "hello");
  failif(!tstr, "expected new expression");
  p = pattern_new(tstr, &stat);
  failif(!p, "expected new pattern");
  p->env = toplevel;
  stat = env_bind(toplevel, i+900, env_new_value(Epattern_t, 0, (void *)p), &ref);
  failif(stat != 0, "expected bind to succeed");
  failif(ref_not_found(ref), "expected a valid ref");
  failif(ref.env != toplevel, "ref should point to toplevel where binding lives");
  t2 = pexle_call(C, ref);
  failif(!t2, "should succeed");
  for(i = 0; i < (3 * EXP_DEPLIST_INITIAL_SIZE); i++) {
    t2 = pexle_seq_f(C, t2, pexle_call(C, ref)); /* also tests correctassociativity */
    failif(!t2, "should succeed");
  }
  printf("Toplevel env is: "); print_env(toplevel, C);
  printf("Expression is: "); print_exp(t2, 1, NULL);

  p = pattern_new(t2, &stat);
  failif(!p, "expected new pattern");
  p->env = toplevel;		/* compilation env */
  compstate_free(cst);
  cst = compstate_new();
  failif(!cst, "expected new comp state");
  cst->pat = p;
  
  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);

  stat = fix_open_calls(cst);
  failif(stat != 0, "expected success");
  stat = build_cfgraph(p, &(cst->g));
  failif(stat != 0, "expected dependency graph");
  failif(!cst->g, "expected dependency graph");
  failif(cst->g->next != 2, "wrong size of dependency graph");
  i = 1;
  while (cst->g->deplists[0][i]) i++;
  i--;			   /* do not count dl[0], the vertex itself */
  failif(i != (3 * EXP_DEPLIST_INITIAL_SIZE) + 1, "wrong size of dependency list");

  print_cfgraph(cst->g); 

  stat = sort_cfgraph(cst->g);
  printf("stat = %d\n", stat);
  failif(stat, "expected successful topo sort");
  failif(!cst->g->order, "expected output structure");
  failif(cst->g->order->size != 2, "wrong size of toposort result");
  print_sorted_cfgraph(cst->g);
  failif(cst->g->order->top != 2, "size of order array");
  failif(cst->g->order->elements[0].vnum != 1, "first component, one vertex");
  failif(cst->g->order->elements[0].cnum != -2, "correct component number");
  failif(cst->g->order->elements[1].vnum != 0, "first component, one vertex");
  failif(cst->g->order->elements[1].cnum != -1, "correct component number");

  pattern_free(p);
  compstate_free(cst);

  /* ----------------------------------------------------------------------------- */

  pexlc_env_free(toplevel);
  context_free(C);

  printf("\nDone.\n");
  printf("Don't forget to run analyzetest3 next.\n");

  return 0;
}

