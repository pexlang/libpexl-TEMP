/*  -*- Mode: C; -*-                                                              */
/*                                                                                */
/* testframework.h  for testing complex hand-written patterns                     */
/*                                                                                */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Vivek Reddy Karri                                                    */

#include <string.h>
#include <stdlib.h>		/* exit() */
#include <errno.h>		/* errno */
#include "compile.h"
#include "vm.h"
#include "package.h"
#include "common.h" /* Compiler Optimization Masks */

#define failif(test, msg)						\
  do { if (test) { fprintf(stderr, "Failed %s:%d: %s\n", __FILE__,	\
			   __LINE__, msg);				\
      fflush(stderr);							\
      exit(-1);								\
    }									\
  } while (0)

/* 
A Generic Template for writing a Pattern
    
Points to Consider before writing one:
1) A Pattern can be Composed of multiple Expressions/References.
2) This is handled by having an array of Expressions & References.
3) Make sure the First Expression in the array is the one which 
                    is the one to be compiled to generate a package.

*/
typedef struct generic_pattern {
    size_t num_expr;
    Context *C;
    Package *pkg;
    Expression **exp_arr; /* Array of Expressions */
    Ref *ref_arr;        /* Array of References */
    Env *toplevel;
    Env *env;
}pattern;

typedef struct _vm_match {
    Buffer *buf;
    PackageTable *pk_tble;
    Match *match;
    SymbolTableEntry *entry;
}vm_match;

/* Methods to operate on Patern & vm_match Struct */

Package* compile_generic_pattern(pattern *, short);
void generic_pattern_free(pattern *);
void vm_match_free(vm_match* );

Package* compile_generic_pattern(pattern* pt, short optim_mask){
    Error err; 
    if (!pt || !pt->ref_arr) {
        printf("No Expression found to compile, \
                         Please Create an Entry Point Expression/Reference at Index 0 \
                                of the Expression Array/Reference Array");
        return NULL;
    }
    pt->pkg = pexlc_compile_bound_expression(pt->C, pt->ref_arr[0], &err, optim_mask);
    return pt->pkg;
}

void generic_pattern_free(pattern* pt){
    if (!pt) return ;
    if (pt->pkg) package_free(pt->pkg);
    if (pt->env) env_free(pt->env);
    if (pt->toplevel) env_free(pt->toplevel);
    if (pt->C) context_free(pt->C);
    free(pt->exp_arr); 
    if (pt->ref_arr) free(pt->ref_arr);
    free(pt);
}

void vm_match_free(vm_match* pkg){
    if (!pkg) return ;
    if (pkg->buf) buf_free(pkg->buf);
    if (pkg->pk_tble) packagetable_free(pkg->pk_tble);
    if (pkg->match->data) buf_free(pkg->match->data);
    if (pkg->match) match_free(pkg->match);
    free(pkg);
}

/* Only user package is set in this function */
/* Any Extra Packages which are to be added to the Package Table
    must be handled outside of this method */
static vm_match* create_pkg_tbl_with_user_pkg(pattern* pt, const char *input, size_t len, const char* prefix){
    vm_match* vm_m;
    if (!pt || !input) return NULL;
    vm_m = (vm_match*)malloc(sizeof(vm_match));

    if (vm_m == NULL) {
        fprintf(stderr, "Error Allocating Memory: %s\n", strerror(errno));
        exit(-1);	
    }

    vm_m->buf = buf_new_wrap(input, len);

    vm_m->pk_tble = packagetable_new(0);
    if (!vm_m) failif(0, "expected new package table");

    if (packagetable_set_user(vm_m->pk_tble, pt->pkg) < 0) {
        failif(0, "expected successful set of user package");
    }

    if (package_set_default_prefix(pt->pkg, prefix) != 0) {
        failif(0, "expected success in setting the prefix");
    }

    vm_m->match = match_new();
    if (!vm_m->match) failif(0, "expected new match structure");
    
    return vm_m;
}




/* Example Pattern functions Implemented */
static pattern* create_ipv6(void);
static pattern* create_ipv4(void);
static pattern* create_ipaddr(void);


/* 
An Example usage of pattern template to create a  pattern.

                    Production Rules for ipv6 Pattern:

xdigit = [0-9a-fA-F]

ipv6_component = [xdigit] [xdigit]{0,3}

ipv6_rest = { ":" !>ipv4 ipv6_component }
Note: First version of this file used this simplified rule: ipv6_rest = { ":" ipv6_component}

ip_address_v6 = { ipv6_component ipv6_rest{7} } /
			    { ipv6_component "::" ipv6_component ipv6_rest{0,4} } /
			    { ipv6_component ipv6_rest{1} "::" ipv6_component ipv6_rest{0,3} } /
			    { ipv6_component ipv6_rest{2} "::" ipv6_component ipv6_rest{0,2} } /
			    { ipv6_component ipv6_rest{3} "::" ipv6_component ipv6_rest{0,1} } /
			    { ipv6_component ipv6_rest{4} "::" ipv6_component } /
			    { ipv6_component ipv6_rest{5} "::" } /
			    { "::" ipv6_component ipv6_rest{0,5} } /
			    { "::" } -- undefined address

ipv6_strict = ip_address_v6

                    Production Rules for ipv4 Pattern:

ipv4_component =  [0] /
			     {[1] [0-9]? [0-9]?} / 
			     {[2] [0-4] [0-9]} / {[2] [5] [0-5]} / {[2] [0-9]?} /
			     {[3-9] [0-9]?} 
			     
ip_address_v4 = { ipv4_component {"." ipv4_component}{3} }


*/


/* ---------------------------  Primitive Chunks for IPv6    ------------------------- */

static const char xdigit_set[] = "0123456789abcdefABCDEF";

static Expression* exp_xdigit(pattern* ip){
    return pexle_from_set(ip->C, xdigit_set, strlen(xdigit_set));
}

static Expression* expr_ipv6_component(pattern* ip){
  return pexle_seq_f(ip->C, 
		     exp_xdigit(ip), 
		     pexle_repeat_f(ip->C, exp_xdigit(ip), -3));
}

static Expression* expr_ipv6_rest(pattern* ip, Ref ipv4_ref){
  return pexle_seq_f(ip->C,
		   pexle_from_string(ip->C, ":"),
		   pexle_seq_f(ip->C,
			     pexle_not_f(ip->C, pexle_call(ip->C, ipv4_ref)),
			     expr_ipv6_component(ip)));
}

static Expression* expr_ipv6_rest_2(pattern* ip, Ref ipv4_ref){
  return pexle_seq_f(ip->C,  expr_ipv6_rest(ip, ipv4_ref), expr_ipv6_rest(ip, ipv4_ref));
}

static Expression* expr_ipv6_rest_4(pattern* ip, Ref ipv4_ref){
  return pexle_seq_f(ip->C,  expr_ipv6_rest_2(ip, ipv4_ref), expr_ipv6_rest_2(ip, ipv4_ref));
}

/* ---------------------------- Simple Patterns ----------------------------  */

/* A -> "a" A "bc" D / "c" B 
   D -> [1-9]{0,3} 
   B -> A */

/* Used in peepholetest but not inlineoptimizertest */
__attribute__((unused)) static pattern* recursive_pattern4(void){
    
    Error err;
    int i;
    pattern* pt = (pattern*)malloc(sizeof(pattern));

    if (pt == NULL) {
        fprintf(stderr, "Error Allocating Memory: %s\n", strerror(errno));
        exit(-1);	
    }

    pt->num_expr = 3;
    pt->toplevel = pexlc_make_recursive_env(NULL);
    pt->C = pexlc_make_context(pt->toplevel);
    pt->env = pexlc_make_recursive_env(pt->toplevel);
    /* Allocate Space for Expressions & References */
    pt->exp_arr = (Expression **)malloc(pt->num_expr*sizeof(Expression*));
    pt->ref_arr = (Ref *)malloc(pt->num_expr*sizeof(Ref));

    pt->ref_arr[2] = pexlc_make_binding(pt->C, pt->env, "D"); 
    pt->ref_arr[1] = pexlc_make_binding(pt->C, pt->env, "B"); 
    pt->ref_arr[0] = pexlc_make_binding(pt->C, pt->env, "A"); 

    pt->exp_arr[2] = pexle_repeat_f(pt->C, pexle_from_set(pt->C, "123456789", 9),  -3); 
    pt->exp_arr[1] = pexle_call(pt->C, pt->ref_arr[0]);
    pt->exp_arr[0] = pexle_choice_f(pt->C, 
                            pexle_seq_f(pt->C,
                                pexle_from_string(pt->C, "a"),
                                pexle_seq_f(pt->C, 
                                    pexle_call(pt->C, pt->ref_arr[0]),
                                    pexle_seq_f(pt->C,
                                        pexle_from_string(pt->C, "bc"),
                                        pexle_call(pt->C,  pt->ref_arr[2])))),
                            pexle_seq_f(pt->C,
                                pexle_from_string(pt->C, "c"),
                                pexle_call(pt->C, pt->ref_arr[1]))); 

    i = pt->num_expr-1;
    for (; i >= 0; i--){
        pexlc_bind_expression(pt->C, pt->exp_arr[i], pt->ref_arr[i], &err);
        failif(err.value != OK, "bind should work");
    }  
    return pt;
}

/* A -> "a" A "b" D / "c" B
   D -> A 
   B -> D */

/* Used in peepholetest but not inlineoptimizertest */
__attribute__((unused)) static pattern* recursive_pattern3(void){
    
    Error err;    
    int i;
    pattern* pt = (pattern*)malloc(sizeof(pattern));

    if (pt == NULL) {
        fprintf(stderr, "Error Allocating Memory: %s\n", strerror(errno));
        exit(-1);	
    }

    pt->num_expr = 3;
    pt->toplevel = pexlc_make_recursive_env(NULL);
    pt->C = pexlc_make_context(pt->toplevel);
    pt->env = pexlc_make_recursive_env(pt->toplevel);
    /* Allocate Space for Expressions & References */
    pt->exp_arr = (Expression **)malloc(pt->num_expr*sizeof(Expression*));
    pt->ref_arr = (Ref *)malloc(pt->num_expr*sizeof(Ref));

    pt->ref_arr[2] = pexlc_make_binding(pt->C, pt->env, "D"); 
    pt->ref_arr[1] = pexlc_make_binding(pt->C, pt->env, "B"); 
    pt->ref_arr[0] = pexlc_make_binding(pt->C, pt->env, "A"); 

    pt->exp_arr[2] = pexle_call(pt->C, pt->ref_arr[0]);
    pt->exp_arr[1] = pexle_call(pt->C, pt->ref_arr[2]);
    pt->exp_arr[0] = pexle_choice_f(pt->C,		
                            pexle_seq_f(pt->C,
                                pexle_from_string(pt->C, "a"),
                                pexle_seq_f(pt->C, 
                                    pexle_call(pt->C, pt->ref_arr[0]),
                                    pexle_seq_f(pt->C,
                                        pexle_from_string(pt->C, "bc"),
                                        pexle_call(pt->C, pt->ref_arr[2])))),
                            pexle_seq_f(pt->C,
                                pexle_from_string(pt->C, "c"),
                                pexle_call(pt->C,  pt->ref_arr[1]))); 

    i = pt->num_expr-1;
    for (; i >= 0; i--){
        pexlc_bind_expression(pt->C, pt->exp_arr[i], pt->ref_arr[i], &err);
        failif(err.value != OK, "bind should work");
    }  
    return pt;
}

/* A -> "a" A "b" / "c" B 
   D -> A 
   B -> D */
static pattern* recursive_pattern2(void){
    
    int i;
    Error err;    
    pattern* pt = (pattern*)malloc(sizeof(pattern));

    if (pt == NULL) {
        fprintf(stderr, "Error Allocating Memory: %s\n", strerror(errno));
        exit(-1);	
    }

    pt->num_expr = 3;
    pt->toplevel = pexlc_make_recursive_env(NULL);
    pt->C = pexlc_make_context(pt->toplevel);
    pt->env = pexlc_make_recursive_env(pt->toplevel);
    /* Allocate Space for Expressions & References */
    pt->exp_arr = (Expression **)malloc(pt->num_expr*sizeof(Expression*));
    pt->ref_arr = (Ref *)malloc(pt->num_expr*sizeof(Ref));

    pt->ref_arr[2] = pexlc_make_binding(pt->C, pt->env, "D"); 
    pt->ref_arr[1] = pexlc_make_binding(pt->C, pt->env, "B"); 
    pt->ref_arr[0] = pexlc_make_binding(pt->C, pt->env, "A"); 

    pt->exp_arr[2] = pexle_call(pt->C, pt->ref_arr[0]);
    pt->exp_arr[1] = pexle_call(pt->C, pt->ref_arr[2]);
    pt->exp_arr[0] = pexle_choice_f(pt->C,		/* A -> "a" A "b" / "c" B */
                            pexle_seq_f(pt->C,
                                pexle_from_string(pt->C, "a"),
                                pexle_seq_f(pt->C, 
                                    pexle_call(pt->C, pt->ref_arr[0]),
                                    pexle_from_string(pt->C, "b"))),
                            pexle_seq_f(pt->C,
                                pexle_from_string(pt->C, "c"),
                                pexle_call(pt->C, pt->ref_arr[1]))); 

    i = pt->num_expr-1;
    for (; i >= 0; i--){
        pexlc_bind_expression(pt->C, pt->exp_arr[i], pt->ref_arr[i], &err);
        failif(err.value != OK, "bind should work");
    }  
    return pt;
}

/* A -> "a" A "b" / "c" B 
   B -> Epsilon
*/
static pattern* recursive_pattern(void){
    
    Error err;
    int i;
    pattern* pt = (pattern*)malloc(sizeof(pattern));

    if (pt == NULL) {
        fprintf(stderr, "Error Allocating Memory: %s\n", strerror(errno));
        exit(-1);	
    }

    pt->num_expr = 2;
    pt->toplevel = pexlc_make_recursive_env(NULL);
    pt->C = pexlc_make_context(pt->toplevel);
    pt->env = pexlc_make_recursive_env(pt->toplevel);
    /* Allocate Space for Expressions & References */
    pt->exp_arr = (Expression **)malloc(pt->num_expr*sizeof(Expression*));
    pt->ref_arr = (Ref *)malloc(pt->num_expr*sizeof(Ref));

    pt->ref_arr[1] = pexlc_make_binding(pt->C, pt->env, "B"); 
    pt->ref_arr[0] = pexlc_make_binding(pt->C, pt->env, "A"); 

    pt->exp_arr[1] = pexle_from_boolean(pt->C, 1); /* Epsilon */
    pt->exp_arr[0] = pexle_choice_f(pt->C,		
                            pexle_seq_f(pt->C,
                                pexle_from_string(pt->C, "a"),
                                pexle_seq_f(pt->C, 
                                    pexle_call(pt->C, pt->ref_arr[0]),
                                    pexle_from_string(pt->C, "b"))),
                            pexle_seq_f(pt->C,
                                pexle_from_string(pt->C, "c"),
                                pexle_call(pt->C, pt->ref_arr[1])));
    i = pt->num_expr-1;
    for (; i >= 0; i--){
        pexlc_bind_expression(pt->C, pt->exp_arr[i], pt->ref_arr[i], &err);
        failif(err.value != OK, "bind should work");
    }  
    return pt;
}

static pattern* create_ipaddr(){
    Error err;
    int i;
    Expression *ipv6_choice_1, *ipv6_choice_2, *ipv6_choice_3, *ipv6_choice_4;
    Expression *ipv6_choice_5, *ipv6_choice_6, *ipv6_choice_7, *ipv6_choice_8;
    Expression *ipv4_choice_1, *ipv4_choice_2, *ipv4_choice_3, *ipv4_choice_4, *ipv4_choice_5;
    Ref ipv4_ref;

    pattern* ip = (pattern*)malloc(sizeof(pattern));

    if (ip == NULL) {
        fprintf(stderr, "Error Allocating Memory: %s\n", strerror(errno));
        exit(-1);	
    }

    ip->num_expr = 11;
    ip->toplevel = pexlc_make_recursive_env(NULL);
    ip->C = pexlc_make_context(ip->toplevel);
    ip->env = pexlc_make_env(ip->toplevel);

    /* Allocate Space for Expressions & References */
    ip->exp_arr = (Expression **)malloc(ip->num_expr*sizeof(Expression*));
    ip->ref_arr = (Ref *)malloc(ip->num_expr*sizeof(Ref));

    ip->ref_arr[0] = pexlc_make_binding(ip->C, ip->env, "ipv6 / ipv4");
    /* References to Ipv6 Related Expressions */
    ip->ref_arr[1] = pexlc_make_binding(ip->C, ip->env, "ipv6_strict");
    ip->ref_arr[2] = pexlc_make_binding(ip->C, ip->env, "ipv6_rest{4}");
    ip->ref_arr[3] = pexlc_make_binding(ip->C, ip->env, "ipv6_rest{2}");
    ip->ref_arr[4] = pexlc_make_binding(ip->C, ip->env, "ipv6_rest{1}");    
    ip->ref_arr[5] = pexlc_make_binding(ip->C, ip->env, "ipv6_component");
    /*  References to Ipv4 Related Expressions */
    ip->ref_arr[6] = pexlc_make_binding(ip->C, ip->env, "ipv4");
    ip->ref_arr[7] = pexlc_make_binding(ip->C, ip->env, "Seq {'.' ipv4_component}"); 
    ip->ref_arr[8] = pexlc_make_binding(ip->C, ip->env, "ipv4_component");
    ip->ref_arr[9] = pexlc_make_binding(ip->C, ip->env, "[0-9]?"); 
    ip->ref_arr[10] = pexlc_make_binding(ip->C, ip->env, "[0-9]");
    
    /* -----------------------------------------  IPv4 Pattern ------------------------  */

    /* Helpful Expressions */
    ip->exp_arr[10] = pexle_from_set(ip->C, "0123456789", 10);
    ip->exp_arr[9] = pexle_repeat_f(ip->C, pexle_from_set(ip->C, "0123456789", 10), -1); /* [0-9]? */
    
    /* {[1] [0-9]? [0-9]?} */
    /* Some Issue With Changing 1st Child of Seq */
    ipv4_choice_1 = pexle_seq_f(ip->C, 
			      pexle_from_string(ip->C, "1"),
			      pexle_seq_f(ip->C, 
					pexle_repeat_f(ip->C, pexle_from_set(ip->C, "0123456789", 10), -1),
					pexle_call(ip->C, ip->ref_arr[9])));

    /* {[2] [0-4] [0-9]} */
    ipv4_choice_2 = pexle_seq_f(ip->C, 
			      pexle_from_string(ip->C, "2"),
			      pexle_seq_f(ip->C, 
					pexle_from_set(ip->C, "01234", 5),
					pexle_call(ip->C, ip->ref_arr[10])));

    /* {[2] [5] [0-5]} */
    ipv4_choice_3 = pexle_seq_f(ip->C, 
			      pexle_from_string(ip->C, "2"),
			      pexle_seq_f(ip->C, 
					pexle_from_string(ip->C, "5"),
					pexle_from_set(ip->C, "012345", 6)));
    /* {[2] [0-9]?} */
    ipv4_choice_4 = pexle_seq_f(ip->C, 
			      pexle_from_string(ip->C, "2"),
			      pexle_call(ip->C, ip->ref_arr[9]));
    
    /* {[3-9] [0-9]?} */
    ipv4_choice_5 = pexle_seq_f(ip->C, 
			      pexle_from_set(ip->C, "3456789", 7),
			      pexle_call(ip->C, ip->ref_arr[9]));

    /* [0] /    {[1] [0-9]? [0-9]?} / 
			     {[2] [0-4] [0-9]} / {[2] [5] [0-5]} / {[2] [0-9]?} /
			     {[3-9] [0-9]?} 
    */

    ip->exp_arr[8] = pexle_choice_f(ip->C, 
				  pexle_from_string(ip->C, "0"),
				  pexle_choice_f(ip->C, 
					       ipv4_choice_1,
					       pexle_choice_f(ip->C, 
							    ipv4_choice_2,
							    pexle_choice_f(ip->C, 
									 ipv4_choice_3,
									 pexle_choice_f(ip->C, 
										      ipv4_choice_4,
										      ipv4_choice_5)))));

    ip->exp_arr[7] = pexle_seq_f(ip->C, pexle_from_string(ip->C, "."),  pexle_call(ip->C, ip->ref_arr[8]));

    /* ip_address_v4 = { ipv4_component {"." ipv4_component}{3} } */
    ip->exp_arr[6] = pexle_seq_f(ip->C, pexle_call(ip->C, ip->ref_arr[8]), 
			       pexle_seq_f(ip->C,
					 pexle_seq_f(ip->C, 
						   pexle_call(ip->C, ip->ref_arr[7]), 
						   pexle_call(ip->C, ip->ref_arr[7])),
					 pexle_call(ip->C, ip->ref_arr[7])));


    ipv4_ref = ip->ref_arr[6];	/* refer to "ipv4" pattern */

    ip->exp_arr[5] = expr_ipv6_component(ip);
    ip->exp_arr[4] = expr_ipv6_rest(ip, ipv4_ref); /* refer to ipv4 */
    ip->exp_arr[3] = expr_ipv6_rest_2(ip, ipv4_ref);
    ip->exp_arr[2] = expr_ipv6_rest_4(ip, ipv4_ref);

    /* -----------------------------------------  IPv6 Pattern ------------------------ */

    /* ipv6_component ipv6_rest{4} ipv6_rest{2} ipv6_rest{1} */
    ipv6_choice_1 = pexle_seq_f(ip->C, 
			      pexle_call(ip->C, ip->ref_arr[5]), 
			      pexle_seq_f(ip->C, 
                                        pexle_seq_f(ip->C, 
						  pexle_call(ip->C, ip->ref_arr[2]), 
						  pexle_call(ip->C, ip->ref_arr[3])),
					pexle_call(ip->C, ip->ref_arr[4])));
    
    /* ipv6_component "::" ipv6_component ipv6_rest{0,4} */
    ipv6_choice_2 = pexle_seq_f(ip->C,
			      pexle_seq_f(ip->C, 
                                        pexle_call(ip->C, ip->ref_arr[5]),
                                        pexle_from_string(ip->C, "::")),
			      pexle_seq_f(ip->C, 
                                        pexle_call(ip->C, ip->ref_arr[5]),
                                        pexle_repeat_f(ip->C, pexle_call(ip->C, ip->ref_arr[4]), -4)));
                                        
    /*  ipv6_component ipv6_rest{1} "::" ipv6_component ipv6_rest{0,3} } */
    ipv6_choice_3 = pexle_seq_f(ip->C,    
			      pexle_seq_f(ip->C,
                                        pexle_seq_f(ip->C, 
						  pexle_call(ip->C, ip->ref_arr[5]),
						  pexle_call(ip->C, ip->ref_arr[4])),
                                        pexle_seq_f(ip->C,
						  pexle_from_string(ip->C, "::"),
						  pexle_call(ip->C, ip->ref_arr[5]))),
			      pexle_repeat_f(ip->C, pexle_call(ip->C, ip->ref_arr[4]), -3));
    
    /*  { ipv6_component ipv6_rest{2} "::" ipv6_component ipv6_rest{0,2} } } */
    ipv6_choice_4 = pexle_seq_f(ip->C,    
			      pexle_seq_f(ip->C,
                                        pexle_seq_f(ip->C, 
						  pexle_call(ip->C, ip->ref_arr[5]),
						  pexle_call(ip->C, ip->ref_arr[3])),
                                        pexle_seq_f(ip->C,
						  pexle_from_string(ip->C, "::"),
						  pexle_call(ip->C, ip->ref_arr[5]))),
			      pexle_repeat_f(ip->C, pexle_call(ip->C, ip->ref_arr[4]), -2));
    
    /* { ipv6_component ipv6_rest{3} "::" ipv6_component ipv6_rest{0,1} } */
    ipv6_choice_5 = pexle_seq_f(ip->C,    
			      pexle_seq_f(ip->C,
                                        pexle_seq_f(ip->C, 
						  pexle_call(ip->C, ip->ref_arr[5]),
						  pexle_seq_f(ip->C,
							    pexle_call(ip->C, ip->ref_arr[3]),
							    pexle_call(ip->C, ip->ref_arr[4]))),
                                        pexle_seq_f(ip->C,
						  pexle_from_string(ip->C, "::"),
						  pexle_call(ip->C, ip->ref_arr[5]))),
			      pexle_repeat_f(ip->C, pexle_call(ip->C, ip->ref_arr[4]), -1));

    /* { ipv6_component ipv6_rest{4} "::" ipv6_component } / */
    ipv6_choice_6 = pexle_seq_f(ip->C,
			      pexle_seq_f(ip->C, 
                                        pexle_call(ip->C, ip->ref_arr[5]),
                                        pexle_call(ip->C, ip->ref_arr[2])),
			      pexle_seq_f(ip->C, 
                                        pexle_from_string(ip->C, "::"),
                                        pexle_call(ip->C, ip->ref_arr[5])));

    /* { "::" ipv6_component ipv6_rest{0,5} } /*/
    ipv6_choice_7 = pexle_seq_f(ip->C,
                                    pexle_seq_f(ip->C, 
                                        pexle_from_string(ip->C, "::"),
                                        pexle_call(ip->C, ip->ref_arr[5])),
                                    pexle_repeat_f(ip->C, pexle_call(ip->C, ip->ref_arr[4]), -5));

    /* ipv6_component ipv6_rest{7} */
    ipv6_choice_8 = pexle_seq_f(ip->C,
                                    pexle_call(ip->C, ip->ref_arr[5]),  
                                        pexle_seq_f(ip->C, 
                                            pexle_call(ip->C, ip->ref_arr[2]), 
                                                pexle_seq_f(ip->C, 
                                                    pexle_call(ip->C, ip->ref_arr[3]), 
                                                    pexle_call(ip->C, ip->ref_arr[4]))));
    
    /* Final Expression for ip_address_strict */
    
    ip->exp_arr[1] =  pexle_choice_f(ip->C, pexle_from_string(ip->C, "::"), 
                                pexle_choice_f(ip->C, 
                                    pexle_choice_f(ip->C,
                                        pexle_choice_f(ip->C, ipv6_choice_1, ipv6_choice_2), 
                                        pexle_choice_f(ip->C, 
                                            pexle_choice_f(ip->C, ipv6_choice_3, ipv6_choice_4), 
                                            pexle_choice_f(ip->C, ipv6_choice_5, ipv6_choice_6))),
                                    pexle_choice_f(ip->C, ipv6_choice_7, ipv6_choice_8)));

    /* ipv4 /ipv6 */
    ip->exp_arr[0] = pexle_choice_f(ip->C, pexle_call(ip->C, ip->ref_arr[6]), pexle_call(ip->C, ip->ref_arr[1])); 

    i = 0;
    for (; i <= 10; i++){
        pexlc_bind_expression(ip->C, ip->exp_arr[i], ip->ref_arr[i], &err);
        failif(err.value != OK, "bind should work");
    } 
    return ip;
}

static pattern* create_ipv4(){
    Error err;
    int i;
    Expression *ipv4_choice_1, *ipv4_choice_2, *ipv4_choice_3, *ipv4_choice_4, *ipv4_choice_5;
    pattern* ip = (pattern*)malloc(sizeof(pattern));

    if (ip == NULL) {
        fprintf(stderr, "Error Allocating Memory: %s\n", strerror(errno));
        exit(-1);	
    }

    ip->num_expr = 5;
    ip->toplevel = pexlc_make_recursive_env(NULL);
    ip->C = pexlc_make_context(ip->toplevel);
    ip->env = pexlc_make_recursive_env(ip->toplevel);

    /* Allocate Space for Expressions & References */
    ip->exp_arr = (Expression **)malloc(ip->num_expr*sizeof(Expression*));
    ip->ref_arr = (Ref *)malloc(ip->num_expr*sizeof(Ref));

    ip->ref_arr[4] = pexlc_make_binding(ip->C, ip->env, "[0-9]");
    ip->ref_arr[3] = pexlc_make_binding(ip->C, ip->env, "[0-9]?");
    ip->ref_arr[2] = pexlc_make_binding(ip->C, ip->env, "ipv4_component");    
    ip->ref_arr[1] = pexlc_make_binding(ip->C, ip->env, "Seq {'.' ipv4_component}");
    ip->ref_arr[0] = pexlc_make_binding(ip->C, ip->env, "ipv4"); 

    /*  References to Ipv4 Related Expressions*/

    /* Helpful Expressions */
    ip->exp_arr[4] = pexle_from_set(ip->C, "0123456789", 10);
    ip->exp_arr[3] = pexle_repeat_f(ip->C, pexle_from_set(ip->C, "0123456789", 10), -1); /* [0-9]? */
    
    /* {[1] [0-9]? [0-9]?} */
    ipv4_choice_1 = pexle_seq_f(ip->C, 
			      pexle_from_string(ip->C, "1"),
			      pexle_seq_f(ip->C, 
					pexle_call(ip->C, ip->ref_arr[3]),
					pexle_call(ip->C, ip->ref_arr[3])));

    /* {[2] [0-4] [0-9]} */
    ipv4_choice_2 = pexle_seq_f(ip->C, 
			      pexle_from_string(ip->C, "2"),
			      pexle_seq_f(ip->C, 
					pexle_from_set(ip->C, "01234", 5),
					pexle_call(ip->C, ip->ref_arr[4])));

    /* {[2] [5] [0-5]} */
    ipv4_choice_3 = pexle_seq_f(ip->C, 
			      pexle_from_string(ip->C, "2"),
			      pexle_seq_f(ip->C, 
					pexle_from_string(ip->C, "5"),
					pexle_from_set(ip->C, "012345", 6)));
    /* {[2] [0-9]?} */
    ipv4_choice_4 = pexle_seq_f(ip->C, 
			      pexle_from_string(ip->C, "2"),
			      pexle_call(ip->C, ip->ref_arr[3]));
    
    /* {[3-9] [0-9]?} */
    ipv4_choice_5 = pexle_seq_f(ip->C, 
			      pexle_from_set(ip->C, "3456789", 7),
			      pexle_call(ip->C, ip->ref_arr[3]));

    /* [0] /    {[1] [0-9]? [0-9]?} / 
			     {[2] [0-4] [0-9]} / {[2] [5] [0-5]} / {[2] [0-9]?} /
			     {[3-9] [0-9]?} 
    */

    ip->exp_arr[2] = pexle_choice_f(ip->C, 
				  pexle_from_string(ip->C, "0"),
				  pexle_choice_f(ip->C, 
					       ipv4_choice_1,
					       pexle_choice_f(ip->C, 
							    ipv4_choice_2,
							    pexle_choice_f(ip->C, 
									 ipv4_choice_3,
									 pexle_choice_f(ip->C, 
										      ipv4_choice_4,
										      ipv4_choice_5)))));

    ip->exp_arr[1] = pexle_seq_f(ip->C, pexle_from_string(ip->C, "."),
			       pexle_call(ip->C, ip->ref_arr[2]));

    /* ip_address_v4 = { ipv4_component {"." ipv4_component}{3} } */
    ip->exp_arr[0] = pexle_seq_f(ip->C, pexle_call(ip->C, ip->ref_arr[2]), 
			       pexle_seq_f(ip->C,
					 pexle_seq_f(ip->C, 
						   pexle_call(ip->C, ip->ref_arr[1]), 
						   pexle_call(ip->C, ip->ref_arr[1])),
					 pexle_call(ip->C, ip->ref_arr[1])));

    /* ip->exp_arr[0] = pexle_call(ip->C, ip->ref_arr[2]); */

    i = ip->num_expr-1;
    for (; i >= 0; i--){
        pexlc_bind_expression(ip->C, ip->exp_arr[i], ip->ref_arr[i], &err);
        failif(err.value != OK, "bind should work");
    }
    
    return ip;
}

static pattern* create_ipv6(){
    Error err;
    int i;
    Expression *ipv6_choice_1, *ipv6_choice_2, *ipv6_choice_3, *ipv6_choice_4;
    Expression *ipv6_choice_5, *ipv6_choice_6, *ipv6_choice_7, *ipv6_choice_8;
    Ref ipv4_ref;
    pattern *ipv4 = create_ipv4();
    pattern *ipv6 = (pattern*)malloc(sizeof(pattern));

    if (ipv6 == NULL) {
        fprintf(stderr, "Error Allocating Memory: %s\n", strerror(errno));
        exit(-1); 
    }

    ipv6->num_expr = 5;
/*     ipv6->toplevel = pexlc_make_recursive_env(NULL); */
/*     ipv6->C = pexlc_make_context(ipv6->toplevel); */
/*     ipv6->env = pexlc_make_env(ipv6->toplevel); */
    ipv6->toplevel = ipv4->toplevel; 
    ipv6->env = ipv4->env; 
    ipv6->C = ipv4->C;

    /* Allocate Space for Expressions & References */
    ipv6->exp_arr = (Expression **)malloc(ipv6->num_expr*sizeof(Expression*));
    ipv6->ref_arr = (Ref *)malloc(ipv6->num_expr*sizeof(Ref));

    /* References to the Expressions */
    ipv6->ref_arr[0] = pexlc_make_binding(ipv6->C, ipv6->env, "ipv6_strict");
    ipv6->ref_arr[1] = pexlc_make_binding(ipv6->C, ipv6->env, "ipv6_rest{4}");
    ipv6->ref_arr[2] = pexlc_make_binding(ipv6->C, ipv6->env, "ipv6_rest{2}");
    ipv6->ref_arr[3] = pexlc_make_binding(ipv6->C, ipv6->env, "ipv6_rest{1}");    
    ipv6->ref_arr[4] = pexlc_make_binding(ipv6->C, ipv6->env, "ipv6_component");


    /* Expressions to use them directly in Final Sequence */

    /* ipv6_component ipv6_rest{4} ipv6_rest{2} ipv6_rest{1} */
    ipv6_choice_1 = pexle_seq_f(ipv6->C, 
			      pexle_call(ipv6->C, ipv6->ref_arr[4]), 
			      pexle_seq_f(ipv6->C, 
                                        pexle_seq_f(ipv6->C, 
						  pexle_call(ipv6->C, ipv6->ref_arr[1]), 
						  pexle_call(ipv6->C, ipv6->ref_arr[2])),
					pexle_call(ipv6->C, ipv6->ref_arr[3])));

    /* ipv6_component "::" ipv6_component ipv6_rest{0,4} */
    ipv6_choice_2 = pexle_seq_f(ipv6->C,
			      pexle_seq_f(ipv6->C, 
                                        pexle_call(ipv6->C, ipv6->ref_arr[4]),
                                        pexle_from_string(ipv6->C, "::")),
			      pexle_seq_f(ipv6->C, 
                                        pexle_call(ipv6->C, ipv6->ref_arr[4]),
                                        pexle_repeat_f(ipv6->C, pexle_call(ipv6->C, ipv6->ref_arr[3]), -4)));
                                        
    /*  ipv6_component ipv6_rest{1} "::" ipv6_component ipv6_rest{0,3} } */
    ipv6_choice_3 = pexle_seq_f(ipv6->C,    
			      pexle_seq_f(ipv6->C,
                                        pexle_seq_f(ipv6->C, 
						  pexle_call(ipv6->C, ipv6->ref_arr[4]),
						  pexle_call(ipv6->C, ipv6->ref_arr[3])),
                                        pexle_seq_f(ipv6->C,
						  pexle_from_string(ipv6->C, "::"),
						  pexle_call(ipv6->C, ipv6->ref_arr[4]))),
			      pexle_repeat_f(ipv6->C, pexle_call(ipv6->C, ipv6->ref_arr[3]), -3));

    /*  { ipv6_component ipv6_rest{2} "::" ipv6_component ipv6_rest{0,2} } } */
    ipv6_choice_4 = pexle_seq_f(ipv6->C,    
			      pexle_seq_f(ipv6->C,
                                        pexle_seq_f(ipv6->C, 
						  pexle_call(ipv6->C, ipv6->ref_arr[4]),
						  pexle_call(ipv6->C, ipv6->ref_arr[2])),
                                        pexle_seq_f(ipv6->C,
						  pexle_from_string(ipv6->C, "::"),
						  pexle_call(ipv6->C, ipv6->ref_arr[4]))),
			      pexle_repeat_f(ipv6->C, pexle_call(ipv6->C, ipv6->ref_arr[3]), -2));
    
    /* { ipv6_component ipv6_rest{3} "::" ipv6_component ipv6_rest{0,1} } */
    ipv6_choice_5 = pexle_seq_f(ipv6->C,    
			      pexle_seq_f(ipv6->C,
                                        pexle_seq_f(ipv6->C, 
						  pexle_call(ipv6->C, ipv6->ref_arr[4]),
						  pexle_seq_f(ipv6->C,
							    pexle_call(ipv6->C, ipv6->ref_arr[2]),
							    pexle_call(ipv6->C, ipv6->ref_arr[3]))),
                                        pexle_seq_f(ipv6->C,
						  pexle_from_string(ipv6->C, "::"),
						  pexle_call(ipv6->C, ipv6->ref_arr[4]))),
			      pexle_repeat_f(ipv6->C, pexle_call(ipv6->C, ipv6->ref_arr[3]), -1));

    /* { ipv6_component ipv6_rest{4} "::" ipv6_component } / */
    ipv6_choice_6 = pexle_seq_f(ipv6->C,
			      pexle_seq_f(ipv6->C, 
                                        pexle_call(ipv6->C, ipv6->ref_arr[4]),
                                        pexle_call(ipv6->C, ipv6->ref_arr[1])),
			      pexle_seq_f(ipv6->C, 
                                        pexle_from_string(ipv6->C, "::"),
                                        pexle_call(ipv6->C, ipv6->ref_arr[4])));

    /* { "::" ipv6_component ipv6_rest{0,5} } /*/
    ipv6_choice_7 = pexle_seq_f(ipv6->C,
			      pexle_seq_f(ipv6->C, 
                                        pexle_from_string(ipv6->C, "::"),
                                        pexle_call(ipv6->C, ipv6->ref_arr[4])),
			      pexle_repeat_f(ipv6->C, pexle_call(ipv6->C, ipv6->ref_arr[3]), -5));

    /* ipv6_component ipv6_rest{7} */
    ipv6_choice_8 = pexle_seq_f(ipv6->C,
			      pexle_call(ipv6->C, ipv6->ref_arr[4]),  
			      pexle_seq_f(ipv6->C, 
					pexle_call(ipv6->C, ipv6->ref_arr[1]), 
					pexle_seq_f(ipv6->C, 
						  pexle_call(ipv6->C, ipv6->ref_arr[2]), 
						  pexle_call(ipv6->C, ipv6->ref_arr[3]))));
    
    /* Final Expression for ip_address_strict */
    
    ipv6->exp_arr[0] =  pexle_choice_f(ipv6->C, pexle_from_string(ipv6->C, "::"), 
                                pexle_choice_f(ipv6->C, 
                                    pexle_choice_f(ipv6->C,
                                        pexle_choice_f(ipv6->C, ipv6_choice_1, ipv6_choice_2), 
                                        pexle_choice_f(ipv6->C, 
                                            pexle_choice_f(ipv6->C, ipv6_choice_3, ipv6_choice_4), 
                                            pexle_choice_f(ipv6->C, ipv6_choice_5, ipv6_choice_6))),
                                    pexle_choice_f(ipv6->C, ipv6_choice_7, ipv6_choice_8)));
    

    /* ipv6->exp_arr[0] =  ipv6_choice_8; */

    ipv4_ref = ipv4->ref_arr[0]; /* refer to "ipv4" pattern */

    /* Create Expressions for using them as callee's in the final expression */
    ipv6->exp_arr[4] = expr_ipv6_component(ipv6);
    ipv6->exp_arr[3] = expr_ipv6_rest(ipv6, ipv4_ref);
    ipv6->exp_arr[2] = expr_ipv6_rest_2(ipv6, ipv4_ref);
    ipv6->exp_arr[1] = expr_ipv6_rest_4(ipv6, ipv4_ref);

    i = ipv6->num_expr-1;
    for (; i >= 0; i--){
        pexlc_bind_expression(ipv6->C, ipv6->exp_arr[i], ipv6->ref_arr[i], &err);
        if (err.value != OK) printf("** bind for exp[%d] returned %d\n", i, err.value);
        failif(err.value != OK, "bind should work");
    }

    return ipv6;
}
