/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  lookaroundtest.c  Testing the new lookahead/lookbehind capabilities      */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#define _GNU_SOURCE		/* for asprintf() */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>		/* exit() */
#include "print.h"
#include "analyze.h"
#include "compile.h"		/* pexlc_env_free() */

#define YES 1
#define NO 0

#define TRUE 1
#define FALSE 0

#define failif(test, msg)						\
  do { if (test) { fprintf(stderr, "Failed %s:%d: %s\n", __FILE__,	\
			   __LINE__, msg);				\
      fflush(stderr);							\
      exit(-1);								\
    }									\
  } while (0)


static int check_match (Match *m,
			int expectation, size_t expected_leftover, size_t exp_match_len,
			const char *input) {
  if ((m->matched == 0) != (expectation == 0)) {
    printf("Match error on input '%s': expected %s\n",
	   input,
	   expectation ? "a match" : "no match");
    return 1;
  }
  if (m->matched) {
    if (m->leftover != expected_leftover) {
      printf("Match error on input \"%s\": expected %lu leftover, found %lu\n",
	     input, expected_leftover, m->leftover);
      return 2;
    }
    /* SPECIAL CASE due to the way match data is currently constructed */
    if ((exp_match_len == 1) ?
	((m->data->next != 0) && (m->data->next != 1)) :
	(m->data->next != exp_match_len)) {
      printf("Match error on input \"%s\": expected %lu bytes of match data, found %lu\n",
	     input, exp_match_len, m->data->next);
      return 3;
    }
  } /* end of tests done only if there was a match */
  return 0;
}

static int test_expression (Context *C, PackageTable *pt,
			    const char *desc, Expression *e,
			    const char *inputs[],
			    int matches[], int leftovers[],
			    size_t exp_match_len) {
  Package *pkg;
  Error err;
  Match *match;
  Buffer *buf;
  int i, stat;
  const char *input;

  printf("\nExpression '%s'\n", desc);

  pkg = pexlc_compile_expression(C, e, C->env, &err, DEFAULT_OPTIM);
  if (!pkg || (err.value != OK)) {
    printf("expected successful compile\n");
    return 1;
  }

  print_package(pkg); 
  
  if (packagetable_set_user(pt, pkg) < 0) {
    printf("expected successful set of user package\n");
    return 2;
  }

  match = match_new();

  i = 0;
  while ((input = inputs[i])) {
    printf("\t input is \"%s\"\n", input);
    buf = buf_new_wrap(input, strnlen(input, UINT_MAX));
    stat = vm_start(pt, pkg->ep, buf, 0, buf->next, byte_encoder, NULL, match);

    if (stat) {
      printf("vm error: %d\n", stat);
      return stat;
    }

    if (check_match(match, matches[i], leftovers[i], exp_match_len, input)) return 3;
    i++;
  } /* while */
  
  package_free(pkg);
  match_free(match);
  return 0;
}

#define IntList(...) (int[]){__VA_ARGS__}
#define StrList(...) (const char *[]){__VA_ARGS__, NULL}

#define TEST(expname, exp, inputs, exp_matches, exp_leftovers, exp_match_len) do { \
    failif(test_expression(C, pt, (expname), (exp),			\
			   (inputs), (exp_matches), (exp_leftovers), (exp_match_len)), \
	   "test failed");						\
  } while (0)

int main(void) {

  Context *C;
  PackageTable *pt;
  Env *toplevel; 
  const char **inputs;  
  printf("$$$ lookaroundtest\n");

  toplevel = env_new(NULL, NO);
  failif(!toplevel, "new toplevel should succeed");
  
  C = context_new(toplevel);
  failif(!C, "expected new context");

  /* ----------------------------------------------------------------------------- */

  pt = packagetable_new(0);
  failif(!pt, "expected new package table");

  /* ----------------------------------------------------------------------------- */
  printf("\nlookaroundtest: Testing lookahead with no captures in target pattern\n\n");

  inputs = StrList("", "a", "aaaaaa", "b", "ab", "aaaaaab", "aaabbb");
  
  TEST(">epsilon", pexle_lookahead_f(C, pexle_from_boolean(C, TRUE)),
       inputs,
       IntList( TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       1);

  /* TEMP TEMP TEMP: This test result must be examined visually (for now) */
  TEST("> >epsilon", pexle_lookahead_f(C, pexle_lookahead_f(C, pexle_from_boolean(C, TRUE))),
       inputs,
       IntList( TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       1);

  TEST(">a", pexle_lookahead_f(C, pexle_from_string(C, "a")),
       inputs,
       IntList( FALSE, TRUE, TRUE, FALSE, TRUE, TRUE, TRUE ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       1);

  TEST(">(a*)", pexle_lookahead_f(C, pexle_repeat_f(C, pexle_from_string(C, "a"), 0)),
       inputs,
       IntList( TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       1);

  TEST(">(a+)", pexle_lookahead_f(C, pexle_repeat_f(C, pexle_from_string(C, "a"), 1)),
       inputs,
       IntList( FALSE, TRUE, TRUE, FALSE, TRUE, TRUE, TRUE ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       1);

  TEST("a*>b", pexle_seq_f(C,
			 pexle_repeat_f(C, pexle_from_string(C, "a"), 0),
			 pexle_lookahead_f(C, pexle_from_string(C, "b"))),
       inputs,
       IntList( FALSE, FALSE, FALSE, TRUE, TRUE, TRUE, TRUE ),
       IntList( 0, 0, 0, 1, 1, 1, 3 ),
       1);

  printf("\nlookaroundtest: Testing lookahead WITH captures in target pattern\n\n");

  TEST("> capture('foo', epsilon)",
       pexle_lookahead_f(C, pexle_capture_f(C, "foo", pexle_from_boolean(C, TRUE))),
       inputs,
       IntList( TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       1);

  TEST("> capture('astar', a*)",
       pexle_lookahead_f(C, pexle_capture_f(C, "astar", pexle_repeat_f(C, pexle_from_string(C, "a"), 0))), 
       inputs,
       IntList( TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       1);

  TEST("capture('astar', a*) >b",
       pexle_seq_f(C,
		 pexle_capture_f(C, "astar", pexle_repeat_f(C, pexle_from_string(C, "a"), 0)),
		 pexle_lookahead_f(C, pexle_from_string(C, "b"))), 
       inputs,
       IntList( FALSE, FALSE, FALSE, TRUE, TRUE, TRUE, TRUE ),
       IntList( 0, 0, 0, 1, 1, 1, 3 ),
       14);

  TEST("> (capture('astar', a*) >b)",
       pexle_lookahead_f(C, 
		       pexle_seq_f(C,
				 pexle_capture_f(C, "astar", pexle_repeat_f(C, pexle_from_string(C, "a"), 0)),
				 pexle_lookahead_f(C, pexle_from_string(C, "b")))), 
       inputs,
       IntList( FALSE, FALSE, FALSE, TRUE, TRUE, TRUE, TRUE ),
       IntList( 0, 0, 0, 1, 2, 7, 6 ),
       1);
  
  TEST("> capture('WILL NOT CAPTURE', a*) capture('astar', a+)",
       pexle_seq_f(C,
		 pexle_lookahead_f(C, 
				 pexle_capture_f(C, "WILL NOT CAPTURE", pexle_repeat_f(C, pexle_from_string(C, "a"), 0))),
		 pexle_capture_f(C, "a_star", pexle_repeat_f(C, pexle_from_string(C, "a"), 1))),
       
       inputs,
       IntList( FALSE, TRUE, TRUE, FALSE, TRUE, TRUE, TRUE ),
       IntList( 0, 0, 0, 1, 1, 1, 3 ),
       15);

  /* ----------------------------------------------------------------------------- */
  printf("\nlookaroundtest: Testing negative lookahead (NOT) \n\n");

  inputs = StrList("", "a", "aaaaaa", "b", "ab", "aaaaaab", "aaabbb");
  
  TEST("!epsilon", pexle_not_f(C, pexle_from_boolean(C, TRUE)),
       inputs,
       IntList( FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       1);

  TEST("!false", pexle_not_f(C, pexle_from_boolean(C, FALSE)),
       inputs,
       IntList( TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       1);

  TEST("> !epsilon", pexle_lookahead_f(C, pexle_not_f(C, pexle_from_boolean(C, TRUE))),
       inputs,
       IntList( FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       1);

  TEST("> !false", pexle_lookahead_f(C, pexle_not_f(C, pexle_from_boolean(C, FALSE))),
       inputs,
       IntList( TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       1);

  TEST("!a", pexle_not_f(C, pexle_from_string(C, "a")),
       inputs,
       IntList( TRUE, FALSE, FALSE, TRUE, FALSE, FALSE, FALSE ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       1);

  TEST("!(a*)", pexle_not_f(C, pexle_repeat_f(C, pexle_from_string(C, "a"), 0)),
       inputs,
       IntList( FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       1);

  TEST("!(a+)", pexle_not_f(C, pexle_repeat_f(C, pexle_from_string(C, "a"), 1)),
       inputs,
       IntList( TRUE, FALSE, FALSE, TRUE, FALSE, FALSE, FALSE ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       1);

  TEST("a*!b", pexle_seq_f(C,
			 pexle_repeat_f(C, pexle_from_string(C, "a"), 0),
			 pexle_not_f(C, pexle_from_string(C, "b"))),
       inputs,
       IntList( TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE ),
       IntList( 0, 0, 0, 1, 1, 1, 3 ),
       1);

  printf("\nlookaroundtest: Testing negative lookahead WITH captures in target pattern\n\n");

  TEST("! capture('foo', epsilon)",
       pexle_not_f(C, pexle_capture_f(C, "foo", pexle_from_boolean(C, TRUE))),
       inputs,
       IntList( FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       1);

  TEST("! capture('foo', false)",
       pexle_not_f(C, pexle_capture_f(C, "foo", pexle_from_boolean(C, FALSE))),
       inputs,
       IntList( TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       1);

  TEST("! capture('bplus', b+)",
       pexle_not_f(C, pexle_capture_f(C, "bplus", pexle_repeat_f(C, pexle_from_string(C, "b"), 1))), 
       inputs,
       IntList( TRUE, TRUE, TRUE, FALSE, TRUE, TRUE, TRUE ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       1);

  TEST("! capture('astar', a*)",
       pexle_not_f(C, pexle_capture_f(C, "astar", pexle_repeat_f(C, pexle_from_string(C, "a"), 0))), 
       inputs,
       IntList( FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       1);

  TEST("capture('astar', a*) !b",
       pexle_seq_f(C,
		 pexle_capture_f(C, "astar", pexle_repeat_f(C, pexle_from_string(C, "a"), 0)),
		 pexle_not_f(C, pexle_from_string(C, "b"))), 
       inputs,
       IntList( TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE ),
       IntList( 0, 0, 0, 1, 1, 1, 3 ),
       14);

  TEST("! (capture('astar', a*) >b)",
       pexle_not_f(C, 
		 pexle_seq_f(C,
			   pexle_capture_f(C, "astar", pexle_repeat_f(C, pexle_from_string(C, "a"), 0)),
			   pexle_lookahead_f(C, pexle_from_string(C, "b")))), 
       inputs,
       IntList( TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       1);
  

  printf("\nlookaroundtest: Testing lookbehind \n\n");
  inputs = StrList("", "a", "aaaaaa", "b", "ab", "aaaaaab", "aaabbb");
  
  TEST("'a' < 'a'", pexle_seq_f(C,
			      pexle_from_string(C, "a"),
			      pexle_lookbehind_f(C, pexle_from_string(C, "a"))),
       inputs,
       IntList( FALSE, TRUE, TRUE, FALSE, TRUE, TRUE, TRUE ),
       IntList( 0, 0, 5, 1, 1, 6, 5 ),
       1);

  TEST("'a' < 'a'", pexle_seq_f(C,
			      pexle_from_string(C, "a"),
			      pexle_lookbehind_f(C, pexle_from_string(C, "a"))),
       inputs,
       IntList( FALSE, TRUE, TRUE, FALSE, TRUE, TRUE, TRUE ),
       IntList( 0, 0, 5, 1, 1, 6, 5 ),
       1);

  TEST("'a' < capture('foo', 'a')",
       pexle_seq_f(C,
		 pexle_from_string(C, "a"),
		 pexle_lookbehind_f(C, pexle_capture_f(C, "foo", pexle_from_string(C, "a")))),
       inputs,
       IntList( FALSE, TRUE, TRUE, FALSE, TRUE, TRUE, TRUE ),
       IntList( 0, 0, 5, 1, 1, 6, 5 ),
       1);

  TEST("'a' capture('foo', <'a')",
       pexle_seq_f(C,
		 pexle_from_string(C, "a"),
		 pexle_capture_f(C, "foo", pexle_lookbehind_f(C, pexle_from_string(C, "a")))),
       inputs,
       IntList( FALSE, TRUE, TRUE, FALSE, TRUE, TRUE, TRUE ),
       IntList( 0, 0, 5, 1, 1, 6, 5 ),
       12);

  TEST("'a' capture('foo', <'a' 'a')",
       pexle_seq_f(C,
		 pexle_from_string(C, "a"),
		 pexle_capture_f(C, "foo",
			       pexle_seq_f(C, 
					 pexle_lookbehind_f(C, pexle_from_string(C, "a")),
					 pexle_from_string(C, "a")))),
       inputs,
       IntList( FALSE, FALSE, TRUE, FALSE, FALSE, TRUE, TRUE ),
       IntList( 0, 0, 4, 1, 1, 5, 4 ),
       12);

  TEST("'a' < 'b'", pexle_seq_f(C,
			      pexle_from_string(C, "a"),
			      pexle_lookbehind_f(C, pexle_from_string(C, "b"))),
       inputs,
       IntList( FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE ),
       IntList( 0, 0, 5, 1, 1, 6, 5 ),
       1);

  TEST("'a' < 'ab'", pexle_seq_f(C,
			       pexle_from_string(C, "a"),
			       pexle_lookbehind_f(C, pexle_from_string(C, "ab"))),
       inputs,
       IntList( FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE ),
       IntList( 0, 0, 5, 1, 1, 6, 5 ),
       1);

  TEST("'ab' < 'ab'", pexle_seq_f(C,
				pexle_from_string(C, "ab"),
				pexle_lookbehind_f(C, pexle_from_string(C, "ab"))),
       inputs,
       IntList( FALSE, FALSE, FALSE, FALSE, TRUE, FALSE, FALSE ),
       IntList( 0, 0, 5, 1, 0, 6, 5 ),
       1);


  TEST("'a*b' (< 'ab') capture('cap_B', 'b')",
       pexle_seq_f(C,
		 pexle_seq_f(C,
			   pexle_repeat_f(C, pexle_from_string(C, "a"), 0),
			   pexle_from_string(C, "b")),
		 pexle_capture_f(C, "cap_B", pexle_from_string(C, "b"))),
       inputs,
       IntList( FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE ),
       IntList( 0, 0, 5, 1, 0, 6, 1 ),
       14);

  printf("\n^^^ NEED TESTS of variable length lookbehind targets ^^^ !\n");

  printf("\nlookaroundtest: Testing negative lookbehind \n\n");
  printf("NO TESTS YET\n");

  pexlc_env_free(toplevel);
  packagetable_free(pt);
  context_free(C);

  /* ----------------------------------------------------------------------------- */
  printf("\nDone.\n");
  printf("Don't forget to run ADSASMDKJASKDJASKDNAKSNDAKSNDKA next.\n");

  return 0;
}

