/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  vmtest.c  TESTING vm.c                                                   */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>		/* exit() */
#include "print.h"
#include "compile.h"
#include "vm.h"
#include "common.h"

#define YES 1
#define NO 0
#define TRUE 1
#define FALSE 0

#define failif(test, msg)						\
  do { if (test) { fprintf(stderr, "Failed %s:%d: %s\n", __FILE__,	\
			   __LINE__, msg);				\
      fflush(stderr);							\
      exit(-1);								\
    }									\
  } while (0)

static int vmtest_full (const char *pattern_desc, Package *pkg, Index entrypoint,
			const char *input, size_t inputlength,
			Match *match, int matched, size_t leftover, int abend) {
  PackageTable *pt = packagetable_new(0);
  Buffer *buf = buf_new_wrap(input, inputlength);

  int stat = packagetable_set_user(pt, pkg);
  if (stat < 0) {
    printf("vmtest: expected successful set of user package\n");
    goto done;
  }
  printf("Running vm on pattern '%s' and input: \"%s\"\n", pattern_desc, input);
  printf("Entrypoint is %d\n", entrypoint);
  stat = vm_start(pt, entrypoint, buf, 0, buf->next, byte_encoder, NULL, match);
  if (stat != 0) {
    printf("vmtest: expected successful execution of vm\n");
    goto done;
  }
  if (match->matched != matched) {
    printf("vmtest: expected %s\n", matched ? "succcess but match failed" : "fail but match succeeded");
    stat = 1;
    goto done;
  }
  if (match->abend != abend) {
    printf("vmtest: unexpected %s termination\n", abend ? "normal" : "abnormal");
    stat = 1;
    goto done;
  }
  if (match->leftover != leftover) {
    printf("vmtest: expected %lu leftover, got %lu\n", leftover, match->leftover);
    stat = 1;
    goto done;
  }

  stat = OK;

 done:
  buf_free(buf);
  packagetable_free(pt);
  return stat;

}

static int vmtest (const char *pattern_desc, Package *pkg, const char *input,
		   Match *match, int matched, size_t leftover) {
  return vmtest_full(pattern_desc, pkg, 0, input, strlen(input),
		   match, matched, leftover, FALSE);
}

static int vmtest_abend (const char *pattern_desc, Package *pkg, const char *input,
			 Match *match, int matched, size_t leftover) {
  return vmtest_full(pattern_desc, pkg, 0, input, strlen(input),
		   match, matched, leftover, TRUE);
}

static void print_match_data (Match *m) {
  uint8_t c;
  size_t i;
  char *s = m->data->data;
  size_t n = m->data->next;
  for (i = 0; i < n; i++) {
    c = *s++;
    if ((c < 33) || (c > 126)) printf("0x%02d ", c);
    else printf("%c ", c);
  }
  printf("\n");
}

int main(void) {

  int stat;
  Match *match;
  
  Context *C;
  Package *pkg;
  Error err;
  
  Env *toplevel, *env; 

  Expression *exp, *exph, *A, *B, *S;
  Ref ref, refA, refB, refS, ref_anon;

  const char *input;
  Buffer *buf;
  PackageTable *pt;
  Encoder encoder;
  Index id;

  Index i;
  SymbolTableEntry *entry;
  const char *name;
  char refname[2];
  refname[0] = '\0';
  refname[1] = '\0';
  
  printf("$$$ vmtest\n");

  encoder = debug_encoder;

  match = match_new();
  failif(!match, "expected new match structure");

  pt = packagetable_new(0);
  failif(!pt, "expected new package table");

  toplevel = pexlc_make_env(NULL);
  failif(!toplevel, "new toplevel should succeed");
  
  C = pexlc_make_context(toplevel);
  failif(!C, "expected new context");

  refS = pexlc_make_binding(C, toplevel, NULL); /* NULL == anonymous binding */
  failif(ref_is_error(refS), "bind should work");

  A = pexle_call(C, refS);				/* A -> S */
  exp = pexle_capture_f(C, "foo", pexle_from_string(C, "H")); /* exp -> capture("foo", "H") */
  S = pexle_seq_f(C, exp, pexle_constant(C, "constcapname", "constcapvalue")); /* S -> exp A */

  pexlc_bind_expression(C, S, refS, &err);
  failif(err.value != OK, "should succeed");

  pkg = pexlc_compile_expression(C, A, toplevel, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  failif(err.value != OK, "should indicate ok");
  pexle_free(A);

  print_package(pkg);
  print_symboltable_stats(pkg->symtab);
  print_symboltable(pkg->symtab);

  input = "Hello";
  buf = buf_new_wrap(input, strlen(input));
  
  /* Error checking */
  stat = vm_start(NULL, 0, buf, 0, strlen(input), encoder, NULL, match);
  failif(stat != VM_ERR_NULL, "expected to catch error that package table arg is null");
  stat = vm_start(pt, 0, NULL, 0, strlen(input), encoder, NULL, match);
  failif(stat != VM_ERR_NULL, "expected to catch error that input arg is null");
  stat = vm_start(pt, 0, buf, 0, strlen(input), encoder, NULL, NULL);
  failif(stat != VM_ERR_NULL, "expected to catch error that match arg is null");
  stat = vm_start(pt, 0, buf, 0, strlen(input), encoder, NULL, match);
  failif(stat != VM_ERR_NULL, "expected to catch error that package table is empty");

  stat = packagetable_set_user(pt, pkg);
  failif(stat < 0, "expected successful set of user package");
  
  /* One last set of error checks */
  stat = vm_start(pt, -1, buf, 0, strlen(input), encoder, NULL, match);
  failif(stat != VM_ERR_ENTRYPOINT, "expected to catch error that entrypoint is invalid");
  stat = vm_start(pt, pkg->codenext, buf, 0, strlen(input), encoder, NULL, match);
  failif(stat != VM_ERR_ENTRYPOINT, "expected to catch error that entrypoint is invalid");

  buf_free(buf);
  packagetable_free(pt);
  
  failif(vmtest("capture(\"foo\", 'H')", pkg, "Hello", match, TRUE, 4), "vm test failed");

/*   stat = vm_start(pt, 0, buf, 0, strlen(input), encoder, NULL, match); */
/*   failif(stat != 0, "expected successful execution of vm"); */
/*   failif(!match->matched, "should have matched"); */
/*   failif(match->abend, "should have terminated normally"); */
/*   failif(match->leftover != 4, "should have this number of leftover chars"); */
/*   failif(!match->data, "basic test that there is a data buffer here"); */
  
  printf("-----\n");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Testing the byte encoder, without and with a prefix \n");

  failif(!pkg, "pre-requisite for next test: reusing package");

  /* First, with no prefix */
  stat = package_set_default_prefix(pkg, NULL);
  failif(stat != 0, "expected success");

  failif(vmtest("capture(\"foo\", 'H')", pkg, "Hello", match, TRUE, 4), "vm test failed");

  printf("Byte-encoded match data (len %lu): ", match->data->next);
  for (i = 0; i < (int) match->data->next; i++)
    if (match->data->data[i] < 27)
      printf("\\%u ", (uint8_t) match->data->data[i]);
    else
      printf("%c ", match->data->data[i]);
  printf("\n");

  failif(match->data->next != 49, "expected this number of bytes in encoded output");

  /* Now with a prefix */
  stat = package_set_default_prefix(pkg, "PREFIX");
  failif(stat != 0, "expected success");

  failif(vmtest("capture(\"foo\", 'H')", pkg, "Hello", match, TRUE, 4), "vm test failed");

  printf("Byte-encoded match data (len %lu): ", match->data->next);
  for (i = 0; i < (int) match->data->next; i++)
    if (match->data->data[i] < 27)
      printf("\\%u ", (uint8_t) match->data->data[i]);
    else
      printf("%c ", match->data->data[i]);
  printf("\n");

  failif(match->data->next != 63, "expected this number of bytes in encoded output");
  
  package_free(pkg);
  printf("-----\n");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Testing the HALT instruction \n");

  exph = pexle_halt(C);
  exp = pexle_seq(C, S, exph);
  failif(!exp, "exp should be a valid expression");
  pexle_free(exph);
  
  pkg = pexlc_compile_expression(C, exp, toplevel, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  failif(err.value != OK, "should indicate ok");
  pexle_free(exp);
  /* do not free S, because S is bound */

  print_package(pkg);
  print_symboltable_stats(pkg->symtab);
  print_symboltable(pkg->symtab);

  printf("Testing the HALT instruction\n");
  
  failif(vmtest_abend("capture(\"foo\", 'H'); HALT", pkg, "Hello", match, TRUE, 4), "vm test failed");

  printf("-----\n");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Testing that encoders see package prefixes \n");

  failif(!pkg, "pre-requisite for next test: reusing package");

  stat = package_set_default_prefix(pkg, "Prefix here!");
  failif(stat != 0, "expected success");
  
  failif(vmtest_abend("capture(\"foo\", 'H'); HALT", pkg, "Hello", match, TRUE, 4), "vm test failed");

  package_free(pkg);
  printf("-----\n");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Grammar with realistic recursion \n");

  /* New env for grammar */
  env = pexlc_make_recursive_env(toplevel);
  failif(!env, "failed to create new env");

  id = context_intern(C, "foo"); /* for later use */
  failif(id < 0, "symbol add should succeed");

  ref = pexlc_make_binding(C, env, "A");
  failif(ref_is_error(ref), "bind should work");

  A = pexle_choice_f(C,		/* A -> "a" A "b" / epsilon */
		 pexle_seq_f(C,
			 pexle_from_string(C, "a"),
			 pexle_seq_f(C, 
				 pexle_call(C, ref),
				 pexle_from_string(C, "b"))),
		 pexle_from_boolean(C, 1));

  failif(!A, "A is a valid expression");
  printf("A is: \n"); 
  print_exp(A, 0, C); 

  pexlc_bind_expression(C, A, ref, &err);
  failif(err.value != OK, "bind should work");

  pkg = pexlc_compile_expression(C, A, env, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  failif(err.value != OK, "bind should work");
  /* do not free A because A is bound */

  print_package(pkg);
  print_symboltable(pkg->symtab);

  printf("Testing that pattern fails on input 'Hello'...\n");
  failif(vmtest("A -> 'a' A 'b' / eps", pkg, "Hello", match, TRUE, 5), "vm test failed");
  printf("... ok. \n");

  printf("Testing that pattern succeeds on appropriate input...\n");
  failif(vmtest("A -> 'a' A 'b' / eps", pkg, "aaabbb", match, TRUE, 0), "vm test failed");
  printf("... ok. \n");
  
  printf("Testing that pattern matches a prefix of the input...\n");
  failif(vmtest("A -> 'a' A 'b' / eps", pkg, "abc", match, TRUE, 1), "vm test failed");
  printf("... ok. \n");
  
  package_free(pkg);
  printf("-----\n");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Now testing entrypoint argument to vm_start() \n");

  pexlc_env_free(env);
  env = pexlc_make_recursive_env(toplevel);
  failif(!env, "failed to create new env");
  
  refA = pexlc_make_binding(C, env, "A");
  failif(ref_is_error(refA), "bind should work");

  /* Here we use the variable B to hold the anonymous expression */
  /* <anon> -> "x" */
  B = pexle_from_string(C, "x");

  failif(!B, "expected new expression");

  ref_anon = pexlc_make_binding(C, env, NULL);
  failif(ref_is_error(ref_anon), "expected success");
  pexlc_bind_expression(C, B, ref_anon, &err);
  failif(err.value != OK, "bind should work");

  /* Here we reuse the variable B to make the exp we will bind to "B" */
  /* B -> "a"+ <anon> == "a"+ "x" */
  B = pexle_seq_f(C, 
	      pexle_repeat_f(C, pexle_from_string(C, "a"), 1), /* "a"+ */
	      pexle_call(C, ref_anon)			 /* "x"  */
	      );
  failif(!B, "expected new expression");

  refB = pexlc_make_binding(C, env, "B");
  failif(ref_is_error(refB), "expected success");
  pexlc_bind_expression(C, B, refB, &err);
  failif(err.value != 0, "expected success");

  /* A -> B "b" == "a"+ "x" "b" */
  A = pexle_seq_f(C, 
	      pexle_call(C, refB),
	      pexle_from_string(C, "b"));

  failif(!A, "A is a valid expression");

  /* Now modify the "A" binding's value to the pattern based on the expression A */
  pexlc_bind_expression(C, A, refA, &err);
  failif(err.value != 0, "expected success");

  pkg = pexlc_compile_bound_expression(C, refA, &err, NO_OPTIM);
  failif(!pkg, "expected success");
  failif(err.value != 0, "expected success");
  print_package(pkg);
  print_symboltable(pkg->symtab);
  
  i = SYMBOLTABLE_ITER_START;
  while ((entry = symboltable_iter(pkg->symtab, &i))) {
    name = symboltable_entry_name(pkg->symtab, entry);
    if (strcmp(name, "A")==0) break;
  }
  failif(!name, "should find the symbol 'A'");
  printf("Found 'A' in symbol table.  Entrypoint is %d\n", entry->value);

  input = "aaaxb"; 
  printf("Testing a+xb pattern on input '%s'...\n", input); 
  failif(vmtest_full("'a'+ 'x' 'b'", pkg, entry->value, input, strlen(input), match, TRUE, 0, FALSE), "vm test failed");
  printf("... ok. \n");
  
  input = "aaaaaaaaaaxb";
  failif(vmtest_full("'a'+ 'x' 'b'", pkg, entry->value, input, strlen(input), match, TRUE, 0, FALSE), "vm test failed");
  printf("... ok. \n");
  
  input = "aaax";
  failif(vmtest_full("'a'+ 'x' 'b'", pkg, entry->value, input, strlen(input), match, FALSE, 4, FALSE), "vm test failed");
  printf("... ok. \n");
  
  input = "aaaxb123";
  failif(vmtest_full("'a'+ 'x' 'b'", pkg, entry->value, input, strlen(input), match, TRUE, 3, FALSE), "vm test failed");
  printf("... ok. \n");
  
  package_free(pkg);
  printf("-----\n");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Misc patterns \n");

  /* New env for grammar */
  pexlc_env_free(env);
  env = pexlc_make_recursive_env(toplevel);
  failif(!env, "failed to create new env");

  exp = pexle_from_set(C, "abcABC", 6);
  failif(!exp, "exp should be a valid expression");
  pkg = pexlc_compile_expression(C, exp, env, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  pexle_free(exp);

  print_package(pkg);
  print_symboltable(pkg->symtab);

  failif(vmtest("[abcABC]", pkg, "A", match, TRUE, 0), "vm test failed");
  package_free(pkg);

  exp = pexle_from_bytes(C, "AB\0", 3);
  failif(!exp, "exp should be a valid expression");
  pkg = pexlc_compile_expression(C, exp, env, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  pexle_free(exp);
  
  print_package(pkg);
  print_symboltable(pkg->symtab);

  failif(vmtest_full("AB\\0", pkg, 0, "AB\0zzz", 6, match, TRUE, 3, FALSE), "vm test failed");
  package_free(pkg);

  exp = pexle_from_number(C, 3);	/* any 3 bytes */
  failif(!exp, "exp should be a valid expression");
  pkg = pexlc_compile_expression(C, exp, env, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  pexle_free(exp);

  print_package(pkg);
  print_symboltable(pkg->symtab);

  failif(vmtest(".{3}", pkg, "abc", match, TRUE, 0), "vm test failed");
  failif(vmtest(".{3}", pkg, "abcde", match, TRUE, 2), "vm test failed");
  failif(vmtest(".{3}", pkg, "ab", match, FALSE, 2), "vm test failed");
  failif(vmtest(".{3}", pkg, "a", match, FALSE, 1), "vm test failed");
  failif(vmtest(".{3}", pkg, "", match, FALSE, 0), "vm test failed");
  package_free(pkg);

  exp = pexle_from_number(C, -3); /* NOT 3 bytes */
  failif(!exp, "exp should be a valid expression");
  pkg = pexlc_compile_expression(C, exp, env, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  pexle_free(exp);
  
  print_package(pkg);
  print_symboltable(pkg->symtab);

  failif(vmtest("!.{3}", pkg, "", match, TRUE, 0), "vm test failed");
  failif(vmtest("!.{3}", pkg, "a", match, TRUE, 1), "vm test failed");
  failif(vmtest("!.{3}", pkg, "ab", match, TRUE, 2), "vm test failed");
  failif(vmtest("!.{3}", pkg, "abc", match, FALSE, 3), "vm test failed");
  failif(vmtest("!.{3}", pkg, "abcd", match, FALSE, 4), "vm test failed");
  failif(vmtest("!.{3}", pkg, "abcde", match, FALSE, 5), "vm test failed");
  package_free(pkg);

  exp = pexle_from_number(C, 0);	/* same as TRUE (== epsilon)*/
  failif(!exp, "exp should be a valid expression");
  pkg = pexlc_compile_expression(C, exp, env, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  pexle_free(exp);

  print_package(pkg);
  print_symboltable(pkg->symtab);

  failif(vmtest("TRUE", pkg, "abcde", match, TRUE, 5), "vm test failed");
  failif(vmtest("TRUE", pkg, "", match, TRUE, 0), "vm test failed");
  package_free(pkg);

  exp = pexle_from_range(C, 65, 64);  /* Error checking */
  failif(exp, "error when from > to");
  exp = pexle_from_range(C, 65, 64 + 26);	/* [A-Z] */
  failif(!exp, "exp should be a valid expression");
  pkg = pexlc_compile_expression(C, exp, env, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  pexle_free(exp);

  print_package(pkg);
  print_symboltable(pkg->symtab);

  failif(vmtest("[A-Z]", pkg, "abcde", match, FALSE, 5), "vm test failed");
  failif(vmtest("[A-Z]", pkg, "", match, FALSE, 0), "vm test failed");
  failif(vmtest("[A-Z]", pkg, "A", match, TRUE, 0), "vm test failed");
  failif(vmtest("[A-Z]", pkg, "OP", match, TRUE, 1), "vm test failed");
  failif(vmtest_full("[A-Z]", pkg, 0, "\0", 1, match, FALSE, 1, FALSE), "vm test failed");
  package_free(pkg);

  exp = pexle_not_f(C, NULL);
  failif(exp, "error, second arg should be an expression");
  exp = pexle_not_f(C, pexle_from_string(C, "JAJ"));
  failif(!exp, "exp should be a valid expression");
  pkg = pexlc_compile_expression(C, exp, env, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  pexle_free(exp);

  print_package(pkg);
  print_symboltable(pkg->symtab);

  failif(vmtest("!'JAJ'", pkg, "", match, TRUE, 0), "vm test failed");
  failif(vmtest("!'JAJ'", pkg, "jaj", match, TRUE, 3), "vm test failed");
  failif(vmtest("!'JAJ'", pkg, "JAJ", match, FALSE, 3), "vm test failed");
  package_free(pkg);

  exp = pexle_not_f(C, pexle_seq_f(C,
			       pexle_from_string(C, "JAJ"),
			       pexle_from_number(C, -1) /* $ */
			       ));
  failif(!exp, "exp should be a valid expression");
  pkg = pexlc_compile_expression(C, exp, env, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  pexle_free(exp);

  print_package(pkg);
  print_symboltable(pkg->symtab);

  failif(vmtest("!('JAJ'$)", pkg, "", match, TRUE, 0), "vm test failed");
  failif(vmtest("!('JAJ'$)", pkg, "jaj", match, TRUE, 3), "vm test failed");
  failif(vmtest("!('JAJ'$)", pkg, "JAJ", match, FALSE, 3), "vm test failed");
  failif(vmtest("!('JAJ'$)", pkg, " JAJ", match, TRUE, 4), "vm test failed");
  failif(vmtest("!('JAJ'$)", pkg, "JAJ3", match, TRUE, 4), "vm test failed");
  package_free(pkg);

  exp = pexle_lookahead_f(C, NULL);
  failif(exp, "error, second arg should be an expression");
  exp = pexle_lookahead_f(C, pexle_from_string(C, "JAJ"));
  failif(!exp, "exp should be a valid expression");
  pkg = pexlc_compile_expression(C, exp, env, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  pexle_free(exp);
  
  print_package(pkg);
  print_symboltable(pkg->symtab);

  failif(vmtest(">'JAJ'", pkg, "", match, FALSE, 0), "vm test failed");
  failif(vmtest(">'JAJ'", pkg, "jaj", match, FALSE, 3), "vm test failed");
  failif(vmtest(">'JAJ'", pkg, "JAJ", match, TRUE, 3), "vm test failed");
  failif(vmtest(">'JAJ'", pkg, "JAJ123", match, TRUE, 6), "vm test failed");
  package_free(pkg);


#if 0
  exp = pexle_lookbehind(C, NULL);
  failif(exp, "error, second arg should be an expression");
  exp = pexle_lookbehind(C, pexle_from_string(C, "JAJ"));
  failif(!exp, "exp should be a valid expression");
  printf("TODO: NOT COMPILING lookbehind right now.  Re-enable when ready.\n");
/*   pkg = pexlc_compile_expression(C, exp, env, &err, NO_OPTIM); */
/*   failif(!pkg, "should succeed"); */
  pexle_free(exp); 
  
  print_package(pkg);
  print_symboltable(pkg->symtab);

  /* TODO: add tests for lookbehind */
#endif

  exp = pexle_subtract_f(C, pexle_from_range(C, 'x', 'z'), NULL);
  failif(exp, "error, second arg should be an expression");
  exp = pexle_subtract_f(C, NULL, pexle_from_string(C, "y"));
  failif(exp, "error, second arg should be an expression");
  exp = pexle_subtract_f(C, pexle_from_range(C, 'x', 'z'), pexle_from_string(C, "y"));
  failif(!exp, "exp should be a valid expression");
  pkg = pexlc_compile_expression(C, exp, env, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  pexle_free(exp);

  print_package(pkg);
  print_symboltable(pkg->symtab);

  failif(vmtest("[xyz]-'y'", pkg, "", match, FALSE, 0), "vm test failed");
  failif(vmtest("[xyz]-'y'", pkg, "y", match, FALSE, 1), "vm test failed");
  failif(vmtest("[xyz]-'y'", pkg, "a", match, FALSE, 1), "vm test failed");
  failif(vmtest("[xyz]-'y'", pkg, "x", match, TRUE, 0), "vm test failed");
  failif(vmtest("[xyz]-'y'", pkg, "z", match, TRUE, 0), "vm test failed");
  package_free(pkg);

  exp = pexle_repeat_f(C, NULL, 0);
  failif(exp, "error, second arg should be an expression");
  exp = pexle_repeat_f(C, pexle_from_string(C, "a"), 0);
  failif(!exp, "exp should be a valid expression");
  pkg = pexlc_compile_expression(C, exp, env, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  pexle_free(exp);

  print_package(pkg);
  print_symboltable(pkg->symtab);

  failif(vmtest("a*", pkg, "", match, TRUE, 0), "vm test failed");
  failif(vmtest("a*", pkg, "a", match, TRUE, 0), "vm test failed");
  failif(vmtest("a*", pkg, "aaaaaaK", match, TRUE, 1), "vm test failed");
  failif(vmtest("a*", pkg, "K", match, TRUE, 1), "vm test failed");
  package_free(pkg);

  exp = pexle_repeat_f(C, pexle_from_string(C, "a"), 4);
  failif(!exp, "exp should be a valid expression");
  pkg = pexlc_compile_expression(C, exp, env, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  pexle_free(exp);
  
  print_package(pkg);
  print_symboltable(pkg->symtab);

  failif(vmtest("a{4,}", pkg, "", match, FALSE, 0), "vm test failed");
  failif(vmtest("a{4,}", pkg, "a", match, FALSE, 1), "vm test failed");
  failif(vmtest("a{4,}", pkg, "aaaaK", match, TRUE, 1), "vm test failed");
  failif(vmtest("a{4,}", pkg, "aaaaaaaaaaaKJ", match, TRUE, 2), "vm test failed");
  package_free(pkg);

  exp = pexle_repeat_f(C, pexle_from_string(C, "a"), -4);
  failif(!exp, "exp should be a valid expression");
  pkg = pexlc_compile_expression(C, exp, env, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  pexle_free(exp);
  
  print_package(pkg);
  print_symboltable(pkg->symtab);

  failif(vmtest("a{,4}", pkg, "", match, TRUE, 0), "vm test failed");
  failif(vmtest("a{,4}", pkg, "a", match, TRUE, 0), "vm test failed");
  failif(vmtest("a{,4}", pkg, "aaaaK", match, TRUE, 1), "vm test failed");
  failif(vmtest("a{,4}", pkg, "aaaaaKJ", match, TRUE, 3), "vm test failed");
  package_free(pkg);

  exp = pexle_capture_f(C, "This is a long capture name", NULL);
  failif(exp, "error, second arg should be an expression");
  exp = pexle_capture_f(C, NULL, pexle_from_string(C, "x"));
  failif(exp, "error, second arg should be an expression");
  exp = pexle_capture_f(C, "This is a long capture name", pexle_from_string(C, "a"));
  failif(!exp, "exp should be a valid expression");
  pkg = pexlc_compile_expression(C, exp, env, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  pexle_free(exp);
  
  print_package(pkg);
  print_symboltable(pkg->symtab);

  failif(vmtest("capture(long name, 'a')", pkg, "", match, FALSE, 0), "vm test failed");
  failif(vmtest("capture(long name, 'a')", pkg, "a", match, TRUE, 0), "vm test failed");
  print_match(match);
  print_match_data(match);
  failif(match->data->next != 36, "wrong length of match data");
  failif(vmtest("capture(long name, 'a')", pkg, "aXYZ", match, TRUE, 3), "vm test failed");
  print_match(match);
  print_match_data(match);
  failif(match->data->next != 36, "wrong length of match data");
  
  package_free(pkg);

  exp = pexle_constant(C, "foo", NULL);
  failif(exp, "error, second arg should be a string");
  exp = pexle_constant(C, NULL, "foo");
  failif(exp, "error, second arg should be a string");
  exp = pexle_constant(C, "This is a long capture name", "And a long value name");
  failif(!exp, "exp should be a valid expression");
  pkg = pexlc_compile_expression(C, exp, env, &err, NO_OPTIM);
  failif(!pkg, "should succeed");
  pexle_free(exp);

  print_package(pkg);
  print_symboltable(pkg->symtab);

  failif(vmtest("constant_capture(long name, long value)", pkg, "XYZ", match, TRUE, 3), "vm test failed");
  print_match(match);
  print_match_data(match);
  failif(match->data->next != 61, "wrong length of match data");

  package_free(pkg);
  pkg = NULL;

  printf("-----\n");

  /* ----------------------------------------------------------------------------- */
  printf("********************** Using compile_environment \n");

#define bind(exp, env, name) do {	 \
    ref = pexlc_make_binding(C, (env), (name));	   \
    failif(ref_is_error(ref), "bind should work"); \
    name[0]++; \
    pexlc_bind_expression(C, (exp), ref, &err);		\
    failif(err.value != OK, "rebind should work");	\
  } while (0)

  /* New env */
  pexlc_env_free(env);
  env = pexlc_make_env(toplevel);
  failif(!env, "failed to create new env");

  refname[0] = 'a';
  refname[1] = '\0';
  exp = pexle_from_set(C, "abcABC", 6);
  failif(!exp, "exp should be a valid expression");
  bind(exp, env, refname);

  /* b */
  exp = pexle_from_bytes(C, "AB\0", 3);
  failif(!exp, "exp should be a valid expression");
  bind(exp, env, refname);

  /* c */
  exp = pexle_from_number(C, 3);	/* any 3 bytes */
  failif(!exp, "exp should be a valid expression");
  bind(exp, env, refname);
  
  /* d */
  exp = pexle_from_number(C, -3); /* NOT 3 bytes */
  failif(!exp, "exp should be a valid expression");
  bind(exp, env, refname);

  /* e */
  exp = pexle_from_number(C, 0);	/* same as TRUE (== epsilon)*/
  failif(!exp, "exp should be a valid expression");
  bind(exp, env, refname);

  /* f */
  exp = pexle_from_range(C, 65, 64);  /* Error checking */
  failif(exp, "error when from > to");
  exp = pexle_from_range(C, 65, 64 + 26);	/* [A-Z] */
  failif(!exp, "exp should be a valid expression");
  bind(exp, env, refname);

  /* g */
  exp = pexle_halt(C);
  failif(!exp, "exp should be a valid expression");
  bind(exp, env, refname);

  /* h */
  exp = pexle_not_f(C, NULL);
  failif(exp, "error, second arg should be an expression");
  exp = pexle_not_f(C, pexle_from_string(C, "JAJ"));
  failif(!exp, "exp should be a valid expression");
  bind(exp, env, refname);

  /* i */
  exp = pexle_lookahead_f(C, NULL);
  failif(exp, "error, second arg should be an expression");
  exp = pexle_lookahead_f(C, pexle_from_string(C, "JAJ"));
  failif(!exp, "exp should be a valid expression");
  bind(exp, env, refname);

  /* j */
  exp = pexle_subtract_f(C, pexle_from_range(C, 'x', 'z'), NULL);
  failif(exp, "error, second arg should be an expression");
  exp = pexle_subtract_f(C, NULL, pexle_from_string(C, "y"));
  failif(exp, "error, second arg should be an expression");
  exp = pexle_subtract_f(C, pexle_from_range(C, 'x', 'z'), pexle_from_string(C, "y"));
  failif(!exp, "exp should be a valid expression");
  bind(exp, env, refname);

  /* k */
  exp = pexle_repeat_f(C, NULL, 0);
  failif(exp, "error, second arg should be an expression");
  exp = pexle_repeat_f(C, pexle_from_string(C, "a"), 0);
  failif(!exp, "exp should be a valid expression");
  bind(exp, env, refname);

  /* l */
  exp = pexle_repeat_f(C, pexle_from_string(C, "a"), 4);
  failif(!exp, "exp should be a valid expression");
  bind(exp, env, refname);

  /* m */
  exp = pexle_repeat_f(C, pexle_from_string(C, "a"), -4);
  failif(!exp, "exp should be a valid expression");
  bind(exp, env, refname);

  /* n */
  exp = pexle_capture_f(C, "This is a long capture name", NULL);
  failif(exp, "error, second arg should be an expression");
  exp = pexle_capture_f(C, NULL, pexle_from_string(C, "x"));
  failif(exp, "error, second arg should be an expression");
  exp = pexle_capture_f(C, "This is a long capture name", pexle_from_string(C, "a"));
  failif(!exp, "exp should be a valid expression");
  bind(exp, env, refname);

  /* o */
  exp = pexle_constant(C, "foo", NULL);
  failif(exp, "error, second arg should be a string");
  exp = pexle_constant(C, NULL, "foo");
  failif(exp, "error, second arg should be a string");
  exp = pexle_constant(C, "foo", "bar");
  failif(!exp, "exp should be a valid expression");
  bind(exp, env, refname);
  
#if 0
  /* p */
  /* SKIPPING LOOKBEHIND FOR NOW */
  exp = pexle_lookbehind(C, NULL);
  failif(exp, "error, second arg should be an expression");
  exp = pexle_lookbehind(C, pexle_from_string(C, "JAJ"));
  failif(!exp, "exp should be a valid expression");
  printf("TODO: NOT COMPILING lookbehind right now.  Re-enable when ready.\n");
#endif  

  print_env(env, C);

  pkg = pexlc_compile_environment(C, env, &err, DEFAULT_OPTIM);
  failif(!pkg, "should succeed");
  failif(err.value != OK, "should work");
  
  print_package(pkg);
  print_symboltable(pkg->symtab);

#define LOOKUP(refname) do {					\
    i = SYMBOLTABLE_ITER_START;					\
    while ((entry = symboltable_iter(pkg->symtab, &i))) {	\
      name = symboltable_entry_name(pkg->symtab, entry);	\
      if (strcmp(name, (refname))==0) break;			\
    }								\
  } while (0);

  /* Spot checks of just a few entrypoints */

  refname[0] = 'a';
  LOOKUP(&refname[0]);
  failif(vmtest_full("ref('a')", pkg, entry->value, "abcABC", 6, match, TRUE, 5, FALSE), "vm test failed");

  refname[0] = 'o';
  LOOKUP(&refname[0]);
  failif(vmtest_full("constant_capture('foo', 'bar')", pkg, entry->value, "ABC", 3, match, TRUE, 3, FALSE), "vm test failed");
  print_match_data(match);
  failif(match->data->next != 19, "wrong length of match data");

  refname[0] = 'f';
  LOOKUP(&refname[0]);
  failif(vmtest_full("[A-Z]", pkg, entry->value, "JKL", 3, match, TRUE, 2, FALSE), "vm test failed");
  failif(vmtest_full("[A-Z]", pkg, entry->value, "....", 4, match, FALSE, 4, FALSE), "vm test failed");
  failif(vmtest_full("[A-Z]", pkg, entry->value, "Z", 1, match, TRUE, 0, FALSE), "vm test failed");


  package_free(pkg);
  pkg = NULL;

  printf("-----\n");


  match_free(match);
  pexlc_env_free(env);
  pexlc_env_free(toplevel);
  context_free(C);

  /* ----------------------------------------------------------------------------- */
  printf("\nDone.\n");
  printf("Don't forget to run peepholetest and inlineoptimizertest next.\n");

  return 0;
}

