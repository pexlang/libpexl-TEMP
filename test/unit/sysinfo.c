/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  sysinfo.c                                                                */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#define _GNU_SOURCE		/* for dladdr() */

#include <stdio.h>
#include <assert.h>
#include <time.h>
#include <inttypes.h>
#include <sys/utsname.h>
#include <libgen.h>		/* dirname */
#include <sys/param.h>		/* MAXPATHLEN */
#include <string.h>		/* strncpy */
#include <stdlib.h>		/* malloc */
#include <dlfcn.h>		/* dladdr */
#include <unistd.h>		/* readlink */
#include <errno.h>		/* errno */
#include <sys/stat.h>		/* stat for file timestamps */

static int little_endian (void) {
  union {
    uint32_t whole;
    char parts[4];
  } endian_test = {0x00030201};
  return endian_test.parts[0];
}

#if 0
/* Follow symbolic links in file system to actual file */
static int actual_file (char *inpath, char **outpath) {
  int n = 0;
  char *in = malloc((size_t) MAXPATHLEN);
  char *out = alloca((size_t) MAXPATHLEN);
  strncpy(in, inpath, MAXPATHLEN);
  while (n != -1) {
    n = readlink(in, out, MAXPATHLEN);
    if (n != -1) {
      strncpy(in, out, n);
      in[n] = '\0';
    }
  }
  if (errno == EINVAL) {
    *outpath = in;
    return 0;
  }
  free(in); 
  return errno; 
}
#endif

/*        int dlinfo(void *handle, int request, void *info); */

static void *static_symbol;

/* Assuming MAXPATHLEN is same as PATH_MAX. TODO: Investigate. */

static char *libdir (const char *prog, const char *suffix) {
  long remaining;
  size_t finalpathlen;
  static char *last, *install_filename, *install_dir, *real_install_dir, *finalpath;
  static Dl_info info;
  char *path = malloc(MAXPATHLEN + 1); 

  last = path;
  remaining = MAXPATHLEN;	/* Reserve a byte for null terminator */
  install_filename = NULL;

  /* Get filename of this dynamic library or executable */
  if (dladdr(&static_symbol, &info) != 0) {
    install_filename = strndup(info.dli_fname, MAXPATHLEN);
    printf("filename of dylib is %s\n", install_dir);
  } else {
    install_filename = strndup(prog, MAXPATHLEN);
    printf("filename of executable is %s\n", install_filename);
  }

  /* Extract just the directory */
  install_dir = dirname(install_filename);

  /* Turn it into an absolute path */
  real_install_dir = realpath(install_dir, NULL);
  if (!real_install_dir) return NULL; /* ERR */
  free(install_filename); /* dirname returns ptr to its own internal storage */
  printf("real_install_dir is %s\n", real_install_dir);

  last = stpncpy(last, real_install_dir, (size_t) remaining);
  remaining = MAXPATHLEN - strnlen(path, MAXPATHLEN);
  if (remaining <= 0) return NULL; /* ERR */
  *last = '/'; last++;
  remaining -= 1;
  if (remaining < 0) return NULL; /* ERR */
  if (!suffix) return path;

  last = stpncpy(last, suffix, (size_t) remaining);
  remaining = MAXPATHLEN - strnlen(path, MAXPATHLEN);
  if (remaining <= 0) return NULL; /* ERR */
  printf("Composite path to target is %s\n", path);

  /* Get absolute, resolved path (dynamically allocated) */
  finalpath = realpath(path, NULL);
  finalpathlen = finalpath ? strnlen(finalpath, MAXPATHLEN) : 0;
  if (finalpathlen > MAXPATHLEN) return NULL; /* ERR */
  printf("Final real path to target is %s\n", finalpath);
  return finalpath;			      /* OK */
}

static int test_open (char *filename) {
  FILE *f = fopen(filename, "r");
  if (f) {
    fclose(f);
    printf("File opened successfully\n");
    return 1;			/* SUCCESS */
  }
  printf("Error: ");
  switch (errno) {
  case ENOTDIR:
    printf("a component of the path is not a directory\n");
    break;
  case EACCES:
    printf("permission denied\n");
    break;
  case ENOENT:
    printf("file not found\n");
    break;
  default:
    printf("other error\n");
  }
  return 0;			/* ERROR */
}

int main (int argc, char **argv) {
  int status;
  struct utsname u;
  size_t len;
  char buf[100];
  struct tm *tmstruct;
  struct timespec time;
  struct stat fileinfo;
  char timestringbuf[26];
  char *exec;

  printf("$$$ sysinfo\n");

  printf("Usage: %s [filename]\n\n", argv[0]);
  
  /* ENDIANNESS */
  printf("Machine is %s endian\n", little_endian() ? "LITTLE" : "BIG");

  /* SYSTEM */
  status = uname(&u);  
  if (status) {
    printf("uname call failed!\n");
    return -1;
  }
  /* u.nodename is the local machine name.  omitting for privacy. */
  printf("System: %s\nRelease: %s\nVersion: %s\nMachine: %s\n",
	 u.sysname, u.release, u.version, u.machine);

  /* OPEN FILE RELATIVE TO INSTALLATION DIR */
  exec = (argc > 0) ? argv[0] : NULL;
  printf("\nName of this executable is %s\n", exec);
  printf("Command line filename is %s\n", argv[1]);
  printf("Relative to this executable, it is %s\n", libdir(exec, argv[1]));
  if ((argc < 1) || !argv[1]) {
    printf("No command line arg, so skipping the opening of relative file.\n");
  } else {
    test_open(libdir(exec, argv[1]));
  }
  
  /* File information */
  status = stat(exec, &fileinfo);
  if (status != 0){
    printf("stat FAILED with code %d\n", status);
  } else {
#ifdef __APPLE__
    /* MacOS */
    ctime_r(&fileinfo.st_atimespec.tv_sec, timestringbuf);
    timestringbuf[24] = '\0';
    printf("  atime: %s (%lu, %lu)\n", timestringbuf, fileinfo.st_atimespec.tv_sec, fileinfo.st_atimespec.tv_nsec);
    ctime_r(&fileinfo.st_mtimespec.tv_sec, timestringbuf);
    timestringbuf[24] = '\0';
    printf("  mtime: %s (%lu, %lu)\n", timestringbuf, fileinfo.st_mtimespec.tv_sec, fileinfo.st_mtimespec.tv_nsec);
    ctime_r(&fileinfo.st_ctimespec.tv_sec, timestringbuf);
    timestringbuf[24] = '\0';
    printf("  ctime: %s (%lu, %lu)\n", timestringbuf, fileinfo.st_ctimespec.tv_sec, fileinfo.st_ctimespec.tv_nsec);
#else
    /* Linux */
    ctime_r(&fileinfo.st_atim.tv_sec, timestringbuf);
    timestringbuf[24] = '\0';
    printf("  atime: %s (%lu, %lu)\n", timestringbuf, fileinfo.st_atim.tv_sec, fileinfo.st_atim.tv_nsec);
    ctime_r(&fileinfo.st_mtim.tv_sec, timestringbuf);
    timestringbuf[24] = '\0';
    printf("  mtime: %s (%lu, %lu)\n", timestringbuf, fileinfo.st_mtim.tv_sec, fileinfo.st_mtim.tv_nsec);
    ctime_r(&fileinfo.st_ctim.tv_sec, timestringbuf);
    timestringbuf[24] = '\0';
    printf("  ctime: %s (%lu, %lu)\n", timestringbuf, fileinfo.st_ctim.tv_sec, fileinfo.st_ctim.tv_nsec);


    printf("  atime: %s", ctime(&fileinfo.st_atim.tv_sec));
    printf("  mtime: %s", ctime(&fileinfo.st_mtim.tv_sec));
    printf("  ctime: %s", ctime(&fileinfo.st_ctim.tv_sec));
#endif
    
  }

  /* TIME */
  status = clock_gettime(CLOCK_REALTIME, &time);
  printf("\nTime value (sec) is %ld bytes, (nsec) is %ld bytes\n",
	 sizeof(time.tv_sec), sizeof(time.tv_nsec));
  printf("Time: %ld, %ld\n", time.tv_sec, time.tv_nsec);

  tmstruct = localtime(&time.tv_sec);
  len = strftime(buf, 100, "%c", tmstruct);
  printf("\nFor reference:\nLocal time %.*s\n", (int) len, buf);
  tmstruct = gmtime(&time.tv_sec);
  len = strftime(buf, 100, "%c", tmstruct);
  printf("UTC time %.*s\n", (int) len, buf);

  return 0;
}
  
