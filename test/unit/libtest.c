/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  libtest.c  Testing libpexl                                               */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>		/* write() */

#include "libpexl.h"

#define failif(test, msg)						\
  do { if (test) { fprintf(stderr, "Failed %s:%d: %s\n", __FILE__,	\
			   __LINE__, msg);				\
      fflush(stderr);							\
      exit(-1);								\
    }									\
  } while (0)

int main(void) {

  /* How should the PEXL_pat type be defined?

     Let's consider different approaches to compilation, because that
     will shed light on the situation:

     1. Compile immediately upon pattern creation.  Would require user
     to compile patterns in dependency order.  Would require user to
     build up a set of recursive bindings before compilation, which
     contradicts the "compile upon creation" notion.

     2. Compile when needed, i.e. upon matching.  This is what lpeg
     does.  It hides compilation from the user, like some regex
     libraries do, thus simplifying the API (i.e. simplifying the
     usage of the library, and making it easier to create interfaces
     from a variety of programming languages).  The main downside is
     that compilation errors and warnings will be raised at match
     time, which complicates the match API.

     3. Compiling is an explicit step that must be done before
     matching.  Of course, a "smart" matching interface could accept
     an uncompiled pattern and compile it.  This is what the Rosie API
     does today, as shown in the Python and other interface libraries.
     (This has the downside of #2, but it moves the choice of whether
     to compile on the fly into the interface library, e.g. the Python
     or Java library, instead of making this decision for the writers
     of those interfaces.

     
     PEXL_pat could be a struct with (1) env, (2) entrypoint binding.

     Notably, the binding can be anonymous and it does not have to be
     IN the environment.  It must be associated with an environment in
     order to resolve references and to compile it.  There can even be
     references TO this binding from others, as long as they are
     direct references and not by name.  E.g. we can refer to an
     anonymous binding by holding a pointer to it.

     However, we currently maintain a collection of all bindings in an
     env -- this is in fact the point of the env structure.  Yet, the
     existence of an additional anonymous binding, B, outside of an
     env, but associated with it, should not cause any problems.  Is
     it the case no anonymous binding needs to be in the binding table
     of its env?  That is, can an anonymous binding point to its env,
     without having a pointer the other way, from an env to an
     anonymous binding?  (The answer should be "yes", unless we messed
     something up in our implementation.)

     What, really, is an anonymous binding, anyway?  The data
     structure is Env_binding (currently), but when it is anonymous,
     it is simply the data structure of an expression.

     There are two ways we can conceive of referring to an expression.
     (1) When building a new expression using copies of existing ones.
     In fact, this is exactly how lpeg works -- new patterns made
     using existing ones have copies of them, which can lead to large
     memory requirements.  (2) When explicitly "calling" another
     expression as a subroutine.  In this case, we are thinking of
     lambda expressions, i.e. anonymous functions.

     A traditional compiler implementation for a functional language
     embeds an environment pointer into the data structure for a
     function, so that closures are supported.  Rosie has, and PEXL
     should have, closures as well.  Without them, we would have
     dynamic scope, which has been all but abandoned in language
     design because it inhibits understanding of code by its lexical
     structure.

     Note that there are (currently) no RPL/PEXL language constructs
     that support mutation.  However, the API provides mutation
     capability in order to support implementation of a REPL, and also
     of future programs that do new and clever things (fancy debugging
     is one example).

     The REPL's need to mutate is seen in this example.  I define
     pattern P that calls Q, among doing other work.  In the REPL, I
     redefine Q in order to alter the behavior of P.  Note that this
     is possible only if Q is visible to the REPL.  (If Q were a local
     definition inside of P, it would not be visible in a traditional
     REPL.)  When P is a closure, the reference to Q is baked into P.
     Through the REPL, I need to mutate the existing binding to Q, not
     create a new (shadow) binding for Q.

     OPTION 1:
     --------

     This discussion suggests some changes to the current set of data
     structures in PEXL to align them better to how functional
     programming languages are implemented:

     - Use a pointer to refer to a binding instead of an index into a
       table of bindings.  This will make it easier to garbage collect
       unneeded bindings.  (They can be freed without also requiring
       table compaction.)

     - An environment does not need to store anonymous bindings, only
       named ones so we can look them up by name.

     - An anonymous binding must have a reference to its environment.

     - The entrypoint table is not needed; we do not need to process
       them as a collection.  A pattern binding can contain a pointer
       to an entrypoint structure, as a string binding can contain a
       reference to a string, etc.
     
     On this consideration, the type of PEXL_pat should be a pattern
     binding.  It must have an environment reference.  It may (or may
     not) be anonymous.  If it has been compiled, it will have code.

     A REPL will need to recompile a pattern P when a dependency, like
     Q, changes.  In the current implementation, pattern dependencies
     are calculated on compilation and then discarded.

     Even if we kept a pattern's dependencies around, what we actually
     need is the inverse map (from Q to other patterns like P that
     reference Q) in order to recompile after a mutation.
     
     We will assume that the use cases for mutation all involve user
     interaction, as do the REPL and hypothetical future debugger, so
     that performance is only a small concern.  Upon mutation of Q,
     then, suppose we do not know which bindings like P depend on Q.
     We could recompile everything.  Or, perhaps just as easily, we
     could recalculate all dependencies (which is a prerequisite to
     compilation anyway) and then scan the dependencies to find
     patterns like P that depend on Q.  After recompiling Q, we
     recursively recompile patterns like P, so that we recompile
     patterns which depend on P, etc.


     PLAN TO IMPLEMENT THESE CHANGES:

     - [IR] Do not fix TOpenCall into a TECall, but rather to a (new)
       TCall, in order to distinguish it in the IR.  Now, a TCall will
       always contain relative coords (local, or upwards in tree) and
       a TECall will always contain absolute coords (to another
       package).

       At this point, we should NOT need to test if coords are
       relative or absolute -- we should know.

     - [IR] Sometimes we test for local coords.  When we no longer
       flatten environments, these tests should go away.  (But we can
       always test for locality of ref later by checking if the
       referred binding has the same env as the current binding
       containing the reference.)

     - [Env] Add an env pointer.  (Later, we will remove the child
       pointer.)  Change the IR for TEcall so a TECall node contains a
       pointer to the binding it is calling.  Do the same for TCall,
       TXCall? 

     - ... Build up to a point where we can compile without flattening
       the environment tree ...


     OPTION 2: 
     -------- 

     How can we support the use cases described above without major
     changes?  A key bit is that the case for mutation involves only
     visible bindings.  When we flatten an environment, we do not get
     rid of any top-level visible bindings.  We delete all child
     environments, but the bindings they contained were all moved to
     the top level either by name (visible) or as anonymous bindings
     (not visible).  In other words, we don't have to support a use
     case where the user reaches into a local environment to mutate
     something AFTER compilation.  This is good, because with our
     current compilation strategy, that local environment no longer
     exists. 
     
     So, how do we reify expressions?  Create an anonymous entrypoint
     binding.  Allow the IR in that entrypoint to refer to other
     bindings by index, like they do now.  A PEXL_pat is then a struct
     of an env and an index into the binding table of that env.

     How avoid garbage piling up in the binding table as the user
     compiles expression after expression?  When the user no longer
     needs an expression, they tell us.  We delete it from the binding
     table, and keep a free list so that we can reuse that table
     slot.  And, we INVALIDATE anything that depends on that
     expression (i.e. anything that refers to that slot in that env). 

     An expensive way to do this invalidation would be recalculate all
     dependencies.  We should start with this, as the performance may
     not be too bad, and certainly later we can make it more efficient
     by storing the "depended upon" data (inverse dependencies).

     BUT, we must avoid giving out (to the user, through the API)
     pointers to environments that we may destroy without notice
     during compilation (flattening).  Maybe, then, we need a special
     API for building up local environments, e.g. for recursive
     patterns ("letrec") or other local definitions ("let").

     ALSO, we must ensure that we can compile -- without special
     handling -- newly added expressions, even ones that have local
     environments.  It should be a matter of resetting the compilation
     state of the top level environment, and then compiling the
     expression. 

     Which brings us to the elephant in the room: we currently compile
     an environment tree (from its root), NOT an expression.

     How can we get from here to there?

     OBSERVATIONS:

     * Any expression the user needs to compile is at top level.  That
       is the API we provide.  

     * An expression may be simple, or it may contain local bindings
       (and therefore local environments).  And, due to our particular
       language semantics, expressions with local environments may
       yield more than one top-level expression.

     * A powerful API would let the user manipulate anonymous
       expressions via "handles" of type PEXL_pat.  We should try to
       achieve this, even with local environments.

     * Handles to local (not at top level) bindings will be invalid
       when the top-level expression that contains them is compiled.
     
     First, the ability to compile an expression.

     - The expression may be anonymous or named, because in both cases
       we represent it as an Env_binding.

     - A simple expression can be built using PEXL operators and
       references to other top-level bindings (anonymously by handle,
       or by name).

     - We can compile an expression by (1) computing its dependencies,
       and (2) compiling them in dependency order -- just like we do
       now with the entire top-level environment.

     - In the PEXL scenario (Rosie/RPL-like use cases), there is a
       pre-populated top-level environment that we call a "prelude".
       Suppose we compile and save the prelude, and that we load and
       reconstruct it on start-up.  Equivalently, at start-up we can
       just seed the top-level environment with the prelude bindings
       and then compile it before the user does anything.  Then, the
       user assembles an expression and wants to compile it; what do
       we do?
       
       1. Calculate dependencies of the expression.
       2. Compile in dependency order, skipping already-compiled ones. 
       3. Rebuild the single code vector for the env, and record the
          offset into the vector in the entrypoint field of the
	  expression. 

     - Clearly, that last step is not efficient, especially when we
       are only ADDING new code, and not changing any existing code.
       In this case, we can realloc the code vector and simply add
       onto the end.

     - What about the case where the API allows a mutation?  If an
       existing top-level binding is changed, and the environment has
       been compiled, we can invalidate the entire compilation by
       resetting the environment's compilation state.  Since mutation
       only occurs in REPL and debugging use cases, we do not need to
       create an especially fast solution.

     - How will the API look when the user wants to assemble an
       expression that has local bindings?  They must create all the
       bindings they need inside a "let" or "letrec" structure, and
       then compile it.  The result of compilation can yield multiple
       values, so perhaps the API should return one and and also an
       iterator (if there are more)?  But it is not that simple.  To
       constructed nested "let" and "letrec" structures, the API must
       allow access to the return values from compiling the inner
       structures -- without compiling them.  I.e. we must allow
       access to those handles before compilation.

       - Open a "letrec" (env, child of main); create expressions,
         anonymous or named, with refs within this env, or to a parent
         env, or to a package env.  Recursive expressions require
         names (as we lack a Y combinator or equivalent).  Each
         created expression must be marked as externally visible or
         not. 

       - Open a "let" env, child of the "letrec"; create expressions,
         at least one marked visible.  Any visible expressions can be
         used to make additional expressions in the "letrec", i.e. the
         parent env.  

       - COULD VISIBILITY BE SET AUTOMATICALLY based on usage?  Sure,
         and this would simplify the API.  As soon as an inner
         binding, e.g. in the "let", is used to make a new expression
         in the "letrec", we can create a forward binding in the
         "letrec" down into the "let".  HOWEVER, how do we know there
         isn't a forward binding already?  It's easy if the binding
         has a name -- we look up that name in the "letrec" and if a
         forward binding exists, it must be the one.  If a regular
         binding exists, then we have an error.  But what if the
         binding from inside the "let" is anonymous?  There are no
         pointers upward to the forward binding.

       - Therefore, the user must declare visibility through the API
         when creating a local binding.  We then create a forward
         binding for it automatically, and give them the handle to
         that.  They can use that handle in the local env (and we
         follow the forwarding child+data to find the local
         binding).  And they can use that handle in the outer env,
         too, which is where the handle lives.

       - Creating multi-link forward chains should work, as long as on
         every use, we follow the forwarding info one link at a time,
         until we get to the env in which the handle is being used.
         (If we do not get to this env, it's an error.)

       - Example:
         - Create "letrec" env (child of main).  
	 - Create "let" env (child of the "letrec").
	 - Create anonymous visible binding, x1, in "let", bound to an
           expression.  Handle will be to forward binding in "letrec"
           env. 
	 - Create anonymous visible binding, x2, in "letrec", with x1
           as its value.  The handle to x2 will be a forward binding
           in the main env, pointing to an anonymous binding in
           "letrec" whose expression refers to x1, which is a forward
           binding to an expression in "let".
	 - Now x2 is visible in main and has the value of an
           expression assembled in a local env within another local
           env within main.  And all are anonymous bindings!
	 - Only x2 (among these bindings) can be compiled, because
           it's in main.  The handle to x1 becomes invalid once x2 is
           compiled. 

       - How do we implement handles becoming invalid?  We could
         simply tell the user to never use those handles again,
         because it is clear to the API user which handles will go
         away.  But it would be better to trap errors.  This means NOT
         giving out pointers to memory that we will free after
         compilation, e.g. pointers to the "letrec" and "let"
         environments.

       - If we merely want to trap accidental errors, we could keep a
         table of active environments, and assign each a number.  A
         32-bit counter could be incremented every time we create a
         new environment, and we could ensure no collisions on
         rollover by checking our table of active environments.  A
         linear search (with the number as the key) of the table
         should suffice, because at most we expect maybe 3 or 4
         entries.  The number of entries is the maximum nesting depth,
         which we could limit if we wished.  Maybe 32 or 64 deep would
         be ok even for programmatically generated patterns?  Storing
         64 pointers, each with an 8 byte counter, uses only 1Kb!  (An
         8 byte counter is overkill, but with 8-byte struct
         alignments, that much space will be used even for a 4-byte
         counter.)

       - Importantly, these handles are only maintained by the API.
         The underlying code would be just what we have now, but for
         the POSSIBLE addition of an env pointer to every binding.

     TO BUILD THE API:

     - API should keep a table of active environments, as described
       above.

     - Handles given out by the API to bindings will have the type
       PEXL_pat, and will be 64 bits: a 32-bit env handle and a 32-bit
       index into the binding table of that env.

     - API will have a gc function that takes a handle and deletes the
       binding (provided the handle's env is still active, else it's a
       no-op). 

     - After a forward binding is compiled, the env it points to can
       be gc'd, and thus removed from the env table of the API.
       Really, this is after ALL the forward bindings pointing into
       that env.

     - Perhaps this is the right model always: A new expression gets a
       new env whose parent is main.  Compiling that expression means
       following the forward pointer to its env, compiling that env,
       and promoting any visible bindings up to main.  This
       generalizes nicely to multiple forward pointers to the same
       child env.  Can we cheaply create new envs?



   */

  PEXL_pat t1, t2;		/* was TTree */
  PEXL_pkg p;			/* was Package */
  PEXL_env main;		/* was Env */
  PEXL_match m;			/* was Match */

  int stat;

  printf("************** libtest: Testing libpexl **************\n\n");

  /* ----------------------------------------------------------------------------- */

  main = pexlc_env();

  printf("**** Basic patterns\n");

  t1 = pexlc_from_string(main, "Hi");
  stat = pexlc_match(t1, "Hi there.", &m);
  print_match(m);

  t2 = pexlc_seq(main, t1, pexlc_from_string(" there."));
  stat = pexlc_match(t2, "Hi there.", &m);
  print_match(m);

  printf("**** Referring to patterns anonymously\n");

  t2 = pexlc_from_string(main, "a");
  t1 = pexlc_repeat(main, pexlc_call(t2), 0);
  stat = pexlc_match(t1, "aaa", &m);
  print_match(m);
  
  printf("**** Binding names to patterns\n");

  stat = pexlc_bind(main, "a", t2);
  t1 = pexlc_repeat(main, pexlc_call("a"), 3);
  stat = pexlc_match(t1, "aaa", &m);
  print_match(m);
  stat = pexlc_match(t1, "aa", &m);
  print_match(m);
  
  t1 = pexlc_bind(main, "a3", t1);
  t2 = pexlc_ref("a3");
  assert( t2 == t1 );

  printf("**** Recursive patterns\n");


  printf("**** Local non-recursive patterns\n");


  printf("**** Using a saved package\n");


  printf("**** Creating and saving a package\n");


  printf("Done.\n");
  return 0;
}

#pragma GCC diagnostic pop
