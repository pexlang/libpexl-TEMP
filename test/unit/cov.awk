function basename(path)
{
    return gensub(".*/", "", "g", path)
}

BEGIN {
    FS = "[: ]";
    printf("Coverage report\n");
    printf("------------------------------------------\n");
};
/logging.h/ { skip = NR; next };
/^File/  { fcount++; printf("%-20s ", basename(substr($2, 2, length($2)-2))) };
/^Lines/ { if (NR != skip+1) {
	printf("%7s  of %3d lines\n",$3,$5);
	lcount += $5
    }
};
END {
    printf("\n%2d files %22s %4d lines\n", fcount, "", lcount);
};

