/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  vm.c  Matching vm                                                        */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "vm.h"
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include <alloca.h>

#include "stack.h"           /* Macros defining static procedures */

#define VMDEBUG 0

#if (VMDEBUG)
#include "print.h"
#pragma GCC diagnostic push	/* requires gcc 4.6 */
#pragma GCC diagnostic ignored "-Wcast-qual"
__attribute__((unused)) static void fprint_inst(const Package *pkg, const Instruction *pc) {
  Instruction *_pc = (Instruction *) pc;
  /* The IGiveup instruction prints better if we pretend it is in its own code vector */
  Instruction *const _op = (opcode(pc) == IGiveup) ? (Instruction *const) pc : (Instruction *const) pkg->code;
  fprint_instruction(stderr, _op, _pc, NULL);
}
#pragma GCC diagnostic pop	/* requires gcc 4.6 */
#endif 

static Instruction giveup;

typedef struct BTEntry {
  union {
    const char *c;	      /* for non-calls: saved position */
    const Package *pkg;       /* for calls: saved package (has instruction vector) */
  } ptr;
  const Instruction *p;	      /* next instruction (in any instruction vector) */
  int caplevel;		      /* -1 for calls, -2 for suspendcaptures; else >= 0 */
} BTEntry;

STACK_OF(BTEntry, INIT_BACKTRACKSTACK)
STACK_INIT_FUNCTION(BTEntry)
STACK_FREE_FUNCTION(BTEntry)
STACK_EXPAND_FUNCTION(BTEntry, MAX_BACKTRACK)
STACK_PUSH_FUNCTION(BTEntry, 0)	/* Second arg: keep statistics if true */
STACK_POP_FUNCTION(BTEntry)

/* Double the size of the array of captures */
static Capture *doublecap (Capture *cap, Capture *initial_capture, int captop) {
  int newcapsize;
  Capture *newc;
  if (captop >= MAX_CAPLISTSIZE) return NULL;
  newcapsize = 2 * captop;
  if (newcapsize > MAX_CAPLISTSIZE) newcapsize = MAX_CAPLISTSIZE;
  newc = (Capture *)malloc(newcapsize * sizeof(Capture));
  memcpy(newc, cap, captop * sizeof(Capture));
  if (cap != initial_capture) free(cap); 
  return newc;
}

/* Enable when needed for debugging */
#if 0
static void BTEntry_stack_print (BTEntry_stack *stack, const char *o, const Instruction *op) {
  BTEntry *top;
  for (top = (stack->next - 1); top >= stack->base; top--)
    fprintf(stderr,
	    "%ld: pos=%ld, pc %ld: %s, caplevel=%d\n",
	   top - stack->base,
	   (top->ptr.c == NULL) ? -1 : (top->ptr.c - o), 
	   (top->p == &giveup) ? -1 : (top->p - op), 
	   OPCODE_NAME(opcode(top->p)),
	   top->caplevel);
}
#endif

static int find_prior_capture (Capture *capture, int captop, uint32_t target_idx,
			       const char **s, const char **e) {
  int i, ii, j, outer_cap, cap_end, balance;
  uint32_t outer_capidx;
  if (captop == 0) return 0;

  for (ii=0; ii<captop; ii++) {
    if (isopencap(&capture[ii])) printf("([%d] %d ", ii, capidx(&capture[ii]));
    else if (isclosecap(&capture[ii])) printf("[%d]) ", ii);
  }

  /* Skip backwards past any immediate OPENs. */
  for (i = captop - 1; i > 0; i--) {
    if (!isopencap(&capture[i])) break;
  }
  cap_end = i;
  LOGf("find_prior_capture", "cap_end = %d", cap_end);
  /* Scan backwards for the first OPEN without a CLOSE. */
  outer_cap = 0;
  outer_capidx = 0;
  balance = 0;
  /* Recall that capture[0] is always an OPEN for the outermost
     capture, which cannot have a matching CLOSE. */
  for (; i >= 0; i--) {
    if (isopencap(&capture[i])) {
      if (balance == 0) break;
      balance += 1;
    } else {
      if (isclosecap(&capture[i])) {
	balance -= 1;
      } 
    }
  }
  outer_cap = i;
  LOGf("find_prior_capture", "outer_cap = %d", outer_cap);
  outer_capidx = capidx(&capture[i]);
  LOGf("find_prior_capture", "outer_capidx = %d", outer_capidx);
  /* Now search backward from the end for the target, skipping any
     other instances of outer_capidx */
  for (i = cap_end; i >= outer_cap; i--) {
    if (isopencap(&capture[i]) && capidx(&capture[i]) == target_idx) {
      balance = 0;
      for (j = i - 1; j >= outer_cap; j--) {
	if (isopencap(&capture[j])) {
	  if ((balance >= 0) && capidx(&capture[j]) == outer_capidx) break;
	  balance += 1;
	} else {
	  if (isclosecap(&capture[j])) {
	    balance -= 1;
	  }
	}
      }
      if (j == outer_cap) {
	break; /* Nothing to skip over */
      }
    }
  } /* for */
  if (i == outer_cap - 1) {
    for (i = outer_cap; i >= 0; i--) {
      if (isopencap(&capture[i]) && capidx(&capture[i]) == target_idx) break;
    } /* for */
    if (! (isopencap(&capture[i]) && capidx(&capture[i]) == target_idx)) {
      return 0;
    }
  }
  LOGf("find_prior_capture",
       "isopencap(&capture[i] = %d, capidx(&capture[i]) = %d",
       isopencap(&capture[i]), capidx(&capture[i]));
  /* This the open capture we are looking for */
  assert (isopencap(&capture[i]));
  *s = capture[i].s;                      /* start position */
  /* Now look for the matching close */
  i++;
  j = 0;
  while (i <= captop) {
    if (isclosecap(&capture[i])) {
      if (j == 0) {
	/* This must be the matching close capture */
	*e = capture[i].s;               /* end position */
	return 1;	                 /* success */
      } else {
	j--;
	assert( j >= 0 );
      }
    } else {
      assert( isopencap(&capture[i]) );
      j++;
    }
    i++;
  } /* while looking for matching close*/
  /* Did not find the matching close */
  return 0;
}

/* Find the prior capture that we want to reference */
static int __attribute__ ((noinline))
find_backref (const Instruction *pc, Capture *capture, int captop,
			 const char *s, const char *e) {
  const char *startptr = NULL; 
  const char *endptr = NULL; 
  size_t prior_len;
  uint8_t target = addr(pc); 
  int have_prior = find_prior_capture(capture, captop, target, 
				      &startptr, &endptr); 
  if (have_prior) { 
    assert( startptr && endptr ); 
    assert( endptr >= startptr );
    prior_len = endptr - startptr; 
    if ( ((size_t)(e - s) >= prior_len) && (memcmp(s, startptr, prior_len) == 0) ) { 
      return prior_len;
    }
  }
  return ERR;
}

#define PUSH_CAPLIST						\
  if (++captop >= capsize) {					\
    capture = doublecap(capture, initial_capture, captop);	\
    if (!capture) {						\
      return MATCH_ERR_CAP;					\
    }								\
    *capturebase = capture;					\
    capsize = 2 * captop;					\
  }

#define JUMPBY(delta) pc = pc + (delta)
#define JUMPTO(dest) pc = (dest)

#define get_package(pt, i) ((((i) >= 0) && ((i) < (pt)->next)) ? (pt)->packages[(i)] : NULL)

static void unwind_suspensions(BTEntry_stack *stack, int *capturing) {
  while (STACK_SIZE(*stack) > 0) {
    if (TOP(*stack)->caplevel != -2) return;	/* TODO: magic number */
    *capturing = (TOP(*stack)->ptr.c != NULL);   /* restore prior setting */
    if (1) printf("Capturing is %s (via unwind_suspensions)\n", *capturing ? "TRUE" : "FALSE");
    BTEntry_stack_pop(stack);
  } /* while */
}

/* Remove pending calls while handling suspensions as well */
static void unwind_calls(BTEntry_stack *stack, int *capturing) {
  while (STACK_SIZE(*stack) > 0) {
    /* Stop when we find a CHOICE frame */
    if (TOP(*stack)->caplevel >= 0) return;
    /* Pop any pending calls */
    if (TOP(*stack)->caplevel == -1) { /* TODO: magic number */
      BTEntry_stack_pop(stack);
      continue;
    }
    /* Unwind any suspensions */
    if (TOP(*stack)->caplevel == -2) { /* TODO: magic number */
      unwind_suspensions(stack, capturing);
      continue;
    }
    else return;		/* stack is corrupt TODO: log a message */
  } /* while */
}

static Index find_choiceframe (BTEntry_stack stack) {
  Index choiceframe = 0;	/* start at TOP */
  while (STACK_SIZE(stack) > 0) {
    /* Stop when we find a CHOICE frame */
    if (PEEK(stack, choiceframe)->caplevel >= 0) return choiceframe;
    choiceframe++;
  } /* while */
  return ERR;
}

/* FUTURE: Consider prefetch intrinsics.  Look at valgrind
   --tool=cachegrind, and --tool=massif.
*/

/* FUTURE: Consider branch prediction macros, e.g.
   #define likely(x)       __builtin_expect((x),1)
   #define unlikely(x)     __builtin_expect((x),0)
*/

/* 
   FUTURE: Look into __attribute__((hot)) for the vm.  Also, move less
   common instructions to their own separate functions, and mark those
   with __attribute__((noinline)).
*/

/* 'stat's can be null; 'ptable' can be null; e is exclusive */

/* TODO: Examine the assertions in the vm, and find the ones that
   protect us against bad bytecode.  Throw an error in those cases. 
*/
static int 
vm (const char *os,	           /* IN: beginning of input byte array */
    const char *c,		   /* IN: first input char to examine */
    const char *oe,		   /* IN: one beyond last char to examine */
    const PackageTable *const pt,  /* IN: package table */ 
    const char **r,                /* OUT: first unmatched input byte  */
    Capture **capturebase,         /* IN/OUT: array where vm stores captures */
    int *num_caps,		   /* OUT: number of capture frames */
    Package *trampoline_package) {

  Index choiceframe;
  BTEntry bt;
  BTEntry_stack stack;
  const Package *pkg;
  const Instruction *pc;
  Capture *initial_capture = *capturebase;
  Capture *capture = *capturebase;
  int capsize = INIT_CAPLISTSIZE;
  int captop = 0;	   /* point to first empty slot in captures */
  int n, len;
  int capturing = 1; 	   /* any non-zero value */

  if (1) printf("Capturing is %s\n", capturing ? "TRUE" : "FALSE");

  /* Bottom of backtrack stack is IGiveup, so matching fails if we pop
     the last element. 
  */
  BTEntry_stack_init(&stack);
  giveup.i.code = IGiveup;
  giveup.i.aux = 0;

  pkg = trampoline_package;
  pc = pkg->code;		/* current instruction is the first one */

#define NOT_EOI (c < oe)
#define CHARS_SEEN (c - os)
#define ADVANCE (c++)
#define RETREAT(i) do {				\
    c -= (i);					\
  } while (0);

  bt.ptr.c = os;		/* first char of input */
  bt.p = &giveup;
  bt.caplevel = 0;		/* IGiveup stack frame poses as a CHOICE */
  BTEntry_stack_push(&stack, bt);

  /* Main loop:
     Switch based on current instruction.
     Advance s through the input when things match.
     Push state onto BTEntry_stack for backtracking.
  */
  for (;;) {
#if (VMDEBUG) 
      fprintf(stderr, "vm: in package %p  ", (const void *) pkg); 
      fprint_inst(pkg, pc); 
#endif
    switch (opcode(pc)) {
    case ITestSet: {
      assert(sizei(pc)==1+CHARSETINSTSIZE);
      assert(addr(pc));
      if (NOT_EOI && testchar((pc+2)->buff, (int)((uint8_t)*c)))
	JUMPBY(1+CHARSETINSTSIZE); /* sizei */
      else JUMPBY(addr(pc));
      continue;
    }
    case IAny: {
      assert(sizei(pc)==1);
      if NOT_EOI { JUMPBY(1); ADVANCE; }
      else goto fail;
      continue;
    }
    case IPartialCommit: {
      assert(sizei(pc)==2);
      assert(addr(pc));
      assert(stack.next > stack.base);
      /* Top of stack should be a CHOICE frame, not a CALL */
      choiceframe = find_choiceframe(stack);
      if (choiceframe < 0) {
	LOG("vm", "in IPartialCommit, did not find choice frame on stack");
	return MATCH_ERR_INTERNAL; /* Actually "improper byte code" */
      }

      assert(PEEK(stack, choiceframe)->caplevel >= 0);

      /* This optional behavior allows us to accommodate repetitions of nullable patterns. */
      /* TODO: Probably we need peek down to the nearest CHOICE frame to look at c? */
      if (aux(pc) && (PEEK(stack, choiceframe)->ptr.c == c)) goto fail;

      PEEK(stack, choiceframe)->ptr.c = c;
      PEEK(stack, choiceframe)->caplevel = captop;
      JUMPBY(addr(pc));
      continue;
    }
    case IEnd: {
      assert(sizei(pc)==1);
      assert(stack.next == stack.base + 1);
      /* This Cclose capture (below) is a sentinel to mark the end of
       * the linked caplist.  If it is the only capture on the list,
       * then walk_captures will see it and not go any further.
       */
      setcapkind(&capture[captop], Cclose);
      capture[captop].s = NULL;
      BTEntry_stack_free(&stack);
      *r = c;
      *num_caps = captop;
      return MATCH_OK;
    }
    case IGiveup: {
      assert(sizei(pc)==1);
      BTEntry_stack_free(&stack);
      *r = NULL;
      *num_caps = captop;
      return MATCH_OK;
    }
    case IRet: {
      assert(sizei(pc)==1);
      unwind_suspensions(&stack, &capturing);
      assert(stack.next > stack.base);
      /* Top of stack should be a CALL frame */
      assert(TOP(stack)->caplevel == -1);
      assert(TOP(stack)->ptr.pkg);
      pkg = TOP(stack)->ptr.pkg;
      pc = TOP(stack)->p;
#if (VMDEBUG) 
	fprintf(stderr, "entering IRet to pkg %p (#%d) entrypoint %d\n", 
	       (const void *) pkg, pkg->number, (int) (pc - pkg->code));
#endif
      BTEntry_stack_pop(&stack);
      continue;
    }
    case ITestAny: {
      assert(sizei(pc)==2);
      assert(addr(pc));
      if NOT_EOI JUMPBY(2);
      else JUMPBY(addr(pc));
      continue;
    }
    case IChar: {
      assert(sizei(pc)==1);
      if (NOT_EOI && ((uint8_t)*c == ichar(pc))) { JUMPBY(1); ADVANCE; }
      else goto fail;
      continue;
    }
    case ITestChar: {
      assert(sizei(pc)==2);
      assert(addr(pc));
      if (NOT_EOI && ((uint8_t)*c == ichar(pc))) JUMPBY(2);
      else JUMPBY(addr(pc));
      continue;
    }
    case ISet: {
      assert(sizei(pc)==CHARSETINSTSIZE);
      if (NOT_EOI && testchar((pc+1)->buff, (int)((uint8_t)*c)))
	{ JUMPBY(CHARSETINSTSIZE); /* sizei */
	  ADVANCE;
	}
      else { goto fail; }
      continue;
    }
    case IBehind: {
      assert(sizei(pc)==1); 
      n = aux(pc); 
      if (n > CHARS_SEEN) goto fail;
      RETREAT(n); JUMPBY(1); 
      continue;
    }
    case ISpan: {
      assert(sizei(pc)==CHARSETINSTSIZE);
      for (; NOT_EOI; ADVANCE) {
	if (!testchar((pc+1)->buff, (int)((uint8_t)*c))) break;
      }
      JUMPBY(CHARSETINSTSIZE);	/* sizei */
      continue;
    }
    case IJmp: {
      assert(sizei(pc)==2);
      assert(addr(pc));
      JUMPBY(addr(pc));
      continue;
    }
    case IChoice: {
      assert(sizei(pc)==2);
      assert(addr(pc));
      bt.ptr.c = c; bt.p = pc + addr(pc); bt.caplevel = captop;
      if (!BTEntry_stack_push(&stack, bt))
	return MATCH_ERR_STACK;
      JUMPBY(2);
      continue;
    }
    case ICall: {
      assert(sizei(pc)==2);
      bt.ptr.pkg = pkg; bt.p = pc + 2; bt.caplevel = -1;
      if (!BTEntry_stack_push(&stack, bt))
	return MATCH_ERR_STACK;
      JUMPTO(pkg->code + addr(pc));		/* ABSOLUTE ADDRESS */
      continue;
    }
    case IXCall: { 
      assert(sizei(pc)==2); 
#if (VMDEBUG) 
	fprintf(stderr, "entering IXCall to package %d, entrypoint %d\n", aux(pc), addr(pc)); 
#endif
      bt.ptr.pkg = pkg; bt.p = pc + 2; bt.caplevel = -1;
      if (!BTEntry_stack_push(&stack, bt))  
	return MATCH_ERR_STACK;  
      pkg = get_package(pt, aux(pc));
      if (!pkg) {
	LOG("vm", "get_package failed in IXCall");
	return MATCH_ERR_INTERNAL;
      }
      JUMPTO(pkg->code + addr(pc)); /* ABSOLUTE ADDRESS */
      continue; 
    } 
    case ICommit: {
      assert(sizei(pc)==2);
      unwind_suspensions(&stack, &capturing);
      assert(addr(pc));
      assert(stack.next > stack.base);
      assert(TOP(stack)->ptr.c != NULL);
      /* Top of stack should be a CHOICE frame */
      assert(TOP(stack)->caplevel >= 0);
      BTEntry_stack_pop(&stack);
      JUMPBY(addr(pc));
      continue;
    }
    case IBackCommit: {
      assert(sizei(pc)==2);
      assert(addr(pc));
      unwind_suspensions(&stack, &capturing);
      assert(stack.next > stack.base);
      assert(TOP(stack)->ptr.c != NULL);
      /* Top of stack should be a CHOICE frame */
      assert(TOP(stack)->caplevel >= 0);
      c = TOP(stack)->ptr.c;
      /* Restoring captop is not necessary now that we use ISuspendCaptures */
      /* captop = TOP(stack)->caplevel; */
      BTEntry_stack_pop(&stack);
      JUMPBY(addr(pc));
      continue;
    }
    case IFailTwice:
      unwind_suspensions(&stack, &capturing);
      assert(stack.next > stack.base);
      /* Top of stack should be a CHOICE frame */
      assert(TOP(stack)->caplevel >= 0);
      BTEntry_stack_pop(&stack);
      /* fallthrough */
    case IFail:
      assert(sizei(pc)==1);
    fail: { /* pattern failed: try to backtrack */
	unwind_calls(&stack, &capturing);
	c = TOP(stack)->ptr.c;
        pc = TOP(stack)->p;
	BTEntry_stack_pop(&stack);
        continue;
      }
    case IBackref: {
      assert(sizei(pc)==2);
      if ((len = find_backref(pc, capture, captop, c, oe)) < 0) goto fail;
      c += len;
      JUMPBY(2);
      continue;
    }
    case ICloseConstCapture: {
      assert(sizei(pc)==2);
      assert(captop > 0);
      capture[captop].s = c;
      setcapidx(&capture[captop], addr(pc)); /* second ktable index */
      setcapkind(&capture[captop], Ccloseconst);
      capture[captop].pkg_number = pkg->number;
      PUSH_CAPLIST;
      JUMPBY(2);
      continue;
    }
    case ICloseCapture: {
      assert(sizei(pc)==1);
      /* Roberto's lpeg checks to see if the item on the stack can
	 be converted to a full capture.  We skip that check,
	 because we have removed full captures.  This makes the
	 capture list 10-15% longer, but saves almost 2% in time.
      */
      if (capturing) {
	assert(captop > 0);
	capture[captop].s = c;
	setcapkind(&capture[captop], Cclose);
	capture[captop].pkg_number = pkg->number;
	PUSH_CAPLIST;
      }
      JUMPBY(1);
      continue;
    }
    case IOpenCapture: {
      assert(sizei(pc)==2);
      if (capturing) {
	capture[captop].s = c;
	setcapkind(&capture[captop], aux(pc)); /* ktable index */
	setcapidx(&capture[captop], addr(pc)); /* kind of capture */
	capture[captop].pkg_number = pkg->number;
	PUSH_CAPLIST;
      }
      JUMPBY(2);
      continue;
    }
    case IHalt: {				    /* rosie */
      assert(sizei(pc)==1);
      /* We could unwind the stack, committing everything so that we
	 can return everything captured so far.  Instead, we simulate
	 the effect of this in caploop() in lpcap.c.  (And that loop
	 is something we should be able to eliminate!)
      */
      setcapkind(&capture[captop], Cfinal);
      capture[captop].s = c;
      capture[captop].pkg_number = pkg->number;
      *r = c;
      BTEntry_stack_free(&stack);
      *num_caps = captop;
      return MATCH_HALT;
    }
    case ISuspendCaptures: {
      assert(sizei(pc)==1);
      bt.caplevel = -2;		/* TODO: magic numbers ==> named constants */
      /* This cast is a temporary hack.  TODO: rework the BTEntry struct */
      bt.ptr.c = (const char *) (long) capturing; /* Save current capturing mode */
      capturing = 0;
      if (1) printf("Capturing is %s (due to ISuspendCaptures)\n", capturing ? "TRUE" : "FALSE");
      if (!BTEntry_stack_push(&stack, bt))
	return MATCH_ERR_STACK;
      JUMPBY(1);
      continue;
    }
    case INoop: {
      JUMPBY(1);
      continue;
    }
    default: {
      LOGf("vm", "illegal opcode at %d: %d\n", (int) (pc - pkg->code), opcode(pc));
      BTEntry_stack_free(&stack);
      return MATCH_ERR_BADINST;
    } } /* switch on opcode */
  } /* loop */
}

/* -------------------------------------------------------------------------- */

/* caploop() processes the sequence of captures created by the vm.

   This sequence encodes a (possibly truncated) nested, balanced list
   of Opens and Closes.

   caploop() would naturally be written recursively, but a few years
   ago, I rewrote it in the iterative form it has now, where it
   maintains its own stack.  I no longer recall why.

   The stack is used to match up a Close capture (when we encounter it
   as we march along the capture sequence) with its corresponding Open
   (which we have pushed on our stack).

   The 'count' parameter contains the number of captures inside the
   Open at the top of the stack.  When it is not zero, the JSON
   encoder starts by emitting a comma "," because it is encoding a
   capture that is within a list of nested captures (but is not the
   first in that list).  Without 'count', a spurious comma would
   invalidate the JSON output.

   Note that the stack grows with the nesting depth of captures.  As
   of this writing (Friday, July 27, 2018), this depth rarely exceeds
   7 in the patterns we are seeing.
 */

#define capstart(cs) (capkind((cs)->cap)==Crosieconst ? NULL : (cs)->cap->s)

static int caploop (CapState *cs, const Encoder encode, Buffer *buf) {
  int err;
  const Capture *cap;		/* points to current capture */
  Capture synthetic;
  Index count = 0;
  Index depth = 0;		/* incr at each Open, decr at each Close */
  Index i;			/* current capture frame */

  for (i = 0; i < cs->n; i++) {
    cap = &(cs->ocap[i]);
    if (isopencap(cap)) {
      depth++;
      err = encode.Open(cs, cap, buf, count);
      if (err) return err;
      count = 0;
    } else if (isclosecap(cap)) {
      depth--;
      if (depth < 0) return MATCH_STACK_ERROR;
      err = encode.Close(cs, cap, buf, count);
      if (err) return err;
      count++;
    } else {
      assert( isfinalcap(cap) );
      break;
    }
  } /* for each capture frame */

  /* If the vm was halted (i.e. abend), there will be a Cfinal capture
     frame, and nothing after it.  In that case, we have a truncated
     list of capture frames, so we synthesize the missing Cclose
     frames and call the output encoder as if each existed. */
  if (depth > 0) {
    synthetic.s = cap->s;
    setcapidx(&synthetic, 0);
    setcapkind(&synthetic, Cclose);
    cap = &synthetic;
    for (; depth; depth--) {
      err = encode.Close(cs, cap, buf, count);
      if (err) return err;
    }
    return MATCH_HALT;		/* indicate abend */
  }
  return MATCH_OK;
}

/*
 * Prepare a CapState structure and traverse the entire list of
 * captures in the stack pushing its results. 's' is the subject
 * string. Call the output encoder functions for each capture (open,
 * close, or full).
 */
static int walk_captures (const Capture *capture,
			  const int num_captures,
			  const char *s,
			  const Encoder encode,
			  const int bytes,
			  PackageTable *pt,
			  /* outputs: */
			  Buffer *buf) {
  int err;
  CapState cs;

  if (num_captures == 0) return MATCH_OK;
  cs.pos_size = bytes;
  cs.ocap = capture;
  cs.s = s;
  cs.n = num_captures;
  cs.pt = pt;
  err = caploop(&cs, encode, buf);
  if (err) return err;
  return MATCH_OK;
}

/* 
   Encoded positions (which are offsets into the input data) are
   signed and 1-based. 'calc_encoding_size' computes the smallest
   encoding size for positions, given the size of the input.
*/
static char calc_encoding_size (size_t diff) {
  int64_t d = (int64_t) diff;
  if (d < (((int64_t) 1)<<15) - 1) return 2;
  if (d < (((int64_t) 1)<<31) - 1) return 4;
  if (d < (((int64_t) 1)<<47) - 1) return 6;
  return 8;
}

static Package *make_trampoline(Index entrypoint) {
  int trampoline_size = 3;
  Package *trampoline_package = malloc(sizeof(Package));
  Instruction *trampoline = malloc(trampoline_size * sizeof(Instruction));

  /* FUTURE: Generate this trampoline in advance, and re-use it every time */
  setopcode(&trampoline[0], IXCall);
  setaux(&trampoline[0], 0x00);	 /* package 0 is the user package */
  setaddr(&trampoline[0], entrypoint);

  setopcode(&trampoline[2], IEnd);

  trampoline_package->code = trampoline;
  trampoline_package->codenext = trampoline_size;
  trampoline_package->codesize = trampoline_size;
  trampoline_package->import_next = 1;
  trampoline_package->import_size = 1;
  trampoline_package->imports = malloc(sizeof(Import));
  trampoline_package->imports[0].pkg = NULL;
  trampoline_package->imports[0].importpath = -1;
  trampoline_package->imports[0].prefix = -1;
  
  trampoline_package->number = -1;
  trampoline_package->symtab = NULL;
  return trampoline_package;
}

/* 
   Positions are 0-based.  Endpos is exclusive, i.e. the vm will look
   at characters in [s, e).
*/
/* FUTURE: Add const and restrict keywords where appropriate */
EXPORT
int vm_start (PackageTable *pt,
	      Index entrypoint, /* entrypoint in package pt[0] */
	      Buffer *input, size_t startpos, size_t endpos,
	      Encoder encoder,
	      /* in/out */
	      Stats *stats,
	      /* outputs: */
	      Match *match) {

  int stat, abend;
  int t0;
  int num_captures;
  const char *r, *o, *s, *e;
  size_t buflen, len;
  char bytes;
  Package *trampoline;
  Capture initial_capture[INIT_CAPLISTSIZE];
  Capture *capture = initial_capture;

  if (!pt) {
    LOG("vm_start", "null package table argument");
    return VM_ERR_NULL;
  } else if (!input) {
    LOG("vm_start", "null input argument");
    return VM_ERR_NULL;
  } else if (!match) {
    LOG("vm_start", "null match argument");
    return VM_ERR_NULL;
  }

  if (!packagetable_get_user(pt)) {
    LOG("vm_start", "no user package in package table");
    return VM_ERR_NULL;
  }

  if ((entrypoint < 0) || (((size_t) entrypoint) >= packagetable_get_user(pt)->codenext)) {
    LOGf("vm_start", "invalid entrypoint %d (instruction vector size = %u)",
	 entrypoint, packagetable_get_user(pt)->codenext);
    return VM_ERR_ENTRYPOINT;
  }

  if (stats) t0 = clock();
  o = input->data;
  buflen = input->next;
  /* startpos, endpos are 0-based */
  if (startpos > buflen) return MATCH_ERR_STARTPOS;
  if ((endpos > buflen) || (startpos > endpos)) return MATCH_ERR_ENDPOS;
  len = endpos - startpos;
  /* FUTURE: Reduce the max length of the input (maybe 48 bits?) */
  if (((uint64_t) len) >= (uint64_t) PEXL_INT64_MAX) return MATCH_ERR_INPUT_LEN;
  s = o + startpos;	  /* start char for matching */
  e = s + endpos;	  /* one beyond last char for matching */
  trampoline = make_trampoline(entrypoint);
  assert( trampoline );
  /* Run the matching vm */
  stat = vm(o, s, e, pt, &r, &capture, &num_captures, trampoline);
  package_free(trampoline);
  switch (stat) {
  case MATCH_OK:
    abend = 0; break;
  case MATCH_HALT:
    abend = 1; break;
  default:
    return stat;
  }
  if (stats) stats->match_time = clock() - t0;
  /* 
     We do not free match->data, because it may be reused over
     successive calls to match().  We do "reset" it to "hide" its old
     contents..
  */
  if (!match->data) match->data = buf_new(0);
  if (!match->data) return MATCH_ERR_OUTPUT_MEM;
  buf_reset(match->data);
  if (r == NULL) {
    match->matched = 0;		/* indicate no match */
    match->leftover = len;	/* leftover value is len */
    match->abend = abend;
    return MATCH_OK;
  }
  match->matched = 1;		/* indicate a match */
  bytes = calc_encoding_size(len);
  if (buf_addlstring(match->data, (const char *) &bytes, 1)) return MATCH_ERR_OUTPUT_MEM;
  stat = walk_captures(capture,
		       num_captures,
		       o,
		       encoder,
		       bytes,
		       pt,
		       match->data);
  if (capture != initial_capture) free(capture);
  if (stat != MATCH_OK) return stat;
  if (stats) stats->total_time = clock() - t0;
  match->leftover = e - r;	/* leftover chars, in bytes */
  match->abend = abend;		/* abnormal end, e.g. via Halt instruction */
  return MATCH_OK;
}

Match *match_new() {
  Match *m = malloc(sizeof(Match));
  m->data = NULL;
  return m;
}

/* 
   N.B. The data buffer is NOT FREED HERE because its extent is
   independent of the match struct that points to it.  If it will not
   be reused, free it first using buf_free(match->data).
*/
void match_free(Match *m) {
  if (!m) return;
  free(m);
}

Encoder debug_encoder = { debug_Open, debug_Close }; 
Encoder byte_encoder = { byte_Open, byte_Close }; 
Encoder noop_encoder = { noop_Open, noop_Close }; 

