/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  analyze.c   Analyze expressions and patterns                             */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

/* These functions operate on the pattern->fixed field, which is an Expression. */

/* #define ANALYZER_LOGGING LOGGING */
#define ANALYZER_LOGGING 0

#include "analyze.h"
#include <string.h> 
#include "env.h"
#include "symboltable.h"

#if (ANALYZER_LOGGING)
  #include "print.h"
#endif

/* Predicates */
#define NULLABLE_P      0
#define NOFAIL_P        1

#define TO_BOOL(e) (e != 0)

/* ----------------------------------------------------------------------------- */
/* Misc                                                                          */
/* ----------------------------------------------------------------------------- */

/* Follow ref, extract binding field, return NULL if it is not a Pattern. */
static Pattern *binding_pattern_value (Ref ref) {
  Binding *b = env_get_binding(ref);
  if (!b) return NULL;		/* invalid ref */
  if (b->val.type != Epattern_t) return NULL;
  return (Pattern *) b->val.ptr;
}

static SymbolTableEntry *entry_from_xcall_node (Node *node, const char *caller) {
  XRef xref;
  if (!node->v.pkg || !node->v.pkg->symtab) {
    LOG(caller, "null pkg or pkg->symtab field in TXCall node");
    return NULL;
  }
  set_xref_from_node(xref, node);
  return symboltable_get(xref.pkg->symtab, xref.index);
}

/* ----------------------------------------------------------------------------- */
/* Does a pattern have captures?                                                 */
/* ----------------------------------------------------------------------------- */

/* Returns YES or NO. */
static int hascaptures (Node *node, int check_calls) {
  Pattern *p;
  int result;
  if (!node) {
    LOG("hascaptures", "null node arg");
    return EXP_ERR_NULL;
  }
 tailcall:
  switch (node->tag) {
  case TCapture: 
    return YES;
  case TFunction:		/* TODO: what to return? */
    return YES;			/* conservative */
  case TCall: {
    if (check_calls) {
      p = node->v.pat;
      if (!p) {
	LOGf("hascaptures", "null target of call at node %p", (void *) node);
	return EXP_ERR_INTERNAL;
      }
      if (!pattern_has_flag(p, PATTERN_READY)) return EXP_ERR_NOTCOMPILED;
      return pattern_has_flag(p, PATTERN_CAPTURES);
    } else {
      /* When not following calls, the question that hascaptures answers
	 is whether the tree rooted at 'node' itself has captures. */
      return NO;
    }
  } 
  default: {
    switch (numsiblings[node->tag]) {
    case 1: {			/* return hascaptures(child1(node)); */
      node = child1(node); goto tailcall;
    }
    case 2: {
      result = hascaptures(child1(node), check_calls);
      if (result < 0) return result; /* error */
      if (result == YES) return YES;
      /* else return hascaptures(child2(node)); */
      node = child2(node); goto tailcall;
    }
    default: {
      assert(numsiblings[node->tag] == 0);
      return NO;
    }}}
  } /* switch */
}

int exp_hascaptures (Node *node) {
  return hascaptures(node, YES);
}

int exp_itself_hascaptures (Node *node) {
  return hascaptures(node, NO);
}

/* /\* */
/*  Return either the number of characters (>= 0) to match a pattern, */
/*  EXP_VARLEN if variable, or an error code < 0. */
/* *\/ */
/* static int32_t exp_fixedlenloop (Node *node, int len) { */
/*   int n1, n2; */
/*   Pattern *p; */
/*   SymbolTableEntry *entry; */
/*   if (!node) { */
/*     LOG("exp_fixedlenloop", "null node arg"); */
/*     return EXP_ERR_NULL; */
/*   } */
/*  tailcall: */
/*   if (len > EXP_MAX_FIXEDLEN) return EXP_VARLEN; /\* too long to check it all *\/ */
/*   switch (node->tag) { */
/*   case TChar: case TSet: case TAny: */
/*     return len + 1; */
/*   case TFalse: case TTrue: case TNot: case TAhead: case TBehind: case THalt: */
/*     return len; */
/*   case TFunction:		/\* TODO: what to return *\/ */
/*     return EXP_VARLEN;		/\* conservative *\/ */
/*   case TRep: */
/*     return EXP_VARLEN; */
/*   case TCapture: { */
/*     /\* return fixedlen(child1(node), count); *\/ */
/*     node = child1(node); goto tailcall; */
/*   } */
/*   case TSeq: { */
/*     len = exp_fixedlenloop(child1(node), len); */
/*     if (len < 0) return len;	/\* error or EXP_VARLEN *\/ */
/*     /\* else return fixedlen(child2(node), count, len); *\/ */
/*     node = child2(node); goto tailcall; */
/*   } */
/*   case TChoice: { */
/*     n1 = exp_fixedlenloop(child1(node), len); */
/*     if (n1 < 0) return n1;	/\* error or EXP_VARLEN *\/ */
/*     n2 = exp_fixedlenloop(child2(node), len); */
/*     if (n2 < 0) return n2;	/\* error or EXP_VARLEN *\/ */
/*     if (n1 == n2) return len + n1; */
/*     else return EXP_VARLEN; */
/*   } */
/*   case TCall: {  */
/*     p = node->v.pat; */
/*     if (!p) { */
/*       LOGf("hascaptures", "invalid target of call at node %p", (void *) node); */
/*       return EXP_ERR_INTERNAL; /\* invalid ref? or non-pattern value? *\/ */
/*     } */
/*     if (!pattern_has_flag(p, PATTERN_READY)) return EXP_ERR_NOTCOMPILED; */
/*     if (pattern_has_flag(p, PATTERN_FIXEDLEN)) */
/*       return len + patlen(p); */
/*     else */
/*       return EXP_VARLEN; */
/*   }  */
/*   case TXCall: { */
/*     entry = entry_from_xcall_node(node, "exp_fixedlen"); */
/*     if (!entry) return EXP_ERR_INTERNAL; /\* invalid reference somehow *\/ */
/*     if (!has_flag(metadata_flags(entry->meta), PATTERN_READY)) */
/*       return EXP_ERR_NOTCOMPILED; */
/*     if (has_flag(metadata_flags(entry->meta), PATTERN_FIXEDLEN)) */
/*       return len + metadata_len(entry->meta); */
/*     else */
/*       return EXP_VARLEN; */
/*   }  */
/*   case TOpenCall: */
/*     LOG("exp_fixedlen", "encountered open call"); */
/*     return EXP_ERR_OPENFAIL; */
/*   default: */
/*     ERRORf("exp_fixedlenloop", "unexpected expression type (%d)", node->tag); */
/*     return EXP_ERR_INTERNAL; */
/*   } */
/* } */

/* /\* exp_fixedlen(t) implies that all strings matched by 't' have the same length *\/ */
/* int32_t exp_fixedlen (Node *node) { */
/*   return exp_fixedlenloop(node, 0); */
/* } */

/* ADD_TO works like a clamp, but with a flag as well.

   If we can add amount to *uint_ptr without exceeding the maximum
   value, then we do that and return.

   Otherwise, we set *uint_ptr to the maximum value and set
   *result_ptr to EXP_UNBOUNDED.
   
*/
static void ADD_TO (uint32_t *uint_ptr, uint32_t amount, int *result_ptr) {
  if ((PATTERN_MAXLEN - *uint_ptr) >= amount) {
      *uint_ptr += amount;
      return;
  }
  *uint_ptr = PATTERN_MAXLEN;
  *result_ptr = EXP_UNBOUNDED;
  return;
}    

/*
  Return min and max, where the pattern at 'node' requires at least
  min bytes to succeed, and at most max bytes.

  A nullable pattern has min = 0.
  A fixed-length pattern has min = max.

  Sets result to EXP_UNBOUNDED for unbounded length pattern:
     Implies that max value is meaningless.
     If min value is PATTERN_MAXLEN, it is conservative (actual min may be larger).
  Sets result to EXP_BOUNDED for bounded-length pattern.
     Both min and max are accurate.
  Returns < 0 for error.
*/

static int patlen_loop (Node *node, uint32_t *min, uint32_t *max, int *result) {
  int stat, result_choice;
  uint32_t min1, max1, min2, max2;
  Pattern *p;
  SymbolTableEntry *entry;
  if (!node) {
    LOG("exp_patlen", "null node arg");
    return EXP_ERR_NULL;
  }
 tailcall:
  /* Can stop counting once min passes the maximum allowed value. */
  if ((*min == PATTERN_MAXLEN) && (*result == EXP_UNBOUNDED)) {
    assert( *max == *min );
    return OK;
  }
  switch (node->tag) {
  case TChar: case TSet: case TAny:
    ADD_TO(min, 1, result);
    ADD_TO(max, 1, result);
    return OK;
  case TFalse: case TTrue: case TNot: case TAhead: case TBehind: case THalt:
    return OK;
  case TFunction:
    /* TODO: What to do here? Conservatively, we'll declare unbounded. */
    /* Return a suspicious value, to aid debugging */
    (*max) = 1024; 
    *result = EXP_UNBOUNDED;
    return OK;
  case TRep:
    /* TRep matches 0 or more occurrences of its child node */
    /* Return a suspicious value, to aid debugging */
    (*max) = 2048;
    *result = EXP_UNBOUNDED;
    return OK;
  case TCapture: {
    /* return patlen(child1(node), min, max); */
    node = child1(node); goto tailcall;
  }
  case TSeq: {
    stat = patlen_loop(child1(node), min, max, result);
    if (stat != OK) return stat; /* Error */
    /* else patlen_loop(child2(node), min, max); */
    node = child2(node); goto tailcall;
  }
  case TChoice: {
    /* Check both branches. If either is UNBOUNDED, then result is UNBOUNDED. */
    min1 = 0; max1 = 0;
    result_choice = EXP_BOUNDED;
    stat = patlen_loop(child1(node), &min1, &max1, &result_choice);
    if (stat < 0) return stat;	/* Error */
    if (result_choice == EXP_UNBOUNDED) *result = EXP_UNBOUNDED;
    min2 = 0; max2 = 0;
    result_choice = EXP_BOUNDED;
    stat = patlen_loop(child2(node), &min2, &max2, &result_choice);
    if (stat < 0) return stat;	/* Error */
    if (result_choice == EXP_UNBOUNDED) *result = EXP_UNBOUNDED;
    ADD_TO(min, (min1 < min2) ? min1 : min2, result);
    ADD_TO(max, (max1 > max2) ? max1 : max2, result);
    return OK;
  }
  case TCall: { 
    p = node->v.pat;
    if (!p) {
      LOGf("exp_patlen", "invalid target of call at node %p", (void *) node);
      return EXP_ERR_INTERNAL; /* invalid ref? or non-pattern value? */
    }
    if (!pattern_has_flag(p, PATTERN_READY)) {
      return EXP_ERR_NOTCOMPILED;
    }
    if (pattern_has_flag(p, PATTERN_UNBOUNDED)) *result = EXP_UNBOUNDED;
    ADD_TO(min, pattern_min_length(p), result);
    ADD_TO(max, pattern_max_length(p), result);
    return OK;
  } 
  case TXCall: {
    entry = entry_from_xcall_node(node, "exp_patlen");
    if (!entry) {
      return EXP_ERR_INTERNAL; /* invalid reference somehow */
    }
    if (!has_flag(metadata_flags(entry->meta), PATTERN_READY)) {
      return EXP_ERR_NOTCOMPILED;
    }
    if (has_flag(metadata_flags(entry->meta), PATTERN_UNBOUNDED)) *result = EXP_UNBOUNDED;
    ADD_TO(min, metadata_minlen(entry->meta), result);
    ADD_TO(max, metadata_maxlen(entry->meta), result);
    return OK;
  } 
  case TOpenCall:
    LOG("exp_patlen", "encountered open call");
    return EXP_ERR_OPENFAIL;
  default:
    LOGf("exp_patlen", "unexpected expression type (%d)", node->tag);
    return EXP_ERR_INTERNAL;
  }
}

/* Returns EXP_BOUNDED, EXP_UNBOUNDED, or error < 0 */
int exp_patlen (Node *node, uint32_t *min, uint32_t *max) {
  int stat, result = EXP_BOUNDED;
  (*min) = 0; (*max) = 0;
  stat = patlen_loop(node, min, max, &result);
  return (stat != OK) ? stat : result;
}

/*
   Explanation of checknode from Roberto Ierusalimschy, who wrote the
   original version in lpeg:

    * Checks how a pattern behaves regarding the empty string, in one
    * of two different ways:
    *
    * A pattern is *nullable* if it can match without consuming any
    * character; A pattern is *nofail* if it never fails for any string
    * (including the empty string).
    *
    * The difference is only for predicates and run-time captures; for
    * other patterns, the two properties are equivalent.  (With
    * predicates, &'a' is nullable but not nofail.  Of course, nofail =>
    * nullable.)
    *
    * These functions are all convervative in the following way:
    *    p is nullable => nullable(p)
    *    nofail(p) => p cannot fail

   This checknode returns 0 (false), 1 (true), or a value < 0 (error). 
*/
static int checknode (Node *node, int pred) {
  Pattern *p;
  SymbolTableEntry *entry;
  int res = 0;
  if (!node) {
    LOG("checknode", "null node arg");
    return EXP_ERR_NULL;
  }
 tailcall:
  switch (node->tag) {
  case TChar: case TSet: case TAny: case TFalse: 
    return NO;  /* not nullable, not nofail */
  case TRep: case TTrue: case THalt: /* rosie adds THalt */
    return YES;  /* nullable, no fail */
/* TODO: Are these all the same now? TNot, TBehind, TAhead? */
  case TNot: case TBehind:  /* nullable, but can fail */
    if (pred == NOFAIL_P) return NO;
    else return YES;  /* nullable */
  case TAhead:  /* nullable; nofail iff body is nofail */
    if (pred == NULLABLE_P) return YES;
    /* else return checknode(child1(node), pred); */
    node = child1(node); goto tailcall;
  case TCall: {
    p = node->v.pat;
    if (!p) return EXP_ERR_INTERNAL; /* invalid ref? or non-pattern value? */
    if (!pattern_has_flag(p, PATTERN_READY)) return EXP_ERR_NOTCOMPILED;
    return (pred == NOFAIL_P) ?
      TO_BOOL(pattern_has_flag(p, PATTERN_NOFAIL)) :
      TO_BOOL(pattern_has_flag(p, PATTERN_NULLABLE));
  } 
  case TXCall: {
    entry = entry_from_xcall_node(node, "checknode");
    if (!entry) return EXP_ERR_INTERNAL; /* invalid reference somehow */
    if (!has_flag(metadata_flags(entry->meta), PATTERN_READY))
      return EXP_ERR_NOTCOMPILED;
    return (pred == NOFAIL_P) ?
      TO_BOOL(has_flag(metadata_flags(entry->meta), PATTERN_NOFAIL)) :
      TO_BOOL(has_flag(metadata_flags(entry->meta), PATTERN_NULLABLE));
  } 
  case TFunction:
    return YES;			/* TODO: What should this be? */
  case TSeq:
    res = checknode(child1(node), pred);
    if (res < 0) return res;
    assert( (res == YES) || (res == NO) );
    if (res == NO) return NO;
    /* else return checknode(child2(node), pred); */
    node = child2(node); goto tailcall;
  case TChoice:
    res = checknode(child2(node), pred);
    if (res < 0) return res;
    if (res == YES) return YES;
    assert( res == NO );
    /* else return checknode(child1(node), pred); */
    node = child1(node); goto tailcall;
  case TCapture: 
    /* return checknode(child1(node), pred); */
    node = child1(node); goto tailcall;
  case TOpenCall:
    LOG("checknode", "encountered open call");
    return EXP_ERR_OPENFAIL;
  default:
    LOGf("checknode", "unexpected expression type (%d)", node->tag);
    return EXP_ERR_INTERNAL;;
  }
}

int exp_nofail (Node *node) {
  return checknode(node, NOFAIL_P);
}

/* FUTURE: exp is nullable if (1) it is not TFalse and (2) its min length is 0 */
int exp_nullable (Node *node) {
  return checknode(node, NULLABLE_P);
}

/*
** If 'headfail(node)' true, then 'node' can fail ONLY depending on the
** next character of the subject.
**
** Returns 0, 1, or error < 0.
*/
int exp_headfail (Node *node) {
  int stat;
  Pattern *p;
  SymbolTableEntry *entry;
  if (!node) {
    LOG("exp_headfail", "null node arg");
    return EXP_ERR_NULL;
  }
 tailcall:
  switch (node->tag) {
  case TChar: case TSet: case TAny: case TFalse:
    return YES;
  case TTrue: case TRep: case TNot:
  case TBehind: case THalt: 
    return NO;
  case TCall: {
    p = node->v.pat;
    if (!p) return EXP_ERR_NOTCOMPILED; /* or could be an invalid ref */
    if (!pattern_has_flag(p, PATTERN_READY)) return EXP_ERR_NOTCOMPILED;
    return pattern_has_flag(p, PATTERN_HEADFAIL);
  } 
  case TXCall: {
    entry = entry_from_xcall_node(node, "headfail");
    if (!entry) return EXP_ERR_INTERNAL; /* invalid reference somehow */
    if (!has_flag(metadata_flags(entry->meta), PATTERN_READY))
      return EXP_ERR_NOTCOMPILED;
    return has_flag(metadata_flags(entry->meta), PATTERN_HEADFAIL);
  } 
  case TCapture: case TAhead:
    node = child1(node); goto tailcall;  /* return headfail(child1(node)); */
  case TSeq:
    /* A seq is headfail only when child1 is headfail and child2 is
       nofail, because even if child1 succeeds based on the next char,
       the seq itself can still fail if child2 fails.
    */
    stat = exp_nofail(child2(node));
    if (stat < 0) return stat;	/* error */
    if (stat == NO) return NO;
    /* else return headfail(child1(node)); */
    node = child1(node); goto tailcall;
  case TChoice:
    stat = exp_headfail(child1(node));
    if (stat < 0) return stat;
    if (stat == NO) return NO;
    /* else return headfail(child2(node)); */
    node = child2(node); goto tailcall;
  case TFunction:	/* TODO: Could we be more intelligent here? */
    return NO;		/* conservative */
  case TOpenCall:
    LOG("headfail", "encountered open call");
    return EXP_ERR_OPENFAIL;
  default:
    LOGf("exp_headfail", "unexpected expression type (%d)", node->tag);
    return EXP_ERR_INTERNAL;
  }
}

/*
** Computes the 'first set' of a pattern.
** The result is a conservative aproximation:
**   match p ax -> x (for some x) ==> a belongs to first(p)
** or
**   a not in first(p) ==> match p ax -> fail (for all x)
**
** The set 'follow' is the first set of what follows the
** pattern (full set if nothing follows it).
**
** The function returns 0 when this resulting set can be used for
** test instructions that avoid the pattern altogether.
** A non-zero return can happen for two reasons:
** 1) match p '' -> ''            ==> return has bit 0 set
** (tests cannot be used because they would always fail for an empty input);
** 2) there is a runtime capture ==> return has bit 1 set
** (optimizations should not bypass runtime captures).
*/
int exp_getfirst (Node *node, const Charset *follow, Charset *firstset) {
  int e, e1, e2;
  Charset csaux;
  Pattern *p;
  SymbolTableEntry *entry;
  if (!node || !follow || !firstset) {
    LOG("exp_getfirst", "null argument (node or followset or firstset)");
    return EXP_ERR_NULL;
  }
 tailcall:
  switch (node->tag) {
   case TChar: case TSet: case TAny: {
     to_charset(node, firstset);
     return 0;
   }
   case TTrue: {
     loopset(i, firstset->cs[i] = follow->cs[i]);
     return 1; /* accepts the empty string */
   }
   case TFalse: {
     loopset(i, firstset->cs[i] = 0);
     return 0;
   }
   case THalt: { /* similar to TFalse (right?) */
     loopset(i, firstset->cs[i] = 0);
     return 0;
   }
   case TChoice: {
     e1 = exp_getfirst(child1(node), follow, firstset);
     if (e1 < 0) return e1;
     e2 = exp_getfirst(child2(node), follow, &csaux);
     if (e2 < 0) return e2;
     loopset(i, firstset->cs[i] |= csaux.cs[i]);
     return e1 | e2;
   }
   case TSeq: {
     if (!exp_nullable(child1(node))) {
       /* When p1 is not nullable, p2 has nothing to contribute;
	  return getfirst(child1(node), fullset, firstset); */
       node = child1(node); follow = fullset; goto tailcall;
      }
     else { /* FIRST(p1 p2, fl) = FIRST(p1, FIRST(p2, fl)) */
       e2 = exp_getfirst(child2(node), follow, &csaux);
       if (e2 < 0) return e2;
       e1 = exp_getfirst(child1(node), &csaux, firstset);
       if (e1 < 0) return e1;
       if (e1 == 0) return 0;  /* 'e1' ensures that first can be used */
       else if ((e1 | e2) & 2)  /* one of the children has a runtime capture? */
 	return 2;  /* pattern has a runtime capture */
       else return e2;  /* else depends on 'e2' */
      }
   }
   case TRep: {
     e = exp_getfirst(child1(node), follow, firstset);
     if (e < 0) return e;	/* error */
     loopset(i, firstset->cs[i] |= follow->cs[i]);
     return 1;  /* accept the empty string */
   }
   case TCapture: {
     /* return getfirst(child1(node), follow, firstset); */
     node = child1(node); goto tailcall;
   }
   case TFunction: {	   /* function invalidates any follow info. */
     e = exp_getfirst(child1(node), fullset, firstset); 
     if (e < 0) return e;	/* error */
     if (e) return 2;		/* function is not "protected"? */ 
     else return 0; /* pattern inside capture ensures first can be used */ 
   }
   case TCall: {
     /* Use the metadata about the target of the call. */
     p = node->v.pat;
     if (!p) {
       LOGf("exp_getfirst", "null call target in node %p", (void *) node);
       return EXP_ERR_INTERNAL;
     }
     loopset(i, firstset->cs[i] = metadata_firstset_ptr(p->meta)->cs[i]);
     return pattern_has_flag(p, PATTERN_NULLABLE);
   }
  case TXCall: {
    entry = entry_from_xcall_node(node, "exp_getfirst");
    if (!entry) return EXP_ERR_INTERNAL; /* invalid reference somehow */
    loopset(i, firstset->cs[i] = metadata_firstset_ptr(entry->meta)->cs[i]);
    return has_flag(metadata_flags(entry->meta), PATTERN_NULLABLE);
  } 
   case TAhead: {
     e = exp_getfirst(child1(node), follow, firstset);
     if (e < 0) return e;
     loopset(i, firstset->cs[i] &= follow->cs[i]);
     return e;
   }
   case TNot: {
     if (to_charset(child1(node), firstset)) {
       cs_complement(firstset);
       return 1;
      }
     __attribute__ ((fallthrough));
   }
   case TBehind: {  /* instruction gives no new information */
     /* call 'getfirst' only to check for runtime captures */
     e = exp_getfirst(child1(node), follow, firstset);
     if (e < 0) return e;
     loopset(i, firstset->cs[i] = follow->cs[i]);  /* uses follow */
     return e | 1;  /* always can accept the empty string */
   }
  case TOpenCall: {
    LOG("getfirst", "encountered open call");
    return EXP_ERR_OPENFAIL;
  }
  default:
    LOGf("exp_getfirst", "unexpected expression type (%d)", node->tag);
    return EXP_ERR_INTERNAL;
  }
}

/*
** Check whether the code generation for the given node can benefit
** from a follow set (to avoid computing the follow set when it is
** not needed)
*/
int exp_needfollow (Node *node) {
  Pattern *p;
  SymbolTableEntry *entry;
  if (!node) {
    LOG("exp_needfollow", "null node arg");
    return EXP_ERR_NULL;
  }
 tailcall:
  switch (node->tag) {
  case TChar: case TSet: case TAny:
  case TFalse: case TTrue: case TAhead: case TNot: case THalt:
  case TFunction: case TBehind: 
    /* Returning 0 is conservative for TCall, TBehind */
    return NO;
  case TCall: {
    /* Use the metadata about the target of the call. */
    p = node->v.pat;
    if (!p) return EXP_ERR_NOTCOMPILED; /* or could be an invalid ref */
    if (!pattern_has_flag(p, PATTERN_READY)) return EXP_ERR_NOTCOMPILED;
    return pattern_has_flag(p, PATTERN_NEEDFOLLOW);
  }
  case TXCall: {
    entry = entry_from_xcall_node(node, "needfollow");
    if (!entry) return EXP_ERR_INTERNAL; /* invalid reference somehow */
    if (!has_flag(metadata_flags(entry->meta), PATTERN_READY))
      return EXP_ERR_NOTCOMPILED;
    return has_flag(metadata_flags(entry->meta), PATTERN_NEEDFOLLOW);
  } 
  case TChoice: case TRep:
    return YES;
  case TCapture:
    node = child1(node); goto tailcall;
  case TSeq:
    node = child2(node); goto tailcall;
  case TOpenCall:
    LOG("needfollow", "encountered open call");
    return EXP_ERR_OPENFAIL;
  default: {
    LOGf("exp_needfollow", "unexpected expression type (%d)", node->tag);
    return EXP_ERR_INTERNAL;
  }}
}

/* Return YES if target is in the SCC [start, finish) */
static int in_SCC (CompState *cs, Pattern *target, Index start, Index finish) {
  Index i;
  for (i = start; i < finish; i++)
    if (cs->g->deplists[cs->g->order->elements[i].vnum][0] == target) return YES;
  return NO;
}

/* A leftcall is the first call in a sequence, where all the preceding
   elements of the sequence are nullable.  A choice may have two left
   calls, one for each child.
*/
static int follow_leftcalls (CompState *cs, Index start, Index finish,
			     Node *node, int *nullable, Index *npassed) {
  int stat, child_nullable;
  Pattern *target;
  Index nrules = finish - start;
  assert( nrules > 0 );
  assert( cs );
  assert( npassed );
  assert( nullable );
 tailcall:
  assert( node );
  switch (node->tag) {
  case TChar: case TSet: case TAny: case TFalse: case THalt: 
    /* Answer is in *nullable already */
    return OK;
  case TTrue: 
    *nullable = YES;
    return OK;
  case TBehind: case TNot: case TAhead: case TRep: case TCapture: 
    /* return follow_leftcalls(child1(node), YES); */
    *nullable = YES; node = child1(node); goto tailcall;
  case TSeq:
    /* If first child has a left call, follow it.  Else, if first
       child is nullable, then check second child. */
    child_nullable = 0;
/*     printf("    Following left calls for SEQ child 1\n"); */
    stat = follow_leftcalls(cs, start, finish, child1(node), &child_nullable, npassed);
/*     printf("    SEQ Result: stat = %d, child_nullable = %d\n", stat, child_nullable); */
    if (stat < 0) return stat;
    if (!child_nullable) {
      *nullable = NO;
      return OK;
    }
    /* Else first child is nullable, so 'node' is nullable IFF child2 is:
       return leftcall(child2(node), nullable); 
    */
/*     printf("    Following left calls for SEQ child 2\n"); */
    node = child2(node); goto tailcall;
  case TChoice:
    /* Must always check both children.  Either child1 or child2 may
       have a left call, and we need to follow both of them.
    */
/*     printf("    Following left calls for CHOICE child 1\n"); */
    stat = follow_leftcalls(cs, start, finish, child1(node), nullable, npassed);
/*     printf("    CHOICE Result: stat = %d, nullable = %d\n", stat, *nullable); */
    if (stat < 0) return stat;
    /* 'nullable' now indicates whether child1 is nullable. */ 
    /* Check child2: return follow_leftcalls(child2(node), nullable); */ 
/*     printf("    Following left calls for CHOICE child 2\n"); */
    node = child2(node); goto tailcall; 
  case TCall:
    target = node->v.pat;
/*     printf("Found call to pattern %p\n", (void *) target); */
    if (!target) {
      LOG("follow_leftcalls", "call target is null");
      return EXP_ERR_INTERNAL;
    }
    if (!target->env) {
      LOG("follow_leftcalls", "call target has null environment");
      return EXP_ERR_INTERNAL;
    }
    /* If target of call is not in this package, it must be compiled
       already.  Use its nullable flag.  (We could add a separate test
       for this, even though the SCC test subsumes it, because a
       package identity test is fast -- just a pointer comparison.)

       Else target is in this package but not in the SCC.  The
       topological sort ensures that this call target will have been
       compiled before this SCC, so we can use its nullable flag. 
    */
    if (!in_SCC(cs, target, start, finish)) {
/*       printf("Call target %p is NOT in this SCC [%d, %d) \n", (void *) target, start, finish); */
/*       printf("==> READY for target is %d\n", pattern_has_flag(target, PATTERN_READY)); */
      if (!pattern_has_flag(target, PATTERN_READY)) return EXP_ERR_NOTCOMPILED;
      *nullable = pattern_has_flag(target, PATTERN_NULLABLE);
      return OK;
    } 
    /* Else target is within this SCC, so count it and follow it. */
    (*npassed)++;
/*     printf("Call target %p is in this SCC [%d, %d).  Now npassed is %d\n", (void *) target, */
/* 	   start, finish, *npassed); */
/*     printf("nrules is %d\n", nrules); */

    /* But first check that we are not going in circles. */
    if (*npassed >= nrules) return EXP_ERR_LEFT_RECURSIVE;
/*     printf("Following call target\n"); */
    node = target->fixed->node; goto tailcall;
  case TOpenCall:
    LOG("follow_leftcalls", "encountered open call");
    return EXP_ERR_OPENFAIL;
  case TFunction:     /* ??? TODO TODO TODO TODO TODO ??? */ 
  default:
    LOGf("follow_leftcalls", "unexpected node type: %d", node->tag);
    return EXP_ERR_INTERNAL;
  }
}

static int exp_nullable_rec (CompState *cs, Pattern *p, Index start, Index finish) {
  int stat, nullable;
  Index npassed;
  npassed = 0;
  nullable = NO;
  /* Follows all left calls.  Sets 'nullable' and updates the 'npassed' count. */
  /* printf("* Following left calls for order index %d which is vertex %d\n", i, vnum); */
  stat = follow_leftcalls(cs, start, finish, p->fixed->node, &nullable, &npassed);
  /* printf("  * Result: stat = %d, cs->err_index = %d which is vertex %d which is pattern %p\n", */
  /* stat, i, cs->g->order->elements[i].vnum, */
  /* (void *) cs->g->deplists[cs->g->order->elements[i].vnum][0]); */
  if (stat < 0) {
    return stat;		/* Could be any error, including LEFT_RECURSIVE */
  }
  return nullable;
}

/* ----------------------------------------------------------------------------- */
/* Setting pattern metadata                                                      */
/* ----------------------------------------------------------------------------- */

/* Set metadata for a non-recursive pattern.  Dependencies are
   EXPECTED TO BE COMPILED ALREADY.  If they are not compiled (or not
   a pattern), then set_nonrecursive_metadata will return EXP_ERR_NOTCOMPILED.
*/
static int set_nonrecursive_metadata1 (Pattern *p) {
  Expression *tree = p->fixed;
  int result;
  uint32_t min, max, flags = 0;

  assert( tree );

#if (ANALYZER_LOGGING)
  fprintf(stderr, "setting metadata for pattern %p\n", (void *) p);
#endif

  result = exp_hascaptures(tree->node);
  if (result < 0) return result;
  set_flag(flags, PATTERN_CAPTURES, result);
#if (ANALYZER_LOGGING)
  fprintf(stderr, "metadata: hascaptures %s\n", result ? "YES" : "NO");  
#endif

  result = exp_nullable(tree->node);
  if (result < 0) return result;
  set_flag(flags, PATTERN_NULLABLE, result);
#if (ANALYZER_LOGGING)
  fprintf(stderr, "metadata: nullable %s\n", result ? "YES" : "NO");
#endif

  result = exp_nofail(tree->node);
  if (result < 0) return result;
  set_flag(flags, PATTERN_NOFAIL, result);
#if (ANALYZER_LOGGING)
  fprintf(stderr, "metadata: nofail %s\n", result ? "YES" : "NO");
#endif

  result = exp_headfail(tree->node);
  if (result < 0) return result;
  set_flag(flags, PATTERN_HEADFAIL, result);
#if (ANALYZER_LOGGING) 
  fprintf(stderr, "metadata: headfail %s\n", result ? "YES" : "NO"); 
#endif
  
  result = exp_getfirst(tree->node, fullset, metadata_firstset_ptr(p->meta));
  if (result < 0) return result;
#if (ANALYZER_LOGGING)
  fprintf(stderr, "metadata: p->meta.firstset is:  ");		
  fprint_charset(stderr, metadata_firstset_ptr(p->meta)->cs);	
  fprintf(stderr, "\n");
#endif

  result = exp_needfollow(tree->node);
  if (result < 0) return result; /* Error */
  set_flag(flags, PATTERN_NEEDFOLLOW, result);

  result = exp_patlen(tree->node, &min, &max);
  if (result < 0) return result; /* Error */
  if (result == EXP_UNBOUNDED) {
    set_flag(flags, PATTERN_UNBOUNDED, YES);
#if (ANALYZER_LOGGING) 
    fprintf(stderr, "metadata: unbounded YES\n"); 
#endif
  }
  set_metadata_minlen(p->meta, min);
  set_metadata_maxlen(p->meta, max);

  set_flag(flags, PATTERN_READY, YES);
  set_patflags(p, flags);
#if (ANALYZER_LOGGING)
  fprintf(stderr, "metadata: minlen = %u\n", min);			
  fprintf(stderr, "metadata: maxlen = %u\n", max);			
  fprintf(stderr, "metadata: READY = %s\n",				
	  pattern_has_flag(p, PATTERN_READY) ? "YES" : "NO");		
#endif
  return OK;
}

/* 
   If any of the recursive set has captures, then they all do
   (conservative approx).  Also, mark the recursive patterns as READY. 
   N.B. Use this ONLY on bindings that are mutually/self recursive.
*/
static int set_recursive_metadata (CompState *cs, Index start, Index finish) {
  int stat;
  Index i, vnum;
  Pattern *p;
  uint32_t flags;
  int hascaps = NO;
  assert( cs->g );
  assert( cs->g->order );
  assert( start >= 0 );
  assert( finish <= cs->g->next );
  assert( start <= finish );

  /* Within this set of patterns, do any have captures? */
  /* While we are iterating, we also set the nullable flag for each. */
  for (i = start; i < finish; i++) {
    assert( i < cs->g->order->top );
    vnum = cs->g->order->elements[i].vnum;
    assert( vnum >= 0 );
    assert( vnum < cs->g->order->top );
    p = cs->g->deplists[vnum][0];
    if (!p) {
      LOGf("set_recursive_metadata", "null pattern (vertex in graph) at order index %d", i);
      cs->err_index = i;
      return EXP_ERR_INTERNAL;
    }
    stat = exp_itself_hascaptures(p->fixed->node); 
    if (stat < 0) {
      cs->err_index = i;
      return stat;	/* error */
    }
    hascaps = hascaps | stat;
  } /* for */

  /* Set the flags for captures (based on previous calculation),
     nofail, and fixedlen.

     FUTURE: Can we make a better approximation here?  E.g. in many
     cases we can compute HEADFAIL, right?
  */
  for (i = start; i < finish; i++) {
    vnum = cs->g->order->elements[i].vnum;
    assert( vnum >= 0 );
    assert( vnum < cs->g->order->top );
    p = cs->g->deplists[vnum][0];
    if (!p) {
      LOGf("set_recursive_metadata", "null pattern (vertex in graph) at position %d", i);
      cs->err_index = i;
      return EXP_ERR_INTERNAL;
    }
    flags = patflags(p);
    set_flag(flags, PATTERN_CAPTURES, hascaps); 

    /* nofail should be set to false to be conservative */
    set_flag(flags, PATTERN_NOFAIL, NO);
    /* unbounded should be set to true (conservative) */
    set_flag(flags, PATTERN_UNBOUNDED, YES);
    /* headfail should be set to false (conservative) */
    set_flag(flags, PATTERN_HEADFAIL, NO);
    /* needfollow is NO only to avoid extra work; conservative answer is YES */
    set_flag(flags, PATTERN_NEEDFOLLOW, YES);
    set_patflags(p, flags);

    set_pattern_minlen(p, 0);	/* conservative */
    set_pattern_maxlen(p, 4096); /* meaningless, so set a conspicuous value */

    /* special nullable check that detects left recursion */
    stat = exp_nullable_rec(cs, p, start, finish);
    if (stat < 0) {
      cs->err_index = i;
      return stat;	/* error, possibly a LEFT_RECURSIVE error */
    }
    set_flag(flags, PATTERN_NULLABLE, stat);

    stat = exp_getfirst(p->fixed->node, fullset, metadata_firstset_ptr(p->meta));
    if (stat < 0) return stat;
/*     printf("*** In set_recursive_metadata, p->meta.firstset is:  "); */
/*     print_charset(p->meta.firstset.cs); printf("\n"); */

  } /* for */

  /* TODO: Where should checklookbehind() go? */
  /* Now we have enough ep metadata to verify that any lookbehind
     operation meets its necessary conditions.
  */
  for (i = start; i < finish; i++) {
    vnum = cs->g->order->elements[i].vnum;
    assert( vnum >= 0 );
    assert( vnum < cs->g->order->top );
    p = cs->g->deplists[vnum][0];
    if (!p) {
      LOGf("set_recursive_metadata", "null pattern (vertex in graph) at position %d", i);
      cs->err_index = i;
      return EXP_ERR_INTERNAL;
    }
/*     stat = checklookbehind(p->fixed->node); */
/*     if (stat < 0) { */
/*       cs->err_index = i; */
/*       return stat;	/\* error *\/ */
/*     } */
    /* And, set the pattern to READY, i.e. metadata is now set. */
    flags = patflags(p);
    set_flag(flags, PATTERN_READY, YES);
    set_patflags(p, flags);
  } /* for */
  return OK;
}

/* ----------------------------------------------------------------------------- */
/* Enforce scope rules, fix open calls, correct associativity                    */
/* ----------------------------------------------------------------------------- */

/* Almost unchanged from lpeg:
 *
 *  Transform left associative constructions into right
 *  associative ones, for sequence and choice; that is:
 *  (t11 + t12) + t2  =>  t11 + (t12 + t2)
 *  (t11 * t12) * t2  =>  t11 * (t12 * t2)
 *  That is, Op (Op t11 t12) t2 => Op t11 (Op t12 t2).
*/
static void correctassociativity (Node *node) {
  Node *t1 = child1(node);
  assert(node->tag == TChoice || node->tag == TSeq);
  while (t1->tag == node->tag) {
    int n1size = node->u.ps - 1;  /* t1 == Op t11 t12 */
    int n11size = t1->u.ps - 1;
    int n12size = n1size - n11size - 1;
    memmove(child1(node), child1(t1), n11size * sizeof(Node)); /* move t11 */
    node->u.ps = n11size + 1;
    child2(node)->tag = node->tag;
    child2(node)->u.ps = n12size + 1;
  }
}

/* Walk expression tree rooted at 'node' to free the 'fixed' copy of
   the expression tree. 
   N.B. This procedure also resets PATTERN_READY and the entrypoint field.
*/
static void free_all_fixed (Node *node) {
  Ref ref;
  Pattern *target;
 tailcall:
  switch (node->tag) {
  case TOpenCall: {
    set_ref_from_node(ref, node);
    target = binding_pattern_value(ref);
    if (!target) return;	/* ignore error */
    if (!target->fixed) return;	/* already processed */
    LOGf("free_all_fixed", "recompiling call target pattern %p", (void *) target);
    pattern_set_flag(target, PATTERN_READY, NO);
    target->entrypoint = -1;	/* NOT SET */
    pexle_free(target->fixed);
    target->fixed = NULL;
    node = target->tree->node;
    goto tailcall;
  }
  default:
    switch (numsiblings[node->tag]) {
    case 1: 
      node = child1(node); goto tailcall;
    case 2:
      free_all_fixed(child1(node));
      node = child2(node); goto tailcall;
    default: 
      assert(numsiblings[node->tag] == 0);
      break;
    }}
  return;
}

/* Create a copy of the exp structure that will be used to fix open calls */
static void refresh_all_fixed (Node *node) {
  Ref ref;
  Pattern *target;
 tailcall:
  switch (node->tag) {
  case TOpenCall: {
    set_ref_from_node(ref, node);
    target = binding_pattern_value(ref);
    if (!target) return;	/* ignore error */
    if (target->fixed) return;	/* processed this pattern already */
    target->fixed = pexle_copy(target->tree);
    assert( target->fixed );
    node = target->tree->node;
    goto tailcall;
  }
  default:
    switch (numsiblings[node->tag]) {
    case 1: 
      node = child1(node); goto tailcall;
    case 2:
      refresh_all_fixed(child1(node));
      node = child2(node); goto tailcall;
    default: 
      assert(numsiblings[node->tag] == 0);
      break;
    }}
  return;
}

/* Recursively (through dependencies) free all of the pat->fixed
   expressions, and then re-create them by copying each pat->tree. */
void reset_compilation_state (Pattern *pat) {
  pat->entrypoint = -1;
  if (pat->fixed) {
    LOGf("reset_compilation_state", "recompiling call target pattern %p", (void *) pat);
    pexle_free(pat->fixed);
    pat->fixed = NULL;
  }
  free_all_fixed(pat->tree->node);
  refresh_all_fixed(pat->tree->node);
  pattern_set_flag(pat, PATTERN_READY, NO);
  if (!pat->fixed) {
    pat->fixed = pexle_copy(pat->tree);
    assert( pat->fixed );
  }
}

/*
   Verify that the target of every call (encoded a 'ref') is a
   pattern, and that it is in scope.  While we are walking the
   expression tree, we also correct the associativity of associative
   constructions (making them right associative).
*/
/* 
   FUTURE: Factor this into separate transformations: correct
   associativity and fix open calls.  Can still do them in the same
   tree traversal if we want, but they are independent
   transformations.
*/
static int fixnode (Env *compilation_env, Node *node) {    
  Ref ref;
  int stat;
  Env *env, *target_env;
  Pattern *target;
 tailcall:
  switch (node->tag) {
  case TOpenCall: {
    set_ref_from_node(ref, node);
    /* Ensure that call target is a pattern */
    target = binding_pattern_value(ref);
    if (!target) {
      if (ref_not_found(ref))
	LOGf("fixnode", "target of call in node %p is invalid reference", (void *) node);
      else
	LOGf("fixnode", "target of call in node %p is not a pattern", (void *) node);
      return EXP_ERR_NO_PATTERN;
    }
    /* Target's env field should be set already  */
    target_env = target->env;
/*     printf("** fixnode: target of call at node %p is %p, target->env is %p\n", */
/* 	   (void *) node, (void *) target, (void *) target_env); */
    if (!target_env) {
      LOG("fixnode", "call target has NULL environment field");
      return EXP_ERR_INTERNAL;
    }
    /* Check that target is in scope */
    env = compilation_env;
    while (env != target_env) {
/*    printf("** fixnode: env is %p\n", (void *) env); */
      env = env->parent;
      if (!env) break;	/* FUTURE: look in prelude */
    }
    if (!env) return EXP_ERR_SCOPE;
    /* Fix TOpenCall => TCall */
    node->tag = TCall;
    node->v.pat = target;
    /* fixnode(target) */
    assert( target->fixed );
    node = target->fixed->node;
    goto tailcall;
  }
  case TSeq: case TChoice: {
    correctassociativity(node);
    /* fallthrough */
  }}
  switch (numsiblings[node->tag]) {
  case 1: 
    node = child1(node); goto tailcall;
  case 2:
    stat = fixnode(compilation_env, child1(node));
    if (stat < 0) return stat;
    node = child2(node); goto tailcall;
  default: {
    assert(numsiblings[node->tag] == 0);
    break;
  }}
  return OK;
}

/* Make final tests and adjustments to all expression trees that we
   need to compile.  Start at cs->pat, fix each TOpenCall node, and
   then recursively follow the resulting TCall node to ensure the call
   targets are all fixed.

   For convenience, we are populating pattern->fixed here, but
   probably we should factor this out and do it somewhere else?

   FUTURE: This computes reachability, so we could build the
   dependency graph at the same time.

*/
int fix_open_calls (CompState *cs) {
  if (!cs) {
    LOG("fix_open_calls", "null compstate arg");
    return EXP_ERR_NULL; 
  }
  if (!cs->pat) {
    LOG("fix_open_calls", "compstate has null pattern field");
    return EXP_ERR_INTERNAL; 
  }
  if (!cs->pat->env) {
    LOG("fix_open_calls", "pattern in compstate has null environment field");
    return EXP_ERR_INTERNAL; 
  }
  if (!cs->pat->fixed) {
    LOG("fix_open_calls", "pattern in compstate has null fixed field XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    return EXP_ERR_INTERNAL; 
  }
  return fixnode(cs->pat->env, cs->pat->fixed->node);
}

/* ----------------------------------------------------------------------------- */
/* Set metadata                                                                  */
/* ----------------------------------------------------------------------------- */

static int set_metadata_component (CompState *cs, Index start, Index finish, Index cnum) {
  int stat;
  void *vptr;
  SCC *order = cs->g->order;
  assert( (finish - start) >= 0 );
  if (cnum > 0) {
#if (ANALYZER_LOGGING)
    fprintf(stderr, "Setting recursive metadata for order indices %d..%d\n", start, finish-1);
#endif
    stat = set_recursive_metadata(cs, start, finish);
    if (stat < 0) return stat;
  } else {
#if (ANALYZER_LOGGING)
    fprintf(stderr, "Setting nonrecursive metadata for order index %d\n", start);
#endif
    assert( order->elements[start].vnum >= 0 );
    assert( ((size_t) order->elements[start].vnum) < ((size_t) cs->g->next) );
    vptr = (void *) *cs->g->deplists[order->elements[start].vnum];
#if (ANALYZER_LOGGING) 
    fprintf(stderr, "  Vertex %d  %p\n", order->elements[start].vnum, vptr);
#endif
    stat = set_nonrecursive_metadata1((Pattern *) vptr);
    if (stat < 0) return stat;
  }
  return OK;
}

int set_metadata (CompState *cs) {
  int stat;
  SCC *order;
  Index i, prev_i;
  Index prev_cnum;
  if (!cs) {
    LOG("set_metadata", "null compstate arg");
    return EXP_ERR_NULL; 
  }
  if (!cs->pat) {
    LOG("set_metadata", "compstate has null pattern field");
    return EXP_ERR_INTERNAL; 
  }
  if (!cs->pat->env) {
    LOG("set_metadata", "pattern in compstate has null environment field");
    return EXP_ERR_INTERNAL; 
  }
  if (!cs->g) {
    LOG("set_metadata", "compstate has null cfgraph field");
    return EXP_ERR_NULL; 
  }
  if (!cs->g->order) {
    LOG("set_metadata", "compstate has null order field");
    return EXP_ERR_NULL; 
  }
  order = cs->g->order;
  assert( ((size_t) order->top) == ((size_t) cs->g->next) );
  if (order->top == 0) return OK; /* empty graph */
  prev_i = 0;
  prev_cnum = order->elements[0].cnum;
  for (i = 1; i < order->top; i++) {
    if (order->elements[i].cnum != prev_cnum) {
      /* Process this component from [prev_i, i) */
      stat = set_metadata_component(cs, prev_i, i, prev_cnum);
      if (stat < 0) return stat;
      /* Set up for next component */
      prev_cnum = order->elements[i].cnum;
      prev_i = i;
    }
  } /* for */
  stat = set_metadata_component(cs, prev_i, i, prev_cnum);
  return stat;
}

