/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  buf.c   Growable buffers to hold binary data                             */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

/*
 * A new buffer comes with a statically allocated data block.  When
 * that initial block overflows, a larger block is malloc'd and the
 * current data is copied there.  If the new dynamic block overflows,
 * it is realloc'd at double its size (or more, if needed).
 *
 * A new buffer can also be made by wrapping an existing block of data
 * to make it behave like a buffer. A wrapped buffer is treated as
 * immutable: its data will not be freed when the buffer itself is
 * freed with 'buf_free', and the various 'add' functions will refuse
 * to modify the data.  Of course, this being C, the caller must
 * ensure that the data remains available for the life of the buffer.
 *
 * The buf_add...() functions add items to the end of the buffer,
 * extending the buffer size as needed.  Numbers are encoded and
 * stored in a fixed number of bytes, and strings are copied into the
 * buffer verbatim.  (Strings are represented by a pointer and a
 * length.)
 *
 * The buf_read...() functions decode numbers of various sizes from
 * the buffer.  They take a Buf_cursor argument, which is a pointer
 * in to the buffer's data.  A new cursor value, pointing to just
 * beyond the decoded value, is returned.  A cursor value of NULL is
 * interpreted as the start of the buffer.
 *
 *
 * Another way to use a buffer is to assure it has sufficient room
 * with buf_prepsize(), then add to it with UNSAFE procedures which do
 * not check whether there is space available.
 *
 */

/* 

   buf_add...() encodes values and writes them at the end of the
     buffer, extending it as needed.  Returns OK (0) or ERR < 0.
     Errors include trying to add to an immutable buffer, out of
     memory, and value too large for encoding size (for 'varsize'
     functions).

   buf_peek...() takes a pointer argument which must be a pointer into
     this buffer or NULL.  NULL means "from the start of the data".
     Returns a valid cursor pointing to the start of the peeked-at
     data, or NULL if there are insufficient bytes.

   buf_read...() takes a pointer like the 'peek' functions, but
     returns cursor that has been advanced just beyond the data read
     (or NULL on error).  Every read consumes at least 1 byte.  Error
     when insufficient bytes at pointer to decode the value.
 */

#include "buf.h"
#include <assert.h>
#include <stdlib.h>
#include <string.h>

/*  Dynamically allocate new storage, returning NULL if error. */
static Buffer *buf_resize (Buffer *b, size_t newsize) {
  char *temp;
  if (buf_isimmutable(b)) return NULL;
  if (buf_isdynamic(b)) {
    temp = realloc(b->data, newsize);
  } else {
    /* not dynamic means that data is in initb (static allocation) */
    temp = malloc(newsize);
    /* copy original content, or as much as will fit if newsize is smaller */ 
    if (temp)
      memcpy(temp, b->initb, sizeof(char) * (newsize > b->next ? b->next : newsize));
  }
  if (temp == NULL && newsize > 0) {	     /* allocation error? */
    if buf_isdynamic(b) free(b->data);
    return NULL;
  }
  b->data = temp;
  b->size = newsize;
  return b;
}

/* 'buf_new' creates a new, empty buffer. */
Buffer *buf_new (size_t minimum_size) {
  Buffer *buf = (Buffer *)malloc(sizeof(Buffer));
  buf->initb = buf->initialbuff;   /* the extra pointer to initialbuff enables buf_wrap */
  buf->data = buf->initb;          /* intially, data storage is statically allocated in initb  */
  buf->next = 0;
  buf->size = BUF_INITIAL_SIZE; /* size of initb */
  if (minimum_size > BUF_INITIAL_SIZE)
    buf = buf_resize(buf, minimum_size);
  return buf;
}

/* 
   'buf_new_from' makes a new buffer, then copies 'size' bytes from
   'data' into it. 
*/
Buffer *buf_new_from (char *data, size_t size) {
  Buffer *buf = buf_new(size);
  if (buf_addlstring(buf, (char *) data, size)) return NULL;
  return buf;
}

/* 
   We suppress the warning because buf_new_wrap produces an immutable
   buffer.  It cannot be extended, and we do not provide operations to
   modify the contents of a buffer (only to add to it).
*/
#pragma GCC diagnostic push  /* GCC 4.6 and up */
#pragma GCC diagnostic ignored "-Wcast-qual"
/* 
   'buf_new_wrap' makes a new immutable buffer containing the 'data'
   pointer and 'size' value.

   buf_new_wrap is intended to be a time saver.  It is expensive in
   memory, costing the unused BUF_INITIAL_SIZE.  But it does not
   copy the data -- it only points to it.  And the buf_ functions
   treat the data as immutable.
*/
Buffer *buf_new_wrap (const char *data, size_t size) {
  Buffer *buf = buf_new(0);
  buf->data = (char *) data; /* Without the pragma, this causes a warning. */
  buf->next = size;
  buf->size = size;
  buf->initb = NULL;         /* This marks a buffer that cannot be extended */
  return buf;
}
#pragma GCC diagnostic pop

/* 
   Return a cursor pointing to first unused byte, assuring at least
   'additional' bytes are available.  Returns NULL for error.
*/
Buf_cursor buf_prepsize (Buffer *b, size_t additional) {
  size_t newsize;
  if (buf_isimmutable(b)) return NULL;
  if ((b->size - b->next) < additional) {
    /* double buffer size */
    newsize = b->size * 2;
    /* still not big enough? increase buffer size just enough to fit 'additional' bytes */
    if (newsize - b->next < additional) newsize = b->next + additional;
    if (!buf_resize(b, newsize)) return NULL;
  }
  return &b->data[b->next];
}

void buf_free (Buffer *b) {
  if (buf_isdynamic(b) && !buf_isimmutable(b)) { 
    free(b->data);
  } 
  free(b);
}

/* Use this before reusing a buffer. */
void buf_reset (Buffer *b) {
  b->next = 0;
}

/* ----------------------------------------------------------------------------- */
/* ADD FUNCTIONS                                                                 */
/* ----------------------------------------------------------------------------- */

/* Copy 'len' bytes starting at 's' into the buffer, extending it if needed. */
int buf_addlstring (Buffer *b, const char *s, size_t len) {
  Buf_cursor next_available;
  if (buf_isimmutable(b)) return ERR;
  if (len > 0) {
    next_available = buf_prepsize(b, len * sizeof(Buf_data));
    if (!next_available) return ERR;
    memcpy(next_available, s, len * sizeof(Buf_data));
    b->next += len;
  }
  return OK;
}

void buf_addlstring_UNSAFE (Buffer *b, const char *s, size_t len) {
  memcpy(&(b->data[b->next]), s, len * sizeof(char));
  b->next += len;
}

void buf_addchar_UNSAFE (Buffer *b, char c) {
  b->data[b->next] = c;
  b->next += sizeof(char);
}

/* UNSAFE.  The user can call strnlen themselves, with their own
   notion of what is the longest string they want to accomodate. */
/* int buf_addstring (Buffer *b, const char *s) { */
/*   return buf_addlstring(b, s, strlen(s)); */
/* } */

/* 
   Encode the value 'n' into 'bytes' bytes written starting at 'str'.
   Caller must ensure that 'str' is at least 'bytes' long. 
*/
static int encodeint_varsize (Buf_cursor str, int64_t n, int bytes) {
  switch (bytes) {
  case 8:
    str[7] = (uint64_t) n & 0xFF; /* cast to unsigned to preserve sign bit */
    n = n >> 8;			  /* want arithmetic shift here */
    str[6] = n & 0xFF;
    n = n >> 8;
    __attribute__ ((fallthrough));
  case 6:
    str[5] = (uint64_t) n & 0xFF;
    n = n >> 8;
    str[4] = n & 0xFF;
    n = n >> 8;
    __attribute__ ((fallthrough));
  case 4:
    str[3] = n & 0xFF;
    n = n >> 8;
    str[2] = n & 0xFF;
    n = n >> 8;
    __attribute__ ((fallthrough));
  case 2:
    str[1] = n & 0xFF;
    n = n >> 8;
    str[0] = n & 0xFF;
    n = n >> 8;
    break;
  default:
    LOGf("encodeint_varsize", "invalid encoding size: %d", bytes);
    return ERR;
  }
  if ((n != -1) && (n != 0)) {
    LOGf("encodeint_varsize", "value exceeds encoding size of %d bytes", bytes);
    return ERR;
  }
  return OK;
}

int buf_addint_varsize (Buffer *b, int64_t n, int bytes) {
  Buf_data str[8];
  if (encodeint_varsize(str, n, bytes)) return ERR;
  if (buf_addlstring(b, (const char *)str, bytes)) return ERR;
  return OK;
}

/* Special case, to avoid needing 8-byte register that buf_addint_varsize uses.
   Always encodes in 4 bytes. 
*/
int buf_addint32 (Buffer *b, int32_t n) {
  char str[4];
  str[3] = n & 0xFF;
  n = ((uint32_t) n) >> 8;
  str[2] = n & 0xFF;
  n = ((uint32_t) n) >> 8;
  str[1] = n & 0xFF;
  n = ((uint32_t) n) >> 8;
  str[0] = n & 0xFF;
  if (buf_addlstring(b, (const char *)str, 4)) return ERR;
  return OK;
}

/* Special case, to avoid needing 8-byte register that buf_addint_varsize uses.
   Always encodes in 2 bytes. 
*/
int buf_addint16 (Buffer *b, int16_t n) {
  char str[2];
  str[1] = n & 0xFF;
  n = ((uint16_t) n) >> 8;
  str[0] = n & 0xFF;
  if (buf_addlstring(b, (const char *)str, 2)) return ERR;
  return OK;
}

/* ----------------------------------------------------------------------------- */
/* PEEK FUNCTIONS                                                                */
/* ----------------------------------------------------------------------------- */

/* Caller must ensure that 's' is at least 'bytes' long */
static int decodeint_varsize (const char *s, int bytes, int64_t *retval) {
  uint64_t n = (unsigned char) *s++;
  switch (bytes) {
  case 8:
    n = n << 8;
    n |= (unsigned char) *(s++);
    n = n << 8;
    n |= (unsigned char) *(s++);
    __attribute__ ((fallthrough));
  case 6:
    n = n << 8;
    n |= (unsigned char) *(s++);
    n = n << 8;
    n |= (unsigned char) *(s++);
    __attribute__ ((fallthrough));
  case 4:
    n = n << 8;
    n |= (unsigned char) *(s++);
    n = n << 8;
    n |= (unsigned char) *(s++);
    __attribute__ ((fallthrough));
  case 2:
    n = n << 8;
    n |= (unsigned char) *s;
    break;
  default:
    LOGf("decodeint_varsize", "invalid encoding size: %d", bytes);
    return ERR;
  }
  switch (bytes) {
  case 8:
    *retval = (int64_t) n;
    break;
  case 6:
    if (n > BUF_INT48_MAX) *retval = (n | 0xFFFF000000000000);
    break;
  case 4:
    *retval = (int32_t) n;
    break;
  case 2:
    *retval = (int16_t) n;
    break;
  }
  return OK;
}

Buf_cursor buf_peekint_varsize (Buffer *buf, Buf_cursor ptr, int64_t *retval, int bytes) {
  if (!ptr) ptr = buf->data;
  if ((ptr + bytes) > (buf->data + buf->next)) return NULL;
  if (decodeint_varsize(ptr, bytes, retval)) return NULL;
  return ptr;
}

/* Special case, to avoid needing 8-byte register that decodeint_varsize uses. */
Buf_cursor buf_peekint32 (Buffer *buf, Buf_cursor ptr, int32_t *retval) {
  uint32_t n;
  unsigned char *s;
  if (!ptr) ptr = buf->data;
  if ((ptr + 4) > (buf->data + buf->next)) return NULL;
  s = (unsigned char *) ptr;
  n = *s++;
  n = n << 8;
  n |= *(s++);
  n = n << 8;
  n |= *(s++);
  n = n << 8;
  n |= *s;
  *retval = (int32_t) n;
  return ptr;
}

/* Special case, to avoid needing 8-byte register that decodeint_varsize uses. */
Buf_cursor buf_peekint16 (Buffer *buf, Buf_cursor ptr, int16_t *retval) {
  uint16_t n;
  unsigned char *s;
  if (!ptr) ptr = buf->data;
  if ((ptr + 2) > (buf->data + buf->next)) return NULL;
  s = (unsigned char *) ptr;
  n = *s++;
  n = n << 8;
  n |= *s;
  *retval = (int16_t) n;
  return ptr;
}

/* 
   Return a valid cursor pointing to at least 'len' bytes, starting at
   'ptr'.  On success, returns a valid cursor that points either to
   the start of the buffer (if 'ptr' supplied was NULL), or 'ptr'
   itself.  If there are not 'len' bytes available starting at 'ptr',
   NULL is returned.
*/
Buf_cursor buf_peeklstring (Buffer *buf, Buf_cursor ptr, size_t len) {
  if (!ptr) ptr = buf->data;
  if ((ptr + len) > (buf->data + buf->next)) return NULL; /* error */
  return ptr;
}

/* ----------------------------------------------------------------------------- */
/* READ FUNCTIONS                                                                */
/* ----------------------------------------------------------------------------- */

Buf_cursor buf_readint_varsize (Buffer *buf, Buf_cursor ptr, int64_t *retval, int bytes) {
  if (!(ptr = buf_peekint_varsize(buf, ptr, retval, bytes))) return NULL;
  return ptr + bytes;
}

Buf_cursor buf_readint32 (Buffer *buf, Buf_cursor ptr, int32_t *retval) {
  if (!(ptr = buf_peekint32(buf, ptr, retval))) return NULL;
  return ptr + 4;
}

Buf_cursor buf_readint16 (Buffer *buf, Buf_cursor ptr, int16_t *retval) {
  if (!(ptr = buf_peekint16(buf, ptr, retval))) return NULL;
  return ptr + 2;
}

Buf_cursor buf_readlstring (Buffer *buf, Buf_cursor ptr, size_t len) {
  if (!(ptr = buf_peeklstring(buf, ptr, len))) return NULL;
  return ptr + len;
}

/* ----------------------------------------------------------------------------- */
/* FILE i/o (convenience functions)                                              */
/* ----------------------------------------------------------------------------- */

/* Write buffer length as signed 64-bit int */
int buf_writelen (FILE *file, Buffer *b) {
  char str[8]; 
  size_t items; 
  if (b->next > SIZE_MAX) return ERR;
  encodeint_varsize(str, (int64_t) b->next, 8);  
  items = fwrite((void *) str, 8, 1, file); 
  if (items != 1) return ERR; 
  return OK; 
}

int buf_write (FILE *file, Buffer *b) {
  size_t items;
  if (b->next == 0) return 0;
  items = fwrite((void *) b->data, b->next, 1, file);
  if (items != 1) return ERR;
  return OK;
}

int buf_readlen (FILE *file, size_t *len) {
  int64_t longlen;
  char str[8];
  size_t items = fread((void *) str, 8, 1, file);
  if (items != 1) {
    LOG("buf_readlen", "read error");
    return ERR;
  }
  if (decodeint_varsize(str, 8, &longlen)) {
    LOG("buf_readlen", "decode error");    
    return ERR;
  }
  if ((longlen < 0) || ((PEXL_INT64_MAX > SIZE_MAX) && (longlen > (int64_t) SIZE_MAX))) {
    LOG("buf_readlen", "range error");
    return ERR;
  }
  *len = (size_t) longlen;
  return OK;
}

Buffer *buf_read (FILE *file, size_t len) {
  Buffer *b = buf_new(len);
  size_t items;
  if (len == 0) return b;
  items = fread((void *) b->data, len, 1, file);
  if (items != 1) {
    buf_free(b);
    return NULL;
  }
  b->next = len;
  return b;
}

