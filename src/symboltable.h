/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  symboltable.h  String/symbol table (ELF style)                           */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#if !defined(symboltable_h)
#define symboltable_h

#include "common.h"
#include "metadata.h"

/* TODO:
   
   - MAYBE use symtab index 0 to mean "undefined symbol" (as is done
     in ELF and other object formats, so that we can used an unsigned
     type as an index.

   - Would be useful (for efficiency) to use a hash table while
     building the symbol table.  We can trash it with some kind of
     symboltable_close() call that will close it to modifications and
     retain only the table entries and the string storage.

   + New symbol type: IMPORT.  Value is offset into string storage
     where the importpath of a dependency is found.  Probably we
     should have an additional field for a checksum or version.

 */


/* String table:

   In the ELF executable format (and others), a STRING TABLE is a
   contiguous block of storage containing null-terminated strings.  A
   SYMBOL TABLE is an array of indexes into that block (usually with
   per-symbol meta-data).

   The string encoding is not specified here, because this code does
   not need to know -- as long as strings do not contain any null
   bytes, that is.

   By convention, the first byte of the block is always 0, so that
   index 0 uniquely stores the empty string (or the absence of a
   string, depending on the context).

   Valid block indexes are in the range: [0, SYMBOLTABLE_MAXBLOCKSIZE - 1].

   Having a SYMBOL TABLE of size N enables these features:
   - Each stored string gets a number (array index) in [0, N-1].
   - Only logN bits are needed to store a string number, whereas the
     index into the block of the start of a string requires log(N+1)
     bits in the best case (1-character strings each with a NUL
     terminator) and log(N+M) bits in general, when stored strings
     have length M.
   - The empty string is always in the block, but may or may not be in
     the string table, depending on whether there is a k such that
     A[k] = 0.
   - As with the ELF symbol table format, having a separate index
     array makes it easy to store symbol meta-data.

*/

/* These can be tuned for performance */

#define SYMBOLTABLE_INITSIZE       100 /* number of symbols */
#define SYMBOLTABLE_INITBLOCKSIZE 1000 /* number of chars */

/* We limit the symbol table to a maximum of 24 bits so that we can
   always encode a symbol table index in 3 bytes, e.g. in intermediate
   representations and vm instructions.
*/
#define SYMBOLTABLE_MAXSIZE (1 << 20)      /* 2^24 entries, maximum */

/* The maximum block size is limited to 24 bits for a different
   reason: We want to pack a symbol table entry into 16 bytes, so we
   put the name (index into the string block) and its meta-data
   together into a single 32-bit field.
 */
#define SYMBOLTABLE_MAXBLOCKSIZE (1 << 22) /* 2^24 bytes, maximum */

/* The max length of a single string is arbitrary and can be as large
   as (2^MAXBLOCKSIZE)-2. (The 2 bytes are the NUL byte at index 0 of
   the block and the NUL byte at the end of every string in the block.
*/
#define SYMBOLTABLE_MAXLEN (1 << 16)  /* 65,535 bytes max for one string */

/* With a 24-bit limit on the number of symbols in a single symbol
   table, we can afford to use a signed int as an index, which means
   we can return error codes as negative numbers when needed.  So we
   can use Index, which is a synonym for int32_t.
*/

typedef struct SymbolTableEntry {
  /* MAYBE: Add pointer to package with the symboltable containing this entry? */
  uint32_t        nameinfo;  /* see below */
  uint32_t        size;	     /* size of *value */
  Index         value;     /* offset for strings; entrypoint for patterns */
  PatternMetaData meta;	     /* see metadata.h */
} SymbolTableEntry;

/*  nameinfo: 3 bytes name, 1 byte info

    name: index into SymbolTableStr->block.
    info: bits 0-6: symbol type
          bit    7: visibility 0=local; 1=global
*/
#define SYMBOL_LOCAL  0
#define SYMBOL_GLOBAL 1

enum SYMBOL_TYPES {
  SYMBOL_TYPE_PATTERN = 0,
  SYMBOL_TYPE_STRING,
  SYMBOL_TYPE_CAPNAME,
  SYMBOL_TYPE_IMPORT
};

#define set_entry_name_offset(entry, index) do {			\
    (entry)->nameinfo =							\
      ((entry)->nameinfo & 0xFF) | (((unsigned) (index & 0x00FFFFFF)) << 8); \
  } while (0);

/* A valid offset is unsigned, but it fits into 24 bits, so it fits into int32_t. */
#define entry_name_offset(entry) ((signed) (((unsigned)(entry)->nameinfo) >> 8))

#define set_entry_visibility(entry, vis) do {	\
  if (vis) (entry)->nameinfo |= 0x80;		\
  else (entry)->nameinfo &= 0xFFFFFF7F;		\
  } while(0);

#define entry_visibility(entry) (((entry)->nameinfo & 0x80) >> 7)

#define set_entry_type(entry, type) do {				\
    (entry)->nameinfo =							\
      ((entry)->nameinfo & 0xFFFFFF80) | (type & 0x7F); \
  } while(0);

#define entry_type(entry) ((entry)->nameinfo & 0x7F)

#define entry_size(entry) ((entry)->size)
#define entry_value(entry) ((entry)->value)

typedef struct SymbolTable {
  SymbolTableEntry   *entries;
  size_t              size;	/* capacity of entries[] */
  size_t              next;     /* next available entry is entries[next] */
  char   *block;		/* storage for strings */
  size_t  blocksize;		/* size of storage block */
  size_t  blocknext;	        /* next available char is block[blocknext] */
} SymbolTable;

/* ----------------------------------------------------------------------------- */
/* Interface                                                                     */
/* ----------------------------------------------------------------------------- */

SymbolTable *symboltable_new (size_t initial_size, size_t initial_blocksize);
void         symboltable_free (SymbolTable *st);
Index      symboltable_add (SymbolTable *st,
			      const char *name,
			      int visibility,
			      int symboltype,
			      uint32_t size,
			      Index value,
			      PatternMetaData *meta);
size_t symboltable_len (SymbolTable *st);
size_t symboltable_blocklen (SymbolTable *st);

SymbolTableEntry *symboltable_get (SymbolTable *st, Index index);
SymbolTableEntry *symboltable_iter (SymbolTable *st, Index *prev); 
SymbolTableEntry *symboltable_search(SymbolTable *st, const char *name_string, Index *index);
const char *symboltable_get_name (SymbolTable *st, Index index) ;
const char *symboltable_entry_name (SymbolTable *st, SymbolTableEntry *entry);
const char *symboltable_block_iter (SymbolTable *st, Index *prev);

#define index_is_emptystring(i) ((i)==0)

#define SYMBOLTABLE_ITER_START -1	/* initial and final value for iter() */

/* ----------------------------------------------------------------------------- */
/* Error and other codes                                                         */
/* ----------------------------------------------------------------------------- */

#define SYMBOLTABLE_ERR_OOM       -1  /* out of memory */
#define SYMBOLTABLE_ERR_STRINGLEN -2  /* max string length exceeded */
#define SYMBOLTABLE_ERR_FULL      -3  /* reached limit of 2^MAXSIZE entries */ 
#define SYMBOLTABLE_ERR_BLOCKFULL -4  /* reached limit of 2^MAXBLOCKSIZE bytes */ 
#define SYMBOLTABLE_ERR_INITSIZE  -5  /* invalid initial size or blocksize requested */ 
#define SYMBOLTABLE_ERR_NULL      -6  /* null symbol table arg */ 

#endif
