/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  analyze.h   Analyze expressions                                          */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#if !defined(analyze_h)
#define analyze_h

#include "common.h"
#include "pattern.h"
#include "expression.h"    

/* ----------------------------------------------------------------------------- */
/* Expression API (external)                                                     */
/* ----------------------------------------------------------------------------- */

/* Return values for exp_patlen, distinguishable from errors because these are > 0 */
#define EXP_UNBOUNDED 101
#define EXP_BOUNDED 202

int exp_hascaptures (Node *node);          /* checks calls made by node */
int exp_itself_hascaptures (Node *node);   /* does not check calls */
int exp_patlen (Node *node, uint32_t *min, uint32_t *max);
int exp_nofail (Node *node);
int exp_nullable (Node *node);
int exp_headfail (Node *node);
int exp_needfollow (Node *node);
int exp_getfirst (Node *node, const Charset *follow, Charset *firstset);

void reset_compilation_state (Pattern *pat);
int  fix_open_calls (CompState *cs);
int  set_metadata (CompState *cs);


#endif
