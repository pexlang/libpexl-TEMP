/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  hashtable.c  Hash table that interns its string keys, has 8-byte payload */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "hashtable.h"
#include <stddef.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

static uint32_t hashcode (const char *s) {
  uint32_t h = 17;
  while (*s) h = (h * 733) + (*s++);
  return h;
}

static uint32_t hashindex (uint32_t hashcode, size_t tablesize, uint32_t i) {
  /* Quadratic probing */
  return ((hashcode & (((uint32_t) tablesize) - 1)) + 3*i*i) % tablesize;
}

static int next_power2 (int n) {
  int i = 0;
  assert( n > 0 );
  while((n = n >> 1)) i++;
  return 1 << (i+1);
}

/* 'size' MUST be a power of 2. */
static HashTable *new_table (size_t size, size_t blocksize) {
  char *block;
  HashTable *ht;
  block = malloc(blocksize);
  if (!block) return NULL;	/* OOM */
  ht = malloc(sizeof(HashTable));
  if (!ht) {
    free(block);
    return NULL;		/* OOM */
  }
  ht->block = block;
  ht->blocksize = blocksize;
  ht->blockcount = 1;	/* block[0] reserved for empty string */
  block[0] = '\0';		/* empty string */
  ht->size = size;
  ht->count = 0;
  ht->entries = malloc(size * sizeof(HashTableEntry));
  if (!ht->entries) {
    free(ht);
    free(block);
    return NULL;		/* OOM */
  }
  memset(ht->entries, HASHTABLE_NOT_FOUND, size * sizeof(HashTableEntry));
  return ht;
}

/* Array is sized based on having HASHTABLE_MAX_LOAD at the number of
   expected entries.  Block storage is sized based on that and the
   expected (average) string size.  When either value is 0, a default
   is used.
*/
HashTable *hashtable_new (size_t exp_entries, size_t exp_string_size) {
  size_t size, blocksize;
  HashTable *ht;
  if (exp_entries == 0) exp_entries = HASHTABLE_MIN_ENTRIES;
  if (exp_string_size == 0) exp_string_size = HASHTABLE_AVG_LEN;
  size = next_power2(exp_entries * 100 / HASHTABLE_MAX_LOAD);
  assert( size > 0 );
  /* Here we right shift the size to see if it exceeds the maximum. */
  if (size >> HASHTABLE_MAX_SLOTS) size = (1 << HASHTABLE_MAX_SLOTS);
  blocksize = exp_entries * exp_string_size;
  assert( blocksize > 0 );
  /* Here we right shift blocksize to see if it exceeds the maximum. */
  if (blocksize >> HASHTABLE_MAX_BLOCKSIZE) blocksize = (1 << HASHTABLE_MAX_BLOCKSIZE);
  ht = new_table(size, blocksize);
  if (!ht) LOG("hashtable_new", "out of memory");
  return ht;
}

void hashtable_free (HashTable *ht) {
  if (!ht) return;
  if (ht->block) free(ht->block);
  if (ht->entries) free(ht->entries);
  free(ht);
}

static int not_equal(const char *storedstring, const char *name) {
  assert( storedstring );
  return (strncmp(storedstring, name, HASHTABLE_MAX_KEYLEN + 1) != 0);
}

const char *hashtable_get_key (HashTable *ht, HashTableEntry entry) {
  if (entry.block_offset == HASHTABLE_NOT_FOUND) return NULL;
  assert( ht );
  assert( ht->block );
  if (((size_t) entry.block_offset) >= ht->blocksize) return NULL;
  return &ht->block[entry.block_offset];
}

/* Ensure space for len bytes PLUS 1 more for a null terminator */
static int ensure_blockspace (HashTable *ht, size_t len) {
  char *new_block;
  size_t new_size;
  if ((ht->blockcount + len) < ht->blocksize) return OK;
  new_size = 2 * ht->blocksize;
  if (new_size < (ht->blockcount + len + 1)) new_size = (ht->blockcount + len + 1);
  /* Right shift to test whether block would be overfull */
  if (new_size >> (HASHTABLE_MAX_BLOCKSIZE + 1)) return HASHTABLE_ERR_BLOCKFULL;
  new_block = realloc(ht->block, new_size);
  if (!new_block) return HASHTABLE_ERR_OOM;
  ht->block = new_block;
  ht->blocksize = new_size;
  return OK;
}

static int32_t put_string (HashTable *ht, const char *str) {
  int stat;
  size_t start;
  size_t len = strnlen(str, HASHTABLE_MAX_KEYLEN + 1);
  if (len == 0) return 0;	/* Special case: null string is at block[0]. */
  if (len > HASHTABLE_MAX_KEYLEN) return HASHTABLE_ERR_KEYLEN;
  if ((stat = ensure_blockspace(ht, len)) != OK) return stat;
  start = ht->blockcount;
  memcpy(&ht->block[start], str, len);
  ht->block[start+len] = '\0';
  ht->blockcount += len + 1;
  return start;
}

/* Returns index into hash table where either the key exists or where
   it should go.
*/
static uint32_t search_index (HashTable *ht, const char *name) {
  HashTableEntry entry;
  uint32_t index, code, i = 0;
  assert( HASHTABLE_MAX_SLOTS <= 31 );
  code = hashcode(name);
  do {
    index = hashindex(code, ht->size, i++);
    entry = ht->entries[index];
  } while ((entry.block_offset != HASHTABLE_NOT_FOUND) &&
	   not_equal(hashtable_get_key(ht, entry), name));
  return index;
}

/* Return -1 for "not found", < 0 for error, else > 0 (block index). */
HashTableEntry hashtable_search (HashTable *ht, const char *name) {
  uint32_t index;
  HashTableEntry entry;
  if (!name) {
    entry.block_offset = HASHTABLE_ERR_NULL; /* error */
    return entry;
  }
  index = search_index(ht, name);
  return ht->entries[index];
}

/* Put into the new 'entries' table one of the entries from the old
   table.  This requires rehashing the existing string entry.
   N.B. Ensure that ht->size and ht->entries are set to the new size
   and array, respectively, before calling put_entry.
*/
static void put_entry (HashTable *ht, HashTableEntry entry) {
  const char *name;
  uint32_t index;
  if (entry.block_offset == HASHTABLE_NOT_FOUND) return; /* skip */
  name = hashtable_get_key(ht, entry);
  /* We only process existing entries, so name should exist */
  assert( name );
  /* Rehash the name, producing the index where name should be stored */
  index = search_index(ht, name);
  assert( ht->entries[index].block_offset == HASHTABLE_NOT_FOUND );
  ht->entries[index] = entry; 
}

/* Expand and re-hash. */
static int expand_table (HashTable *ht, int load) {
  size_t oldsize, newsize, newsizebytes, i;
  HashTableEntry *newentries, *oldentries;
  /* Double the current size */
  newsize = ht->size << 1;
  /* Here we right shift newsize to ensure it is not too large */
  if (newsize >> (HASHTABLE_MAX_SLOTS + 1)) {
    /* Cannot make the array bigger.  Can the load factor grow? */
    if (load > HASHTABLE_MAX_LOAD)
      return HASHTABLE_ERR_SIZE;
    else
      return 0;		       /* OK, we will allow the higher load */
  }
  newsizebytes = newsize * sizeof(HashTableEntry);
  oldentries = ht->entries;
  oldsize = ht->size;
  newentries = malloc(newsizebytes);
  if (!newentries) return HASHTABLE_ERR_OOM;
  ht->entries = newentries;
  ht->size = newsize;
  memset(newentries, HASHTABLE_NOT_FOUND, newsizebytes);
  /* Rehash existing entries, put them into new table */
  for (i = 0; i < oldsize; i++)
    put_entry(ht, oldentries[i]);
  free(oldentries);
  return 0;			/* OK */
}

static int32_t add (HashTable *ht, int32_t index, const char *name, HashTableData data) {
  int err, load;
  int32_t offset;
  assert( index >= 0 );
  assert( name );
  /* String is not in table, so we have to create a new entry. */
  load = (ht->count * 100) / ht->size;
  if (load > HASHTABLE_EXPAND_LOAD) {
    if ((err = expand_table(ht, load))) return err;
    /* Rehash the entry we need to add to the table: */
    index = search_index(ht, name);
    offset = ht->entries[index].block_offset;
    assert( offset == HASHTABLE_NOT_FOUND );
  }
  if ((offset = put_string(ht, name)) < 0) return offset; /* error */
  ht->entries[index].block_offset = offset;
  /* Write the data */
  memcpy(&ht->entries[index].data.chars, data.chars, 8);
  ht->count++;
  /* return the offset, which uniquely and forever denotes this string */
  return offset;
}

/* 
   Adds a string to the hash table, with a NUL terminator.  
   Returns block offset >= 0 for OK, or error < 0.  Already in table is an error.
*/
int32_t hashtable_add (HashTable *ht, const char *name, HashTableData data) {
  int32_t index;
  if (!ht) {
    LOG("hashtable_add", "null hashtable argument");
    return HASHTABLE_ERR_NULL;
  }
  if (!name) {
    LOG("hashtable_add", "null name argument");
    return HASHTABLE_ERR_NULL;
  }
  /* Find where this string is, or where it should go */
  index = search_index(ht, name);
  if (ht->entries[index].block_offset == HASHTABLE_NOT_FOUND)
    return add(ht, index, name, data);
  /* else index is the index into the hash table where 'name' exists */
  return HASHTABLE_ERR_FOUND;
}

/* 
   Adds a string to the hash table, with a NUL terminator.  
   Returns block offset >= 0 for OK, or error < 0.
*/
int32_t hashtable_add_update (HashTable *ht, const char *name, HashTableData data) {
  int32_t index;
  if (!ht) {
    LOG("hashtable_add_update", "null hashtable argument");
    return HASHTABLE_ERR_NULL;
  }
  if (!name) {
    LOG("hashtable_add_update", "null name argument");
    return HASHTABLE_ERR_NULL;
  }
  /* Find where this string is, or where it should go */
  index = search_index(ht, name);
  if (ht->entries[index].block_offset == HASHTABLE_NOT_FOUND)
    return add(ht, index, name, data);
  /* else index is the index into the hash table where 'name' exists */
  /* so update the data */
  memcpy(&ht->entries[index].data.chars, data.chars, 8);  
  /* and return the offset, which uniquely and forever denotes this string */
  return ht->entries[index].block_offset;
}

/* 
   Start iterating by calling with prev == -1.  HASHTABLE_ITER_START
   is provided for this purpose.  Note that the empty string will be
   one of the strings returned iff it was explicitly added to the hash
   table.
 */
HashTableEntry hashtable_iter (HashTable *ht, int32_t *prev) {
  HashTableEntry entry;
  size_t size = ht->size;
  size_t i = ((size_t) (*prev)) + 1;
  entry.block_offset = HASHTABLE_NOT_FOUND; /* signal end of iteration */
  /* This test is not needed as long as HashTableEntry is an unsigned type: */
  /*   if (i < 0) return entry; */
  while ((i < size) && (ht->entries[i].block_offset == -1)) i++;
  if (i == size) return entry;
  *prev = i;
  return ht->entries[i];
}


