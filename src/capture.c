/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  capture.c  The byte and debug (printing) capture processors              */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "capture.h"
#include <string.h> 

/* Note: 

   The open/close encoder functions will always BOTH be called, and
   they will be called in the nested way that one would expect from
   the fact that the captures form a tree.

   This expected behavior is partly the result of the way capture
   instructions are generated (OPEN ... other code .. CLOSE).  

   There should be only one exception to this rule, which is that the
   HALT instruction truncates the capture list but inserts a special
   Cfinal close capture frame to signal the truncation.  Then,
   caploop() generates a synthetic Close capture frame for each
   missing one.

*/

/* 
   FUTURE: Add memoization to speed things up.  Particularly when
   looking up package prefix string.

   TODO: Currently using the DEFAULT package prefix, not the
   appropriate one for the caller!

 */

static const char *string_ref (CapState *cs, const Capture *cap) {
  Index offset = capidx(cap);
  Package *p = packagetable_get(cs->pt, cap->pkg_number);
  if (!p) {
    LOGf("string_ref", "invalid package number %d while encoding match results", cap->pkg_number);
    return NULL;
  }
  return symboltable_get_name(p->symtab, offset);
}

/* TODO: Update when we change the capture contents to reflect the correct prefix */
static void print_capture(CapState *cs, const Capture *cap) {
  const char *prefix = NULL;
  const char *name;
  Package *p;
  p = packagetable_get(cs->pt, cap->pkg_number);
  printf("  kind = %s\n", CAPTURE_NAME(capkind(cap)));
  printf("  pos (0-based) = %lu\n", cap->s ? (cap->s - cs->s) : 0);
  name = string_ref(cs, cap);
  prefix = symboltable_get_name(p->symtab, package_default_prefix(p));
  if (name) {
    printf("  package prefix     '%s'\n", prefix);
    printf("  idx = %2u           '%s'\n", capidx(cap), name);
  }
}

/* The 'debug' output encoder simply prints the details of the match results. */
int debug_Close(CapState *cs, const Capture *cap, Buffer *buf, const int count) {
  UNUSED(buf); UNUSED(count); 
  if (isopencap(cap)) {
    LOGf("debug_Close", "expected open capture, found %d (%s)", capkind(cap), CAPTURE_NAME(capkind(cap)));
    return MATCH_CLOSE_ERROR;
  }
  printf("CLOSE: %s (%d)\n", CAPTURE_NAME(capkind(cap)), capkind(cap));
  if (capkind(cap) == Ccloseconst)
    print_capture(cs, cap);
  else
    printf("  pos (0-based) = %lu\n", cap->s ? (cap->s - cs->s) : 0);
  return MATCH_OK;
}

int debug_Open(CapState *cs, const Capture *cap, Buffer *buf, const int count) {
  UNUSED(buf); UNUSED(count); 
  if (!acceptable_capture(capkind(cap))) {
    LOGf("debug_Open", "expected open capture, found %d (%s)", capkind(cap), CAPTURE_NAME(capkind(cap)));
    return MATCH_OPEN_ERROR;
  }
  printf("OPEN:\n");
  print_capture(cs, cap);
  return MATCH_OK;
}

/* ---------------------------------------------------------------------------------------- 
 * The 'byte' output encoder emits a compact (and simple) linear encoding:
 *
 * OLD FORMAT:
 * -----------
 * Cap := -start_pos -shortlen <name> shortlen <constdata> Cap? end_pos
 *     |= -start_pos shortlen <name> Cap? end_pos
 *
 * Where start_pos, end_pos are 32-bit unsigned ints and shortlen is
 * unsigned 16-bit int.  Currently, the max length of a string table
 * entry is 1023, so only 10 bits of the 16-bit shortlen fields are
 * used.
 *
 * NEW FORMAT:
 * -----------
 * Stream := Magic FormatVer PosSize CapFrame
 *   Magic := 'PEXD'
 *   FormatVer := one byte with value 00, indicating THIS VERSION
 *   PosSize := one byte with value 2, 4, or 6
 * 
 *   CapFrame := -start_pos Capture CapFrame? end_pos
 *
 *     Capture := PkgNumber CapType [...]
 *         The format of the remainder [...] depends on CapType.
 *
 *     16 bits PkgNumber is index into Package Table, where prefix and
 *     symbol table are found.
 *     
 *     8 bit CapType:
 *         0x0 Ordinary capture, single offset follows
 *         0x1 Constant capture, two offsets (name and value) follow
 *         0x2 Match-time capture, length-prefixed capture contents follows
 *
 *     24 bits Offset is byte index into block storage of package's
 *     symbol table
 *     
 * start_pos, end_pos are PosSize bytes, and their absolute values are
 * offsets into the input data.  The max offsets (i.e. the max input
 * lengths) are:
 *
 *   PosSize = 2: 2^15-1 = 32,767.              ~  32KB max input size
 *   PosSize = 4: 2^31-1 = 2,147,483,647.       ~   2GB max input size
 *   PosSize = 6: 2^47-1 = 140,737,488,355,327. ~ 128TB max input size
 * 

 *   Situation in the Rosie project: The maximum input data length
 *     must fit in 32 bits unsigned, so it's 4GB-1.  The startpos and
 *     endpos arguments to vm_start() are uint32_t.  A Capture stores
 *     a char pointer into the input buffer, so it is not dependent on
 *     input size, but the Encoders for capture data (in capture.c)
 *     currently assume a uint32_t offset from the start of the input.
 *
 *   Current situation: The vm arguments now accept starting and
 *   ending positions as size_t.  Probably we should change these to
 *   uint64_t to avoid variation across platforms.

 *   FUTURE: We'd like to be able to process inputs larger than 4GB
 *     (and using mmap for even medium sized files is a likely
 *     technique we will adopt).  Beyond 4GB, we are looking at 256TB
 *     (minus 1) as the next logical stopping point, where offsets fit
 *     into an unsigned 48-bit field.  An issue already, with 32-bit
 *     offsets is that, once encoded (e.g. in our binary format), most
 *     of that space is unused most of the time, because inputs tend
 *     to be short.
 *
 *     Perhaps the binary output encoder should use k bytes to encode
 *     offsets, where k is one of {2, 4, 6}?  A program decoding this
 *     format would read k at the start of the encoded data, and use
 *     it as a loop bound to decode offsets.  When decoded into a
 *     32-bit integer only, the 6-byte format would be rejected.  A
 *     64-bit platform could decode into a 64-bit int (or a size_t)
 *     and handle all possible data.
 */

static void encode_pos (int64_t pos, int negate, Buffer *buf, int bytes) {
  int sign = negate ? -1 : 1;
  if (!buf_addint_varsize(buf, (pos+1) * sign, bytes)) return;
  PANIC("encode_pos", "cannot encode position %lld in %d bytes", pos, bytes);
}

static void encode_startpos (int64_t pos, Buffer *buf, int bytes) {
  encode_pos(pos, YES, buf, bytes);
}

static void encode_endpos (int64_t pos, Buffer *buf, int bytes) {
  encode_pos(pos, NO, buf, bytes);
}

static int encode_string (const char *str, size_t len, Buffer *buf, int follows) {
  int32_t val;
  assert( str );
  val = ((int) len) | (follows ? 0x40000000 : 0); /* bit 22 */
  val = val | 0x80000000;			  /* bit 23 = actual name in the data */
  buf_addint32(buf, val);
  /* encode the string by copying it into the buffer */
  return buf_addlstring(buf, str, len); 
}

static int encode_prefix_name (const char *prefix, const char *name, Buffer *buf, int follows) {
  int err, prefix_len, name_len;
  int32_t val;
  prefix_len = prefix ? strnlen(prefix, MAX_STRINGLEN) : 0;
  name_len = strnlen(name, MAX_STRINGLEN);
  val = prefix ? prefix_len+1 : 0;
  val += name_len;
  val |= (follows ? 0x40000000 : 0);   /* bit 22 */
  val = val | 0x80000000;	       /* bit 23 = actual name in the data */
  if ((err = buf_addint32(buf, val))) return err;
  /* encode the string by copying it into the buffer */
  if (prefix && prefix[0]!='\0') {
    if ((err = buf_addlstring(buf, prefix, prefix_len))) return err;
    if ((err = buf_addlstring(buf, ".", 1))) return err;
  }
  if ((err = buf_addlstring(buf, name, name_len))) return err;
  return OK;
}

int byte_Close (CapState *cs, const Capture *cap, Buffer *buf, const int count) {
  size_t e, len;
  const char *name;
  UNUSED(count); 
  if (isopencap(cap)) {
    LOGf("byte_Close", "expected close capture, found %d (%s)", capkind(cap), CAPTURE_NAME(capkind(cap)));
    return MATCH_CLOSE_ERROR;
  }
  if (capkind(cap) == Ccloseconst) {
    name = string_ref(cs, cap);
    assert( name );
    len = strnlen(name, MAX_STRINGLEN);
    encode_string(name, len, buf, 0);
  }
  e = cap->s - cs->s;
  encode_endpos(e, buf, cs->pos_size);
  return MATCH_OK;
}

int byte_Open (CapState *cs, const Capture *cap, Buffer *buf, const int count) {
  size_t s;
  const char *name, *prefix = NULL;
  Package *p = packagetable_get(cs->pt, cap->pkg_number);
  UNUSED(count);
  if (!acceptable_capture(capkind(cap))) {
    LOGf("byte_Open", "expected open capture, found %d (%s)", capkind(cap), CAPTURE_NAME(capkind(cap)));
    return MATCH_OPEN_ERROR;
  }
  s = cap->s - cs->s;
  encode_startpos(s, buf, cs->pos_size);
  prefix = symboltable_get_name(p->symtab, package_default_prefix(p));
  name = string_ref(cs, cap);
  assert( name );
  encode_prefix_name(prefix, name, buf, (capkind(cap) == Crosieconst));
  return MATCH_OK;
}

/* ----------------------------------------------------------------------------------------
 * The 'noop' output encoder looks for structural errors in the data,
 * but otherwise does nothing.
 *
 */

int noop_Close (CapState *cs, const Capture *cap, Buffer *buf, const int count) {
  UNUSED(cs); UNUSED(buf); UNUSED(count); 
  if (isopencap(cap)) {
    LOGf("noop_Close", "expected close capture, found %d (%s)", capkind(cap), CAPTURE_NAME(capkind(cap)));
    return MATCH_CLOSE_ERROR;
  }
  return MATCH_OK;
}

int noop_Open (CapState *cs, const Capture *cap, Buffer *buf, const int count) {
  UNUSED(cs); UNUSED(buf); UNUSED(count); 
  if (!acceptable_capture(capkind(cap))) {
    LOGf("noop_Open", "expected open capture, found %d (%s)\n", capkind(cap), CAPTURE_NAME(capkind(cap)));
    return MATCH_OPEN_ERROR;
  }
  return MATCH_OK;
}

