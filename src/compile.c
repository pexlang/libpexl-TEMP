/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  compile.c      Compile expressions and environments                      */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "compile.h"
#include "analyze.h"
#include "codegen.h"

/* ----------------------------------------------------------------------------- */
/* Misc                                                                          */
/* ----------------------------------------------------------------------------- */

/* Returns YES if env is within the context C.  The arg, env, is
   either the top-level env in C or a descendant of it.
*/
int env_in_context (Env *env, Context *C) {
  while (env) {
    if (env == C->env) return YES;
    env = env->parent;
  }
  return NO;
}

static const char *bound_name (Context *C, Pattern *pat) {
  Ref ref;
  Binding *b;
  Index name;
  ref.env = pat->env;
  ref.index = pat->binding_index;
  b = env_get_binding(ref);
  name = b ? b->name : -1;
  return context_retrieve(C, name);
}

/* ----------------------------------------------------------------------------- */
/* Compile                                                                       */
/* ----------------------------------------------------------------------------- */

/* Convert E into a Pattern
   Create a compstate.
   Dependencies
   Toposort
   If recursive, check that the appropriate env allows this
   Compute meta-data
   Return the pattern

link_pattern: create package, codegen into package, return package

*/
int add_pattern (Context *C, Pattern *pat) {
  int stat;
  CompState *cst;
  LOGf("compile", "TEMP TEMP adding pattern '%s' (%p)", bound_name(C, pat), (void *) pat);

  if (!C || !pat) {
    LOG("add_pattern", "null argument");
    return ERR_INTERNAL;
  }
  /* Confirm that C->cst exists */
  cst = C->cst;
  if (!cst) {
    LOG("add_pattern", "null compilation context");
    return ERR_INTERNAL;
  }
  /* Set cst->pat to pattern being compiled */
  cst->pat = pat;
  /* Fix open calls in pat.  Note: fix_open_calls looks at cst->pat. */
  stat = fix_open_calls(cst);
  if (stat != 0) {
    LOGf("add_pattern", "TEMP TEMP TEMP: fix_open_calls returned %d", stat);
    return stat;
  }
  /* Extend the existing cfgraph starting at the newly-added pat */
  stat = add_to_cfgraph(pat, cst->g);
  if (stat != 0) {
    LOGf("add_pattern", "TEMP TEMP TEMP: add_to_cfgraph returned %d", stat);
    return stat;
  }
  return OK;
}

int compile_init (Context *C) {
  if (C->cst) {
    LOG("compile_init", "freeing prior compilation state");
    compstate_free(C->cst);
  }
  C->cst = compstate_new();
  if (!C->cst) return EXP_ERR_OOM; /* already logged */
  C->cst->g = cfgraph_new();
  if (!C->cst->g) return EXP_ERR_OOM; /* already logged */
  return OK;
}
  
/* AFTER all patterns have been added, sort the graph, set metadata, etc. */
int compile_now (Context *C) {  
  int stat;
  if (!C) {
    LOG("compile_now", "null context argument");
    return EXP_ERR_NULL;
  }
  if (!C->cst || !C->cst->g) {
    LOG("compile_now", "null compile state or control flow graph");
    return ERR_INTERNAL;
  }
  stat = sort_cfgraph(C->cst->g);
  if (stat != 0) {
    LOGf("compile_now", "TEMP TEMP TEMP: sort_cfgraph returned %d", stat);
    return stat;
  }
  assert( C->cst->g->order );
  stat = set_metadata(C->cst); 
  if (stat != 0) {
    LOGf("compile_now", "TEMP TEMP TEMP: set_metadata returned %d", stat);
    return stat;
  }
  return OK;
}

static int compile_pattern_internal (Context *C, Pattern *pat, Env *env, Value *retval) {
  int stat;
  CompState *cst;
  LOGf("compile", "compiling pattern '%s' (%p)", bound_name(C, pat), (void *) pat);
  if (C->cst) {
    LOG("compile_pattern_internal", "freeing prior compilation state");
    compstate_free(C->cst);
    C->cst = NULL;
  }
  pat->env = env;		/* compilation env */
  cst = compstate_new();
  if (!cst) return EXP_ERR_OOM;	/* already logged */
  cst->pat = pat;
  stat = fix_open_calls(cst);
  if (stat != 0) {
    LOGf("compile_expression", "TEMP TEMP TEMP: fix_open_calls returned %d", stat);
    goto err1;
  }
  stat = build_cfgraph(pat, &(cst->g));
  if (stat != 0) {
    LOGf("compile_expression", "TEMP TEMP TEMP: build_cfgraph returned %d", stat);
    goto err1;
  }
  assert( cst->g );
  stat = sort_cfgraph(cst->g);
  if (stat != 0) {
    LOGf("compile_expression", "TEMP TEMP TEMP: build_cfgraph returned %d", stat);
    goto err1;
  }
  assert( cst->g->order );
  stat = set_metadata(cst); 
  if (stat != 0) {
    LOGf("compile_expression", "TEMP TEMP TEMP: set_metadata returned %d", stat);
    goto err1;
  }
  C->cst = cst;
  *retval = env_new_value(Epattern_t, 0, (void *)pat);
  return OK;

 err1:
  compstate_free(cst);
  return stat;
}

static int compile_expression1 (Context *C, Expression *E, Env *env, Value *retval, int reset) {
  int stat;
  Pattern *pat;
  if (!C) {
    LOG("compile_expression", "null context arg");
    return EXP_ERR_NULL;
  }
  if (!E) {
    LOG("compile_expression", "null expression arg");
    return EXP_ERR_NULL;
  }
  if (!env) {
    LOG("compile_expression", "null environment arg");
    return EXP_ERR_NULL;
  }
  if (!retval) {
    LOG("compile_expression", "null return value arg");
    return EXP_ERR_NULL;
  }
  if (!env_in_context(env, C)) {
    LOG("compile_expression", "compilation env is outside of given context");
    return EXP_ERR_SCOPE;
  }
  pat = pattern_new(E, &stat);
  if (!pat) {
    LOGf("compile_expression", "TEMP TEMP TEMP: pattern_new returned %d", stat);
    return stat;
  }
  pat->env = env;
  if (reset) reset_compilation_state(pat);
  stat = compile_pattern_internal(C, pat, env, retval);
  if (stat != 0) {
    pattern_only_free(pat);	/* do not free E */
    assert( !C->cst );
  }
  return stat;
}

/* Compile anonymous and possibly unbound expression */
int compile_expression (Context *C, Expression *E, Env *env, Value *retval) {
  return compile_expression1(C, E, env, retval, YES);
}

/* Compile pattern anonymously (though it may be bound and have a name) */
int compile_pattern (Context *C, Pattern *pat, Env *env, Value *retval) {
  if (!C) {
    LOG("compile_pattern", "null context arg");
    return EXP_ERR_NULL;
  }
  if (!pat) {
    LOG("compile_pattern", "null pattern arg");
    return EXP_ERR_NULL;
  }
  if (!env) {
    LOG("compile_expression", "null environment arg");
    return EXP_ERR_NULL;
  }
  if (!retval) {
    LOG("compile_expression", "null return value arg");
    return EXP_ERR_NULL;
  }
  if (!env_in_context(env, C)) {
    LOG("compile_expression", "compilation env is outside of given context");
    return EXP_ERR_SCOPE;
  }
  reset_compilation_state(pat);
  return compile_pattern_internal(C, pat, env, retval);
}

int compile_environment (Context *C, Env *env, Index *errbinding) {
  int stat;
  Binding *b;
  Pattern *p;
  Index i;

  /* Clear all prior compilation state from env */
  i = ENV_ITER_START;
  while ((b = env_local_iter(env, &i))) {
    if (b->val.type != Epattern_t) continue;
    p = (Pattern *) b->val.ptr;
    if (!p) {
      *errbinding = i;
      return ERR_INTERNAL;
    }
    LOGf("compile_environment", "resetting compilation state for pattern %p", (void *) p);
    reset_compilation_state(p);
 } /* while */

  stat = compile_init(C);
  if (stat < 0) return stat;	/* already logged */
  /* Compile all bound expressions */
  i = ENV_ITER_START;
  while ((b = env_local_iter(env, &i))) {
    printf("ADDING PATTERN IN BINDING #%d\n", i);
    if (b->val.type != Epattern_t) {
      /* FUTURE: handle any other value types */
      continue;
    }
    p = (Pattern *) b->val.ptr;
    if (!p) {
      *errbinding = i;
      return ERR_INTERNAL;
    }
    if (pattern_has_flag(p, PATTERN_READY)) continue;
    LOGf("compile_environment", "adding pattern '%s' (%p)", bound_name(C, p), (void *) p); 
    stat = add_pattern(C, p);
    if (stat < 0) {
      *errbinding = i;
      return stat;
    }
  } /* while */

  return compile_now(C);
}
    
/* ----------------------------------------------------------------------------- */
/* Link                                                                          */
/* ----------------------------------------------------------------------------- */

int link_pattern (Context *C, Pattern *P, Package **retval, short optim_mask) {
  int stat;
  Package *pkg;
  if (!C) {
    LOG("link_pattern", "null context arg");
    return EXP_ERR_NULL;
  }
  if (!P) {
    LOG("link_pattern", "null pattern arg");
    return EXP_ERR_NULL;
  }
  if (!retval) {
    LOG("link_pattern", "null return value arg");
    return EXP_ERR_NULL;
  }
  if (!C->cst) {
    LOG("link_pattern", "null compilation state in this context");
    return EXP_ERR_NOTCOMPILED;
  }
  if (!pattern_has_flag(P, PATTERN_READY)) {
    LOG("link_pattern", "pattern not compiled");
    return EXP_ERR_NOTCOMPILED;
  }
  pkg = package_new();
  if (!pkg) {
    LOG("link_pattern", "out of memory");
    return EXP_ERR_OOM;
  }
  stat = codegen(C, P, pkg, optim_mask);
  if (stat != 0) {
    /* TODO: Translate error from codegen into something intelligible. */
    LOGf("link_pattern", "error from codegen: %d", stat);
    package_free(pkg);
    return stat;
  }
  compstate_free(C->cst);
  C->cst = NULL;
  *retval = pkg;
  return OK;
}

int link_environment (Context *C, Env *env, Index *errbinding, Package **retval, short optim_mask) {
  int stat;
  Package *pkg;
  if (!C) {
    LOG("link_environment", "null context arg");
    return EXP_ERR_NULL;
  } else if (!env) {
    LOG("link_environment", "null environment arg");
    return EXP_ERR_NULL;
  } else if (!errbinding) {
    LOG("link_environment", "null error binding arg (a return value)");
    return EXP_ERR_NULL;
  } else if (!retval) {
    LOG("link_environment", "null return value arg (package)");
    return EXP_ERR_NULL;
  } else if (!C->cst) {
    LOG("link_environment", "null compilation state in this context");
    return EXP_ERR_NOTCOMPILED;
  }
  pkg = package_new();
  if (!pkg) {
    LOG("link_environment", "out of memory");
    return EXP_ERR_OOM;
  }

  /* NULL for entrypoint_pattern because there is no unique entrypoint in this case. */
  stat = codegen(C, NULL, pkg, optim_mask);
  if (stat != 0) {
    /* TODO: Translate codegen errors into something intelligible. */
    LOGf("link_environment", "error from codegen: %d at pattern %d", stat, C->cst->err_index);
    package_free(pkg);
    return stat;
  }

  *retval = pkg;
  return OK;
}

/* ----------------------------------------------------------------------------- */
/* Interface                                                                     */
/* ----------------------------------------------------------------------------- */

#define NOT_RECURSIVE 0
#define RECURSIVE 1
#define NODATA 0

EXPORT
Env *pexlc_make_env (Env *parent) {
  return env_new(parent, NOT_RECURSIVE);
}

EXPORT
Env *pexlc_make_recursive_env (Env *parent) {
  return env_new(parent, RECURSIVE);
}

/* Free all of env's patterns, bindings, and env itself */
EXPORT
void pexlc_env_free (Env *env) {
  Binding *b;
  Index i = ENV_ITER_START;
  while ((b = env_local_iter(env, &i))) {
    if (b->deplist) deplist_free((struct Pattern **)b->deplist);
    switch (bindingtype(b)) {
    case Epattern_t:		/* pointer to Pattern */
      pattern_free((Pattern *) b->val.ptr);
      break;
    case Eexpression_t:		/* pointer to tree IR */
      pexle_free((Expression *) b->val.ptr);
      break;
    case Epackage_t:		/* pointer to Package */
      /* should not free another package */
      break;
    default:
      break;
    } /* switch on binding type */
  }
  env_free(env);
}

EXPORT
Context *pexlc_make_context (Env *env) {
  return context_new(env);
}

EXPORT
Ref pexlc_make_binding (Context *C, Env *env, const char *name) {
  int stat;
  Ref ref;
  Index stringID;
  Value unspecified;
  if (!C || !env) {
    ref.env = NULL;		/* signal error */
    ref.index = EXP_ERR_NULL;	/* specify which error  */
    return ref;
  }
  stringID = name ? context_intern(C, name) : context_intern(C, "");
  if (stringID < 0) {
    ref.env = NULL;
    ref.index = stringID;
    return ref;
  }
  unspecified.type = Eunspecified_t;
  unspecified.ptr = NULL;
  stat = env_bind(env, stringID, unspecified, &ref);
  if (stat < 0) {
    ref.env = NULL;
    ref.index = stat;
    return ref;
  }
  return ref;
}

EXPORT
void pexlc_bind_expression (Context *C, Expression *exp, Ref ref, Error *err) {
  int stat;
  Pattern *p;
  if (!err) return;		/* no way to report error */
  err->position = 0;
  if (!C || !exp) {
    err->value = EXP_ERR_NULL;
    return;
  }
  if ((ref_is_error(ref) || ref_not_found(ref))) {
    err->value = ERR_BADREF;
    return;
  }
  if (!ref.env) {
    LOG("bind_expression", "corrupt reference (null environment field)");
    err->value = ERR_INTERNAL;
    return;
  }
  if (!env_in_context(ref.env, C)) {
    err->value = EXP_ERR_SCOPE;
    return;
  }
  p = pattern_new(exp, &stat);
  if (!p) {
    err->value = stat;
    return;
  }
  p->env = ref.env;
  p->binding_index = ref.index;
  stat = env_rebind(ref, env_new_value(Epattern_t, NODATA, (void *)p));
  if (stat != 0) {
    err->value = stat;
    return;
  }
  err->value = OK;
  return;
}

static Package *compile_and_link (Context *C, Pattern *p, Error *err, short optim_mask) {
  int stat;
  Value val;
  Package *pkg;
  stat = compile_pattern(C, p, p->env, &val);
  if (stat != OK) {
    err->value = stat;
    return NULL;
  }
  stat = link_pattern(C, (Pattern *) val.ptr, &pkg, optim_mask);
  if (stat != OK) {
    err->value = stat;
    return NULL;
  }
  err->value = OK;
  return pkg;
}

EXPORT
Package *pexlc_compile_bound_expression (Context *C, Ref ref, Error *err, short optim_mask) {
  Pattern *p;
  Binding *b;
  if (!err) {
    LOG("pexlc_compile_bound_expression", "null error argument");
    return NULL;
  } else if (!C) {
    LOG("pexlc_compile_bound_expression", "null context argument");
    err->value = EXP_ERR_NULL;
    return NULL;
  } else if (ref_is_error(ref)) {
    /* Ref contains an error from an earlier step.  Pass it back to caller. */
    err->value = ref.index;
    return NULL;
  }
  /* Follow ref, extract binding value */
  b = env_get_binding(ref);
  if (!b) {
    err->value = ERR_BADREF;
    return NULL;
  }
  if (b->val.type != Epattern_t) {
    err->value = ERR_REFNOTPATTERN;
    return NULL;
  }
  p = (Pattern *) b->val.ptr;
  if (!p) {
    err->value = ERR_INTERNAL;
    return NULL;
  }
  return compile_and_link(C, p, err, optim_mask);
}

EXPORT
Package *pexlc_compile_expression (Context *C, Expression *exp, Env *env, Error *err, short optim_mask) {
  int stat;
  Pattern *p;
  Package *pkg;
  if (!err) {
    LOG("pexlc_compile_expression", "null error argument");
    return NULL;
  } else if (!C) {
    LOG("pexlc_compile_expression", "null context argument");
    err->value = EXP_ERR_NULL;
    return NULL;
  } else if (!exp) {
    LOG("pexlc_compile_expression", "null expression argument");
    err->value = EXP_ERR_NULL;
    return NULL;
  } else if (!env) {
    LOG("pexlc_compile_expression", "null environment argument");
    err->value = EXP_ERR_NULL;
    return NULL;
  }
  p = pattern_new(exp, &stat);
  if (!p) {
    err->value = stat;
    return NULL;
  }
  p->env = env;
  pkg = compile_and_link(C, p, err, optim_mask);
  pattern_only_free(p);
  return pkg;
}

EXPORT
Package *pexlc_compile_environment (Context *C, Env *env, Error *err, short optim_mask) {
  int stat;
  Package *pkg;
  Index errbinding;
  if (!err) {
    LOG("pexlc_compile_environment", "null error argument");
    return NULL;
  } else if (!C) {
    LOG("pexlc_compile_environment", "null context argument");
    err->value = EXP_ERR_NULL;
    return NULL;
  } else if (!env) {
    LOG("pexlc_compile_environment", "null environment argument");
    err->value = EXP_ERR_NULL;
    return NULL;
  }
  stat = compile_environment(C, env, &errbinding);  
  if (stat < 0) {
    LOGf("pexlc_compile_environment", "TEMP TEMP TEMP compile_environment returned %d at index %d", stat, errbinding);
    err->value = stat;
    err->position = errbinding;
    return NULL;
  }
  stat = link_environment(C, env, &errbinding, &pkg, optim_mask);
  if (stat < 0) {
    LOGf("pexlc_compile_environment", "TEMP TEMP TEMP link_environment returned %d at index %d", stat, errbinding);
    err->value = stat;
    err->position = errbinding;
    return NULL;
  }
  return pkg;
}
