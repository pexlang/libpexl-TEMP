/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  scc.h                                                                    */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */


#if !defined(SCC_h)
#define SCC_h

#include "common.h"

#define SCC_OK 0
#define SCC_ERR_OOM   -10    /* out of memory */
#define SCC_ERR_SIZE  -11    /* max number of vertices exceeded */
#define SCC_ERR_GRAPH -12    /* nonexistent vertex appears in edge list */

#define INDEX_MAX INT32_MAX
#define INDEX_UNDEFINED -1

typedef struct SCC_Component {
  Index vnum;			/* vertex number */
  Index cnum;			/* SCC number */
} SCC_Component;

typedef struct SCC {		/* output stack of SCC Components */
  SCC_Component *elements;
  Index top;
  Index size;
} SCC;

void SCC_free (SCC *output);

/* 'base' points to the start of an array of pointers to dependency
   lists.  Each dependency list is a null-terminated sequence of
   elements.  Caller supplies a null value for comparison.
 */

int SCC_run (void   **base,    /* array of vertices, each with its edge list */
	     size_t   nel,	   /* number of vertices */
	     size_t   width,   /* size of each vertex object (often a pointer) */
	     void    *null,
	     int    (*compare)(void *, const void *, const void *),
	     void    *context,  /* context argument passed to compare() */
	     /* output */
	     SCC  **outstack);

#endif
