/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  env.c  Environment and value types                                       */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

/* 
   Goals (beyond what Rosie/RPL 1.x can do):

   - Control inlining (always, sometimes, never)
   - Ahead of time compilation
   - Separate compilation (in units of a package or a non-package file)

   To address these needs, we are replacing Rosie's Lua-based
   environment (environment.lua) with this C-based implementation
   (env.c).

   This new set of structures replaces the lpeg ktable (which was only
   a string table), and the Rosie Project's use of the ktable to hold
   pattern information for patterns that are bound to names.  

   Also, a Rosie "pattern matching engine" maintains a "package table"
   indexed by the tuple: (importpath, prefix).  Given an indentifier
   in an RPL program that has a package prefix, e.g. "net.ip", there
   must be an import of that package ("net") in that RPL program
   (really: in that compilation unit, which today is a single file).

   With the import path and the prefix from the import declaration, we
   can look up in the package table the environment containing all the
   bindings for that package.  In our example, we can see if "ip" is
   an exported (visible) binding there, so that "net.ip" has a
   meaning.

   STRUCTURES:

   An ENVIRONMENT is a tree in which each node is a collection of
   bindings.  Although it is occasionally confusing, we also call a
   single collection of bindings an environment.

   A BINDING maps a name to a value, though a binding may be
   anonymous.

   A VALUE has a type and some data.  E.g. a string value's data is a
   pointer to a null-terminated byte sequence.  A pattern consists of
   compiled byte-code and some meta-data, all in a single structure.
   A pattern value contains a pointer to such a structure.

   The root of an env tree is a TOP LEVEL ENVIRONMENT, which means:
     - it has a pointer to a "prelude"
     - it belongs to a "machine"

   A MACHINE has a package table, a hash table (for interned strings),
   and some additional state (such as the default prelude to use when
   creating new packages).

   INTERFACE:

   See plan.txt

 */

#include "env.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

/* ----------------------------------------------------------------------------- */
/* Values                                                                        */
/* ----------------------------------------------------------------------------- */

Value env_new_value (int32_t type, int32_t data, void *ptr) {
  Value val;
  val.type = type;
  val.data = data;
  val.ptr = ptr;
  return val;
}

/* Value env_new_value_error (int number) { */
/*   return env_new_value(Eerror_t, number, NULL); */
/* } */

/* Value env_new_value_unspecified () { */
/*   return env_new_value(Eunspecified_t, 0, NULL); */
/* } */

/* Value env_new_value_int32 (int32_t i) { */
/*   return env_new_value(Eint32_t, i, NULL); */
/* } */

/* Value env_new_value_string (SymbolTableIndex entry) { */
/*   return env_new_value(Estring_t, entry, NULL); */
/* } */

/* ----------------------------------------------------------------------------- */
/* Environment                                                                   */
/* ----------------------------------------------------------------------------- */

Env *env_new (Env *parent, int recursive) {
  Env *env;
  int initial_size = ENV_INIT_SIZE;
  env = malloc(sizeof(Env));
  if (!env) {
    LOG("env_new", "out of memory");
    return NULL;
  }
  env->size = initial_size;
  env->next = 0;
  env->bindings = calloc(env->size, sizeof(Binding));
  if (!env->bindings) {
    LOG("env_new", "out of memory");
    free(env);
    return NULL;
  }
  env->parent = parent;
  env->meta = 0;		/* Clear all flags */
  set_env_attr(env, Eattr_recursive, recursive);
  return env;
}

void env_free (Env *env) {
  LOGf("env_free", "freeing %p", (void *) env);
  if (env) {
    if (env->bindings) free(env->bindings);
    free(env);
  }
}

static Binding *local_get (Env *env, Index i) {
  if (!env) {
    LOG("local_get", "null env arg");
    return NULL;
  }
  return ((i < 0) || (i >= env->next)) ? NULL : &(env->bindings[i]);
}

/* In first call, set index to ENV_ITER_START.  Returns NULL
   after binding.  Skips bindings that are marked as deleted.
*/
Binding *env_local_iter(Env *env, Index *index) {
  Binding *binding;
  if (!env) {
    LOG("env_local_iter", "null env arg");
    return NULL;
  }
  if (!index) {
    LOG("env_local_iter", "null previous iteration arg");
    return NULL;
  }
  do {
    if (*index >= env->next) return NULL;
    binding = local_get(env, *index);
    (*index)++;
  } while (binding && binding_has(binding, Eattr_deleted));
  return binding;
}

Binding *env_get_binding (Ref ref) {
  Binding *binding = local_get(ref.env, ref.index);
  if (binding && binding_has(binding, Eattr_deleted)) return NULL;
  return binding;
}

/* Linear search of local env (not parent). Returns 0 for "not found"
   else index+1.
*/
static Index local_lookup (Env *env, Index entry) {
  Index i, next;
  /* (entry < 0) ==> name not interned ==> not found */
  /* (entry == 0) ==> name is empty string ==> anon binding ==> not found */
  if (entry <= 0) return 0;	/* not found */
  next = env->next;
  for (i = 0; i < next; i++) 
    if (env->bindings[i].name == entry) return i+1;
  return 0; /* not found */
} 

Ref env_local_lookup (Env *env, Index entry) {
  Index pseudoindex;
  Ref ref;
  ref.env = NULL;		/* signals the "not found" error */
  if (!env) {
    LOG("env_local_lookup", "null env arg");
    return ref;		/* not found */
  }
  pseudoindex = local_lookup(env, entry);
  if (pseudoindex == 0) return ref; /* not found */
  if (binding_has(&(env->bindings[pseudoindex - 1]), Eattr_deleted))
    return ref;			    /* not found */
  ref.env = env;
  ref.index = pseudoindex - 1;
  return ref; /* found */
}

/* Search 'env' and its ancestors.  Ignore bindings marked for
   deletion.  Cannot look up an anonymous binding (whose name is the
   empty string).
*/
Ref env_lookup (Env *env, Index entry) {
  Binding *binding;
  int pseudoindex;
  Ref ref;
  ref.env = NULL;		/* prepare for "not found" */
  if (!env) {
    LOG("env_lookup", "null env arg");
    return ref;		/* not found */
  }
 tailcall:
  if (!env) return ref;
  pseudoindex = local_lookup(env, entry);
  if (pseudoindex == 0) {
    env = env->parent;
    goto tailcall;
  }
  binding = &(env->bindings[pseudoindex - 1]);
  if (binding_has(binding, Eattr_deleted)) {
    env = env->parent;
    goto tailcall;
  }
  ref.env = env;		/* Will be NULL if not found */
  ref.index = (pseudoindex) ? pseudoindex - 1 : 0;
  return ref;
}

static int ensure_binding_table_space (Env *env) {
  size_t newsize;
  void *temp;
  if (env->next < env->size) return ENV_OK;
  if (((size_t) env->size) >= ENV_MAX_SIZE) return ENV_ERR_FULL;
  newsize = 2 * env->size * sizeof(Binding);
  temp = realloc(env->bindings, newsize);
  if (!temp) {
    LOG("ensure_binding_table_space", "out of memory");
    return ENV_ERR_OOM;
  }
  env->bindings = temp;
  env->size = 2 * env->size;
  LOGf("ensure_binding_table_space", "extending to %d slots (%d in use)", env->size, env->next);
  return ENV_OK;
}

/* Create new binding record.  Return index+1 or 0 for OOM. */
static Index create_new_binding (Env *env, Binding **binding) {
  int err = ensure_binding_table_space(env);
  if (err < 0) return err;
  assert( env->next < env->size );
  *binding = &(env->bindings[env->next]);
  env->next++;
  (*binding)->env = env;
  (*binding)->val = env_new_value(Eunspecified_t, 0, NULL);
  (*binding)->meta = 0;
  (*binding)->deplist = NULL;
  return env->next;		/* pseudoindex = index + 1 */
}

/* Returns 0 for success, or error < 0.  'entry' can be 0 for anonymous binding. */
int env_bind (Env *env, Index entry, Value val, Ref *ref) {
  Binding *binding;
  Index pseudoindex;
  if (!env) {
    LOG("env_bind", "null env arg");
    return ENV_ERR_NULL;
  }
  if (!ref) {
    LOG("env_bind", "null return value arg");
    return ENV_ERR_NULL;
  }
  /* local_lookup will not find an anonymous binding */
  pseudoindex = local_lookup(env, entry);
  if (pseudoindex) {
    /* found a binding with this name */
    binding = local_get(env, pseudoindex - 1);
    assert( binding );
    if (!binding_has(binding, Eattr_deleted))
      return ENV_ERR_EXISTS;
    /* else we will reuse this binding */
  } else {
    /* not found, so create a new binding */
    pseudoindex = create_new_binding(env, &binding);
    if (pseudoindex == 0)
      return ENV_ERR_OOM;
    assert( binding );
  }
  binding->name = entry;
  binding->meta = 0;		/* no attributes set */
  binding->val = val;
  ref->env = env;
  ref->index = pseudoindex - 1;
  return ENV_OK;
}

/* 'rebind' is needed to create recursive patterns.  Beyond that, it
   is intended only to support debugging and REPL use cases.  The
   caller should free the old value before calling 'rebind' if that is
   appropriate.  Return 0 for success, or an error < 0.
*/
int env_rebind (Ref ref, Value val) {
  Binding *binding;
  binding = env_get_binding(ref);
  if (!binding) return ENV_NOT_FOUND;
  binding->val = val;
  return ENV_OK;
}

/* 'unbind' is meant only to support debugging and REPL use cases.  It
   marks the binding as deleted.  The caller should free the old value
   and replace it with the unspecified value before calling 'unbind',
   if that is appropriate.  The caller may also want to change the
   binding name to anonymous so that it will not be reused by
   env_bind().  Return 0 for success, or an error < 0.
*/
int env_unbind (Ref ref) {
  Binding *binding;
  binding = env_get_binding(ref);
  if (!binding) return ENV_NOT_FOUND;
  set_binding_flag(binding, Eattr_deleted, YES);
  return ENV_OK;
}
