/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  charset.c   Character set manipulation                                   */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */


#include "charset.h"

/*
** A few basic operations on Charsets
*/
void cs_complement (Charset *cs) {
  loopset(i, cs->cs[i] = ~cs->cs[i]);
}

int cs_equal (const uint8_t *cs1, const uint8_t *cs2) {
  loopset(i, if (cs1[i] != cs2[i]) return 0);
  return 1;
}

int cs_disjoint (const Charset *cs1, const Charset *cs2) {
  loopset(i, if ((cs1->cs[i] & cs2->cs[i]) != 0) return 0;)
  return 1;
}

