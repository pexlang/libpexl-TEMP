/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  charset.h   Character set manipulation                                   */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */


#if !defined(charset_h)
#define charset_h

#include "common.h"
#include <limits.h>

/* Character set manipulation */
#define BITSPERCHAR		8 /* Must be 8, because we use uint8_t throughout */
#define CHARSETSIZE		((UCHAR_MAX/BITSPERCHAR) + 1)

/* size (in elements) for an instruction plus extra l bytes */
#define instsize(l)  (((l) + sizeof(Instruction) - 1)/sizeof(Instruction) + 1)
/* size (in elements) for a ISet instruction */
#define CHARSETINSTSIZE		instsize(CHARSETSIZE)

typedef struct Charset {
  uint8_t cs[CHARSETSIZE];
} Charset;

/* access to charset */
#define treebuffer(t)      ((uint8_t *)((t->node) + 1))
#define nodebuffer(t)      ((uint8_t *)((t) + 1))
/* set 'b' bit in charset 'cs' */
#define setchar(cs,b)   ((cs)[(b) >> 3] |= (1 << ((b) & 7)))
/* access the bit corresponding to character c (c is a byte) */
#define testchar(st,c)	(((int)(st)[((c) >> 3)] & (1 << ((c) & 7))))
/* set each byte in a charset according to supplied expression */
#define loopset(v,b)    { int v; for (v = 0; v < CHARSETSIZE; v++) {b;} }

static const Charset fullset_ =
  {{0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF}};

__attribute__((unused))
static const Charset *fullset = &fullset_;

void cs_complement (Charset *cs);
int  cs_equal (const uint8_t *cs1, const uint8_t *cs2);
int  cs_disjoint (const Charset *cs1, const Charset *cs2);

#endif
