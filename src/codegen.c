/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  codegen.c   RPL compiler back-end                                        */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

/*
*/

#include "codegen.h"

#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <alloca.h>
#include "env.h"		/* Binding, Ref */
#include "analyze.h"
#include "instruction.h"
#include "symboltable.h"

#define CODEGENDEBUG 0
#if (CODEGENDEBUG)
  #include "print.h"
#endif

/* Inline Optimizer Policies */
static uint32_t naive_policy(Package *pkg, int inline_start);

/* Inline Optimizer Helper Functions */
static void adjust_ins_offset(Package *pkg, uint32_t inline_start, uint32_t inline_size, size_t size_of_ins);
static void adjust_symbol_table(Package *pkg, uint32_t inline_start, uint32_t inline_size, size_t size_of_ins);
static int inliner(Package *pkg, int inline_start, uint32_t inline_size);
static void inlining_optimizer(Package *pkg, uint32_t (*policy)(Package *pkg, int inline_start));
static int has_recursion(Package* pkg, int inline_start, int inline_size);

/* Verify Optimizer Flags */
static int check_optimzer_flag(short inp, short expected);

/* We rely on NOINST being < 0 but higher than all other ERR codes. */
#define NOINST -1

typedef struct CodegenState {
  Context *context;
  Package *pkg;
  Index current_order_index;
} CodegenState;

static int symtab_err_to_codegen_err (int symidx) {
  switch (symidx) {
  case SYMBOLTABLE_ERR_OOM: return CODEGEN_ERR_OOM;
  case SYMBOLTABLE_ERR_FULL: return CODEGEN_ERR_SYMTAB_FULL;
  case SYMBOLTABLE_ERR_BLOCKFULL: return CODEGEN_ERR_SYMTAB_BLOCK_FULL;
  default:
    LOGf("codegen", "symboltable_add returned unexpected code %d", symidx);
    return CODEGEN_ERR_INTERNAL;
  }
}

static const char *bound_name (Context *C, Pattern *pat) {
  Ref ref;
  Binding *b;
  Index name;
  ref.env = pat->env;
  ref.index = pat->binding_index;
  b = env_get_binding(ref);
  name = b ? b->name : -1;
  return context_retrieve(C, name);
}

/* ----------------------------------------------------------------------------- */
/* Code vector                                                                   */
/* ----------------------------------------------------------------------------- */

#define gethere(compst) 	((compst)->codenext)
#define getinstr(cs,i)		((cs)->code[i])

/*
** Patch 'instruction' to jump to 'target'
*/
static void jumptothere (Package *pkg, int instruction, int target) {
  int op;
  if (instruction >= 0) {
    if (target == instruction) {
      LOGf("jumptothere", "instruction == target in package %p at index %d",
	   (void *) pkg, instruction);
    } else {
      op = opcode(&getinstr(pkg, instruction)); 
      setaddr(&getinstr(pkg, instruction), target - instruction);
      assert(opcode(&getinstr(pkg, instruction)) == op);
      assert(addr(&getinstr(pkg, instruction)) == (target - instruction));
    }
  }
}

/*
** Patch 'instruction' to jump to current position
*/
static void jumptohere (Package *pkg, int instruction) {
  jumptothere(pkg, instruction, gethere(pkg));
}

static int realloccode (Instruction **code, size_t newsize) {
  Instruction *temp;
  if (newsize >= MAX_INSTRUCTIONS) {
    LOGf("realloccode", "reached max instruction limit of %zu", (size_t) MAX_INSTRUCTIONS);
    return ERR;
  }
  temp = (Instruction *) realloc(*code, newsize * sizeof(Instruction));
  if (!temp) {
    LOG("realloccode", "out of memory");
    return ERR;
  }
  *code = temp;
  return OK;
}

static int realloccode_pkg (Package *pkg, size_t newsize) { 
  if (realloccode(&(pkg->code), newsize)) return CODEGEN_ERR_OOM;
  pkg->codesize = newsize; 
  return OK; 
} 

static int nextinstruction (Package *pkg) {
  /*   if(pkg->codenext > pkg->codesize) printf("codenext = %u, codesize=%u\n", pkg->codenext, pkg->codesize); */
  assert( pkg->codenext <= pkg->codesize );
  if (pkg->codenext == pkg->codesize)
    if (realloccode_pkg(pkg, pkg->codesize * 2))
      return CODEGEN_ERR_OOM;
  return pkg->codenext++;
}

static int addinstruction1 (Package *pkg, Opcode op) {
  int i = nextinstruction(pkg);
  if (i < 0) return i;		/* Error */
  setopcode(&getinstr(pkg, i), op);
  return i;
}

static int addinstruction (Package *pkg, Opcode op) {
  int i = addinstruction1(pkg, op);
  if (i < 0) return i;		/* Error */
  /* Consistency check */
  assert( opcode(&getinstr(pkg, i)) == op );
#if (CODEGENDEBUG)
    printf("Adding instruction %d, opcode %s\n", i, OPCODE_NAME(op));
#endif
  return i;
}

/* ----------------------------------------------------------------------------- */
/* Code generation                                                               */
/* ----------------------------------------------------------------------------- */

static int gen (CodegenState *CS, Node *node, int opt, int tt, const Charset *fl);

static int addinstruction_char (Package *pkg, Opcode op, int c) {
  int i = addinstruction(pkg, op); /* instruction */
  if (i < 0) return i;		   /* Error */
  setichar(&getinstr(pkg, i), c);
  assert(ichar(&getinstr(pkg, i)) == (c & 0xFF));
  assert(!(c & 0xFFFFFF00));	/* ensure only 8 bits are being used */
  assert(sizei(&getinstr(pkg, i)) == 1);
  return i;
}

static int addinstruction_aux (Package *pkg, Opcode op, int k) {
  int i = addinstruction(pkg, op); /* instruction */
  if (i < 0) return i;		/* Error */
  setaux(&getinstr(pkg, i), k);
  assert(aux(&getinstr(pkg, i)) == (k & 0xFFFFFF));
  assert(!(k & 0xFF000000));	/* ensure only 24 bits are being used */
  assert(sizei(&getinstr(pkg, i)) == 1);
  return i;
}

static int addinstruction_offset (Package *pkg, Opcode op, int offset) {
  int err, i = addinstruction1(pkg, op);
  if (i < 0) return i;		/* Error */
  err = nextinstruction(pkg);	/* space for offset */
  if (err < 0) return err;	/* Error */
  setaddr(&getinstr(pkg, i), offset);
  setaux(&getinstr(pkg, i), 0);	/* default value */
  assert(addr(&getinstr(pkg, i)) == offset);
  assert(aux(&getinstr(pkg, i)) == 0);
  return i;
}

/*
** Code an IChar instruction, or IAny if there is an equivalent
** test dominating it
*/
static void codechar (CodegenState *CS, int c, int tt) {
  Instruction *inst = &getinstr(CS->pkg, tt);
  if ((tt >= 0) && opcode(inst) == ITestChar && ichar(inst) == c)
    addinstruction(CS->pkg, IAny);
  else {
    addinstruction_char(CS->pkg, IChar, c);
  }
}

/*
** Check whether a charset is empty (returns IFail), singleton (IChar),
** full (IAny), or none of those (ISet). When singleton, '*c' returns
** which character it is. (When generic set, the set was the input,
** so there is no need to return it.)
*/
static Opcode charsettype (const uint8_t *cs, int *c) {
  int count = 0;  /* number of characters in the set */
  int i, b;
  int candidate = -1;  /* candidate position for the singleton char */
  for (i = 0; i < CHARSETSIZE; i++) {  /* for each uint8_t */
    b = cs[i];
    if (b == 0) {  /* is uint8_t empty? */
      if (count > 1)  /* was set neither empty nor singleton? */
        return ISet;  /* neither full nor empty nor singleton */
      /* else set is still empty or singleton */
    }
    else if (b == 0xFF) {  /* is uint8_t full? */
      if (count < (i * BITSPERCHAR))  /* was set not full? */
        return ISet;  /* neither full nor empty nor singleton */
      else count += BITSPERCHAR;  /* set is still full */
    }
    else if ((b & (b - 1)) == 0) {  /* has uint8_t only one bit? */
      if (count > 0)  /* was set not empty? */
        return ISet;  /* neither full nor empty nor singleton */
      else {  /* set has only one char till now; track it */
        count++;
        candidate = i;
      }
    }
    else return ISet;  /* uint8_t is neither empty, full, nor singleton */
  }
  switch (count) {
    case 0: return IFail;  /* empty set */
    case 1: {  /* singleton; find character bit inside uint8_t */
      b = cs[candidate];
      *c = candidate * BITSPERCHAR;
      if ((b & 0xF0) != 0) { *c += 4; b >>= 4; }
      if ((b & 0x0C) != 0) { *c += 2; b >>= 2; }
      if ((b & 0x02) != 0) { *c += 1; }
      return IChar;
    }
    default: {
       assert(count == CHARSETSIZE * BITSPERCHAR);  /* full set */
       return IAny;
    }
  }
}

/*
** Add a charset postfix to an instruction
*/
static void addcharset (Package *pkg, const uint8_t *cs) {
  int err, i, p = gethere(pkg);
  /* make space for buffer */
  for (i = 0; i < (int)CHARSETINSTSIZE - 1; i++) err = nextinstruction(pkg);
  if (err < 0) {
    LOG("addcharset", "out of memory"); /* TODO: return something? exit? */
  }
  /* fill buffer with charset */
  loopset(j, getinstr(pkg, p).buff[j] = cs[j]);
}

/*
** code a char set, optimizing unit sets for IChar, "complete"
** sets for IAny, and empty sets for IFail; also use an IAny
** when instruction is dominated by an equivalent test.
*/
static void codecharset (CodegenState *CS, const uint8_t *cs, int tt) {
  int c = 0;  /* (=) to avoid warnings */
  Opcode op = charsettype(cs, &c);
  switch (op) {
    case IChar: codechar(CS, c, tt); break;
    case ISet: {  /* non-trivial set? */
      if (tt >= 0 && opcode(&getinstr(CS->pkg, tt)) == ITestSet &&
          cs_equal(cs, getinstr(CS->pkg, tt + 1).buff))
        addinstruction(CS->pkg, IAny);
      else {
        addinstruction(CS->pkg, ISet);
        addcharset(CS->pkg, cs);
      }
      break;
    }
  default: addinstruction_char(CS->pkg, op, c); break;
  }
}

/*
** code a test set, optimizing unit sets for ITestChar, "complete"
** sets for ITestAny, and empty sets for IJmp (always fails).
** 'e' is true iff test should accept the empty string. (Test
** instructions in the current VM never accept the empty string.)
*/
static int codetestset (CodegenState *CS, Charset *cs, int e) {
  Opcode op;
  int i, c = 0;
  if (e) return NOINST;  /* no test */
  else {
    op = charsettype(cs->cs, &c);
    switch (op) {
    case IFail: return addinstruction_offset(CS->pkg, IJmp, 0);  /* always jump */
    case IAny: return addinstruction_offset(CS->pkg, ITestAny, 0);
    case IChar: {
      i = addinstruction_offset(CS->pkg, ITestChar, 0);
      setichar(&getinstr(CS->pkg, i), c);
      return i;
    }
    case ISet: {
      i = addinstruction_offset(CS->pkg, ITestSet, 0);
      addcharset(CS->pkg, cs->cs);
      return i;
    }
    default:
      LOGf("codetestset", "inappropriate opcode %d", op);
      return CODEGEN_ERR_INTERNAL;
    }
  }
}

/*
** Not predicate; optimizations:
** In any case, if first test fails, 'not' succeeds, so it can jump to
** the end. If pattern is headfail, that is all (it cannot fail
** in other parts); this case includes 'not' of simple sets. Otherwise,
** use the default code (a choice plus a failtwice).
*/
static int codenot (CodegenState *CS, Node *node) {
  int test, stat, pchoice, err;
  Charset st;
  int e = exp_getfirst(node, fullset, &st);
  if (e < 0) return e;		/* Error */
  addinstruction_aux(CS->pkg, ISuspendCaptures, 1); /* aux is any non-zero value */
  test = codetestset(CS, &st, e);
  if (test < NOINST) return test; /* Error */
  stat = exp_headfail(node);
  if (stat < 0) return stat;   /* Error */
  if (stat)  /* test (fail(p1)) -> L1; fail; L1:  */
    addinstruction(CS->pkg, IFail);
  else {
    /* test(fail(p))-> L1; choice L1; <p>; failtwice; L1:  */
    pchoice = addinstruction_offset(CS->pkg, IChoice, 0);
    err = gen(CS, node, 0, NOINST, fullset);
    if (err) return err;
    addinstruction(CS->pkg, IFailTwice);
    jumptohere(CS->pkg, pchoice);
  }
  jumptohere(CS->pkg, test);
  return 0;			/* Success */
}

/*
** Code first child of a sequence
** (second child is called in-place to allow tail call)
** Return 'tt' for second child
*/
static int codeseq1 (CodegenState *CS, Node *p1, Node *p2,
                     int *tt, const Charset *fl) {
  uint32_t min, max;
  Charset fl1;
  int l, err;
  if (exp_needfollow(p1)) {
    err = exp_getfirst(p2, fl, &fl1);  /* p1 follow is p2 first */
    if (err < 0) return err;
    err = gen(CS, p1, 0, *tt, &fl1);
    if (err) return err;
  } else  { /* use 'fullset' as follow */
    err = gen(CS, p1, 0, *tt, fullset);
    if (err) return err;
  }
  l = exp_patlen(p1, &min, &max);
  if (l < 0) return l;			      /* Error */
  if (min > 0) { /* will 'p1' always consume something? */
    *tt = NOINST;  /* invalidate test */
    return 0;	   /* Success */
  } else {
    /* else 'tt' still protects child2 */
    return 0;	   /* Success */
  }
}

/*
** Add a capture instruction:
** 'op' is the capture instruction; 'cap' the capture kind;
** 'idx' the key into ktable;
*/
static int addinstcap (Package *pkg, Opcode op, int cap, int idx) {
  int i;
  i = addinstruction_offset(pkg, op, idx);
  setaux(&getinstr(pkg, i), cap);
  assert(aux(&getinstr(pkg, i)) == cap);
  return i;
}

static int codecapture (CodegenState *CS, Node *node, int tt, const Charset *fl) {
  int err;
  int symidx;
  symidx = symboltable_add(CS->pkg->symtab,
			   context_retrieve(CS->context, node->u.n),
			   SYMBOL_LOCAL,
			   SYMBOL_TYPE_CAPNAME,
			   0, 0, NULL);
  if (symidx < 0) return symtab_err_to_codegen_err(symidx);
  addinstcap(CS->pkg, IOpenCapture, node->cap, symidx);
  if (node->cap == Crosieconst) {
    assert(child1(node)->tag == TTrue);
    /* TODO: Insert a new instruction that includes
       node->v.constant_capture_offset and causes the vm to do the
       right thing.
    */
    symidx = symboltable_add(CS->pkg->symtab,
			     context_retrieve(CS->context, node->v.constant_capture_offset),
			     SYMBOL_LOCAL,
			     SYMBOL_TYPE_CAPNAME,
			     0, 0, NULL);
    if (symidx < 0) return symtab_err_to_codegen_err(symidx);
    addinstruction_offset(CS->pkg, ICloseConstCapture, symidx);
  } else {
    err = gen(CS, child1(node), 0, tt, fl);
    if (err) return err;
    addinstruction(CS->pkg, ICloseCapture);
  }
  return 0;			/* Success */
}

static int32_t lookup_placeholder (CodegenState *CS, Pattern *target) {
  Index i;
  SCC_Component component;
  Index cnum;
  Pattern *p;
  i = CS->current_order_index;	/* currently compiling this pattern */
  cnum = CS->context->cst->g->order->elements[i].cnum; /* current SCC component number */
  do {
    if (i >= CS->context->cst->g->next) {
      LOGf("lookup_placeholder", "reached end of graph from strongly connected component %d", cnum);
      return CODEGEN_ERR_INTERNAL;
    }
    component = CS->context->cst->g->order->elements[i];
    if (component.cnum != cnum) {
      LOGf("lookup_placeholder", "reached end of strongly connected component %d", cnum);
      return CODEGEN_ERR_INTERNAL;
    }
    p = CS->context->cst->g->deplists[component.vnum][0];
    i++;
  } while (p != target);
  return i - CS->current_order_index ; /* placeholder >= 1 */
}

/* Generate ICall instruction for local calls (within the current package). */
static int codeICall (CodegenState *CS, Node *node) {
  int stat;
  Index target_address;
  Pattern *target;
  assert( node );
  target = node->v.pat;
  assert( target );

  /* Examine the target pattern of the call.  If the code of the
     target has been generated already, it will have a valid
     entrypoint (!= -1).  This is the easy path: we can emit an ICall
     instruction containing that entrypoint.

     If the target has not had code generated yet, we emit an ICall
     instruction containing a placeholder value where the entrypoint
     should be.  A placeholder is < 0, so it cannot be confused with a
     valid entrypoint.

     Note that a placeholder is only needed in the following
     situation: We are generating code for a strongly connected
     component with 2 or more vertices (a set of mutually recursive
     patterns) or with 1 recursive pattern.  The reason we do not need
     placeholders in other situations is that we are generating code
     for the patterns in topological sort order.  As a result, we
     almost always generate the code for all of a pattern's
     dependencies before we generate code for the pattern itself.

     To choose a value for the placeholder, we look in the
     topologically ordered graph structure and find the current
     strongly connected component.  The call target must be one of the
     patterns in that SCC at or after the pattern being compiled.  The
     absolute value of the placeholder is the 1-based index into the
     current SCC, starting at the current_order_index, where the
     target pattern is found.

     In fixupSCC(), we post-process the code generated for each
     pattern in a recursive SCC.  We scan the code for an ICall with a
     negative target address (i.e. a placeholder), which we convert
     into an index into the ordered graph.  The pattern at that index
     is the target of the call.
 */

  target = node->v.pat;
  assert( target );
  target_address = target->entrypoint;

  if (target_address < 0) {
    if (target_address == -1) {
      stat = lookup_placeholder(CS, target);
#if (CODEGENDEBUG) 
	printf("codeICall: Note: pattern %p not yet compiled -- placeholder value is %d", (void *) target, stat); 
#endif
      if (stat <= 0) return CODEGEN_ERR_INTERNAL; /* already logged */
      target_address = - stat;	                  /* placeholder < 0 */
    } else {
      LOGf("codeICall", "invalid call target %d", target_address);
      return CODEGEN_ERR_INTERNAL;
    }
  }
  addinstruction_offset(CS->pkg, ICall, target_address);
  return OK;
}

static Index get_package_num (CodegenState *CS, Package *pkg) {
  Index index;
  if (!CS->context) {
    LOG("codeXCall", "null context field in codegen state");
    return CODEGEN_ERR_INTERNAL;
  }
  if (!CS->context->packages) {
    LOG("codeXCall", "null package table field in context");
    return CODEGEN_ERR_INTERNAL;
  }
  index = packagetable_getnum(CS->context->packages, pkg);
  if (index < 0) {
    LOGf("codeXCall", "no entry in package table for package %p", (void *) pkg);
    return CODEGEN_ERR_NO_PACKAGE;
  }
  return index;
}

/* Generate code for a call to a pattern in a compiled package */
static int codeXCall (CodegenState *CS, Node *node) {
  int i;
  XRef xref;
  Index package_num;
  assert( node );
  set_xref_from_node(xref, node);
  package_num = get_package_num(CS, xref.pkg);
  if (package_num < 0) return package_num; /* already logged */
  /* aux field (24 bits) holds index into package table */
  /* addr fields (32 bits) holds index into that package's symtab */
  i = addinstruction_offset(CS->pkg, IXCall, xref.index);
  setaux(&getinstr(CS->pkg, i), package_num);
  return OK;
}

/*
** The PEG 'and' predicate, i.e. unbounded lookahead
** Optimization: 
**     fixedlen(p) = n ==> <&p> == <p>; behind n
** Captures are suspended during the lookahead.
*/
/* TODO: Make these transformations at IR (tree) level:
   - If target pattern is False, compile to simply False
   - If target pattern is nullable AND nofail, this is a no-op 
*/
#define FIXEDLEN(res, min, max) ((res != EXP_UNBOUNDED) && (min == max))
static int codeahead (CodegenState *CS, Node *node, int tt) {
  int err, pcommit, pchoice;
/*   uint32_t min, max; */
/*   int fixedlen; */
/*   err = exp_patlen(node, &min, &max); */
/*   fixedlen = FIXEDLEN(err, min, max); */
/*   if (fixedlen && !exp_hascaptures(node)) { */
/*     err = gen(CS, node, 0, tt, fullset); */
/*     if (err) return err; */
/*     if (fixedlen) { */
/*       assert( min == max ); */
/*       addinstruction_aux(CS->pkg, IBehind, min); */
/*     } */
/*   } */
/*   else {   */
  /* default: Choice L1; SuspendCaptures; p1; BackCommit L2; L1: Fail; L2: */ 
  pchoice = addinstruction_offset(CS->pkg, IChoice, 0);
  addinstruction_aux(CS->pkg, ISuspendCaptures, 1); /* aux is any non-zero value */
  err = gen(CS, node, 0, tt, fullset);
  if (err) return err;
  pcommit = addinstruction_offset(CS->pkg, IBackCommit, 0);
  jumptohere(CS->pkg, pchoice);
  addinstruction(CS->pkg, IFail);
  jumptohere(CS->pkg, pcommit);
/*   } */
  return 0;			/* Success */
}

/*

<behind(p)> ==>
      Choice SRCH
      SuspendCaptures and Set Fencepost
      i = 0
REDO: Behind BY min_length(p)     ;; Maybe use a register to hold the Behind argument?
      <p>
      If at fencepost, jump to OK
SRCH: Behind BY i++
      JUMP REDO
OK:   Commit     
     
*/
/* TODO: Make these transformations at IR (tree) level:
   - If target pattern is False, compile to simply False
   - If target pattern is nullable AND nofail, this is a no-op 
*/
static int codebehind (CodegenState *CS, Node *node) { 
  uint32_t min, max;
  int err, pcommit, pchoice;
  err = exp_patlen(node, &min, &max);	    /* FUTURE: Cache this the first time it's computed! */
  if (err < 0) return err;
  pchoice = addinstruction_offset(CS->pkg, IChoice, 0);
  addinstruction_aux(CS->pkg, ISuspendCaptures, 1); /* aux is any non-zero value */
  addinstruction_aux(CS->pkg, IBehind, min);        /* TEMP: Coding only for the min length right now */
  err = gen(CS, node, 0, NOINST, fullset);
  if (err < 0) return err;
  /* TODO: Test if at fencepost */
  pcommit = addinstruction_offset(CS->pkg, ICommit, 0);
  jumptohere(CS->pkg, pchoice);
  addinstruction(CS->pkg, IFail);
  jumptohere(CS->pkg, pcommit);

  printf("*** TEMP: Coding lookbehind without ending fencepost AND without search\n");
  return 0;
} 

/*
** Repetion; optimizations:
** When pattern is a charset, can use special instruction ISpan.
** When pattern is head fail, or if it starts with characters that
** are disjoint from what follows the repetions, a simple test
** is enough (a fail inside the repetition would backtrack to fail
** again in the following pattern, so there is no need for a choice).
** When 'opt' is true, the repetion can reuse the Choice already
** active in the stack.
*/
/* Returns 0 for OK, or error < 0 */
static int coderep (CodegenState *CS, Node *node, int opt, const Charset *fl) {
  Charset st;
  int err, stat, e1, jmp, test, commit, l2, pchoice;
  if (to_charset(node, &st)) {
    addinstruction(CS->pkg, ISpan);
    addcharset(CS->pkg, st.cs);
  }
  else {
    e1 = exp_getfirst(node, fullset, &st);
    if (e1 < 0) return e1;	/* Error */
    stat = exp_headfail(node);
    if (stat < 0) return stat;	/* Error */

    if (stat || (!e1 && cs_disjoint(&st, fl))) {
      /* L1: test (fail(p1)) -> L2; <p>; jmp L1; L2: */
      test = codetestset(CS, &st, 0);
      if (test < NOINST) return test;
      err = gen(CS, node, 0, test, fullset);
      if (err) return err;
      jmp = addinstruction_offset(CS->pkg, IJmp, 0);
      jumptohere(CS->pkg, test);
      jumptothere(CS->pkg, jmp, test);
    }
    else {
      /* test(fail(p1)) -> L2; choice L2; L1: <p>; partialcommit L1; L2: */
      /* or (if 'opt'): partialcommit L1; L1: <p>; partialcommit L1; */
      test = codetestset(CS, &st, e1);
      if (test < NOINST) return test; /* Error */
      pchoice = NOINST;
      if (opt) 
	jumptohere(CS->pkg, addinstruction_offset(CS->pkg, IPartialCommit, 0));
      else
        pchoice = addinstruction_offset(CS->pkg, IChoice, 0);
      l2 = gethere(CS->pkg);
      err = gen(CS, node, 0, NOINST, fullset);
      if (err) return err;
      commit = addinstruction_offset(CS->pkg, IPartialCommit, 0);
      setaux(&getinstr(CS->pkg, commit), 1); /* Special IPartialCommit */
      jumptothere(CS->pkg, commit, l2);
      jumptohere(CS->pkg, pchoice);
      jumptohere(CS->pkg, test);
    }
  }
  return OK;			/* Success */
}

/*
** Choice; optimizations: 
** 
** N.B. This is the ONLY procedure that can set the 'opt' parameter to true.
**
** - When p1 is headfail or when first(p1) and first(p2) are disjoint,
**   then a character not in first(p1) cannot go to p1, and a character
**   in first(p1) cannot go to p2 (as it is not in first(p2)).
**   (The optimization is not valid if p1 accepts the empty string,
**   as then there is no character at all...)
** - When p2 is empty and opt is true; a IPartialCommit can reuse
**   the Choice already active in the stack.
*/
/* Returns 0 for OK, or error < 0 */
static int codechoice (CodegenState *CS, Node *p1, Node *p2, int opt, const Charset *fl) {
  int test, jmp, headfailp1, pcommit, pchoice, err = 0;
  int haltp2 = (p2->tag == THalt);
  int emptyp2 = (p2->tag == TTrue);
  Charset cs1, cs2;
  int e1 = exp_getfirst(p1, fullset, &cs1);
  if (e1 < 0) return e1;
  headfailp1 = exp_headfail(p1);
  if (headfailp1 < 0) return headfailp1; 	/* error */

  if (!haltp2 && 
      (headfailp1 || 
       (!e1 && 
	((err = exp_getfirst(p2, fl, &cs2)), cs_disjoint(&cs1, &cs2))))) { 
    if (err < 0) return err;
    /* <p1 / p2> == test (fail(p1)) -> L1 ; p1 ; jmp L2; L1: p2; L2: */
    test = codetestset(CS, &cs1, 0);
    if (test < NOINST) return test;
    jmp = NOINST;
    err = gen(CS, p1, 0, test, fl);
    if (err) return err;
    if (!emptyp2)
      jmp = addinstruction_offset(CS->pkg, IJmp, 0); 
    jumptohere(CS->pkg, test);
    err = gen(CS, p2, opt, NOINST, fl);
    if (err) return err;
    jumptohere(CS->pkg, jmp);
  }
  else if (opt && emptyp2) {
    /* p1? == IPartialCommit; p1 */
    jumptohere(CS->pkg, addinstruction_offset(CS->pkg, IPartialCommit, 0));
    err = gen(CS, p1, 1, NOINST, fullset);
    if (err) return err;
  }
  else {
    /* <p1 / p2> == 
        test(first(p1)) -> L1; choice L1; <p1>; commit L2; L1: <p2>; L2: */
    test = codetestset(CS, &cs1, e1);
    if (test < NOINST) return test;
    pchoice = addinstruction_offset(CS->pkg, IChoice, 0);
    err = gen(CS, p1, emptyp2, test, fullset);
    if (err) return err;
    pcommit = addinstruction_offset(CS->pkg, ICommit, 0);
    jumptohere(CS->pkg, pchoice);
    jumptohere(CS->pkg, test);
    err = gen(CS, p2, opt, NOINST, fl);
    if (err) return err;
    jumptohere(CS->pkg, pcommit);
  }
  return 0;			/* Success */
}

/* TODO: What conditions can or should be tested here? */
/* static void codebackref (Package *pkg, Node *node) {  */
/*   addinstruction_offset(pkg, IBackref, node->v.n); */
/* } */

static void fixup_pattern (CodegenState *CS, Pattern *p, Index SCCstart) {
  Index vnum;
  Index actual;
  Index i, last;
  Pattern *target;
  Package *pkg = CS->pkg;
  Instruction *inst, *code = pkg->code;
  DepList *deplists = CS->context->cst->g->deplists;
  SCC_Component *ordered_elements = CS->context->cst->g->order->elements;
  last = p->entrypoint + p->codesize;
#if (CODEGENDEBUG) 
    printf("    Entering fixup_pattern %p\n", (void *) p); 
#endif
  for (i = p->entrypoint; i < last; i += sizei(&code[i])) {
    inst = &code[i];
#if (CODEGENDEBUG) 
      printf("    PC = %d  opcode = %s (%d)\n", i, OPCODE_NAME(opcode(inst)), opcode(inst));
#endif
    if (opcode(inst) == ICall) {
      if (addr(inst) < 0) {
	vnum = ordered_elements[SCCstart - addr(inst) - 1].vnum;
	target = deplists[vnum][0];
	actual = target->entrypoint;
	assert( actual >= 0 );
#if (CODEGENDEBUG) 
	  printf("    ICall target is %d, fixing to vnum %d == target %p == addr %d\n", 
		 addr(inst), vnum, (void *) target, actual); 
#endif
	setaddr(&getinstr(pkg, i), actual);
      }
    }
  } /* for each instruction generated for pattern p */
}

static void fixupSCC (CodegenState *CS, Index start, Index finish) {
  Pattern *p;
  Index i, n;
  SCC_Component element;
  DepList *deplists = CS->context->cst->g->deplists;
  SCC_Component *ordered_elements = CS->context->cst->g->order->elements;
  n = finish - start + 1;
  assert( n > 0 );
  assert( start >= 0 );
  for (i = 0; i < n; i++) {
    element = ordered_elements[start + i];
    p = deplists[element.vnum][0];
    fixup_pattern(CS, p, start);
  }
}

/* ----------------------------------------------------------------------------- */
/* Code generation dispatch                                                      */
/* ----------------------------------------------------------------------------- */
  
static void peephole (Package *pkg);

static int check_optimzer_flag(short inp, short expected){
    return inp & expected;
}

/* Returns 0 for OK, or error < 0.
   On success, sets ep->code, ep->codesize.
   'optim_mask' governs the switching on and off of optimizers.
*/
int codegen (Context *C, Pattern *entrypoint_pattern, Package *pkg, short optim_mask) {
  int stat;
  int symidx;
  uint8_t i;
  Pattern *pat;
  CodegenState *CS;
  SCC_Component element;
  Index cnum = 0;		/* the only SCC component number NOT used */
  Index cnum_start = 0;

  CS = (CodegenState *) alloca(sizeof(CodegenState));
  if (!CS) {
    LOG("codegen", "out of memory");
    return CODEGEN_ERR_OOM;
  }
  CS->context = C;
  CS->pkg = pkg;

#if (CODEGENDEBUG) 
    printf("-------------------------------------------------\n"); 
    printf("-- Entering codegen for package %p --\n", (void *) pkg); 
    printf("-------------------------------------------------\n"); 
#endif
  if (!C) {
    LOG("codegen_pattern", "null context argument");
    return CODEGEN_ERR_NULL;
  }
  if (!C->cst) {
    LOG("codegen_pattern", "context has no compilation state");
    return CODEGEN_ERR_NOTCOMPILED;
  }
  if (!C->cst->g->order) {
    LOG("codegen_pattern", "compilation state has no ordered graph");
    return CODEGEN_ERR_INTERNAL;
  }
  for (i = 0; i < C->cst->g->order->top; i++) {
    CS->current_order_index = i;
    element = C->cst->g->order->elements[i];
    /* Are we starting a new strongly-connected component? */
    if (element.cnum != cnum) {
      /* Was there a previous one, and was it recursive?  */
      if (cnum > 0) fixupSCC(CS, cnum_start, i-1); 
      /* Set up to track the next SCC */
      cnum = element.cnum;
      cnum_start = i;
    }
    pat = C->cst->g->deplists[element.vnum][0];
    if (!pat->fixed) {
      LOGf("codegen_pattern", "pattern %p has no 'fixed' expression tree", (void *) pat);
      return CODEGEN_ERR_NOTCOMPILED;
    }
    if (!pattern_has_flag(pat, PATTERN_READY)) {
      LOGf("codegen_pattern", "pattern %p not compiled", (void *) pat);
      return CODEGEN_ERR_NOTCOMPILED;
    }
    /* Allocate initial space for code, if needed */
    assert( pkg->codenext <= pkg->codesize );
    if ((pkg->codesize < INITIAL_CODESIZE) && (realloccode_pkg(pkg, INITIAL_CODESIZE)))
	return CODEGEN_ERR_OOM;
    assert( pkg->codenext <= pkg->codesize );
    /* Save the entrypoint for the code we are going to generate */
    pat->entrypoint = pkg->codenext;
    /* If the caller specified an entrypoint pattern, save it in the package struct */
    if (pat == entrypoint_pattern) pkg->ep = pkg->codenext;
#if (CODEGENDEBUG) 
      printf(";; pattern %p has entrypoint %d\n", (void *) pat, pat->entrypoint); 
#endif
    /* And generate the code, writing into the code vector in pkg */
    stat = gen(CS, pat->fixed->node, 0, NOINST, fullset);
    if (stat < 0) return stat;
    /* Finally, add a return instruction */
    addinstruction(pkg, IRet);
    pat->codesize = pkg->codenext - pat->entrypoint;
#if (CODEGENDEBUG)
      printf(";;   pattern %p has codesize %d\n", (void *) pat, pat->codesize); 
#endif
    symidx = symboltable_add(pkg->symtab,
			     bound_name(C, pat),
			     SYMBOL_GLOBAL,
			     SYMBOL_TYPE_PATTERN,
			     pat->codesize,
			     pat->entrypoint,
			     &pat->meta);
    if (symidx < 0) return symtab_err_to_codegen_err(symidx);
  } /* for each pattern in the ordered control flow graph */

  if (element.cnum > 0) fixupSCC(CS, cnum_start, i-1);

  if (optim_mask != NO_OPTIM) {

#if (CODEGENDEBUG)
      printf("\n** Before optimizers:\n"); 
      print_package(pkg); 
#endif
    if (check_optimzer_flag(optim_mask, ONLY_INLINE_OPTIM) > 0) {
#if (CODEGENDEBUG)
	printf("\n** Running Inline pass...\n"); 
#endif
      inlining_optimizer(pkg, &naive_policy); 
    }
    if (check_optimzer_flag(optim_mask, ONLY_PEEPHOLE_OPTIM) > 0) {
#if (CODEGENDEBUG) 
	printf("\n** Running Peephole pass...\n"); 
#endif
      peephole(pkg); 
    } 
#if (CODEGENDEBUG) 
      printf("\n** After optimizers:\n");   
      print_package(pkg);
#endif
  }

  stat = realloccode_pkg(pkg, pkg->codenext); /* trim to final size */
  if (stat < 0) return stat;
  assert( pkg->codesize == pkg->codenext );

  return OK;
}

/*
** code generation is recursive; 'opt' indicates that the code is being
** generated as the last thing inside an optional pattern (so, if that
** code is optional too, it can reuse the 'IChoice' already in place for
** the outer pattern). 'tt' points to a previous test protecting this
** code (or NOINST). 'fl' is the follow set of the pattern.
*/

/*
 * The technique and a lot of this code comes directly from lpeg
 * (though it has been modified to reflect Rosie tree types, opcodes,
 * and auxiliary data structures).
 *
** Main code-generation function: dispatch to auxiliary functions
** according to kind of node. ('exp_needfollow' should return true
** only for constructions that use 'fl'.)
*/
static int gen (CodegenState *CS, Node *node, int opt, int tt, const Charset *fl) {
  int err; 
  assert( node ); assert( CS->pkg ); assert( CS->context );
 tailcall: 
  switch (node->tag) { 
  case TChar: codechar(CS, node->u.n, tt); break; 
  case TAny: addinstruction(CS->pkg, IAny); break; 
  case TSet: codecharset(CS, nodebuffer(node), tt); break; 
  case TTrue: break; 
  case TFalse: addinstruction(CS->pkg, IFail); break; 
  case THalt: addinstruction(CS->pkg, IHalt); break; /* rosie */ 
  case TChoice: return codechoice(CS, child1(node), child2(node), opt, fl); break; 
  case TRep: return coderep(CS, child1(node), opt, fl); break; 
  case TBehind: return codebehind(CS, child1(node)); break;  
  case TNot: return codenot(CS, child1(node)); break; 
  case TAhead: return codeahead(CS, child1(node), tt); break; 
  case TCapture: return codecapture(CS, node, tt, fl); break; 
/*   case TBackref: codebackref(CS, node); break;  */
  case TCall: {
    err = codeICall(CS, node);
    if (err) return err;
    break;
  }
  case TXCall: {
    err = codeXCall(CS, node);
    if (err) return err;
    break;
  }
  case TOpenCall: {
    /* All TOpenCall nodes should have been converted to TCall nodes. */
    return CODEGEN_ERR_OPENFAIL;
  }
  case TSeq: {  
    err = codeseq1(CS, child1(node), child2(node), &tt, fl);  /* code 'p1' */  
    if (err) return err;  
    /* gen(pkg, p2, opt, tt, fl); */  
    node = child2(node); goto tailcall;  
  }  
  default: {
    LOGf("codegen", "illegal IR node type %d", node->tag);
    return CODEGEN_ERR_IRTYPE;
  }} 
  return OK;			/* Success */ 
}



static int relative_to_abs(const Instruction *op, const Instruction *p) {
  return (int)(p + (p + 1)->offset - op);
}


/* 
Policy governs the amount of inlining. 
It should return the size of the chunk to be inlined.
return 0 if nothing has to be inlined 
*/
uint32_t naive_policy(Package *pkg, int i){
    int vec_offset;
    SymbolTableEntry *ste = NULL;
    Index prev = SYMBOLTABLE_ITER_START;

    if (opcode(&pkg->code[i]) != ICall) return 0;

    vec_offset = addr(&pkg->code[i]);
    ste = symboltable_iter(pkg->symtab, &prev);
    while(ste != NULL){ 
      if (entry_value(ste) == vec_offset) return entry_size(ste);
      ste = symboltable_iter(pkg->symtab, &prev);
    }

    return 0;
}

/*
Adjusts the offset of the Jump/Call type instructions in the code vector after inlining  
*/
static void adjust_ins_offset(Package *pkg, uint32_t inline_start, uint32_t inline_size, size_t size_of_ins){
    
    Instruction *start = pkg->code; /* start address */
    Instruction *pc = NULL;
    uint32_t abs_addr;
    uint32_t ind = 0;

    while(ind < pkg->codenext){
        pc = &start[ind];
        switch(opcode(pc)){
          case ICall: {
	    if (((uint32_t) addr(pc)) > inline_start){
	      setaddr(pc, addr(pc)+inline_size-size_of_ins);
	    }
	    break;
          }
          /* jump instructions with relative offset */
          case IJmp: case ICommit: case IPartialCommit:
          case IBackCommit: case IChoice: case ITestAny: 
          case ITestChar:  case ITestSet: 
          {
	      abs_addr = relative_to_abs(start, pc);
              if (abs_addr > inline_start && ind < inline_start){
                  setaddr(pc, addr(pc)+inline_size-size_of_ins);
              }
              /* Negative Jump Offset */
              /* Should write Unit Tests for this case */
              else if(abs_addr <= inline_start && ind > inline_start){
                  setaddr(pc, addr(pc)-inline_size+size_of_ins);
              }
              break;
          }
          default: break;
      }
      ind += sizei(pc);
    }
}

/*
Adjusts the SymbolTable Information after inlining  
*/
static void adjust_symbol_table(Package *pkg, uint32_t i, uint32_t inline_size, size_t size_of_ins){
    
    SymbolTableEntry *ste = NULL;
    Index prev = SYMBOLTABLE_ITER_START;

    ste = symboltable_iter(pkg->symtab, &prev);

    while(ste != NULL){
      /* |------------|  Original 
              |+++++++|++++++++++|   inlined     */ 
      if (entry_value(ste) <= ((int32_t) i) && (entry_value(ste) + entry_size(ste) - 1 >= i)){
          entry_size(ste) += inline_size - size_of_ins;
      }
      /*       |------------|  Original
        |+++++|                inlined     */ 
      else if(entry_value(ste) > ((int32_t) i)) {
          entry_value(ste) += inline_size - size_of_ins;
      }
      ste = symboltable_iter(pkg->symtab, &prev);
    }
}


/*
Function Which Performs Inlining
*/
static int inliner(Package *pkg, int i, uint32_t inline_size){
    size_t size_of_ins, num_shift;
    uint32_t new_idx;
    Instruction *temp_vector, *to, *from;
    
    if (opcode(&pkg->code[i]) != ICall) return 0;
    if (inline_size == 0) return sizei(&pkg->code[i]);
    size_of_ins = sizei(&pkg->code[i]); /* Size of Call Instruction */
   
    /* Offset from Which the Instructions have to be inlined */
    new_idx = addr(&pkg->code[i]);

    inline_size--; /* Leave out the last Return Instruction */

    /* Confirm Whether the last instruction is a Return */
    assert(opcode(&pkg->code[new_idx+inline_size]) == IRet); 

    if (pkg->codenext + inline_size - size_of_ins > pkg->codesize){
       if (realloccode_pkg(pkg, 2*pkg->codesize) == CODEGEN_ERR_OOM) return size_of_ins;
    }
    
    /* Have the vector (which is to be inlined) stored in a different address */
    temp_vector = (Instruction *)malloc(sizeof(Instruction)*inline_size);
    memcpy(temp_vector, &pkg->code[new_idx], inline_size*sizeof(Instruction));

    /* Shift the instructions */
    to = &pkg->code[i+inline_size]; /* Since the Call instruction also have to be replaced */ 
    from = &pkg->code[i+size_of_ins]; /* From the next instruction after the call instruction at i*/
    num_shift = pkg->codenext-i-size_of_ins;
    memmove(to, from, num_shift*sizeof(Instruction));

    pkg->codenext += inline_size - size_of_ins;

    /* Copy the inline Instructions */
    memcpy(&pkg->code[i], temp_vector, inline_size*sizeof(Instruction)); /* Filling Spaces */
    
    /* Adjust the Instructions in the inlined vector */
    adjust_ins_offset(pkg, i, inline_size, size_of_ins); 
    adjust_symbol_table(pkg, i, inline_size, size_of_ins);
   
    /* print_package(pkg); */
    free(temp_vector);

    return inline_size;
} 


/* 
Checks for Recursion
Return OK if no recursion and !OK if has recursion
*/
/* TODO: Change this to check the symbol table? */
static int has_recursion(Package* pkg, int i, int inline_size){
    Instruction *start, *pc;
    int start_location, ind;

    /* && !(i >= addr(pc) && i < addr(pc)+inline_size) */
    if (opcode(&pkg->code[i]) != ICall) return 0;

    start = pkg->code; /* start address */
    pc = NULL;

    /* Location from Which the Instructions have to be inlined */
    start_location = addr(&pkg->code[i]);
    ind = start_location;

    while(ind < start_location + inline_size){
        pc = &start[ind];
        switch(opcode(pc)){
          case ICall: {
              if (addr(pc) == start_location){ /* Found Recursion */
                  return !OK;
              }
              break;
          }
          default: break;
      }
      ind += sizei(pc);
    }
    return OK;
}

/*
inlines the instructions, when policy returns true for a call instruction
*/
void inlining_optimizer(Package *pkg, uint32_t (*policy)(Package *pkg, int i)){
  
    uint32_t i = 0;
    Instruction *pc = NULL;
    uint32_t inline_size = 0;
    int recur_check;
    
    while(i < pkg->codenext) {
        pc = &pkg->code[i];
        if (opcode(pc) == ICall){
           inline_size = policy(pkg, i);
           /* TODO: Allow Recursion for certain depth*/
           recur_check = has_recursion(pkg, i, inline_size);
           if (recur_check == OK && inline_size > 1) { 
              inliner(pkg, i, inline_size);
              continue;
           }
        }
        i += sizei(pc);
    }  
}

/* ----------------------------- Peephole Helper Methods ---------------------------- */

/*
** Find the final destination of a sequence of jumps
*/
static int finaltarget (Instruction *code, int i) {
  Instruction *pc = &code[i];
  while (opcode(pc) == IJmp) pc += addr(pc);
  return pc - code;
}

/*
** final label (after traversing any jumps)
*/
static int finallabel (Instruction *code, int i) {
  Instruction *pc = &code[i];
  return finaltarget(code, i + addr(pc));
}

/*
** Apply Peephole Optimization over a ICall Instruction
   Returns OK when an optimization is made.
*/
static int optimize_icall(Package *pkg, int i){
  Instruction *target_ins, *next_ins;
  Instruction *codevec = pkg->code; /* Code Vector */
  Instruction *pc = &codevec[i];
  if (opcode(pc) != ICall) return !OK;
  /* Since Call uses Absolute Addressing */
  target_ins = codevec + addr(pc);  
  if (target_ins != pc){
    switch(opcode(target_ins)){
      /* 1 word instructions */
    case INoop: {
      setopcode(pc, opcode(target_ins));
      setopcode(pc+1, INoop);
      return OK;
    }
      /* 2 word instructions with absolute address 
	 TODO: If the destination of a call is another call followed immediately by a return,
	 then we can call the ultimate target directly.  (Eta conversion.)
      */     
    case ICall:{
      /* TODO: TEMP TEMP TEMP */
      return !OK;
      /* FUTURE: If the next instruction is IRet, replace the call+return instructions by a jump. */
	
/*       next_ins = target_ins + sizei(target_ins); */
/*       if ((next_ins < codevec + pkg->codenext) && opcode(next_ins) == IRet && (addr(pc) != addr(target_ins))){ */
/* 	setaddr(pc, addr(target_ins)); */
/* 	return OK; */
/*       } */
/*       else{ */
/* 	return !OK; */
/*       } */
    } 
         
      /* 3 word instructions with relative address  
	 Abs_Addr = target_ins + Relative - start
      */
    case IJmp: {
      setaddr(pc, target_ins + addr(target_ins) - codevec);
      return OK;
    }
      /* This is not Expected Happen*/
    case IEnd: {
      LOGf("codegen_pattern", "IEnd appeared after ICall instruction at Line: %d in Package: %p", i, (void *)pkg);
      return !OK;
    } 
    default: break;
    }
  }
  return !OK;
} 

/*
** Optimize jumps and other jump-like instructions.
** * Update labels of instructions with labels to their final
** destinations (e.g., choice L1; ... L1: jmp L2: becomes
** choice L2)
** * Jumps to other instructions that do jumps become those
** instructions (e.g., jump to return becomes a return; jump
** to commit becomes a commit)
*/
/* MAYBE: MODIFY peephole to take an instruction range so that we can
   run it on the code for one pattern at a time.  We should run it on
   the entire package when the package is the result of compiling a
   single pattern, though.
*/
void peephole (Package *pkg) {
  
  uint32_t final, ft, fft;
  Instruction *code = pkg->code;
  Instruction *inst, *target_inst;
  uint32_t i;
#if (CODEGENDEBUG) 
    printf("Entering peephole optimizer, pkg = %p, %d/%d instructions\n", 
	   (void *) pkg, pkg->codenext, pkg->codesize); 
#endif
  for (i = 0; i < pkg->codenext; i += sizei(&code[i])) {
   redo:
    inst = &code[i];
    switch (opcode(inst)) {
      /* instructions with labels */
    case ICall: {
      /* Unlike all other jumps, calls use an absolute address. */
      if (addr(inst) < 0) {
	/* TODO: Decide how to handle this, after logging it */
	printf("*** \n*** Peephole optimizer: skipping ICall to invalid target %d\n***\n", addr(inst));
	break;
      } else {
        if (optimize_icall(pkg, i) == OK) goto redo;
        else break;
      }
      break;   
    }
    case IPartialCommit: case ITestAny: case IChoice:
    case ICommit: case IBackCommit: 
    case ITestChar: case ITestSet: {
      final = finallabel(code, i);
      jumptothere(pkg, i, final);  /* optimize label */
      break;
    }
    case IJmp: {
      ft = finaltarget(code, i);
      assert( ft < pkg->codenext );
      target_inst = &code[ft];
      /* switch on what this inst is jumping to */
      switch (opcode(target_inst)) {
	/* instructions with unconditional implicit jumps */
      case IRet: case IFail: case IFailTwice:
      case IHalt: case IEnd: {
          code[i] = code[ft];  /* jump becomes that instruction */
          code[i+1].i.code = INoop;    /* 'no-op' for target position */
          break;
      }
      case ICommit: case IPartialCommit:
      case IBackCommit: { /* inst. with unconditional explicit jumps */
          fft = finallabel(code, ft);
          assert( fft < pkg->codenext );
          code[i] = code[ft];  /* jump becomes that instruction... */
          jumptothere(pkg, i, fft);  /* but must correct its offset */
          goto redo;  /* reoptimize its label */
      }
      default: {
          jumptothere(pkg, i, ft);  /* optimize label */
          break;
      }} /* switch */ 
      break;
    } /* case IJmp */
    default: break;
    } /* switch */
  } /* for */
  assert(opcode(inst) == IRet);
}



