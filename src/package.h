/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  package.h                                                                */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#if !defined(package_h)
#define package_h

#include "common.h"
#include "symboltable.h"
#include "instruction.h"

/*

   A PACKAGE is a run-time structure with a code vector, symbol table,
   and string storage.

   A PACKAGE TABLE is an array of packages.  To do matching the
   Matching Virtual Machine takes a Package Table, an Address in that
   table (Package Number, Symbol Index), and input data.

*/

/* Default number of entries in initial package table */
#define PACKAGETABLE_INITSIZE 28

typedef uint16_t PackageNumber;	       /* must be an UNSIGNED type */
#define PACKAGETABLE_MAXSIZE (1 << 12) /* must fit into PackageNumber */
/* To support a larger package table, the symbol table implementation
   must be improved (e.g. to use a hash table) so that new symbols can
   be added quickly.
*/

struct Package;			       /* forward reference */

typedef struct Import {
  struct Package *pkg;	        /* NULL until pkg is "loaded" */
  Index         importpath;   /* Unique identifier (symbol storage offset) */
  Index         prefix;       /* Prefix string for captures in this pkg */
} Import;

#define IMPORTTABLE_INITSIZE 20
#define IMPORTTABLE_MAXSIZE  1000    /* Must be < INT32_MAX/2 */
/* NOTE: package_add_import() performs 2 insertions into the symbol
   table, and each one searches the block storage to see if the string
   already exists.  This is expensive!  To support a number of imports
   beyond, say, 1000, change the symbol table implementation to
   include a hash table.
*/

typedef struct Package {
  SymbolTable        *symtab;	      /* symbol table */
  Instruction  *code;	        
  uint32_t            codenext;	      /* number of instructions in 'code' */
  uint32_t            codesize;	      /* size of 'code', <= codenext */
  uint32_t            ep;	      /* default entrypoint */
  Index             source;         /* symbol storage offset, e.g. filename */
/*   Index    origin;         /\* symbol storage offset, e.g. URL *\/ */
/*   Index    doc;            /\* symbol storage offset, e.g. doc string *\/ */
  uint32_t            import_next;    /* always > 0 */
  uint32_t            import_size;    /* size of import table */
  Import             *imports;	      /* imports[0] describes THIS PACKAGE */
  PackageNumber       number;	      /* index into global package table */ 
  uint8_t             major;	      /* required language major version */
  uint8_t             minor;	      /* minimum language minor version */
} Package;

#define package_importpath(p) ((p)->imports[0].importpath)
#define package_default_prefix(p) ((p)->imports[0].prefix)

typedef struct PackageTable {
  Package      **packages;        /* package table */
  PackageNumber size;		  /* size of packages array */
  PackageNumber next;		  /* next available is packages[next] */
} PackageTable;

/* ----------------------------------------------------------------------------- */
/* Errors                                                                        */
/* ----------------------------------------------------------------------------- */

#define PACKAGE_OK              0	/* OK (must be 0) */
#define PACKAGE_ERR_OOM        -1	/* out of memory */
#define PACKAGE_ERR_SIZE       -2	/* initial table size arg out of range */
#define PACKAGE_ERR_FULL       -3	/* package table or symbol table full (see log message) */
#define PACKAGE_ERR_NULL       -4	/* NULL argument where not allowed */
#define PACKAGE_ERR_ARG        -5	/* invalid argument (see log message) */
#define PACKAGE_ERR_EXISTS     -6	/* importpath already in package table */
#define PACKAGE_NOT_FOUND      -7	/* not found in package table */
#define PACKAGE_ERR_INTERNAL   -8	/* bug */
#define PACKAGE_ERR_STRLEN     -9	/* string arg too long (see log message) */
#define PACKAGE_ERR_IMPORTPATH -10	/* importpath not set */

/* ----------------------------------------------------------------------------- */
/* Interface                                                                     */
/* ----------------------------------------------------------------------------- */

PackageTable *packagetable_new (size_t initial_packagetable_size);
void          packagetable_free (PackageTable *pt);
void          packagetable_free_packages (PackageTable *pt);

/* All args are immutable after being set in the constructor */
Package *package_new (void);
void     package_free (Package *package);
  
int package_set_importpath (Package *p, const char  *importpath);
int package_set_default_prefix (Package *p, const char  *default_prefix);
int package_set_source (Package *p, const char  *source);
int package_set_lang_version (Package *p, uint8_t major, uint8_t minor);
int package_set_code (Package *p, Instruction *code, uint32_t codesize);

/* Adding requires LINEAR TIME currently, to ensure unique import path */
int  packagetable_add     (PackageTable *pt, Package *package);
int  packagetable_remove  (PackageTable *pt, Package *package);

/* Special interface for the unique user package */
int      packagetable_set_user (PackageTable *pt, Package *p);
Package *packagetable_get_user (PackageTable *pt);

/* Retrieval by package number */
Package *packagetable_get (PackageTable *pt, Index package_number);

/* lookup requires LINEAR TIME, because there is no hashtable/index */
Index packagetable_lookup (PackageTable *pt, const char *importpath);
/* getnum requires LINEAR TIME */
Index packagetable_getnum (PackageTable *pt, Package *pkg);

/* Requires LINEAR TIME search of symbol table */
int package_add_import (Package *p, const char *importpath, const char *prefix);
/* Requires LINEAR TIME search of symbol table */
int package_add_string (Package *p, const char *str);

/* Generic utility function */
int walk_instructions (Instruction *p, 
		       int codesize, 
		       /* start of instruction vec, current instruction, context */
		       int (*operation)(Instruction * const, Instruction *, void * const), 
		       void *context);


#endif
