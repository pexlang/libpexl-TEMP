/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  print.c                                                                  */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <ctype.h>

#include "print.h"

#define INDENT(amt) {				\
    printf("%*s", amt, "");			\
  } while (0);

/* ----------------------------------------------------------------------------- */
/* Print env, package table, patterns                                         */
/* ----------------------------------------------------------------------------- */

/* Number of chars to use for a length string: 
   Two 4-char lengths plus a separator character between them.
*/
#define chars_in_lengths_string (2 * 4 + 1)

/* Write exactly 4 chars */
static void write_length (uint32_t len, char *str) {
  uint32_t printlen;
  if (len < 1024) sprintf(str, "%4u", len);
  else {
    printlen = (len + (1<<9)) >> 10;
    if (printlen < 1000) sprintf(str, "%3uK", printlen);
    else {
      printlen = (len + (1<<19)) >> 20;
      if (printlen < 1000) sprintf(str, "%3uM", printlen);
      else {
	printlen = (len + (1<<29)) >> 30;
	assert( printlen < 1000 );
	sprintf(str, "%3uG", printlen);
      }
    }
  }
}

static char *pattern_lengths_to_string (PatternMetaData meta, char *str) {
  /* Always show the min value */
  write_length(metadata_minlen(meta), str);
  /* If unbounded, do not show the max value, which is meaningless */
  if (!has_flag(metadata_flags(meta), PATTERN_UNBOUNDED)) {
    write_length(metadata_maxlen(meta), &str[5]);
    str[4] = '/';
  } else {
    sprintf(&str[4], "    ");
  }
  str[chars_in_lengths_string] = '\0';
  return str;
}

static void print_compact_pattern_metadata (PatternMetaData meta) {
  char lengths_string[chars_in_lengths_string + 1];
  if (has_flag(metadata_flags(meta), PATTERN_READY))
    printf("  %c%c%c%c%c%c  %9s",
	   has_flag(metadata_flags(meta), PATTERN_CAPTURES)   ? 'C' : '-',
	   has_flag(metadata_flags(meta), PATTERN_NULLABLE)   ? '0' : '-',
	   has_flag(metadata_flags(meta), PATTERN_NOFAIL)     ? 'N' : '-',
	   has_flag(metadata_flags(meta), PATTERN_HEADFAIL)   ? 'h' : '-',
	   has_flag(metadata_flags(meta), PATTERN_NEEDFOLLOW) ? 'f' : '-',
	   has_flag(metadata_flags(meta), PATTERN_UNBOUNDED)  ? 'U' : 
	   metadata_fixedlen(meta) ? 'F' : '-',
	   pattern_lengths_to_string(meta, lengths_string));
  else {
    printf("  unset            ");
  }
}

void print_symboltable_stats(SymbolTable *st) {
  printf("Symbol table %p: %ld/%ld used.  Block %ld/%ld bytes used.\n",
	 (void *)st, st->next, st->size, st->blocknext, st->blocksize);
}

void print_symboltable(SymbolTable *st) {
  size_t i;
  const char *name;
  SymbolTableEntry *entry;
  printf("Symbol table %p\n", (void *) st);
  if (st->next == 0) {
    printf("   Empty symbol table\n");
    return;
  }
  printf(" Index  Offset Vis  Type  Size  Value  MetaData    PatLen\n");
  for (i = 0; i < st->next; i++) {
    entry = &(st->entries[i]);
    printf("%5ld: ", i);
    printf("[%5d] ", entry_name_offset(entry));
    printf("%s ", entry_visibility(entry) ? "Glob" : "Loc ");
    printf("%s ", entry_type(entry) ? "Data" : "Patt");
    printf("%5d ", entry_size(entry));
    printf("%6d ", entry_value(entry));
    print_compact_pattern_metadata(entry->meta);
    name = &(st->block[entry_name_offset(entry)]);
    printf("    %s\n", name);
  }
}

/* ----------------------------------------------------------------------------- */
/* Print package, package table                                                  */
/* ----------------------------------------------------------------------------- */

void print_package (Package *p) { 
  if (p) {
    printf("Package %p: size: %d/%d instructions\n", (void *)p, p->codenext, p->codesize); 
    printf("Default entrypoint: %u\n", p->ep); 
    print_instructions(p->code, p->codenext, p->symtab);
  } else {
    printf("NULL package\n");
  }
} 

void print_packagetable_stats (PackageTable *pt) { 
  printf("PackageTable %p: size: %d/%d entries\n", (void *)pt, pt->next, pt->size); 
} 

void print_packagetable (PackageTable *pt) { 
  const char *importpath, *prefix, *source; 
  Package *current; 
  size_t i; 
  printf("    Package        ImportPath   Prefix     Source       Lang   Code           (Size)\n"); 
  for (i = 0; i < pt->next; i++) { 
    current = pt->packages[i]; 
    if (!current) {
      printf("%2ld: %-14p\n", i, (void *) current);
      continue;
    }
    importpath = symboltable_get_name(current->symtab, package_importpath(current)); 
    prefix = symboltable_get_name(current->symtab, package_default_prefix(current)); 
    source = symboltable_get_name(current->symtab, current->source); 
    printf("%2ld: %-14p %-12.12s %-10.10s %-12.12s %2d.%-2d  %-14p (%u)\n", 
	   i, (void *) current,
	   importpath, prefix, source, current->major, current->minor, 
	   (void *)current->code, current->codesize); 
  } 
} 

void print_importtable (Package *p) { 
  const char *importpath, *prefix; 
  uint32_t i; 
  if (!p) {
    printf("NULL package passed to print_importtable\n");
    return;
  } else if (!p->imports) {
    printf("NULL imports field in package passed to print_importtable\n");
    return;
  }
  printf("Import Table for package %p  Import table entries %d/%d\n", (void *) p, p->import_next, p->import_size);
  printf("    Package         ImportPath               Prefix\n"); 
  for (i = 0; i < p->import_next; i++) { 
    printf("%2d  %-14p", i, (void *) p->imports[i].pkg);
    importpath = symboltable_get_name(p->symtab, p->imports[i].importpath); 
    prefix = symboltable_get_name(p->symtab, p->imports[i].prefix); 
    printf("  (%3d) %-12.12s (%3d) %-10.10s\n", 
	   p->imports[i].importpath, importpath,
	   p->imports[i].prefix, prefix);
  } /* for */
} 

/* ----------------------------------------------------------------------------- */
/* Print env, patterns                                                           */
/* ----------------------------------------------------------------------------- */

void print_env_stats(Env *env) {
  Env *root = env;
  while (root->parent) root = root->parent;
  printf("Environment %p size: %d/%d entries, parent: %p, root: %p\n",
	 (void *)env,
	 env->next, env->size,
	 (void *)env->parent,
	 (void *)root);
}

static void print_compact_pattern (Pattern *ep) {
  if (!ep) {
    printf("Error: no pattern!  ");
    return;
  }
  printf(" %p", (void *) ep);
  printf(" EP=%4d", ep->entrypoint);
  print_compact_pattern_metadata(ep->meta);
}

static const char *typename (Value val) {
  switch (val.type) {
  case Eerror_t:       return "<ErrorType>";
  case Eunspecified_t: return "<Unspecified>";
  case Estring_t:      return "<String>";
  case Eint32_t:       return "<Int32>";
  case Epackage_t:     return "<Package>";
  case Epattern_t:     return "<Pattern>";
  case Eexpression_t:  return "<Expression>";
  case Efunction_t:    return "<Function>";
  case Emacro_t:       return "<Macro>";
  default:             return "*ERROR*";
  }
}

static void print_env_value (Value val) {
  printf("%-13s ", typename(val));
  switch (val.type) {
  case Estring_t:
    printf("   #%04d", val.data);
    printf("%13s", "");
    break;
  case Eint32_t:
    printf("%8d", val.data);
    printf("%13s", "");
    break;
  case Eerror_t:
    printf("   %-5d", val.data);
    printf("%13s", "");
    break;
  case Epackage_t:
  case Efunction_t:
  case Emacro_t:
    printf(" %-p", val.ptr);
    printf("%17s", "");
    break;
  case Epattern_t: 
    print_compact_pattern((Pattern *) val.ptr);
    break;
  default:
    printf("%21s", "");
  }
}

#define NUMFLAGS 4
static const char *binding_attribute_flags (Binding *b, char *flagbuf) {
  int i;
  for (i=0; i < NUMFLAGS; i++) flagbuf[i] = ' ';
  if (binding_has(b, Eattr_deleted)) flagbuf[0] = 'D';
  if (binding_has(b, Eattr_visible)) flagbuf[1] = 'V';
  return flagbuf;		/* for convenience */
}

static void print_binding_name (Binding *binding, Context *C) {
  if (C)
    printf("%s", context_retrieve(C, binding->name));
  else
    printf("<interned string %d>", binding->name);
}

void print_env (Env *env, Context *C) {
  Binding *binding;
  char flags[NUMFLAGS];
  Index i, n;
  if (!env) {
    printf("Null environment\n");
    return;
  }
  n = (int) env->next;
  printf("Index  Attr  Value                                  PFlags  PLen  Name\n");
  for (i = 0; i < n; i++) {
    binding = &env->bindings[i];
    assert( binding );
    printf("%4d:", i);
    printf("  %-.*s  ", NUMFLAGS, binding_attribute_flags(binding, flags));
    print_env_value(binding->val);
    if (binding->name) {
      printf("  ");
      print_binding_name(binding, C);
    }
    else {
      printf("  anon/%d", i);
    }
    printf("\n");
  }
}

/* ----------------------------------------------------------------------------- */
/* Print intermediate rep (trees)                                                */
/* ----------------------------------------------------------------------------- */

void print_charset (const uint8_t *st) {
  fprint_charset (stdout, st);
}
void fprint_charset (FILE *channel, const uint8_t *st) {
  int i, first;
  fprintf(channel, " [");
  for (i = 0; i <= UCHAR_MAX; i++) {
    first = i;
    while (i <= UCHAR_MAX && testchar(st, i)) i++;
    if (i - 1 == first)		/* unary range? */
      fprintf(channel, "(%02x)", first);
    else if (i - 1 > first)	/* non-empty range? */
      fprintf(channel, "(%02x-%02x)", first, i - 1);
  }
  fprintf(channel, "]");
}

static const char *tagnames[] = {
  "char", "set", "any",
  "true", "false",
  "rep", "seq", "choice",
  "not", "ahead",
  "call", "xcall",
  "opencall",
  "behind",
  "capture",
  "run-time",
  "function",
  "halt",
  "no-tree"
};

static void printnode (Node *node, int indent, Context *C) {
  int i, c, sibs;
  if (!node)
    { printf("NULL node\n");
      return;
    }
  for (i = 0; i < indent; i++) printf(" ");
  printf("%s", tagnames[node->tag]);
  switch (node->tag) {
  case TChar: {
    c = node->u.n;
    if (isprint(c))
      printf(" '%c'\n", c);
    else
      printf(" (%02X)\n", c);
    break;
  }
  case TSet: {
    print_charset(nodebuffer(node));
    printf("\n");
    break;
  }
  case TBehind: {
    printf(" %d\n", node->u.n);
    printnode(child1(node), indent + 2, C);
    break;
  }
  case TCapture: {
    if (C)
      printf(" kind: %s capname: '%s' ", CAPTURE_NAME(node->cap), context_retrieve(C, node->u.n));
    else
      printf(" kind: %s capname offset: %d ", CAPTURE_NAME(node->cap), node->u.n);
    if (node->cap == Crosieconst) {
      if (C)
	printf("constant value: '%s'\n", context_retrieve(C, node->v.constant_capture_offset));
      else
	printf("constant value offset: %d\n", node->v.constant_capture_offset);
    } else {
      printf("\n");
      printnode(child1(node), indent + 2, C);
    }
    break;
  }
  case TCall: { 
    printf(" %p\n", (void *) node->v.pat);
    break; 
  } 
  case TOpenCall: {
    printf(" to ref(%p, %d)\n", (void *) node->v.env, node->u.index);
    break;
  }
  case TFunction: {
/*     index = node->v.key; */
/*     if (st) name1 = symboltable_get_name(st, index); */
/*     printf(" to: '%s'\n", name1); */
    break;
  }
  default: {
    sibs = numsiblings[node->tag];
    printf("\n");
    if (sibs >= 1) {
      printnode(child1(node), indent + 2, C);
      if (sibs >= 2)
	printnode(child2(node), indent + 2, C);
    }
    break;
  }}
}

void print_exp (Expression *tree, int indent, Context *C) {
  Node *node;
  if (!tree)
    printf("NULL expression\n");
  else {
    printf("(%d node%s)\n", tree->len, tree->len==1 ? "" : "s");
    node = &(tree->node[0]);
    printnode(node, indent, C);
  }
}

void print_value (Value V, int indent, Context *C) {
  switch (V.type) {
  case Epattern_t:
    printf("Value type: PATTERN\n");
    print_pattern((Pattern *) V.ptr, indent, C);
    break;
  case Eunspecified_t:
    printf("Value type: UNSPECIFIED\n");
    break;
  default:
    printf("Error: unhanded value type: %d\n", V.type);
  }
}

void print_cfgraph (CFgraph *g) {
  Index i, j;
  Pattern **deplist;
  
  if (!g) {
    printf("print_cfgraph: NULL graph\n");
    return;
  }
  printf("CFgraph %p of size %d\n", (void *) g, g->next);
  for (i = 0; ((size_t) i) < ((size_t) g->next); i++) {
    deplist = g->deplists[i];
    printf("[%2d] %p: ", i, (void *) deplist[0]);
    j = 1;
    while (deplist[j]) {
      printf(" %p", (void *) deplist[j]);
      j++;
    }
  printf("\n");
  } /* for */
}

void print_sorted_cfgraph (CFgraph *g) {
  void *vptr;
  Index i, prev;
  if (!g) {
    printf("print_sorted_cfgraph: NULL graph argument\n");
    return;
  }
  if (!g->deplists) {
    printf("print_sorted_cfgraph: NULL deplists field in graph %p\n", (void *) g);
    return;
  }
  if (!g->order) {
    printf("print_sorted_cfgraph: NULL order field in graph %p\n", (void *) g);
    return;
  }
  printf("Sorted CFgraph %p\n", (void *) g);
  assert( ((size_t) g->order->top) == ((size_t) g->next) );
  prev = 0;			/* 0 is not a valid component number */
  for (i = 0; i < g->next; i++) {
    if (g->order->elements[i].cnum != prev) {
      prev = g->order->elements[i].cnum;
      printf("Component #%d:\n", prev);
    }
    if (g->order->elements[i].vnum < 0) 
      printf("error! vnum %d out of range\n", g->order->elements[i].vnum);
    else if (((size_t) g->order->elements[i].vnum) >= ((size_t) g->next))
      printf("error! vnum %d out of range\n", g->order->elements[i].vnum);
    else {
      vptr = (void *) *g->deplists[g->order->elements[i].vnum];
      printf("  Vertex %d  %p\n", g->order->elements[i].vnum, vptr);
    }
  } /* for */
}

#define FLAG(name) (pattern_has_flag(p, PATTERN_ ## name) ? "Yes" : "No")
void print_pattern_metadata (Pattern *p, int indent) {
  INDENT(indent); printf("Ready..... %s\n", FLAG(READY));
  if (pattern_has_flag(p, PATTERN_READY)) {
    INDENT(indent); printf("Captures.. %s\n", FLAG(CAPTURES));
    INDENT(indent); printf("Nullable.. %s\n", FLAG(NULLABLE));
    INDENT(indent); printf("Nofail.... %s\n", FLAG(NOFAIL));
    INDENT(indent); printf("Headfail.. %s\n", FLAG(HEADFAIL));
    INDENT(indent); printf("NeedFollow %s\n", FLAG(NEEDFOLLOW));
    INDENT(indent); printf("Unbounded  %s\n", FLAG(UNBOUNDED));
    INDENT(indent); printf("Min len    %d\n", metadata_minlen(p->meta));
    INDENT(indent); printf("Max len    %d\n", metadata_maxlen(p->meta));
    INDENT(indent); printf("Firstset:  "); print_charset(metadata_firstset_ptr(p->meta)->cs);
    printf("\n");
  }
}

void print_pattern (Pattern *p, int indent, Context *C) {
  INDENT(indent);
  printf("Pattern %p: ", (void *) p);
  if (p) {
    print_exp(p->tree, indent, C);
    print_pattern_metadata(p, indent);
  } else {
    INDENT(indent); printf("(null)\n");
  }
}


/* ----------------------------------------------------------------------------- */
/* Print instruction vectors                                                     */
/* ----------------------------------------------------------------------------- */

static void fprintjmp (FILE *channel, const Instruction *op, const Instruction *p) {
  fprintf(channel, "JMP to %d", (int)(p + (p + 1)->offset - op));
}
static void printjmp (const Instruction *op, const Instruction *p) {
  fprintjmp(stdout, op, p);
}

/* static void print_charset (const uint8_t *st) { */
/*   int i, first; */
/*   printf("["); */
/*   for (i = 0; i <= UCHAR_MAX; i++) { */
/*     first = i; */
/*     while (testchar(st, i) && i <= UCHAR_MAX) i++; */
/*     if (i - 1 == first)  /\* unary range? *\/ */
/*       printf("(%02x)", first); */
/*     else if (i - 1 > first)  /\* non-empty range? *\/ */
/*       printf("(%02x-%02x)", first, i - 1); */
/*   } */
/*   printf("]"); */
/* } */

static void fprintcapkind (FILE *channel, int kind) {
  fprintf(channel, "%s", CAPTURE_NAME(kind));
}
/* static void printcapkind (int kind) { */
/*   fprintcapkind(stdout, kind); */
/* } */

/* /\* Linear search.  Returns index of binding, if found, else -1. *\/ */
/* static int find_by_pattern (Env *env, uint32_t n, Binding **retbinding) { */
/*   Pattern *ep; */
/*   int i = ENV_ITER_START; */
/*   while ((*retbinding = env_pattern_iter(env, &i))) { */
/*     ep = env_binding_pattern(env, *retbinding); */
/*     if (ep && (ep->pattern == n)) return i; */
/*   } */
/*   return -1; */
/* } */

/* Print instructions with their absolute addresses, showing jump
 * destinations with their computed target addresses.
 */
int print_instruction (Instruction *const op, Instruction *p, void *context) {
  return fprint_instruction(stdout, op, p, context);
}
int fprint_instruction (FILE *channel, Instruction *const op, Instruction *p, void *context) {
  SymbolTable *symtab = (SymbolTable *) context;
  const char *name = NULL;
  if (opcode(p) >= 27) fprintf(channel, "ILLEGAL OPCODE: %d\n", opcode(p));
  else fprintf(channel, "%4ld  %s ", (long)(p - op), OPCODE_NAME(opcode(p)));
  switch (opcode(p)) {
  case ICall: {
    fprintf(channel, "to %d", (int)((p + 1)->offset));
    break;
  }
  case IXCall: {
    fprintf(channel, "to pkg #%d, symtab index %d", aux(p), addr(p));
    break;
  }
  case IChar: {
    fprintf(channel, "'%c'", signedaux(p));
    break;
  }
  case ITestChar: {
    fprintf(channel, "'%c' - on failure ", aux(p)); printjmp(op, p);
    break;
  }
  case IOpenCapture: {
    fprintcapkind(channel, aux(p));
    fprintf(channel, " #%d", addr(p));
    if (symtab) {
      name = symboltable_get_name(symtab, addr(p));
      fprintf(channel, " '%s'", name);
    }
    break;
  }
  case ICloseConstCapture: {
    fprintf(channel, " #%d", addr(p));
    if (symtab) {
      name = symboltable_get_name(symtab, addr(p));
      fprintf(channel, " '%s'", name);
    }
    break;
  }
  case IBackref: {
    fprintf(channel, " #%d", addr(p));
    if (symtab) {
      name = symboltable_get_name(symtab, addr(p));
      fprintf(channel, " '%s'", name);
    }
    break;
  }
  case ISet: {
    fprint_charset(channel, (p+1)->buff);
    break;
  }
  case ITestSet: {
    fprint_charset(channel, (p+2)->buff); fprintf(channel, " - on failure ");
    fprintjmp(channel, op, p);
    break;
  }
  case ISpan: {
    fprint_charset(channel, (p+1)->buff);
    break;
  }
  case IBehind: {
    fprintf(channel, "%d", aux(p));
    break;
  }
  case ITestAny: {
    fprintf(channel, " - on failure "); fprintjmp(channel, op, p);
    break;
  }
  case IJmp: {
    fprintf(channel, "to %d", (int)(p + (p + 1)->offset - op));
    break;
  }
  case IChoice: {
    fprintf(channel, "- on failure "); fprintjmp(channel, op, p);
    break;
  }
  case ICommit: {
    fprintf(channel, "and "); fprintjmp(channel, op, p);
    break;
  }
  case IPartialCommit: case IBackCommit: {
    if (aux(p)) fprintf(channel, "w/progress check ");
    fprintjmp(channel, op, p);
    break;
  }
  default: break;
  }
  fprintf(channel, "\n");
  return 0;			/* OK */
}

/* symbol table is optional */
void print_instructions (Instruction *const p, int codesize, SymbolTable *symtab) {
  printf("Code vector with %d instructions:\n", codesize);
  walk_instructions(p, codesize, &print_instruction, symtab);
}

void print_match (Match *match) {
  printf("Match %s\n", match->matched ? "succeeded" : "failed");
  printf("  vm terminated %s\n", match->abend ? "with ABEND" : "normally");
  printf("  %lu bytes left over\n", match->leftover);
  printf("  %lu bytes of match data produced\n", match->data ? match->data->next : 0);
}


