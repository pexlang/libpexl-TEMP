/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  common.h   Logging, error reporting, common structures                   */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#if !defined(common_h)
#define common_h

#ifndef _POSIX_C_SOURCE
#  define _POSIX_C_SOURCE 200809L
#endif

#include <stdint.h>	    /* int32_t, etc. */
#include <stdlib.h>	    /* exit() */
#include "errors.h"	    /* definitions of PEXL error codes */
#include "logging.h"

#ifndef DEBUG
#  define DEBUG 0
#endif

/* Symbol visibility in the final library.  Recall that "default" means public. */
#define EXPORT __attribute__ ((visibility("default")))

#define UNUSED(x) (void)(x)

/* When logging is insufficient because it would be perilous to
   continue executing, use PANIC.
*/
#define PANIC(who, fmt, ...)						\
  do {									\
    fprintf(stderr, "%s:%d:%s(): " fmt, __FILE__,			\
	    __LINE__, (who), __VA_ARGS__);				\
    fputs("Cannot continue\n\n", stderr);				\
    fflush(stderr);							\
    exit(-255);								\
  } while (0)

#define ERR -999999		/* avoid overlap with other error codes */
#define OK 0

#define YES 1
#define NO 0

/* longlong literals are apparently not part of c89, yet this definition works: */
#define PEXL_INT64_MAX 0x7FFFFFFFFFFFFFFF

/* Index must be a signed type, because we use negative numbers to
   signal unassigned values (and possibly errors in some cases).
*/
typedef int32_t Index;

/* Generic bitflag macros */
#define bitpos(n) ((unsigned)1<<(n))
#define has_flag(flags, flagname) ((flags)&(flagname))
#define with_flag(flags, flagname, val)				\
  ((val) ? ((flags) | (flagname)) : ((flags) & ~(flagname)))
#define set_flag(flags, flagname, val) do {		\
    (flags) = with_flag((flags), (flagname), (val));	\
  } while (0)

#endif


/* Macros Enabling Optimizers
Set 1st bit for enabling inline optimizer
Set 5th bit for enabling peephole optimizer 
*/
#define NO_OPTIM 0x0000
#define ONLY_INLINE_OPTIM 0x0001
#define ONLY_PEEPHOLE_OPTIM 0x0010
#define DEFAULT_OPTIM 0x0011
