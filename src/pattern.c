/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  pattern.c  Pattern structures and dependency calculations                */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "pattern.h"

#include <stdlib.h> 
#include <alloca.h>
#include <assert.h>
#include <string.h> 

#include "expression.h"

/* ----------------------------------------------------------------------------- */
/* Pattern structures                                                            */
/* ----------------------------------------------------------------------------- */

Pattern *pattern_new (struct Expression *tree, int *err) {
  Pattern *p; 
  if (!err) {
    LOG("pattern_new", "null error arg");
    return NULL;
  }
  if (!tree) {
    LOG("pattern_new", "null expression arg");
    *err = EXP_ERR_NULL;
    return NULL;
  }
  p = malloc(sizeof(Pattern)); 
  if (!p) {
    LOG("pattern_new", "out of memory");
    *err = EXP_ERR_OOM;
    return NULL;
  }
  p->tree = tree; 
  p->fixed = NULL;		/* indicates "not compiled" */
  p->deplist = NULL;		/* indicates "not compiled" */
  p->entrypoint = 0; 		/* indicates "not linked" */
  p->codesize = 0;  		/* indicates "not linked" */
  p->env = NULL;
  p->binding_index = -1;	/* "not bound" */
  set_patflags(p, 0); 
  set_pattern_minlen(p, 0); 
  set_pattern_maxlen(p, 0); 
  /* firstset not initialized */
  *err = 0;
  return p; 
} 

void pattern_free (Pattern *p) {
  if (!p) return;
  if (p->tree) pexle_free(p->tree);
  if (p->fixed) pexle_free(p->fixed);
  if (p->deplist) deplist_free(p->deplist);
  /* we do not free p->env, because it is shared across patterns */
  free(p);
}

/* Does not free the expression */
void pattern_only_free (Pattern *p) {
  if (!p) return;
  if (p->fixed) pexle_free(p->fixed);
  if (p->deplist) deplist_free(p->deplist);
  /* we do not free p->env, because it is shared across patterns */
  free(p);
}

/* ----------------------------------------------------------------------------- */
/* Control Flow graph */
/* ----------------------------------------------------------------------------- */

/* NOTE: These functions operate on the pattern->fixed field, which is
   an Expression that was created by copying the pattern->tree field.
   We should NOT touch the pattern->tree field.
*/

CFgraph *cfgraph_new (void) {
  CFgraph *g;
  int initial_size = EXP_CFG_INITIAL_SIZE;
  g = malloc(sizeof(CFgraph));
  if (!g) {
    LOG("cfgraph_new", "out of memory");
    return NULL;
  }
  g->size = initial_size;
  g->next = 0;
  g->order = NULL;
  g->deplists = calloc(g->size, sizeof(DepList));
  if (!g->deplists) {
    LOG("cfgraph_new", "out of memory");
    free(g);
    return NULL;
  }
  return g;
}

void deplist_free (DepList dl); /* forward reference */

void cfgraph_free (CFgraph *g) {
  Index i;
  if (!g) return;
  SCC_free(g->order);
  if (g->deplists) {
    for (i = 0; i < g->next; i++) 
      /* free each expression's dependency list */
      deplist_free(g->deplists[i]);
    free(g->deplists);
  }
  free(g);
}

static int ensure_vertex_space (CFgraph *g) {
  size_t newsize;
  void *temp;
  if (g->next < g->size) return OK;
  newsize = 2 * g->size * sizeof(DepList *);
  temp = realloc(g->deplists, newsize);
  if (!temp) {
    LOG("ensure_vertex_space", "out of memory");
    return EXP_ERR_OOM;
  }
  g->deplists = temp;
  g->size = 2 * g->size;
  LOGf("ensure_vertex_space", "extending to %d slots (%d in use)", g->size, g->next);
  return OK;
}

/* Add vertex and its edge list, i.e. a DepList.  Return index+1 or 0 for OOM. */
static Index cfgraph_add_vertex (CFgraph *g, DepList dl) {
  Index index = g->next;
  int err = ensure_vertex_space(g);
  if (err < 0) return err;	/* already logged */
  assert( g->next < g->size );
  g->deplists[index] = dl;
  g->next++;
  return index;
}

/* Currently LINEAR time in the number of verticies in g. */
static int cfgraph_has_vertex (CFgraph *g, struct Pattern *pat) {
  Index i;
  for (i = 0; i < g->next; i++) {
    if (g->deplists[i][0] == pat) return YES;
  }
  return NO;
}  

/* ----------------------------------------------------------------------------- */
/* Dependency lists                                                              */
/* ----------------------------------------------------------------------------- */

static DynamicDepList *dynamic_deplist_new (void) {
  DynamicDepList *d = malloc(sizeof(DynamicDepList));
  if (!d) {
    LOG("dynamic_deplist_new", "out of memory");
    return NULL;
  }
  d->next = 0;
  d->size = EXP_DEPLIST_INITIAL_SIZE;
  d->deps = malloc(d->size * sizeof(struct Pattern *));
  if (!d->deps) {
    LOG("dynamic_deplist_new", "out of memory");
    free(d);
    return NULL;
  }
  return d;
}

/* This does NOT free the deplist itself, only the structure around it */
static void dynamic_deplist_free (DynamicDepList *d) {
  if (!d) return;
  free(d);
}

static DepList to_static_deplist (DynamicDepList *d) {
  DepList dl;
  assert( d );
  dl = d->deps;
  dynamic_deplist_free(d);
  return dl;
}

void deplist_free (DepList dl) {
  if (dl) free(dl);
}

static int ensure_deplist_space (DynamicDepList *dl) {
  DepList temp;
  size_t newsize;
  if (dl->next < dl->size) return OK;
  newsize = 2 * dl->size;
  temp = realloc(dl->deps, newsize * sizeof(struct Pattern *));
  if (!temp) {
    LOG("ensure_deplist_space", "out of memory");
    return EXP_ERR_OOM;
  }  
  dl->deps = temp;
  dl->size = newsize;
  LOGf("ensure_deplist_space", "extending to %lu slots (%lu in use)", dl->size, dl->next);
  return OK;
}

static int dynamic_deplist_add (DynamicDepList *dl, struct Pattern *pat) {
  int err = ensure_deplist_space(dl);
  if (err < 0) return err;
  assert( dl->size > dl->next );
  dl->deps[dl->next] = pat;
  dl->next++;
  return OK;
}

/* ----------------------------------------------------------------------------- */
/* Dependency collection                                                         */
/* ----------------------------------------------------------------------------- */

/* 
   A Dependency List (DepList) is a NULL-terminated array of Pattern pointers.

   We collect dependencies by walking a pattern's 'fixed' expression
   tree, which starts off as a copy of the expression tree created by
   the user through our API using e.g. from_string(), choice(),
   seq(). The copy is then modified by fix_open_calls(), which
   replaces each TOpenCall node with a TCall node.  The TCall node
   contains a Pattern pointer.

   The structure of a DepList is an edge-list representation of
   reachability, where each directed edge A->B means "pattern A may
   call pattern B":
     - The first element is the pattern itself (a vertex in the control flow graph).
     - Zero or more additional patterns (endpoints of the edges in the control flow graph).
     - The last element is NULL.
*/

/* Add the dependencies of 'node' to the end of 'dl'.  Return OK or an error < 0. */
static int add_node_dependencies (DynamicDepList *dl, Node *node) {
  int err;
  struct Pattern *target;
 tailcall:
  assert( node );
  switch (node->tag) {
  case TCall:
    target = node->v.pat;
    assert( target );
    assert( target->env );
    if ((err = dynamic_deplist_add(dl, node->v.pat))) return err;
    break;
  case TOpenCall: 
    LOG("add_node_dependencies", "encountered TOpenCall");
    return EXP_ERR_OPENFAIL;
  default:
    /* not a call */
    switch (numsiblings[node->tag]) {
    case 1: 
      node = child1(node);
      goto tailcall;
    case 2:
      err = add_node_dependencies(dl, child1(node));
      if (err) return err; 
      node = child2(node);
      goto tailcall;
    default:
      assert( numsiblings[node->tag] == 0 );
    } /* switch on number of child siblings (children) of node */
  } /* switch on type of node */
  return OK;
}

static DepList copy_deplist (DepList dl) {
  DepList ptr;
  DepList new;
  Index i, len = 1;
  ptr = dl;
  while (*ptr++) len++;
  new = malloc(len * sizeof(struct Pattern *));
  if (!new) {
    LOG("copy_deplist", "out of memory");
    return NULL;
  }
  for (i = 0; i < len; i++) new[i] = dl[i];
  return new;
}

/* If 'pat' has a dependency list cached, return a copy of it that can
   be added directly to the control flow graph.  Else compute the
   dependencies, cache it in the pattern, and return a copy.
*/
static int compute_dependencies (struct Pattern *pat, DepList *retval) {
  int err;
  DepList dl;
  DynamicDepList *ddl;

  if (!pat || !retval) return EXP_ERR_NULL;
  dl = pat->deplist;
  if (dl) {
    *retval = copy_deplist(dl);
    return OK;
  }
  /* Else calculate the dependencies */
  if (!pat->fixed) {
    LOG("compute_dependencies", "pattern expression missing fixed-call tree");
    return EXP_ERR_INTERNAL;
  }
  ddl = dynamic_deplist_new();
  if (!ddl) return EXP_ERR_OOM;	/* already logged */
  /* Insert the first entry in a DepList, the vertex itself */
  err = dynamic_deplist_add(ddl, pat);
  if (err) goto error;
  err = add_node_dependencies(ddl, pat->fixed->node);
  if (err) goto error;
  /* Add a null terminator to the end of the list */
  err = dynamic_deplist_add(ddl, NULL);
  if (err) goto error;
  assert( ddl->next > 1 );   /* at minimum: vertex, null terminator */
  dl = to_static_deplist(ddl);
  assert( dl );
  /* Cache a copy of the deplist for future use */
  pat->deplist = dl;
  *retval = copy_deplist(dl);
  return OK;

 error:
  deplist_free(ddl->deps);
  dynamic_deplist_free(ddl);
  return err;			/* already logged */
}

/* PRECONDITION: The graph g has at least one vertex, which may be for
   an anonymous expression or for a bound expression.  The last vertex
   in the graph is examined, and for each of its dependencies, we add
   their dependencies to g.
*/
static int add_transitive_closure (CFgraph *g) {
  int err;
  DepList dl;
  struct Pattern **deps;
  struct Pattern *pat;
  Index cursor;
  assert( g );
  cursor = g->next - 1;		/* last vertex added */
  while (cursor < g->next) {
    dl = g->deplists[cursor];
    assert( dl );
    deps = &dl[1];		/* First dependency of vertex at cursor */
    while ((pat = *deps++)) {
      /* If 'pat' is not in the graph, compute its deplist and add it to the graph */
      if (!cfgraph_has_vertex(g, pat)){
	err = compute_dependencies(pat, &dl);
	if (err) return err;
	/* Add the new vertex to g */
	err = cfgraph_add_vertex(g, dl);
	if (err < 0) return err;
      }
    } /* while there are more dependencies of the current vertex */
    cursor++;
  } /* while the cursor has not advanced past the last vertex in g */
  return OK;
}

int add_to_cfgraph (struct Pattern *pat, CFgraph *g) {
  int err;
  DepList dl;
  assert( pat );
  assert( g );
  if (cfgraph_has_vertex(g, pat)) return OK;
  err = compute_dependencies(pat, &dl);
  if (err < 0) return err;
  /* Add the dl to the graph. (The dl is the pattern itself, plus its dependencies.) */
  err = cfgraph_add_vertex(g, dl);
  if (err < 0) return err;
  /* Add the transitive closure of the newly added vertex */
  return add_transitive_closure(g);
}

/* Build a control flow / dependency graph with a specific starting
   point, which is a pattern.
*/
int build_cfgraph (struct Pattern *pat, CFgraph **retval) {
  int err;
  CFgraph *g;
  if (!pat || !retval) {
    LOG("build_cfgraph", "null arg (either pattern or return value)");
    return EXP_ERR_NULL;
  }
  if (!pat->env) {
    LOG("build_cfgraph", "pattern compilation environment field is null");
    return EXP_ERR_NULL;
  }
  g = cfgraph_new();
  if (!g) return EXP_ERR_OOM;	/* already logged */
  err = add_to_cfgraph(pat, g);
  if (err) {
    cfgraph_free(g);
    return err;
  }
  *retval = g;
  return OK;
}

/* Topologically sort the control-flow graph, while detecting cycles,
   using Tarjan's algorithm.
*/

static int cmp_ptr (void *context, const void *v, const void *w) {
  const void *const *vv = *(const void *const *) v;
  const void *const *ww = *(const void *const *) w;
  UNUSED(context);
  return !(vv == ww);		/* 0 when they are equal */
}

int sort_cfgraph (CFgraph *G) {
  int stat;
  SCC *output = NULL;
  void *nullptr = NULL;
  
  if ((!G) || (!G->deplists)) return EXP_ERR_NULL;

  stat = SCC_run((void **) G->deplists, G->next, sizeof(struct Pattern *), &nullptr, cmp_ptr, NULL, &output);
  if (stat == SCC_OK) {
    SCC_free(G->order);		/* in case there was a prior one */
    G->order = output;
    return OK;
  }
  SCC_free(output);
  switch (stat) {
  case SCC_ERR_OOM:   return EXP_ERR_OOM;
  case SCC_ERR_GRAPH: return EXP_ERR_INTERNAL;
  default:
    LOGf("sort_cfgraph", "unexpected error code from SCC: %d", stat);
    return EXP_ERR_INTERNAL;
  }
}
    

