/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  expression.c                                                             */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "expression.h"

#include <stdlib.h> 
#include <assert.h>
#include <string.h> 		/* memcpy */
#include "instruction.h"	/* Crosiecap, Crosieconst */
#include "pattern.h"		/* cfgraph */

/* ----------------------------------------------------------------------------- */
/* Compilation state structure                                                   */
/* ----------------------------------------------------------------------------- */

CompState *compstate_new () {
  CompState *cs;
  cs = malloc(sizeof(CompState));
  if (!cs) {
    LOG("compstate_new", "out of memory");
    return NULL;
  }
  cs->pat = NULL;
  cs->g = NULL;
  cs->err_index = 123456;	/* helps catch uninitialized usage */
  return cs;
}

void compstate_free (CompState *cs) {
  if (!cs) return;
  /* Leave pattern alone; free only the transient compilation state. */
  if (cs->g) cfgraph_free(cs->g);
  free(cs);
}

/* ----------------------------------------------------------------------------- */
/* Context structure                                                             */
/* ----------------------------------------------------------------------------- */

Context *context_new (struct Env *top_level) {
  Context *C;
  if (!top_level) {
    LOG("context_new", "null top-level environment arg");
    return NULL;
  }

  C = malloc(sizeof (Context));
  if (!C) goto oom1;
  
  C->env = top_level;

  C->stringtab = hashtable_new(0, 0); /* Use default sizes */
  if (!C->stringtab) goto oom2;

  C->packages = packagetable_new(0); /* Use default size */
  if (!C->packages) goto oom3;

  C->cst = NULL;
  return C;

 oom3:
  hashtable_free(C->stringtab);
 oom2:
  free(C);
 oom1:
  return NULL;
}

void context_free (Context *C) {
  if (!C) return;
  if (C->cst) compstate_free(C->cst);
  if (C->stringtab) hashtable_free(C->stringtab);
  if (C->packages) packagetable_free(C->packages);
  free(C);
}

Index context_intern (Context *C, const char *name) {
  Index entry;
  HashTableData nodata;
  if (!C) {
    LOG("intern", "null expression context");
    return EXP_ERR_NULL;
  }
  nodata.ptr = NULL;
  entry = hashtable_add_update(C->stringtab, name, nodata);
  if (entry < 0)
    switch (entry) {
    case HASHTABLE_ERR_NULL: return EXP_ERR_NULL;
    case HASHTABLE_ERR_OOM: return EXP_ERR_OOM;
    default:
      LOGf("context_intern", "unexpected error %d from hash table", entry);
      return EXP_ERR_INTERNAL;
    }
  return entry;
}

const char *context_retrieve (Context *C, Index offset) {
  HashTableEntry entry;
  if (!C) return NULL;
  entry.block_offset = offset; entry.data.ptr = NULL;
  return hashtable_get_key(C->stringtab, entry);
}

/* ----------------------------------------------------------------------------- */
/* Expression tree structures                                                    */
/* ----------------------------------------------------------------------------- */


/* Value exp_new_value_expression (Expression *tree) { */
/*   if (!tree) { */
/*     LOG("exp_new_value_expression", "null arg"); */
/*   } */
/*   return env_new_value(Eexpression_t, 0, tree); */
/* } */

Expression *newtree (int len) {
  size_t size = (len - 1) * sizeof(Node) + sizeof(Expression);
  Expression *tree = (Expression *) malloc(size);
  if (!tree) {
    LOG("newtree", "out of memory");
    return NULL;
  }
  tree->len = len;
  return tree;
}

Expression *pexle_copy (Expression *tree) {
  Expression *new;
  if (!tree) return NULL;
  new = newtree(tree->len);
  if (!new) return NULL;	/* already logged */
  memcpy(new->node, tree->node, (tree->len) * sizeof(Node));
  return new;
}

void pexle_free (Expression *tree) {
  /* Do not free tree->env, because it is shared. */
  if (tree) free(tree);
}

/*
 * If 'tree' is a 'char' pattern (TSet, TChar, TAny), convert it into a
 * charset and return YES (True); else return NO (False).
*/
int to_charset (Node *node, Charset *cs) {
  if (!node) {
    LOG("to_charset", "null node arg");
    return NO;	    /* What would the caller do if we returned ERR? */
  }
  if (!cs) {
    LOG("to_charset", "null charset arg");
    return NO;	    /* What would the caller do if we returned ERR? */
  }
  switch (node->tag) {
    case TSet: {		/* copy set */
      loopset(i, cs->cs[i] = nodebuffer(node)[i]);
      return YES;
    }
    case TChar: {		/* only one char */
      assert(0 <= node->u.n && node->u.n <= UCHAR_MAX);
      loopset(i, cs->cs[i] = 0);  /* erase all chars */
      setchar(cs->cs, node->u.n);  /* add that one */
      return YES;
    }
    case TAny: {
      loopset(i, cs->cs[i] = 0xFF); /* add all characters to the set */
      return YES;
    }
    default: 
      return NO;
  }
}

static Expression *newleaf (int tag) {
  Expression *tree = newtree(1);
  if (!tree) return NULL;
  tree->node->tag = tag;
  tree->node->v.env = NULL;
  return tree;
}

/*
 * Build a sequence of 'n' nodes, each with tag 'tag' and 'u.n' from
 * the array 's' (or 0 if array is NULL). (TSeq is binary, so it must
 * build a sequence of sequence of sequence...)
*/
static void fillseq (Node *node, int tag, int n, const char *s) {
  int i;
  for (i = 0; i < n - 1; i++) {  /* initial n-1 copies of Seq tag; Seq ... */
    node->tag = TSeq; node->u.ps = 2;
    child1(node)->tag = tag;
    child1(node)->u.n = s ? (uint8_t)s[i] : 0;
    node = child2(node);
  }
  node->tag = tag;  /* last one does not need TSeq */
  node->u.n = s ? (uint8_t)s[i] : 0;
}

/* ----------------------------------------------------------------------------- */
/* Create primitive patterns                                                     */
/* ----------------------------------------------------------------------------- */

/* The argument to 'from_bytes' can contain NUL characters, '\0'. */
EXPORT
Expression *pexle_from_bytes (Context *C, const char *ptr, size_t len) {
  Expression *tree;
  UNUSED(C);
  if (len <= 0)			  /* empty string? */
    return newleaf(TTrue);	  /* always match */
  if (!ptr) return NULL;
  tree = newtree(2 * (len - 1) + 1);
  if (!tree) return NULL;
  fillseq(tree->node, TChar, len, ptr); /* sequence of 'len' chars */
  return tree;
}

/* Arg is a null-terminated string */
EXPORT
Expression *pexle_from_string (Context *C, const char *str) {
  size_t len = strnlen(str, EXP_MAXSTRING + 1);
  if (len > EXP_MAXSTRING) return NULL;
  UNUSED(C);
  return pexle_from_bytes(C, str, len);
}

/*
 * Numbers as patterns:
 * 0 == true (always match); n == TAny repeated 'n' times;
 * -n == not (TAny repeated 'n' times)
*/
EXPORT
Expression *pexle_from_number (Context *C, int n) {
  UNUSED(C);
  if (n == 0)
    return newleaf(TTrue);
  else {
    Expression *tree;
    Node *node;
    if (n > 0) {
      tree = newtree(2 * n - 1);
      if (!tree) return NULL;
      node = tree->node;
    }
    else {  /* negative: code it as !(-n) */
      n = -n;
      tree = newtree(2 * n);
      if (!tree) return NULL;
      node = tree->node;
      node->tag = TNot;
      node = child1(node);
    }
    fillseq(node, TAny, n, NULL);  /* sequence of 'n' any's */
    return tree;
  }
}

EXPORT
Expression *pexle_from_boolean (Context *C, int bool) {
  Expression *tree = bool ? newleaf(TTrue) : newleaf(TFalse);
  UNUSED(C);
  if (!tree) {
    LOG("pexle_from_boolean", "out of memory");
    return NULL;
  }
  return tree;
}

static Expression *newcharset (void) { 
  Expression *tree = newtree(bytes2slots(CHARSETSIZE) + 1); 
  if (!tree) return NULL;
  tree->node->tag = TSet; 
  loopset(i, treebuffer(tree)[i] = 0); 
  return tree; 
} 

EXPORT
Expression *pexle_from_set (Context *C, const char *set, size_t len) {
  Expression *tree;
  UNUSED(C);
  if (!set) {
    LOG("pexle_from_set", "null set arg");
    return NULL;
  }
  tree = newcharset();
  if (!tree) {
    LOG("pexle_from_set", "out of memory");
    return NULL;
  }
  while (len--) {
    setchar(treebuffer(tree), (uint8_t)(*set));
    set++;
  }
  return tree;
}

/* FUTURE: Optimize a one-character range into a TChar node */
/* FUTURE: Optimize range execution by adding a new tree type TRange,
 * with a corresponding instruction.  Steve Poole (IBM) suggests it
 * will be faster, based on his JIT experiments. 
 */
EXPORT
Expression *pexle_from_range (Context *C, uint8_t from, uint8_t to) {
  Expression *tree;
  int c;
  UNUSED(C);
  if (from > to) {
    LOG("pexle_from_range", "args out of order (required: from <= to)");
    return NULL;
  }
  tree = newcharset();
  if (!tree) {
    LOG("pexle_from_range", "out of memory");
    return NULL;
  }
  for (c = from; c <= to; c++)
      setchar(treebuffer(tree), (uint8_t) c);
  return tree;
}

/* ----------------------------------------------------------------------------- */
/* Halt                                                                          */
/* ----------------------------------------------------------------------------- */

EXPORT
Expression *pexle_halt (Context *C) {
  Expression *tree = newleaf(THalt);
  UNUSED(C);
  if (!tree) {
    LOG("pexle_halt", "out of memory");
    return NULL;
  }
  return tree;
}

/* ----------------------------------------------------------------------------- */
/* Sequence                                                                      */
/* ----------------------------------------------------------------------------- */

/* Create a new tree, with a new root and 2 siblings (tree1, tree2). */
static Expression *newroot2sib (int tag, Expression *tree1, Expression *tree2) {
  int s1 = tree1->len;
  int s2 = tree2->len;
  Expression *tree = newtree(1 + s1 + s2);  /* create new tree */
  Node *node;
  if (!tree) return NULL;
  node = tree->node;
  node->tag = tag;
  node->v.env = NULL;
  node->u.ps = 1 + s1;
  memcpy(child1(node), tree1->node, s1 * sizeof(Node));
  memcpy(child2(node), tree2->node, s2 * sizeof(Node));
  return tree;
}

/*
 * Sequence operator; optimizations:
 * false x => false, x true => x, true x => x
 * (cannot do x . false => false because x may have runtime captures)
 *
 * FUTURE: Decide whether Rosie will ever have a concept equivalent to
 * runtime captures, and if not, optimize accordingly.
 */
EXPORT
Expression *pexle_seq (Context *C, Expression *tree1, Expression *tree2) {
  Expression *tree;
  UNUSED(C);
  if (!tree1 || !tree2) {
    LOG("pexle_seq", "null arg");
    return NULL;
  }
  if (tree1->node->tag == TFalse || tree2->node->tag == TTrue)
    tree = pexle_copy(tree1);	/* false . x == false, x . true = x */
  else if (tree1->node->tag == TTrue)
    tree = pexle_copy(tree2);	/* true . x = x */
  else tree = newroot2sib(TSeq, tree1, tree2);
  if (!tree) {
    LOG("pexle_seq", "out of memory");
    return NULL;
  }
  return tree;
}

/* Same as pexle_seq() but frees its expression arguments */
EXPORT
Expression *pexle_seq_f (Context *C, Expression *t1, Expression *t2) {
  Expression *tree = pexle_seq(C, t1, t2);
  pexle_free(t1); pexle_free(t2);
  return tree;
}

/* ----------------------------------------------------------------------------- */
/* Choice                                                                        */
/* ----------------------------------------------------------------------------- */

/*
 * Choice operator; optimizations:
 * charset / charset => charset
 * true / x => true
 * x / false => x
 * false / x => x
 * (x / true is not equivalent to true)
 *
 * FUTURE: For rosie's THalt, could do this optimization: THalt / x => THalt 
 */
EXPORT
Expression *pexle_choice (Context *C, Expression *t1, Expression *t2) { 
  Expression *tree;
  Charset st1, st2; 
  UNUSED(C);
  if (!t1 || !t2) {
    LOG("pexle_choice", "null arg");
    return NULL;
  }
  if (to_charset(t1->node, &st1) && to_charset(t2->node, &st2)) { 
    tree = newcharset(); 
    if (!tree) {
      LOG("pexle_choice", "out of memory");
      return NULL;
    }
    loopset(i, treebuffer(tree)[i] = st1.cs[i] | st2.cs[i]); 
    return tree;
  } 

/* FUTURE: Re-introduce the optimization below that calls nofail() IN A LATER STAGE.
   Need to introduce an explicit stage for doing tree optimizations AFTER we have fixed all the open calls.
*/

/*   res = nofail(t1->node, env); */
/*   if (res == ERR) return NULL; */
/*   if (res || t2->node->tag == TFalse) */
/*     return pexle_copy(t1);       /\* true / x => true, x / false => x *\/  */
/*   if (t1->node->tag == TFalse) */
/*     return pexle_copy(t2);       /\* false / x => x *\/  */
  /* else */

  if ((t1->node->tag == TTrue) || ( t2->node->tag == TFalse))
    return pexle_copy(t1);       /* true / x => true, x / false => x */  
  if (t1->node->tag == TFalse) 
    return pexle_copy(t2);       /* false / x => x */  

  tree = newroot2sib(TChoice, t1, t2); 
  if (!tree) {
    LOG("pexle_choice", "out of memory");
    return NULL;
  }
  return tree;
} 

/* Same as pexle_choice() but frees its expression arguments */
EXPORT
Expression *pexle_choice_f (Context *C, Expression *t1, Expression *t2) {
  Expression *tree = pexle_choice(C, t1, t2);
  pexle_free(t1); pexle_free(t2);
  return tree;
}

/* ----------------------------------------------------------------------------- */
/* Not                                                                           */
/* ----------------------------------------------------------------------------- */

/*
 * Create a new tree, with a new root and one sibling.
 */
static Expression *newroot1sib (int tag, Expression *sibling) {
  Expression *tree = newtree(1 + sibling->len);
  if (!tree) return NULL;
  tree->node->tag = tag;
  tree->node->v.env = NULL;
  memcpy(child1(tree->node), sibling->node, sibling->len * sizeof(Node));
  return tree;
}

/* Not (!p in the original PEG presentation) */
EXPORT
Expression *pexle_not (Context *C, Expression *tree1) {
  Expression *tree;
  UNUSED(C);
  if (!tree1) {
    LOG("pexle_not", "null arg");
    return NULL;
  }
  tree = newroot1sib(TNot, tree1);
  if (!tree) {
    LOG("pexle_not", "out of memory");
    return NULL;
  }
  return tree;
}

/* Same as pexle_not() but frees its expression argument */
EXPORT
Expression *pexle_not_f (Context *C, Expression *t1) {
  Expression *tree = pexle_not(C, t1);
  pexle_free(t1);
  return tree;
}

/* ----------------------------------------------------------------------------- */
/* Unlimited lookahead                                                           */
/* ----------------------------------------------------------------------------- */

/* Lookahead (&p in the original PEG presentation) */
EXPORT
Expression *pexle_lookahead (Context *C, Expression *tree1) {
  Expression *tree;
  UNUSED(C);
  if (!tree1) {
    LOG("pexle_lookahead", "null arg");
    return NULL;
  }
  tree = newroot1sib(TAhead, tree1);
  if (!tree) {
    LOG("pexle_lookahead", "out of memory");
    return NULL;
  }
  return tree;
}

/* Same as pexle_lookahead() but frees its expression argument */
EXPORT
Expression *pexle_lookahead_f (Context *C, Expression *t1) {
  Expression *tree = pexle_lookahead(C, t1);
  pexle_free(t1);
  return tree;
}

/* ----------------------------------------------------------------------------- */
/* Finite lookbehind                                                             */
/* ----------------------------------------------------------------------------- */

EXPORT
Expression *pexle_lookbehind (Context *C, Expression *tree1) {
  Expression *tree;
  UNUSED(C);
  if (!tree1) {
    LOG("pexle_lookbehind", "null arg");
    return NULL;
  }
  tree = newroot1sib(TBehind, tree1);
  if (!tree) {
    LOG("pexle_lookbehind", "out of memory");
    return NULL;
  }
  /*   tree->node->u.n = -1;		/\* -1 means "value not set yet" *\/ */
  return tree;
}

/* Same as pexle_lookahead() but frees its expression argument */
EXPORT
Expression *pexle_lookbehind_f (Context *C, Expression *t1) {
  Expression *tree = pexle_lookbehind(C, t1);
  pexle_free(t1);
  return tree;
}

/* ----------------------------------------------------------------------------- */
/* Subtraction                                                                   */
/* ----------------------------------------------------------------------------- */

/*
 * t1 - t2 == Seq (Not t2) t1
 * If t1 and t2 are charsets, make their difference.
*/
EXPORT
Expression *pexle_subtract (Context *C, Expression *t1, Expression *t2) {
  Node *node;
  Expression *tree;
  Charset st1, st2;
  int s1, s2;
  UNUSED(C);
  if (!t1 || !t2) {
    LOG("pexle_subtract", "null arg");
    return NULL;
  }
  s1 = t1->len;
  s2 = t2->len;
  if (to_charset(t1->node, &st1) && to_charset(t2->node, &st2)) {
    tree = newcharset();
    if (!tree) {
      LOG("pexle_subtract", "out of memory");
      return NULL;
    }
    loopset(i, treebuffer(tree)[i] = st1.cs[i] & ~st2.cs[i]);
  }
  else {
    tree = newtree(2 + s1 + s2);
    if (!tree) {
      LOG("pexle_subtract", "out of memory");
      return NULL;
    }
    node = tree->node;
    node->tag = TSeq;		/* sequence of... */
    node->u.ps =  2 + s2;
    child1(node)->tag = TNot;	/* not... */
    memcpy(child1(child1(node)), t2->node, s2 * sizeof(Node)); /* t2... */
    memcpy(child2(node), t1->node, s1 * sizeof(Node));       /* and t1 */
  }
  return tree;
}

EXPORT
Expression *pexle_subtract_f (Context *C, Expression *t1, Expression *t2) {
  Expression *tree = pexle_subtract(C, t1, t2);
  pexle_free(t1); pexle_free(t2);
  return tree;
}

/* ----------------------------------------------------------------------------- */
/* Repetition                                                                    */
/* ----------------------------------------------------------------------------- */

/*
 *  Add to tree a sequence where first sibling is 'sib' (with size
 * 'sibsize').   Return position for second sibling.
 */
static Node *seqaux (Node *node, Node *sib, int sibsize) { 
  node->tag = TSeq;
  node->u.ps = sibsize + 1; 
  memcpy(child1(node), sib, sibsize * sizeof(Node)); 
  return child2(node); 
} 

/* 
 * n >= 0: tree1{n,inf} "n or more repetitions of tree1"
 * n < 0: tree1{0,-n} "at most n repetitions of tree1"
 *
 * Common cases using regex/RPL notation:
 *   n == -1 is tree1?
 *   n == 0 is tree1*
 *   n == 1 is tree1+
 *
 * When tree1 is nullable, 
 *   tree1* result is same as when tree1 is not nullable (but requires extra test+branch)
 *   tree1? == tree1
 *   tree1+ == tree1* BUT capture(tree1+) returns empty capture if tree1 matched nothing
 */
EXPORT
Expression *pexle_repeat (Context *C, Expression *tree1, int n) {
  Expression *tree;
  Node *node;
  int size1;
  UNUSED(C);
  if (!tree1) {
    LOG("pexle_repeat", "null arg");
    return NULL;
  }
  size1 = tree1->len;
  if (n >= 0) {
    /* seq tree1 (seq tree1 ... (seq tree1 (rep tree1))) */
    tree = newtree((n + 1) * (size1 + 1));
    if (!tree) {
      LOG("pexle_repeat", "out of memory");
      return NULL;
    }
    node = tree->node;
    while (n--) node = seqaux(node, tree1->node, size1);
    node->tag = TRep;
    memcpy(child1(node), tree1->node, size1 * sizeof(Node));
  }
  else {
    /* choice(seq tree1 ... choice tree1 true ...) true */
    n = -n;
    /* size = (choice + seq + tree1 + true) * n, but the last has no seq */
    tree = newtree(n * (size1 + 3) - 1);
    if (!tree) {
      LOG("pexle_repeat", "out of memory");
      return NULL;
    }
    node = tree->node;
    for (; n > 1; n--) {	/* repeat (n - 1) times */
      node->tag = TChoice;
      node->u.ps = n * (size1 + 3) - 2;
      child2(node)->tag = TTrue;
      node = child1(node);
      node = seqaux(node, tree1->node, size1);
    }
    node->tag = TChoice;
    node->u.ps = size1 + 1;
    child2(node)->tag = TTrue;
    memcpy(child1(node), tree1->node, size1 * sizeof(Node));
  }
  return tree;
}

EXPORT
Expression *pexle_repeat_f (Context *C, Expression *t1, int n) {
  Expression *tree = pexle_repeat(C, t1, n);
  pexle_free(t1);
  return tree;
}

/* ----------------------------------------------------------------------------- */
/* PEXL capture                                                                  */
/* ----------------------------------------------------------------------------- */

EXPORT
Expression *pexle_capture (Context *C, const char *name, Expression *tree1) {
  int32_t offset;
  Expression *tree;
  if (!tree1 || !name) {
    LOG("pexle_capture", "null arg");
    return NULL;
  }
  if ((offset = context_intern(C, name)) < 0) return NULL;
  tree = newroot1sib(TCapture, tree1);
  if (!tree) {
    LOG("pexle_capture", "out of memory");
    return NULL;
  }
  tree->node->cap = Crosiecap;
  tree->node->u.n = offset;
  return tree;
}

EXPORT
Expression *pexle_capture_f (Context *C, const char *name, Expression *t1) {
  Expression *tree = pexle_capture(C, name, t1);
  pexle_free(t1);
  return tree;
}

/* ----------------------------------------------------------------------------- */
/* PEXL call - call a pattern by Ref                                             */
/* ----------------------------------------------------------------------------- */

/* A TOpenCall expression converts to TCall during binding */
EXPORT
Expression *pexle_call (Context *C, Ref ref) {
  Expression *tree;
  UNUSED(C);
  if (!ref.env) {
    LOG("pexle_call", "null env in the ref arg");
    return NULL;
  }
  tree = newleaf(TOpenCall);
  if (!tree) {
    LOG("pexle_call", "out of memory");
    return NULL;
  }
  set_node_ref(tree->node, ref);
  return tree;
}

/* ----------------------------------------------------------------------------- */
/* PEXL external call - call a pattern in another package by XRef                */
/* ----------------------------------------------------------------------------- */

EXPORT
Expression *pexle_xcall (Context *C, XRef xref) {
  Expression *tree;
  UNUSED(C);
  if (!xref.pkg) {
    LOG("pexle_xcall", "null package field in the xref arg");
    return NULL;
  }
  tree = newleaf(TXCall);
  if (!tree) {
    LOG("pexle_xcall", "out of memory");
    return NULL;
  }
  set_node_xref(tree->node, xref);
  return tree;
}

/* ----------------------------------------------------------------------------- */
/* "Constant capture" consumes no input but captures a predetermined string      */
/* ----------------------------------------------------------------------------- */

EXPORT
Expression *pexle_constant (Context *C, const char *name, const char *value) { 
  int32_t name_offset, value_offset;
  Expression *tree, *temp;
  /* Name cannot be the empty string (intern id 0), but value can */
  if ((name_offset = context_intern(C, name)) <= 0) return NULL;
  if ((value_offset = context_intern(C, value)) < 0) return NULL;
  temp = newleaf(TTrue);
  tree = newroot1sib(TCapture, temp);
  pexle_free(temp);		/* because newroot1sib makes a copy of its arg */
  if (!tree) {
    LOG("pexle_constant", "out of memory");
    return NULL;
  }
  tree->node->cap = Crosieconst;
  tree->node->v.constant_capture_offset = value_offset;
  tree->node->u.n = name_offset;
  return tree;
}

/* ----------------------------------------------------------------------------- */
/* Rosie/PEXL backreference                                                      */
/* ----------------------------------------------------------------------------- */

/* TODO: Re-code this using TFunction (store a "primitive function
   number" in the TFunction node) */

/* Rosie/PEXL backreferences are different from lpeg backreferences. 
 * First arg is pattern (used at compile time), second is pattern name (used at runtime) 
 */
/* Expression *pexle_backreference (Env *env, const char *capname) { */
/*   Expression *tree; */
/*   SymbolTableIndex entry; */
/*   entry = shtable_put(env->string_table, capname); */
/*   if (entry <= 0) return NULL; */
/*   tree = newleaf(TBackref); */
/*   if (!tree) return NULL; */
/*   tree->node->v.key = entry; */
/*   return tree; */
/* } */

