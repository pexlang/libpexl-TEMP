/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  buf.h                                                                    */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#if !defined(buf_h)
#define buf_h

#include "common.h"
#include <stdio.h>		/* for FILE */

/* FUTURE: allow caller to specify a default initial size? */
#define BUF_INITIAL_SIZE (8192 * sizeof(char))

typedef       char      Buf_data;
typedef const Buf_data  Buf_data_immutable;
typedef       Buf_data *Buf_cursor;

/* Our data, which will be freed by a call to buf_free(). */
typedef struct Buffer {
  Buf_cursor  data;		/* pointer to initialbuff or a malloc'd block */
  size_t      size;		/* current capacity in bytes */
  size_t      next;		/* next available byte (= number of bytes in use) */
  Buf_cursor  initb;
  Buf_data    initialbuff[BUF_INITIAL_SIZE];
} Buffer;

/* true when buffer's data has overflowed initb and is now allocated elswhere */
#define buf_isdynamic(B)  ((B)->data != (B)->initb)

/* true when buffer is immutable, created with 'wrap') */
#define buf_isimmutable(B)  ((B)->initb == NULL)

Buffer *buf_new (size_t minimum_size);
Buffer *buf_new_from (char *data, size_t size);
Buffer *buf_new_wrap (const char *data, size_t size);

/* Free the buffer contents (unless created by 'wrap') and Buffer itself. */
void buf_free (Buffer *b);
/* Reset the buffer for reuse. */
void buf_reset (Buffer *b);

/* int     buf_addstring (Buffer *b, const char *s); */
int     buf_addlstring (Buffer *b, const char *s, size_t len);
int     buf_addint_varsize (Buffer *b, int64_t n, int bytes);
#define buf_addint64(buf, val) buf_addint_varsize((buf), (val), 8)
int     buf_addint32 (Buffer *b, int32_t n);
int     buf_addint16 (Buffer *b, int16_t n);

Buf_cursor buf_readlstring (Buffer *buf, Buf_cursor ptr, size_t len);
Buf_cursor buf_readint_varsize (Buffer *buf, Buf_cursor ptr, int64_t *retval, int bytes);
#define    buf_readint64(buf, ptr, retval) buf_readint_varsize((buf), (ptr), (retval), 8)
Buf_cursor buf_readint32 (Buffer *buf, Buf_cursor ptr, int32_t *retval);
Buf_cursor buf_readint16 (Buffer *buf, Buf_cursor ptr, int16_t *retval);

Buf_cursor buf_peeklstring (Buffer *buf, Buf_cursor ptr, size_t len);
Buf_cursor buf_peekint_varsize (Buffer *buf, Buf_cursor ptr, int64_t *retval, int bytes);
#define    buf_peekint64(buf, ptr, retval) buf_peekint_varsize((buf), (ptr), (retval), 8)
Buf_cursor buf_peekint32 (Buffer *buf, Buf_cursor ptr, int32_t *retval);
Buf_cursor buf_peekint16 (Buffer *buf, Buf_cursor ptr, int16_t *retval);

/* Alternate interface for adding to a buffer. */
Buf_cursor   buf_prepsize (Buffer *b, size_t additional);
void         buf_addlstring_UNSAFE (Buffer *b, const char *s, size_t len);
void         buf_addchar_UNSAFE (Buffer *b, char c);

/* FILE I/O */

/* Write the buffer's length, encoding it in 8 bytes. Returns 0 for OK. */
int buf_writelen (FILE *file, Buffer *b);
/* Write the buffer's contents.  Returns 0 for OK. */
int buf_write (FILE *file, Buffer *b);
/* Read the buffer's length into 'len'. Returns 0 for OK. */
int buf_readlen (FILE *file, size_t *len);
/* Create a new buffer and fill it by reading 'len' bytes */
Buffer *buf_read (FILE *file, size_t len);

#define BUF_INT48_MAX ((int64_t) 0x7FFFFFFFFFFF)
#define BUF_INT48_MIN ((int64_t) (- BUF_INT48_MAX - 1))

#endif
