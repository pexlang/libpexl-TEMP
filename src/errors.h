/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  errors.h                                                                 */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#if !defined(errors_h)
#define errors_h

/* Here, 0 always means OK or SUCCESS.

   The error values from -1 down to -100 are reserved for code that is
   meant to be generic (easily usable outside of this project), like
   the implementation of Tarjan's algorithm for Strongly Connected
   Components in scc.c.  

   Error values in such generically useful code are allowed to overlap
   with each other, and so they should not escape beyond the PEXL API.
   In other words, any user of a PEXL API should see only the error
   values given in this file.

   Similarly, the value -999999 is reserved in common.h for a generic
   ERR value, and this value should not be returned to the caller of a
   PEXL API.
*/

/* ----------------------------------------------------------------------------- */
/* COMPILER                                                                      */
/* ----------------------------------------------------------------------------- */

/* env.c */
#define ENV_OK                       0  /* must be 0 */
#define ENV_NOT_FOUND             -101  /* not an error, per se */
#define ENV_ERR_EXISTS            -102  /* name already bound */
#define ENV_ERR_NULL              -103  /* NULL arg where not allowed */
#define ENV_ERR_MAX_FIXEDLEN      -104  /* pattern's fixed length too long */
#define ENV_ERR_OOM               -105  /* out of memory */
#define ENV_ERR_NOT_PATTERN       -106  /* type is not pattern. */
#define ENV_ERR_INTERNAL          -107  /* internal error, should not happen! */
#define ENV_ERR_NAMELEN           -108  /* name cannot be empty nor too long for string table */
#define ENV_ERR_FULL              -109  /* binding table full and at ENV_MAX_SIZE */ 

/* expression.c */
#define EXP_ERR_OOM               -201	/* out of memory */
#define EXP_ERR_INTERNAL          -202 /* internal error; this is a bug */
#define EXP_ERR_OPENFAIL          -203 /* encountered TOpenCall node */
#define EXP_ERR_NULL              -204 /* required arg is null (see log) */

/* analyze.c */
/* #define EXP_VARLEN                -251	/\* Pattern may not have a fixed length *\/ */
#define EXP_ERR_NOTCOMPILED       -252 /* Dependent value has not been compiled */
#define EXP_ERR_PATLEN            -253 /* Pattern length exceeds EXP_MAX_BEHIND */
#define EXP_ERR_CAPTURES          -254 /* Pattern has captures where not allowed */
#define EXP_ERR_NO_PATTERN        -255 /* No value, or value not a pattern */
#define EXP_ERR_SCOPE             -256 /* Call target not in scope */
#define EXP_ERR_LEFT_RECURSIVE    -257 /* Pattern is left-recursive */

/* compile.c */
#define COMPILE_ERR_OOM           -301	/* out of memory */

/* codegen.c */
#define CODEGEN_ERR_OOM           -401 /* Out of memory */
#define CODEGEN_ERR_INTERNAL      -402 /* Internal error, should not happen! */
#define CODEGEN_ERR_NULL          -403 /* Required arg was null */
#define CODEGEN_ERR_IRTYPE        -404 /* Unknown intermediate rep (tree) type */ 
#define CODEGEN_ERR_OPENFAIL      -405 /* OpenCall failed; Or, unexpected TOpenCall node */
#define CODEGEN_ERR_NOTCOMPILED   -407 /* Encountered PATTERN_READY not set */
#define CODEGEN_ERR_SYMTAB_FULL   -408 /* Symbol table full */
#define CODEGEN_ERR_NO_PACKAGE    -409 /* Missing package in package table (XCall) */
#define CODEGEN_ERR_SYMTAB_BLOCK_FULL -410 /* Block storage in symbol table full */

/* interface.c */
#define ERR_BADREF                -501 /* Bad reference */
#define ERR_REFNOTPATTERN         -502 /* Reference value not a pattern */
#define ERR_INTERNAL              -503 /* Bug. Should not happen. */

/* ----------------------------------------------------------------------------- */
/* RUNTIME                                                                       */
/* ----------------------------------------------------------------------------- */

#define  MATCH_OK                 0
#define  VM_OK                    0

/* Matching system errors begin at 1000 */
#define  MATCH_ERR_INTERNAL    -1001
#define  MATCH_ERR_OOM         -1002
#define  MATCH_ERR_BADINST     -1003
#define  MATCH_ERR_CAP         -1004
#define  MATCH_ERR_INPUT_LEN   -1005
#define  MATCH_ERR_STARTPOS    -1006
#define  MATCH_ERR_ENDPOS      -1007
#define  MATCH_ERR_OUTPUT_MEM  -1008
#define  MATCH_ERR_NOLINK      -1009
#define  MATCH_HALT            -1010
#define  MATCH_ERR_STACK       -1011

/* VM errors begin at 1100 */
#define  VM_ERR_NULL             -1101 /* null argument where not allowed */
#define  VM_ERR_ENTRYPOINT       -1102 /* invalid entrypoint */

/* Capture processing errors begin at 1200 */
#define  MATCH_OPEN_ERROR      -1201
#define  MATCH_CLOSE_ERROR     -1202
#define  MATCH_FULLCAP_ERROR   -1203
#define  MATCH_STACK_ERROR     -1204
#define  MATCH_INVALID_ENCODER -1205

#endif
