/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  env.h  Environment and value types                                       */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#if !defined(env_h)
#define env_h

#include "common.h"
#include "expression.h"		/* Ref */

/* 
   ENVIRONMENT

   An ENVIRONMENT is a tree in which each node is a collection of
   bindings.  A binding is either anonymous or it has a name that is
   unique in its local collection.

   Each of these collections is an 'Env'.

   An Env is a map from names to boxed values.  (An Env may also
   contain anonymous bindings, which are accessed by Refs.)  An Env
   has a parent pointer (to another Env).  It is prohibited, though
   not enforced, to create Env cycles, and so the environment forms a
   tree.

   The maximum number of entries in a single Env is subject to these
   limits:

   - Must fit in 24 bits (unsigned) due to how vm instructions are
     encoded.

   See package.h for PACKAGE and MACHINE definitions.

*/

/* ----------------------------------------------------------------------------- */
/* Parameters that can be tuned for performance                                  */
/* ----------------------------------------------------------------------------- */

#define ENV_INIT_SIZE 7		/* Default allocation of entries in new env */

/* ----------------------------------------------------------------------------- */
/* Constants that should not be changed without careful consideration            */
/* ----------------------------------------------------------------------------- */

#define ENV_MAX_SIZE (1<<22)    /* Max number of bindings, must fit in EnvIndex */

/* ----------------------------------------------------------------------------- */
/* Value types                                                                   */
/* ----------------------------------------------------------------------------- */

/* When adding a new ValueType that holds a pointer, be sure to free
   it in the env_free() in interface.c
*/
typedef enum ValueTypes {
  Eerror_t = 0,		   /* error */
  Eunspecified_t,          /* unspecified "placeholder", pointer field is NULL */
  Epattern_t,		   /* pointer to Pattern */
  Eexpression_t,           /* pointer to tree IR */
  Epackage_t,		   /* pointer to Package */
  Estring_t,		   /* data is handle into string table */
  Eint32_t,		   /* data is int value */
  Efunction_t,		   /*  */
  Emacro_t		   /*  */
} ValueTypes;

/* Due to 8-byte alignment, there's no reason to pack type & data together */
typedef struct Value {
  void       *ptr;
  int32_t     type;
  int32_t     data;
} Value;

/* #define ENV_MAX_NESTING (1 << 12) /\* Can be raised to 2^24 (see above) *\/ */

/* ----------------------------------------------------------------------------- */
/* Bindings and Refs                                                             */
/* ----------------------------------------------------------------------------- */

typedef struct Binding {
  struct Env       *env;        /* binding is in this env */
  struct DepList   *deplist;    /* NULL-terminated list of dependencies */
  Value             val;
  Index           name;
  uint32_t          meta;
} Binding;

#define bindingtype(binding) ((binding)->val.type)

typedef enum Binding_attr {
 Eattr_deleted = bitpos(0),		/* Marked for deletion */
 Eattr_visible = bitpos(1)		/* Visible to parent env */
} Binding_attr;

#define binding_flags(b) ((b)->meta)
#define set_binding_flags(b, flags) do {			\
  (b)->meta = flags;						\
  } while (0)

#define binding_has(b, flagname) (has_flag(binding_flags(b), (flagname)))
#define set_binding_flag(b, flagname, val) do {				    \
    set_binding_flags((b), with_flag(binding_flags(b), (flagname), (val))); \
  } while(0)

/* ----------------------------------------------------------------------------- */
/* An Env is a single collection of bindings with an optional parent             */
/* ----------------------------------------------------------------------------- */

typedef struct Env {
  Binding      *bindings;	/* bindings array */
  struct Env   *parent;		/* optional parent env */
  Index       size;		/* capacity of bindings array */
  Index       next;		/* next available is bindings[next] */
  uint32_t      meta;		/* flags, see below */
} Env;

typedef enum Env_attr {
  Eattr_recursive = bitpos(0) /* Marked as allowing recursive definitions */
} Env_attr;

#define env_has_attr(env, attr) ((env)->meta & (attr))
#define set_env_attr(env, attr, setting) do {				\
    if (setting)							\
    (env)->meta |= (attr);						\
  else									\
    (env)->meta &= ~(attr);						\
  } while (0)

/* ----------------------------------------------------------------------------- */
/* Interfaces                                                                    */
/* ----------------------------------------------------------------------------- */

Env   *env_new  (Env *parent, int recursive);
void   env_free (Env *env);

/* In first call to iter(), set index to ITER_START. Returns NULL after last binding. */
#define ENV_ITER_START 0
Binding *env_local_iter(Env *env, Index *index);

/* Search by name only in local env, not ancestors; a deleted entry is "not found" */
Ref env_local_lookup (Env *env, Index entry);

/* Search by name locally then up through ancestors; a deleted entry is "not found" */
Ref env_lookup (Env *env, Index entry);

/* Direct access, returns NULL if binding is marked deleted */
Binding *env_get_binding (Ref ref);

int env_bind (Env *env, Index entry, Value val, Ref *ref);
int env_rebind (Ref ref, Value val);
int env_unbind (Ref ref);

/* ----------------------------------------------------------------------------- */
/* Internal API                                                                  */
/* ----------------------------------------------------------------------------- */

/* Values */

Value env_new_value (int32_t type, int32_t data, void *ptr);

/* Other value type makers are defined elsewhere */
/* Value env_new_value_error (int number); */
/* Value env_new_value_unspecified (void); */
/* Value env_new_value_int32 (int32_t i); */
/* Value env_new_value_string (SymbolTableIndex entry); */

#endif
