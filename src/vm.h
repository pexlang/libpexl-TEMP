/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  vm.h                                                                     */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */


#if !defined(vm_h)
#define vm_h

#include "common.h"
#include "capture.h"

/* Parms for call/backtrack stack in vm.c */
/* Typical usage with pattern all.things needs 12 backtrack stack frames. */
#define INIT_BACKTRACKSTACK	 40
#define MAX_BACKTRACK            USHRT_MAX

/* Capture list assembled by vm() grows and shrinks (when submatches fail) */
#define INIT_CAPLISTSIZE         1000
#define MAX_CAPLISTSIZE          INT32_MAX

/* Parms for capture nesting depth (stack used by caploop in walk_captures) */
#define INIT_CAPDEPTH            40
#define MAX_CAPDEPTH             USHRT_MAX 


/* static const char *MATCH_MESSAGES[] __attribute__ ((unused)) = { */
/*   /\* Match vm: *\/ */
/*   "ok", */
/*   "halt/abend", */
/*   "backtracking stack limit exceeded", */
/*   "invalid instruction for matching vm", */
/*   "capture limit exceeded (or insufficient memory for captures)", */
/*   "input too large", "invalid start position", "invalid end position", */
/*   "insufficient memory for match data", */
/*   "no entrypoint for symbol", */
/*   /\* Capture processing: *\/ */
/*   "open capture error in rosie match", */
/*   "close capture error in rosie match", */
/*   "full capture error in rosie match", */
/*   "capture stack overflow in rosie match", */
/*   "invalid encoder in rosie match", */
/*   "implementation error", */
/*   "out of memory", */
/* }; */

typedef struct Stats {
  unsigned int total_time;
  unsigned int match_time;
} Stats;

typedef struct Match {
  short   matched;  /* boolean; if 0, then ignore data field */
  short   abend;    /* boolean; meaningful only if matched==1 */
  size_t  leftover; /* leftover characters, in bytes */
  Buffer *data;	    /* match structure (compact linear encoding) */
} Match;

Match *match_new (void);
void   match_free (Match *m);

int vm_start (PackageTable *pt,
	      Index entrypoint,
	      Buffer *input, size_t startpos, size_t endpos,
	      Encoder encoder,
	      /* in/out */
	      Stats *stats,
	      /* outputs: */
	      Match *match);

extern Encoder debug_encoder; 
extern Encoder byte_encoder; 
extern Encoder noop_encoder; 

#endif

