/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  capture.c  The byte and debug (printing) capture processors              */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#if !defined(capture_h)
#define capture_h

#include "common.h" 
#include "package.h"
#include "instruction.h"
#include "buf.h"

#define capidx(cap) ((cap)->data & 0xFFFFFF)
#define setcapidx(cap, newidx) do {		\
    (cap)->data &= 0xFF000000;			\
    (cap)->data |= (newidx);			\
  } while (0)
#define capkind(cap) (((cap)->data >> 24) & 0xFF) 
#define setcapkind(cap, kindval) do {				\
    (cap)->data &= 0xFFFFFF;					\
    (cap)->data |= (((uint32_t) ((kindval) & 0xFF)) << 24);	\
} while (0)

/* Notes:
   - The high 8 bits of 'data' stores the capture kind, and the low 24
     bits is the (unsigned) offset of the capture name in the string
     block.
   - The size of a Capture struct is currently 16 bytes.
*/
typedef struct Capture {
  const char    *s;	     /* position in input data */
  Index          pkg_number; /* package whose code is being executed */
  uint32_t       data;	     /* see above */
} Capture;

#define isopencap(cap)	(!(capkind(cap) & 0x80)) /* test high bit */
#define isfinalcap(cap)	(capkind(cap) == Cfinal)
#define isclosecap(cap)	((capkind(cap) & 0x80) && (capkind(cap) != Cfinal))

typedef struct CapState {
  const Capture      *ocap;	/* (original) capture list */
  const char         *s;	/* input string */
  PackageTable       *pt;
  Index               n;	/* number of capture frames at ocap */
  int                 pos_size;	/* 2, 4, or 6: bytes needed to encode position */
} CapState;


#define acceptable_capture(kind) (((kind) == Crosiecap) || ((kind) == Crosieconst) || ((kind) == Cbackref))

/* FUTURE: 
   - Revise to expand range of representable numbers 
   - Use a faster itoa algorithm
*/

/* Signed 32-bit integers: from −2,147,483,648 to 2,147,483,647  */
#define MAXNUMBER2STR 16
#define INT_FMT "%d"
#define r_inttostring(s, i) (snprintf((char *)(s), (MAXNUMBER2STR), (INT_FMT), (i)))

int debug_Close(CapState *cs, const Capture *cap, Buffer *buf, const int count);
int debug_Open(CapState *cs, const Capture *cap, Buffer *buf, const int count);

int byte_Close(CapState *cs, const Capture *cap, Buffer *buf, const int count);
int byte_Open(CapState *cs, const Capture *cap, Buffer *buf, const int count);

int noop_Close(CapState *cs, const Capture *cap, Buffer *buf, const int count);
int noop_Open(CapState *cs, const Capture *cap, Buffer *buf, const int count);

typedef struct {  
  int (*Open)(CapState *cs, const Capture *cap, Buffer *buf, const int count);
  int (*Close)(CapState *cs, const Capture *cap, Buffer *buf, const int count);
} Encoder;
 
#endif
