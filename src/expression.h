/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  expression.h                                                             */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#if !defined(expression_h)
#define expression_h

#include "common.h"
#include "hashtable.h"
#include "charset.h"
#include "package.h"

/* number of siblings for each tree */
static const uint8_t numsiblings[] = {
  0, 0, 0,	  /* char, set, any */
  0, 0,		  /* true, false */	
  1, 2, 2,	  /* rep, seq, choice */
  1, 1,		  /* not, ahead */
  0,		  /* open call to ref */
  0,		  /* call */
  0,		  /* xcall */
  1,		  /* behind */
  1,		  /* capture */
  2,		  /* function (binary, unary, or 0-ary) */
  0,		  /* halt the vm */
  0,		  /* no tree (placeholder) */
};

/* Types of trees (stored in expression->tag) */
typedef enum TTag {
  TChar = 0, TSet, TAny,  /* standard PEG elements */
  TTrue, TFalse,
  TRep, TSeq, TChoice,
  TNot, TAhead,
  TCall,
  TXCall,
  TOpenCall,
  TBehind,   /* match behind */
  TCapture,  /* capture (various kinds) */
  TFunction, /* dispatch to primitive function, e.g. backref */
  THalt      /* stop the vm (abend) */
} TTag;

/*
  Expressions are trees.

  The first sibling of a tree (if there is one) is immediately after
  the tree.  A reference to a second sibling (ps) is its position
  relative to the position of the tree itself.  
 
  A Node is 14 bytes, aligned to 8, so will consume 16 bytes of storage.
 */
/* FUTURE: REWORK the Node structure */
typedef struct Node {
  union {
    struct Env     *env;	/* for TOpenCall, which uses 'index' also */
    struct Package *pkg;	/* for TXCall, which uses 'index' also */
    struct Pattern *pat;	/* for TCall */
    int32_t constant_capture_offset;
  } v;
  union {
    Index    index;	/* index into env's binding table */
    int32_t    ps;	/* occasional second sibling */
    int32_t    n;	/* occasional counter */
  } u;
  uint8_t      tag;
  uint8_t      cap;	/* kind of capture (if it is a capture) */
} Node;

typedef struct Expression {
  uint32_t len;	      /* Number of instances of Node */
  Node     node[1];   /* Must be last, because it will grow via realloc */
} Expression;

/* access to child nodes */
#define child1(t)         ((t) + 1)
#define child2(t)         ((t) + (t)->u.ps)

/* number of slots needed for 'n' bytes */
#define bytes2slots(n)  (((n) - 1) / sizeof(Node) + 1)

/* We cannot return through the API a pointer to a binding, because
   that pointer may not be valid after an expansion (realloc) of the
   bindings table.  But the index of a binding within the bindings
   table does not change during expansion.  So a Ref points to a
   binding by a pair: (env, index) which indexes into the binding
   array of 'env'.
 */

typedef struct Ref {
  struct Env *env;   /* NULL means "not found" or error (see below) */
  Index     index; /* binding index or error code */
} Ref;

/* NULL env and index > 1 is a corrupt reference (should not occur) */
#define ref_not_found(r) (((r).env == NULL) && ((r).index == 0))
#define ref_is_special(r) (((r).env == NULL) && ((r).index == 1))
#define ref_is_error(r) (((r).env == NULL) && ((r).index < 0))

#define set_ref_from_node(r, node) do {	\
    (r).env = (node)->v.env;			\
    (r).index = (node)->u.index;		\
  } while (0);

#define set_node_ref(node, r) do {		\
    (node)->v.env = (r).env;			\
    (node)->u.index = (r).index;		\
  } while (0);

typedef struct XRef {
  struct Package *pkg;		/* NULL means "not found" or error (see below) */
  Index         index;	/* symbol table index or error code */
} XRef;

/* NULL pkg and index >= 0 is a corrupt reference (should not occur) */
#define xref_is_error(r) (((r).pkg == NULL) && ((r).index < 0))

#define set_xref_from_node(r, node) do {	\
    (r).pkg = (node)->v.pkg;			\
    (r).index = (node)->u.index;		\
  } while (0);

#define set_node_xref(node, r) do {		\
    (node)->v.pkg = (r).pkg;			\
    (node)->u.index = (r).index;		\
  } while (0);

/* Maximum length of strings that 'from_string' or 'from_bytes' will accept. */
#define EXP_MAXSTRING  (1 << 20)	/* 1Mb */

/* Internal use */
Expression *newtree (int len);

/* General */
void        pexle_free (Expression *tree);
Expression *pexle_copy (Expression *tree);
int         to_charset (Node *node, Charset *cs);

/* ----------------------------------------------------------------------------- */
/* Context (structure needed for building expressions                            */
/* ----------------------------------------------------------------------------- */

typedef struct Context {
  struct Env         *env;	  /* top level environment, passed in to context_new() */
  struct CompState   *cst;	  /* set by compile_(), used then freed by link_() */
  HashTable          *stringtab;  /* created by context_new() */
  PackageTable       *packages;	  /* created by context_new() */
} Context;

/* Interning strings */
Index     context_intern (Context *C, const char *name);
const char *context_retrieve (Context *C, Index offset);

/* Context create/destroy */
Context *context_new (struct Env *env);
void     context_free (Context *C);

/* ----------------------------------------------------------------------------- */
/* Compilation state (internal)                                                  */
/* ----------------------------------------------------------------------------- */

typedef struct CompState {
  struct Pattern    *pat;
  struct CFgraph    *g;
  Index              err_index;	/* index in CFgraph of pattern causing an error */
} CompState;

CompState *compstate_new (void);
void       compstate_free (CompState *cs);

/* ----------------------------------------------------------------------------- */
/* Interface (external API) for constructing expressions                         */
/* ----------------------------------------------------------------------------- */

/* Primitive patterns */
Expression *pexle_halt (Context *C);
Expression *pexle_from_bytes (Context *C, const char *ptr, size_t len);
Expression *pexle_from_string (Context *C, const char *str);
Expression *pexle_from_number (Context *C, int n);
Expression *pexle_from_boolean (Context *C, int bool);
Expression *pexle_from_set (Context *C, const char *set, size_t len);
Expression *pexle_from_range (Context *C, uint8_t from, uint8_t to);

/* Binary combinators */
Expression *pexle_seq (Context *C, Expression *tree1, Expression *tree2);
Expression *pexle_choice (Context *C, Expression *tree1, Expression *tree2);
Expression *pexle_subtract (Context *C, Expression *t1, Expression *t2);
/* These will free their expression arguments, to facilitate inline use */
Expression *pexle_seq_f (Context *C, Expression *t1, Expression *t2);
Expression *pexle_choice_f (Context *C, Expression *t1, Expression *t2);
Expression *pexle_subtract_f (Context *C, Expression *t1, Expression *t2);

/* Unary combinators */
Expression *pexle_not (Context *C, Expression *tree);
Expression *pexle_lookahead (Context *C, Expression *tree);
Expression *pexle_lookbehind (Context *C, Expression *tree1);
Expression *pexle_repeat (Context *C, Expression *tree1, int n);
/* These will free their expression arguments, to facilitate inline use */
Expression *pexle_not_f (Context *C, Expression *t1);
Expression *pexle_lookahead_f (Context *C, Expression *t1);
Expression *pexle_lookbehind_f (Context *C, Expression *t1);
Expression *pexle_repeat_f (Context *C, Expression *t1, int n);

/* Call */
Expression *pexle_call (Context *C, Ref ref);
Expression *pexle_xcall (Context *C, XRef xref);

/* Captures */
/* Note that string arguments are never freed by these PEXL functions */
Expression *pexle_constant (Context *C, const char *name, const char *value);
Expression *pexle_capture (Context *C, const char *name, Expression *tree1);
/* These will free their expression arguments, to facilitate inline use */
Expression *pexle_capture_f (Context *C, const char *name, Expression *t1);

/* TODO: Change to use TFunction */
/* EXPERIMENTAL */
/* Expression *pexle_backreference (Env *env, const char *capname); */


#endif
