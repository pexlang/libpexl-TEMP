/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  symboltable.c  String table (ELF style)                                  */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "symboltable.h"
#include <stdlib.h>
#include <string.h>

SymbolTable *symboltable_new (size_t initial_size, size_t initial_blocksize) {
  SymbolTable *st;
  if (initial_size == 0) initial_size = SYMBOLTABLE_INITSIZE; /* Default */
  if (initial_size > SYMBOLTABLE_MAXSIZE) initial_size = SYMBOLTABLE_MAXSIZE;
  if (initial_blocksize == 0) initial_blocksize = SYMBOLTABLE_INITBLOCKSIZE; /* Default */
  if (initial_blocksize > SYMBOLTABLE_MAXBLOCKSIZE) initial_blocksize = SYMBOLTABLE_MAXBLOCKSIZE;
  st = malloc(sizeof(SymbolTable));
  if (!st) {
    LOG("symboltable_new", "out of memory");
    return NULL;
  }
  st->entries = malloc(initial_size * sizeof(SymbolTableEntry));
  if (!st->entries) {
    free(st);
    LOG("symboltable_new", "out of memory");
    return NULL;
  }
  st->size = initial_size;
  st->next = 0;
  st->block = malloc(initial_blocksize * sizeof(char));
  if (!st->block) {
    free(st->entries);
    free(st);
    LOG("symboltable_new", "out of memory");
    return NULL;
  }
  st->blocksize = initial_blocksize;
  st->block[0] = '\0';
  st->blocknext = 1;		/* block[0] always 0 */
  return st;			/* OK */
}

void symboltable_free (SymbolTable *st) {
  if (!st) return;
  free(st->entries);
  free(st->block);
  free(st);
  return;
}

/* Return number of entries (how many entries are in use) */
size_t symboltable_len (SymbolTable *st) {
  return st->next;
}

/* Return length in bytes (how much of the block is in use) */
size_t symboltable_blocklen (SymbolTable *st) {
  return st->blocknext;
}

static int ensure_blockspace(SymbolTable *st, size_t len) {
  size_t newsize;
  void *temp;
  if ((st->blocksize - st->blocknext) > len) return 0; /* OK */
  /* Avoid very small allocations */
  if (st->blocksize < 2048)
    newsize = 2 * 2048;
  else
    newsize = 2 * st->blocksize;
  if (newsize > SYMBOLTABLE_MAXBLOCKSIZE)
    newsize = SYMBOLTABLE_MAXBLOCKSIZE;
  /* Ensure we are adding enough new space for len bytes plus a NUL */
  if (newsize < st->blocknext + len + 1)
    newsize = st->blocknext + len + 1;
  /* Would this exceed the configured maximum? */
  if (newsize > SYMBOLTABLE_MAXBLOCKSIZE) {
    LOG("symboltable_add", "symbol table block full");
    return SYMBOLTABLE_ERR_BLOCKFULL;
  }
  temp = realloc(st->block, newsize);
  if (!temp) {
    LOG("symboltable_add", "out of memory");
    return SYMBOLTABLE_ERR_OOM;
  }
  st->block = temp;
  st->blocksize = newsize;
  return 0;			/* OK */
}

static int ensure_space (SymbolTable *st) {
  size_t newsize;
  void *temp;
  if (st->next < st->size) return 0; /* OK */
  if (st->size >= SYMBOLTABLE_MAXSIZE) {
    LOG("symboltable_add", "symbol table full");
    return SYMBOLTABLE_ERR_FULL;
  }
  newsize = 2 * st->size;
  if (newsize > SYMBOLTABLE_MAXSIZE) newsize = SYMBOLTABLE_MAXSIZE;
  temp = realloc(st->entries, newsize * sizeof(SymbolTableEntry));
  if (!temp) {
    LOG("symboltable_add", "out of memory");
    return SYMBOLTABLE_ERR_OOM;
  }
  st->entries = temp;
  st->size = 2 * st->size;
  LOGf("ensure_space", "extending symbol table to %ld slots (%ld in use)", st->size, st->next);
  return 0;			/* OK */
}

#if 0
/* Iterate over the storage block by finding each NUL terminator, and
   determining if 'name' matches the characters leading up to it.
   This is a LINEAR SEARCH and will be slow with large block sizes.
*/
static Index block_search (SymbolTable *st, const char *name, size_t len) {
  Index prev, i, z, start, test, maxlen;
  prev = 0;	                   /* Block guaranteed to be at least 1 byte long */
  assert(st->block[prev] == '\0'); /* First byte guaranteed to be \0 */
  i = 1;
/*   printf("block_search: len = %lu'\n", len); */
  for (;;) {
    if (((size_t) i) >= st->blocknext) {
      return 0; /* NOT FOUND */
    }
    assert(st->block[i] != '\0');
    maxlen = st->blocknext - i;
/*     printf("st->blocknext = %lu, i = %d, maxlen = %d\n", st->blocknext, i, maxlen); */
    z = strnlen(&st->block[i], maxlen);
/*     printf("  initial z = %d\n", z); */
    if (z == maxlen) break;	/* no more NUL string terminators to check */
    z = z + i;		        /* z is the location of the NUL terminator */
/*     printf("  actual z = %d\n", z); */
    start = z - len;
/*     printf("  start = %d\n", start); */
    if (start >= i) {		/* 'name' would fit here */
      test = memcmp(&st->block[start], name, len);
      if (test == 0) {
	return start; /* FOUND */
      }
    }
    prev = z;
    assert(st->block[prev] == '\0');
    i = z + 1;
  }
  return 0;			/* NOT FOUND */
}
#endif

static size_t add_to_block (SymbolTable *st, const char *name) {
  Index index;
  size_t len;
  if (!name) return 0;		/* allow NULL as synonym for empty string */
  len = strnlen(name, SYMBOLTABLE_MAXLEN + 1);
  if (len == 0) return 0;	/* empty string always stored at index 0 */
  if (len > SYMBOLTABLE_MAXLEN) {
    LOG("symboltable_add", "symbol name too long");
    return SYMBOLTABLE_ERR_STRINGLEN;
  }
#if 0
  /* Search block to see if string already exists */
  index = block_search(st, name, len);
  if (index > 0) return index;	/* Found! */
#endif
  if ((index = ensure_blockspace(st, len))) {
    assert( index < 0 );
    return index;
  }
  index = st->blocknext;
  memcpy(st->block + st->blocknext, name, len);
  st->block[st->blocknext + len] = '\0';
  st->blocknext += len + 1;
  return index;			/* > 0 */
}
  
Index symboltable_add (SymbolTable *st,
			 const char *name,
			 int visibility,
			 int symboltype,
			 uint32_t size,
			 Index value,
			 PatternMetaData *meta) {
  Index index, blockindex;
  SymbolTableEntry *entry;
  int err;
  if (!st) {
    LOG("symboltable_add", "null symbol table arg");
    return SYMBOLTABLE_ERR_NULL;
  }
  if ((blockindex = add_to_block(st, name)) < 0) return blockindex;
  if ((err = ensure_space(st))) {
    assert( err < 0 );
    return err;
  }
  index = st->next++;
  entry = &st->entries[index];
  set_entry_name_offset(entry, blockindex);
  set_entry_visibility(entry, visibility);
  set_entry_type(entry, symboltype);
  entry->size = size;
  entry->value = value;
  if (meta) entry->meta = *meta;
  else memset((void *) &(entry->meta), 0, sizeof(PatternMetaData));
  return index;
}

SymbolTableEntry *symboltable_get (SymbolTable *st, Index index) {
  if (!st) {
    LOG("symboltable_get", "null symbol table arg");
    return NULL;
  }
  if (((size_t) index) >= (size_t) st->next) return NULL;
  return &(st->entries[index]);
}

const char *symboltable_get_name (SymbolTable *st, Index index) {
  SymbolTableEntry entry;
  if (!st) {
    LOG("symboltable_get_name", "null symbol table arg");
    return NULL;
  }
  if (index < 0) return NULL;
  if (((size_t) index) >= (size_t) st->next) return NULL;
  entry = st->entries[index];
/*   printf("symboltable_get_name: index = %d, entry_name_offset = %d, string = %s\n", */
/* 	 index, entry_name_offset(&entry), &(st->block[entry_name_offset(&entry)])); */
  return &(st->block[entry_name_offset(&entry)]);
}

const char *symboltable_entry_name (SymbolTable *st, SymbolTableEntry *entry) {
  if (!st) {
    LOG("symboltable_entry_name", "null argument");
    return NULL;
  }
  if ((entry->value < 0) || (((size_t)entry->value) > st->blocksize)) {
    LOG("symboltable_entry_name", "invalid value in symboltable entry");
    return NULL;
  }
  return &(st->block[entry_name_offset(entry)]);
}

/* Start iterating by calling with prev == SYMBOLTABLE_ITER_START.*/
SymbolTableEntry *symboltable_iter(SymbolTable *st, Index *prev) {
  if (!st) {
    LOG("symboltable_iter", "null symbol table arg");
    return NULL;
  }
  if (!prev) {
    LOG("symboltable_iter", "null previous iteration arg");
    return NULL;
  }
  (*prev)++;
  if (*prev < 0) return NULL;
  if (((size_t) *prev) >= st->next) return NULL;
  return &(st->entries[*prev]);
}

/* Start iterating by calling with prev == SYMBOLTABLE_ITER_START.*/
const char *symboltable_block_iter (SymbolTable *st, Index *prev) {
  if (!st) {
    LOG("symboltable_block_iter", "null symbol table arg");
    return NULL;
  }
  if (!prev) {
    LOG("symboltable_block_iter", "null previous iteration arg");
    return NULL;
  }
  if (*prev == SYMBOLTABLE_ITER_START)
    *prev = 0;
  if ((*prev < 0) || (((size_t) *prev) >= st->blocknext))
    return NULL;
  *prev += 1 + strnlen(&(st->block[*prev]), SYMBOLTABLE_MAXLEN + 1);
  if ((*prev < 1) || (((size_t) *prev) >= st->blocknext))
    return NULL;
  return &(st->block[*prev]);
}

/* LINEAR in size of string storage block AND number of symbol table entries. */
SymbolTableEntry *symboltable_search (SymbolTable *st, const char *name_string, Index *index) {
  const char *str;
  Index i = SYMBOLTABLE_ITER_START;
  Index name = SYMBOLTABLE_ITER_START;
  SymbolTableEntry *entry;
  if (!st) {
    LOG("symboltable_search", "null symbol table argument");
    return NULL;
  }
  if (!name_string) {
    LOG("symboltable_search", "null name argument");
    return NULL;
  }
  while ((str = symboltable_block_iter(st, &name)))
    if (strncmp(str, name_string, SYMBOLTABLE_MAXLEN + 1) == 0) break;
  if (!str) return NULL;
  while ((entry = symboltable_iter(st, &i))) 
    if (entry_name_offset(entry) == name) {
      if (index) *index = i;
      return entry;
    }
  return NULL;
}
