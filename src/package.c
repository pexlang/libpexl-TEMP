/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  package.c                                                                */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "package.h"
#include <stdlib.h>
#include <string.h>

/* ----------------------------------------------------------------------------- */
/* Package table                                                                 */
/* ----------------------------------------------------------------------------- */

PackageTable *packagetable_new (size_t initial_packagetable_size) {
  PackageTable *pt;
  if (initial_packagetable_size == 0)
    initial_packagetable_size = PACKAGETABLE_INITSIZE;
  if (initial_packagetable_size > PACKAGETABLE_MAXSIZE)
    initial_packagetable_size = PACKAGETABLE_MAXSIZE;
  pt = malloc(sizeof(PackageTable));
  if (!pt) return NULL;
  assert( initial_packagetable_size > 0 );
  pt->packages = malloc(initial_packagetable_size * sizeof(Package *));
  if (!pt->packages) {
    free(pt);
    return NULL;
  }
  pt->size = initial_packagetable_size;
  pt->next = 1;			/* entry 0 reserved for "user" package */
  pt->packages[0] = NULL;
  return pt;
}

void packagetable_free_packages (PackageTable *pt) {
  uint32_t i;
  if (pt && pt->packages)
    for (i = 0; i < pt->next; i++) package_free(pt->packages[i]);
}

void packagetable_free (PackageTable *pt) {
  if (!pt) return;
  if (pt->packages) free(pt->packages);
  free(pt);
}

static int ensure_package_space (PackageTable *pt) {
  size_t newsize;
  void *temp;
  if (pt->next < pt->size) return PACKAGE_OK;
  if (((size_t) pt->size) >= PACKAGETABLE_MAXSIZE) return PACKAGE_ERR_FULL;
  newsize = 2 * pt->size;
  if (newsize > PACKAGETABLE_MAXSIZE) newsize = PACKAGETABLE_MAXSIZE;
  temp = realloc(pt->packages, newsize * sizeof(Package));
  if (!temp) return PACKAGE_ERR_OOM;
  pt->packages = temp;
  pt->size = newsize;
  LOGf("ensure_package_space", "extending to %d packages, %d used ", pt->size, pt->next);
  return PACKAGE_OK;
}

/* Returns index (>= 0), or an error code < 0. */
int packagetable_add (PackageTable *pt, Package *p) {
  int index;
  const char *importpath;
  if (!pt) {
    LOG("packagetable_add", "null package table arg");
    return PACKAGE_ERR_NULL;
  }
  if (!p) {
    LOG("packagetable_add", "null package arg");
    return PACKAGE_ERR_NULL;
  }
  if (package_importpath(p) == -1)
    return PACKAGE_ERR_IMPORTPATH;
  importpath = symboltable_get_name(p->symtab, package_importpath(p));
  if (!importpath) {
    LOGf("packagetable_add", "package importpath not in symbol table (package %p)", 
	 (void *) p);
    return PACKAGE_ERR_INTERNAL;
  }

  if (packagetable_lookup(pt, importpath)) return PACKAGE_ERR_EXISTS;

  /* At this point we have a unique importpath, which should guarantee
     a unique package.  However, a possible user error would be to add
     a package, change its importpath, and add it again.  This
     presents a problem for packagetable_free(), which would then free
     the same package twice.  So we check for that situation here. */
  for (index = 0; index < pt->next; index++)
    if (p == pt->packages[index]) {
      LOG("packagetable_add", "package in table already, with a different importpath");
      return PACKAGE_ERR_INTERNAL;
    }

  if ((index = ensure_package_space(pt))) {
    assert( index < 0 );
    return index; /* error */
  }
  pt->packages[pt->next] = p;
  p->number = pt->next;
  pt->next++;
  return PACKAGE_OK;
}

/* Set the user package */
int packagetable_set_user (PackageTable *pt, Package *p) {
  if (!pt) {
    LOG("packagetable_set_user", "null package table arg");
    return PACKAGE_ERR_NULL;
  }
  if (!p) {
    LOG("packagetable_set_user", "null package arg");
    return PACKAGE_ERR_NULL;
  }
  if (pt->packages[0])
    LOG("packagetable_set_user", "replacing user package");
  pt->packages[0] = p;
  p->number = 0;
  return PACKAGE_OK;
}

Package *packagetable_get_user (PackageTable *pt) {
  if (!pt) {
    LOG("packagetable_get_user", "null package table argument");
    return NULL;
  }
  return pt->packages[0];
}

Package *packagetable_get (PackageTable *pt, Index package_number) {
  if (!pt) {
    LOG("packagetable_get", "null package table argument");
    return NULL;
  }
  if (package_number < 0) return NULL;
  if (package_number >= pt->next) {
    LOGf("packagetable_get", "package number %d out of range (only %d packages in this table)",
	 package_number, pt->next);
    return NULL;
  }
  return pt->packages[package_number];
}

int packagetable_remove (PackageTable *pt, Package *p) {
  if (!pt || !p) return PACKAGE_ERR_NULL;
  assert( pt->packages );
  if (!pt->packages[p->number]) return PACKAGE_OK; /* idempotent */
  if (pt->packages[p->number] != p) return PACKAGE_NOT_FOUND;
  pt->packages[p->number] = NULL;
  return PACKAGE_OK;
}

static int name_eq (const char *n1, const char *n2) {
  assert( n1 );
  assert( n2 );
  if (strncmp(n1, n2, SYMBOLTABLE_MAXLEN + 1) == 0) return 1;
  return 0; 			/* NOT EQUAL */
}

/* Linear search by importpath. Does NOT examine the user package.
   Returns 0 if not found, else package number (index into package
   table) which is > 0 because we do not check the user package.  If
   error, returns < 0.*/
Index packagetable_lookup (PackageTable *pt, const char *importpath) {
  size_t i;
  Package *p;
  const char *existing_importpath;
  if (!pt) {
    LOG("packagetable_lookup", "null package table arg");
    return PACKAGE_ERR_NULL;
  }
  if (!importpath) importpath = ""; /* synonym for empty string */
  for (i = 1; i < pt->next; i++) {
    p = pt->packages[i];
    /* Be sure to skip NULL entries! */
    if (p) {
      existing_importpath = symboltable_get_name(p->symtab, package_importpath(p));
      if (!existing_importpath) {
	LOGf("packagetable_lookup", "importpath for existing package %p not in its symbol table (%p)",
	     (void *) p, (void *) p->symtab);
	return PACKAGE_ERR_INTERNAL;
      }
      if (name_eq(importpath, existing_importpath)) return i;
    } /* if p */
  } /* for each package in table */
  return 0; /* not found */
}

/* Linear search for package (by the address of the package structure) */
Index packagetable_getnum (PackageTable *pt, Package *pkg) {
  Index i;
  if (!pt) {
    LOG("packagetable_getnum", "null package table argument");
    return PACKAGE_ERR_NULL;
  }
  if (!pkg) return PACKAGE_NOT_FOUND; /* cannot look up NULL */
  for (i = 0; i < pt->next; i++) 
    if (pt->packages[i] == pkg) return i;
  return PACKAGE_NOT_FOUND;
}

/* ----------------------------------------------------------------------------- */
/* Package                                                                       */
/* ----------------------------------------------------------------------------- */

Package *package_new (void) {
  Package *p;
  SymbolTable *st;
  p = malloc(sizeof(Package));
  if (!p) {
    LOG("packge_new", "out of memory");
    return NULL;
  }
  st = symboltable_new(SYMBOLTABLE_INITSIZE, SYMBOLTABLE_INITBLOCKSIZE);
  if (!st) {
    free(p);
    return NULL;		/* already logged */
  }
  p->symtab = st;
  p->source = -1;
  p->code = NULL;
  p->codenext = 0;
  p->codesize = 0;
  p->ep = 0;
  p->number = 0; 
  p->major = 0;
  p->minor = 0;
  p->import_next = 1;
  p->import_size = IMPORTTABLE_INITSIZE;
  p->imports = malloc(IMPORTTABLE_INITSIZE * sizeof(Import));
  if (!p->imports) {
    LOG("packge_new", "out of memory");
    free(p);
    symboltable_free(st);
    return NULL;
  }
  p->imports[0].pkg = NULL;
  p->imports[0].importpath = -1; /* not yet set */
  p->imports[0].prefix = -1;	 /* not yet set */
  return p;
}

void package_free (Package *p) {
  if (!p) return;
  if (p->symtab) symboltable_free(p->symtab);
  if (p->code) free(p->code);
  if (p->imports) free(p->imports);
  free(p);
}

static int add_to_symtab (Package *p, const char *str, int type) {
  int stat;
  stat = symboltable_add(p->symtab, str, SYMBOL_LOCAL, type, 0, 0, NULL);
  /* The symbol table errors below have already been logged */
  if (stat == SYMBOLTABLE_ERR_OOM) return PACKAGE_ERR_OOM; 
  if (stat == SYMBOLTABLE_ERR_STRINGLEN) return PACKAGE_ERR_STRLEN; 
  if (stat == SYMBOLTABLE_ERR_FULL) return PACKAGE_ERR_FULL;
  if (stat == SYMBOLTABLE_ERR_BLOCKFULL) return PACKAGE_ERR_FULL;
  if (stat < 0) return PACKAGE_ERR_INTERNAL;
  return stat;			/* index into symtab */
}

static int addstring (Package *p, const char *str) {
  return add_to_symtab(p, str, SYMBOL_TYPE_STRING);
}

static int addimport (Package *p, const char *str) {
  return add_to_symtab(p, str, SYMBOL_TYPE_IMPORT);
}

static int ensure_import_table_space (Package *p) {
  size_t newsize;
  void *temp;
  if (p->import_next == p->import_size) {
    if (((size_t) p->import_size) >= IMPORTTABLE_MAXSIZE) {
      LOG("package_add_import", "import table full");
      return PACKAGE_ERR_FULL;
    }
    newsize = 2 * p->import_size;
    if (newsize > UINT32_MAX) return PACKAGE_ERR_FULL;
    if (newsize > IMPORTTABLE_MAXSIZE) newsize = IMPORTTABLE_MAXSIZE;
    temp = realloc(p->imports, newsize * sizeof(Import));
    if (!temp) {
      LOG("package_add_import", "out of memory");
      return PACKAGE_ERR_OOM;
    }
    p->imports = temp;
    p->import_size = (uint32_t) newsize;
  }
  return OK;
}

/* Add to the list of imported packages.  If prefix is NULL, then the
   prefix entry will be -1, meaning "unset".  Returns the symbol table
   index if OK, else < 0 if error.
*/
int package_add_import (Package *p, const char *importpath, const char *prefix) {
  SymbolTableEntry *entry;
  Index index;
  int stat;
  if (!p || !importpath) return PACKAGE_ERR_NULL;
  index = SYMBOLTABLE_ITER_START;
  while ((entry = symboltable_iter(p->symtab, &index))) {
    if (entry_type(entry) == SYMBOL_TYPE_IMPORT)
      if (name_eq(importpath, symboltable_entry_name(p->symtab, entry))) {
	LOGf("package_add_import", "package '%s' already in symbol table as IMPORT", importpath);
	return PACKAGE_ERR_EXISTS;
      }
  } /* while */
  stat = ensure_import_table_space(p);
  if (stat < 0) return stat;
  index = addimport(p, importpath);
  if (index < 0) return index;
  p->imports[p->import_next].pkg = NULL;
  p->imports[p->import_next].importpath = index;
  stat = prefix ? addstring(p, prefix) : -1;
  p->imports[p->import_next].prefix = index;
  p->import_next++;
  return index;
}

int package_add_string (Package *p, const char *str) {
  SymbolTableEntry *entry;
  Index index;
  if (!p || !str) return PACKAGE_ERR_NULL;
  index = SYMBOLTABLE_ITER_START;
  while ((entry = symboltable_iter(p->symtab, &index))) {
    if (entry_type(entry) == SYMBOL_TYPE_STRING)
      if (name_eq(str, symboltable_entry_name(p->symtab, entry)))
	return index;
  } /* while */
  return addstring(p, str);
}

int package_set_importpath (Package *p, const char *importpath) {
  Index idx;
  if (!p) return PACKAGE_ERR_NULL;
  idx = addstring(p, importpath);
  if (idx < 0) return idx;
  assert(p->imports);
  p->imports[0].importpath = idx;
  return PACKAGE_OK;
}

int package_set_default_prefix (Package *p, const char *default_prefix) {
  Index idx;
  if (!p) return PACKAGE_ERR_NULL;
  idx = addstring(p, default_prefix);
  if (idx < 0) return idx;
  assert(p->imports);
  p->imports[0].prefix = idx;
  return PACKAGE_OK;
}

int package_set_source (Package *p, const char *source) {
  Index idx;
  if (!p) return PACKAGE_ERR_NULL;
  idx = addstring(p, source);
  if (idx < 0) return idx;
  p->source = idx;
  return PACKAGE_OK;
}

int package_set_lang_version (Package *p, uint8_t major, uint8_t minor) {
  if (!p) return PACKAGE_ERR_NULL;
  p->major = major;
  p->minor = minor;
  return PACKAGE_OK;
}

int package_set_code (Package *p, Instruction *code, uint32_t codesize) {
  if (!p) return PACKAGE_ERR_NULL;
  p->code = code;
  p->codesize = codesize;
  return PACKAGE_OK;
}

/* ----------------------------------------------------------------------------- */
/* Generic utility function, useful for printing for example                     */
/* ----------------------------------------------------------------------------- */

int walk_instructions (Instruction *p, 
		       int codesize, 
		       int (*operation)(Instruction * const, Instruction *, void * const), 
		       void *context) { 
  int stat;
  int n = (int) codesize; 
  Instruction *op = p; 
  while (p < op + n) { 
    if ((stat = (*operation)(op, p, context)) != 0) return stat; 
    p += sizei(p); 
  } 
  return OK;
} 
