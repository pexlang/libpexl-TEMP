/*  -*- Mode: C/l; -*-                                                       */
/*                                                                           */
/*  logging.h                                                                */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

/* NOTE: Requires -Wno-variadic-macros when -Wpedantic is used,
   because -Wpedantic enables a warning that variadic macros are not
   allowed by c89.  And c89 is the recommended dialect for code that
   needs to compile also on Windows (with minGW). 
*/

#if !defined(logging_h)
#define logging_h

#include <stdio.h> 
#include <assert.h> 

/* display() is used in only the most awkward situations, when there
   is no easy way to return a specific error to the caller, AND when
   we do not want to ask the user to recompile with LOGGING in order
   to understand that something very strange and unrecoverable occurred. 
*/
static void __attribute__((unused)) display (const char *msg) {
  fprintf(stderr, "%s: %s\n", __FILE__, msg);
  fflush(stderr);
}

/* ----------------------------------------------------------------------------------------
 * Logging and debugging: Compile with LOGGING=1 to enable logging
 * ----------------------------------------------------------------------------------------
 */

#ifndef LOGGING
#define LOGGING 0
#else
#define LOGGING 1
#endif

#define LOG(who, msg)							\
  do { if (LOGGING) fprintf(stderr, "%s:%d: %s(): %s\n", __FILE__,	\
			    __LINE__, (who), msg);			\
    fflush(stderr);							\
  } while (0)

#define LOGf(who, fmt, ...)						\
  do { if (LOGGING) fprintf(stderr, "%s:%d: %s(): " fmt, __FILE__,	\
			    __LINE__, (who), __VA_ARGS__);		\
    fputs("\n", stderr);						\
    fflush(stderr);							\
  } while (0)

#endif
