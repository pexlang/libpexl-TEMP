/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  print.h                                                                  */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#if !defined(print_h)
#define print_h

#include "symboltable.h" 
#include "env.h" 
#include "package.h"
#include "expression.h" 
#include "pattern.h" 
#include "instruction.h"
#include "vm.h"			/* Match type */

void print_symboltable_stats (SymbolTable *st);
void print_symboltable (SymbolTable *st);

void print_env_stats (Env *env);
void print_env (Env *env, Context *C);

void print_package (Package *p);
void print_importtable (Package *p);

void print_packagetable_stats (PackageTable *pt);
void print_packagetable (PackageTable *pt);

void print_charset (const uint8_t *st);
void fprint_charset (FILE *channel, const uint8_t *st);

void print_exp (Expression *tree, int indent, Context *C);
void print_pattern (Pattern *p, int indent, Context *C);
void print_pattern_metadata (Pattern *p, int indent);

void print_cfgraph (CFgraph *g);
void print_sorted_cfgraph (CFgraph *g);

void print_value (Value V, int indent, Context *C);

void print_instructions (Instruction *const p, int codesize, SymbolTable *symtab);
int  print_instruction (Instruction *const op, Instruction *p, void *context);
int  fprint_instruction (FILE *channel, Instruction *const op, Instruction *p, void *context);

void print_match (Match *match);


/* void  print_deps (Env_coords *deps, Env *env); */

#endif
