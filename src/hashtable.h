/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  hashtable.h     a hash table that interns its string keys                */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#if !defined(hashtable_h)
#define hashtable_h

#include "common.h"
#include <stddef.h>

/* Keys are strings; data fields are 8 bytes; hash indices are int32_t. 

   - A valid hash table index is >= 0, limiting the hash table array
     to 2^31 - 1 entries.

   - The table size must always be a power of 2 in this design.

   - Implements open addressing with quadratic probing.  

  Experiments on small string keys suggest that a simple hash
  function, a simple compression scheme, and quadratic probing works
  great for load factors around 20% and below.

*/

/* Parameters that can be tuned for performance */

#define HASHTABLE_EXPAND_LOAD 23  /* Max allowed load factor % */
#define HASHTABLE_MAX_LOAD 28     /* Expand table if above this load factor % */
#define HASHTABLE_MIN_ENTRIES 50  /* Minimum "expected entries" value */
#define HASHTABLE_AVG_LEN 9	  /* Average "expected string length" value. Note:
				     This value is only used when creating a new
				     string table, not when expanding one. */

/* ----------------------------------------------------------------------------- */
/* Constants that should not be changed without review                           */
/* ----------------------------------------------------------------------------- */

#define HASHTABLE_MAX_KEYLEN  1023    /* Max key length in bytes; must be < 2^31    */
#define HASHTABLE_MAX_SLOTS     22    /* = 2^22 (must be <= 31 to fit into int32_t) */
#define HASHTABLE_MAX_BLOCKSIZE 26    /* = 2^26 (must be <= 31 to fit into int32_t) */

typedef union HashTableData {
  void *ptr;			/* .data.ptr */
  struct int32 {
    int32_t a;			/* .data.int32.a */
    int32_t b;			/* .data.int32.b */
  } int32;
  char chars[8];		/* .data.chars[] */
} HashTableData;

typedef struct HashTableEntry {
  int32_t         block_offset;	/* offset into block storage or < 0 for UNUSED */
  HashTableData data;		/* payload */
} HashTableEntry;

typedef struct HashTable {
  size_t            size;	/* total slots in array */
  size_t            count;      /* count of slots filled */
  size_t            blocksize;	/* size of block in bytes, >= 1 */
  size_t            blockcount;	/* count of bytes in block used, >= 1 */
  char             *block;      /* block storage for strings */
  HashTableEntry *entries;    /* array */
} HashTable;

/* ----------------------------------------------------------------------------- */
/* Interface                                                                     */
/* ----------------------------------------------------------------------------- */

#define HASHTABLE_ITER_START -1    /* initial value for hashtable_iter() */

HashTable      *hashtable_new (size_t exp_entries, size_t exp_string_size);
void            hashtable_free (HashTable *ht);

int32_t         hashtable_add (HashTable *ht, const char *name, HashTableData data);
int32_t         hashtable_add_update (HashTable *ht, const char *name, HashTableData data);

HashTableEntry  hashtable_search (HashTable *ht, const char *name);
HashTableEntry  hashtable_iter (HashTable *ht, int32_t *prev);

const char     *hashtable_get_key (HashTable *ht, HashTableEntry entry);

/* ----------------------------------------------------------------------------- */
/* Error and other codes                                                         */
/* ----------------------------------------------------------------------------- */

#define HASHTABLE_NOT_FOUND     -1  /* must be < 0 */
#define HASHTABLE_ERR_OOM       -2  /* out of memory */
#define HASHTABLE_ERR_KEYLEN    -3  /* max key length exceeded */
#define HASHTABLE_ERR_NULL      -4  /* name is NULL */ 
#define HASHTABLE_ERR_BLOCKFULL -5  /* reached limit of 2^HASHTABLE_MAX_BLOCKSIZE bytes */ 
#define HASHTABLE_ERR_SIZE      -6  /* reached limit of 2^HASHTABLE_MAX_SLOTS */ 
#define HASHTABLE_ERR_INTERNAL  -7  /* this is a bug */ 
#define HASHTABLE_ERR_FOUND     -8  /* entry already exists */

#endif
