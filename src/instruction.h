/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  instruction.h   definitions needed by both compiler and runtime          */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */


#if !defined(instruction_h)
#define instruction_h

#include "common.h"
#include "charset.h"
#include <limits.h>

/* ----------------------------------------------------------------------------- */
/* Captures                                                                      */
/* ----------------------------------------------------------------------------- */

/* We will use 8 bits to store the capture kind, with bit 8 reserved
 * to indicate a closing capture, leaving 0-127 for arbitrary captures
 * and 128-255 for close captures.
 */
typedef enum CapKind { 
  Crosiecap = 0,
  Crosieconst, Cbackref,  
  Cclose = 0x80,		/* high bit set */
  Cfinal, Ccloseconst
} CapKind; 

static const char *const OPEN_CAPTURE_NAMES[] = {
  "RosieCap", "RosieConst", "Backref",
};
static const char *const CLOSE_CAPTURE_NAMES[] = {
  "Close", "Final", "CloseConst",
};

#define CAPTURE_NAME(c) (((c) & 0x80) ? CLOSE_CAPTURE_NAMES[(c) & 0x7F] : OPEN_CAPTURE_NAMES[(c & 0x7F)])

#define MAX_INSTRUCTIONS (1 << 24)  /* 24 bits aux field indexes into code vector */

#define opcode(pc) ((pc)->i.code)
#define setopcode(pc, op) ((pc)->i.code) = (op)

#define addr(pc) ((pc+1)->offset)
#define setaddr(pc, addr) (pc+1)->offset = (addr)

/* Convenient ways of accessing the aux field: */
#define aux(pc) ((pc)->i.aux)
#define signedaux(pc) ((aux((pc)) & 0x800000) ? (int) (aux((pc)) | 0xFF000000) : aux((pc)))
#define setaux(pc, idx) (pc)->i.aux = ((idx) & 0xFFFFFF)
#define ichar(pc) ((pc)->i.aux & 0xFF)
#define setichar(pc, c) (pc)->i.aux = ((pc)->i.aux & 0xFFFF00) | (c & 0xFF)

typedef struct CodeAux {
  uint8_t code;		   /* 8-bit opcode */
  unsigned int aux : 24;   /* 24-bit aux field: [0 .. 16,777,215] env index, or other operands, like chars */
} CodeAux;

typedef union Instruction {
  CodeAux i;	       /* opcode and aux field packed into a 32-bit word */
  int32_t offset;      /* follows an opcode that needs an offset value */
  uint8_t buff[1];     /* char set following an opcode that needs one */
} Instruction;

/* ----------------------------------------------------------------------------- */
/* Instructions                                                                  */
/* ----------------------------------------------------------------------------- */

typedef enum Opcode {
  /* Bare instruction ------------------------------------------------------------ */
  IGiveup = 0,               /* for internal use by the vm */
  INoop,		     /* No operation */
  IAny,			     /* if no char, fail */
  IRet,			     /* return from a rule */
  IEnd,			     /* end of pattern */
  IHalt,		     /* abnormal end (abort the match) */
  IFailTwice,		     /* pop one choice from stack and then fail */
  IFail,                     /* pop stack (pushed on choice), jump to saved offset */
  ICloseCapture,	     /* push close capture marker onto cap list */
  ISuspendCaptures,	     /* suspend capturing until stack unwinds past this point */
  /* Aux ------------------------------------------------------------------------- */
  IBehind,                   /* walk back 'aux' characters (fail if not possible) */
  IChar,                     /* if char != aux, fail */
  /* Charset --------------------------------------------------------------------- */
  ISet,		             /* if char not in buff, fail */
  ISpan,		     /* read a span of chars in buff */
  /* Offset ---------------------------------------------------------------------- */
  ICall,                     /* call entrypoint at 'offset' in current code vector */
  ICloseConstCapture,        /* push const close capture and index onto cap list */
  IBackref,		     /* match same data as prior capture (key in 'offset')*/
  ITestAny,                  /* if no chars left, jump to 'offset' */
  IJmp,	                     /* jump to 'offset' */
  IChoice,                   /* stack a choice; next fail will jump to 'offset' */
  ICommit,                   /* pop choice and jump to 'offset' */
  IBackCommit,		     /* "fails" but jumps to its own 'offset' */
  /* Offset and aux -------------------------------------------------------------- */
  IPartialCommit,            /* update top choice to current position and jump */
  IOpenCapture,		     /* start a capture ('aux' holds, 'offset' holds key) */
  ITestChar,                 /* if char != aux, jump to 'offset' */
  IXCall,		     /* jump to pkgtable[aux]->code[offset] */
  /* Offset and charset ---------------------------------------------------------- */
  ITestSet                   /* if char not in buff, jump to 'offset' */
  /* Offset and aux and charset -------------------------------------------------- */
  /* none (so far) */
} Opcode;

/*
 * Size of an instruction in 32-bit words.  A one-word instruction
 * contains an opcode and maybe some operands in the aux field.
 * Multi-word instructions start with a word containing the opcode
 * (and maybe something in aux) but are followed by other words
 * containing operands.
 */

#define OPCODE_NAME(code) (OPCODE_NAME[(code)])
static const char *const OPCODE_NAME[] = {
 /* Bare instruction ------------------------------------------------------------ */
   "giveup",
   "noop",
   "any",
   "ret",
   "end",
   "halt",
   "failtwice",
   "fail",
   "closecapture",
   "suspendcaptures",
 /* Aux ------------------------------------------------------------------------- */
   "behind",
   "char",
 /* Charset --------------------------------------------------------------------- */
   "set",
   "span",
 /* Offset ---------------------------------------------------------------------- */
   "call",
   "closeconstcapture",
   "backref",
   "testany",
   "jmp",
   "choice",
   "commit",
   "backcommit",
 /* Offset and aux -------------------------------------------------------------- */
   "partialcommit",
   "opencapture",
   "testchar",
   "xcall",
 /* Offset and charset ---------------------------------------------------------- */
   "testset"
};

static const int OPCODE_SIZE[] = {
 /* Bare instruction ------------------------------------------------------------ */
    1, /* "giveup" */
    1, /* "noop" */
    1, /* "any" */
    1, /* "ret" */
    1, /* "end" */
    1, /* "halt" */
    1, /* "failtwice" */
    1, /* "fail" */
    1, /* "closecapture" */
    1, /* "suspendcaptures" */
 /* Aux ------------------------------------------------------------------------- */
    1, /* "behind" */
    1, /* "char" */
 /* Charset --------------------------------------------------------------------- */
    CHARSETINSTSIZE, /* "set" */
    CHARSETINSTSIZE, /* "span" */
 /* Offset ---------------------------------------------------------------------- */
    2, /* "call" */
    2, /* "closeconstcapture" */
    2, /* "backref" */
    2, /* "testany" */
    2, /* "jmp" */
    2, /* "choice" */
    2, /* "commit" */
    2, /* "backcommit" */
 /* Offset and aux -------------------------------------------------------------- */
    2, /* "partialcommit" */
    2, /* "opencapture" */
    2, /* "testchar" */
    2, /* "xcall" */
 /* Offset and charset ---------------------------------------------------------- */
    1 + CHARSETINSTSIZE /* "testset */
};

/* ----------------------------------------------------------------------------- */
/* API                                                                           */
/* ----------------------------------------------------------------------------- */

/* Utilities for other modules */
#define sizei(pc) (OPCODE_SIZE[opcode(pc)])

/* Max length of a string that we can store in the string table.  This
   is set arbitrarily.  The total string storage we can handle is
   currently 2^24-1 bytes, or 2^14 strings if all have max length 2^10.
*/
#define MAX_STRINGLEN 1024

#endif
