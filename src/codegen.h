/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  codegen.h                                                                */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

/*
*/

#if !defined(codegen_h)
#define codegen_h

#include "common.h"
#include "expression.h"
#include "pattern.h"

#define INITIAL_CODESIZE 1000 

int codegen (Context *C, Pattern *ep_pattern, Package *pkg, short optim_mask); 

#endif
