/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  metadata.h  Pattern metadata used by compiler and runtime                */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */


#if !defined(metadata_h)
#define metadata_h

#include "common.h"
#include "charset.h"

typedef struct PatternMetaData {
  uint32_t           flags_;	    /* pattern flags (see below) */
  uint32_t           minlen_;	    /* min bytes consumed in a match */
  uint32_t           maxlen_;	    /* max bytes consumed in a match, unless UNBOUNDED */
  Charset            firstset_;	    /* firstset of pattern (256 bits = 64 bytes) */
} PatternMetaData;

/* Field accessors */

#define metadata_flags(meta) ((meta).flags_)
#define set_metadata_flags(meta, flags) do {				\
    (meta).flags_ = (uint32_t) (flags);					\
  } while (0) 

#define metadata_minlen(meta) ((meta).minlen_)
#define set_metadata_minlen(meta, len) do {				\
    (meta).minlen_ = (uint32_t) (len);					\
  } while (0)

#define metadata_maxlen(meta) ((meta).maxlen_)
#define set_metadata_maxlen(meta, len) do {				\
    (meta).maxlen_ = (uint32_t) (len);					\
  } while (0)

#define metadata_firstset_ptr(meta) (&((meta).firstset_))

/* Maximum length (in bytes) that we are willing to count.  If a
   pattern exceeds this length, we set the UNBOUNDED flag.  When that
   flag is set, the value of the maxlen_ field is meaningless.  Of
   course, the UNBOUNDED flag is also set when we can prove that the
   pattern really is unbounded in the number of bytes it can consume.
*/
#define PATTERN_MAXLEN ((uint32_t) 0x0000FFFF) /* 2^16 */

/* Pattern flags, by bit position (stored in an Pattern struct) */
#define PATTERN_READY      bitpos(0) /* The other flags are valid only when READY */
#define PATTERN_NULLABLE   bitpos(1)
#define PATTERN_NOFAIL     bitpos(2)
#define PATTERN_HEADFAIL   bitpos(3)
#define PATTERN_UNBOUNDED  bitpos(4)
#define PATTERN_NEEDFOLLOW bitpos(5)
#define PATTERN_CAPTURES   bitpos(6)

/* Convenience */
#define metadata_fixedlen(meta) (!has_flag((meta).flags_, PATTERN_UNBOUNDED) \
				 && ((meta).minlen_ == (meta).maxlen_))


#endif
