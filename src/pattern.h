/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  pattern.h  Pattern structures and dependency calculations                */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */


#if !defined(pattern_h)
#define pattern_h

#include "common.h"
#include "metadata.h"
#include "scc.h"

/* ----------------------------------------------------------------------------- */
/* Patterns                                                                      */
/* ----------------------------------------------------------------------------- */

typedef struct Pattern **DepList; /* a deplist is an array (list) of patterns */

typedef struct Pattern {
  struct Expression *tree;	    /* IR (intermediate representation) */
  struct Expression *fixed;	    /* IR with call targets fixed */
  struct Env        *env;	    /* compilation env */
  Index              binding_index; /* >= 0 if bound */
  DepList            deplist;	    /* cache of dependencies (NULL if not compiled) */
  Index              entrypoint;    /* index into instruction vector or -1 if not set */
  uint32_t           codesize;	    /* number of instruction words in code */ 
  PatternMetaData    meta;	    /* flags, length, firstset, etc. */
} Pattern;

/* Convenience */
#define pattern_min_length(p) (metadata_minlen((p)->meta))
#define pattern_max_length(p) (metadata_maxlen((p)->meta))
#define patflags(p) (metadata_flags((p)->meta))

#define set_pattern_minlen(p, len) do {		\
    set_metadata_minlen((p)->meta, (len));	\
  } while (0) 
#define set_pattern_maxlen(p, len) do {		\
    set_metadata_maxlen((p)->meta, (len));	\
  } while (0) 
#define set_patflags(p, flags) do {	        \
    set_metadata_flags((p)->meta, (flags));	\
  } while (0) 

#define pattern_has_flag(p, flagname) (has_flag(patflags(p), (flagname))) 
#define pattern_set_flag(p, flagname, val) do {			\
    set_patflags(p, with_flag(patflags(p),(flagname),(val)));	\
  } while (0)

/* ----------------------------------------------------------------------------- */
/* Pattern interface */
/* ----------------------------------------------------------------------------- */

Pattern *pattern_new (struct Expression *tree, int *err);
void     pattern_free (Pattern *p);
void     pattern_only_free (Pattern *p);
  
/* ----------------------------------------------------------------------------- */
/* Dependency lists and Control Flow Graph                                       */
/* ----------------------------------------------------------------------------- */

typedef struct CFgraph {
  DepList          *deplists;	/* array of DepLists */
  SCC              *order;	/* topological sort, showing cycles */
  Index             next;	/* next free entry is *deplists[next] */
  Index             size;	/* size of *deplists */
} CFgraph;

/* A dependency list tracks how an RPL pattern (recursive or not)
   depends on other patterns, because it includes references (calls)
   to them.

   A 'deplist' starts with a pattern, followed by the patterns it
   depends on (possibly none), followed by NULL.  Here, a pattern has
   type *Pattern.

   The list of dependencies can contain repeated entries, e.g. if A
   makes 3 calls to B and no other calls, its deplist would be:
   { A, B, B, B, NULL }.

   N.B. In the graph structure, the array of deplists must contain
   exactly one for each pattern being compiled.  That is, if pattern A
   is being compiled, there must be exactly one deplist in 'deplists'
   that begins with A.
 */

/* Number of dependencies, plus vertex, plus null terminator */
#define EXP_DEPLIST_INITIAL_SIZE 20
/* Number of vertices in the graph (number of patterns in the package) */
#define EXP_CFG_INITIAL_SIZE 100

/* The DynamicDepList structure is used while building the dependency
   list for an expression, because we cannot know how large it will
   need to grow.  When the dependency list (for a single expression)
   is finished, we (1) put a NULL entry after the last valid entry in
   'deps' to mark the end, and (2) discard the struct itself.  The
   'deps' part is retained, and stored with the expression.
 */
typedef struct DynamicDepList {
  DepList  deps;
  size_t   next;
  size_t   size;
} DynamicDepList;

/* ----------------------------------------------------------------------------- */
/* Dependency interface */
/* ----------------------------------------------------------------------------- */

void cfgraph_free (CFgraph *G);
void deplist_free (DepList  dl);

CFgraph *cfgraph_new (void);
int      build_cfgraph (struct Pattern *pat, CFgraph **retval);
int      add_to_cfgraph (struct Pattern *pat, CFgraph *g);
int      sort_cfgraph (CFgraph *G);

#endif

