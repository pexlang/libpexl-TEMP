/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  scc.c    Strongly Connected Components (and topological sort)            */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

/* Tarjan's algorithm for finding Strongly Connected Components in a
   directed graph, which also produces a topological sort.

   This implementation runs in quadratic time: O(|V|*|E|), whereas
   Tarjan's has worst-case linear time: O(|V| + |E|).

   To speed up this implementation, replace the linear search for a
   vertex in index_of() with a constant time one, using a hash table
   (which can be stored in the Graph_t structure).

   The protocol for using this implementation is:

   - Create a simple graph representation: an array of |V| pointers,
     where each points to an adjacency list.

   - The adjacency list format is a contiguous sequence of 2 or more
     vertex identifiers.  The first identifier is the vertex itself,
     followed by its neighbors, followed by a null vertex terminating
     the list.

   - The representation of a vertex is defined by the caller, who must
     provide:
     - the width (in bytes) of the vertex representation
     - a function that compares two vertices (see below)
     - a pointer to a null vertex, for detecting the end of the list

  The comparison function has the same signature as one passed to
  qsort(), including a context argument, in case that is needed.  The
  only constraint on the comparison function supplied by the caller is
  that it must return 0 iff two identifiers represent the same vertex.
  (It does not need to impose an order on them.)

  OUTPUT

  SCC() returns an array of SCC Components:
  - Each vertex occurs once in this array.  
  - The array order is a topological sort of the input graph.

  - Each vertex has a "component number", corresponding to which SCC
    it belongs.  0 is the only invalid component number.

  - All elements of a single SCC appear contiguously in the array, so
    the end of a component is signaled by a change in the component
    number.

  - In an enhancement to Tarjan's algorithm, the component number
    encodes whether or not the component includes a loop.  Singleton
    components without a self-loop are the only SCCs that do not have
    any loops, and they are tagged with a component number < 0.  All
    other components have loops and have a component number > 0.

   RESTRICTIONS

   The first elements of the dependency lists are the vertices of the
   graph, and thus may not repeat, i.e. no two adjacency lists may
   start with the same vertex.

   Anti-restriction: It is NOT required that edges be listed only
   once.  E.g. a valid adjacency list is {X, Y, Y, Z, Y, null} meaning
   that X depends on both Y and Z.

   EXAMPLES

   The examples will say "A depends on B" to mean "vertex A has a
   directed edge to vertex B".

   * Example 1:  A, B, C, null
                 C, null
		 B, C, D, null
		 D, null

   In this example, node A depends on B and C.  Node C has no
   dependencies; B depends on C and D; and D has no dependencies.

   Output for example 1:   (1, -2) (3, -4) (2 -3) (0 -1)
   
   The first number in each pair is the vertex number, taken from the
   input.  In this example, the vertices numbered 0, 1, 2, 3 are A, C,
   B, D.

   The components of this graph are: 1, 3, 2, 0 (i.e. C, D, B, A).
   No component number repeats, so each vertex is in its own SCC.
   Each component number is < 0, so there are no loops.

   * Example 2, with cycles:  A, B, C, NULL
                              C, NULL
			      D, D, NULL
			      B, A, NULL

   In this example, node A depends on B and C.  Node C has no
   dependencies, node B depends on A, and D depends on itself.
   
   Output from example 2: (1, -2) (3, 1) (0, 1) (2, 4)
   The SCCs are: {1} {3, 0} {2}
   Translated to vertex ids, this is: {C} {B, A} {D}
   The SCC {C} has no loops (and its component number is -2, which is
   negative, to indicate the lack of loops), but the SCC {D} does (and
   its component number is 4).

*/

/* #define SCC_LOGGING LOGGING */
#define SCC_LOGGING 0

#include "scc.h"

#include <alloca.h>
#include <string.h>
#include <assert.h>
#include <stdint.h>
#include <stdlib.h>

#if SCC_LOGGING
#include <stdio.h>
#endif 

#define TRUE 1
#define FALSE 0
#define MIN(i, j) ((i) < (j) ? (i) : (j))

typedef struct Vertex_t {
  Index index;
  Index lowlink;
  uint8_t on_stack;
} Vertex_t;

typedef struct Graph_t {
  uint8_t  *neighbors;		/* adjacency matrix */
  Vertex_t *vertices;		/* meta data */
  Index   size;		/* number of vertices */
  /* 
     Below are the inputs to the algorithm, here for convenience
  */
  void **base;
  int (*compare)(void *, const void *, const void *);
  void *context;		/* first arg to compare() */
  size_t width;
} Graph_t;

typedef struct Index_Stack_t {
  Index *elements;
  Index top;
  Index size;
} Index_Stack_t;

static int adjacency (Graph_t *G, Index i, Index j) {
  return (int) ((&((G)->neighbors)[i*G->size])[j]);
}

static int set_adjacency (Graph_t *G, Index i, Index j) {
  if ((i < 0) || (j < 0)) return 1; /* Error */
  (&((G)->neighbors)[i*G->size])[j] = 1; 
  return 0;			    /* OK */
}

#if SCC_LOGGING
static void print_graph (Graph_t *G) {
  Index i, j;
  fprintf(stderr, "Graph %p, with %d %s\n", (void *) G, G->size, (G->size==1) ? "vertex" : "vertices");
  for (i = 0; i < G->size; i++) {
    for (j = 0; j < G->size; j++) fprintf(stderr, "  %d", adjacency(G, i, j));
    fprintf(stderr, "\n");
  }
}
#endif

static void stack_push (Index_Stack_t *st, Index element) {
  assert( st->top < st->size );
  st->elements[st->top++] = element;
}

static Index stack_pop (Index_Stack_t *st) {
  assert( st->top > 0 );
  st->top--;
  return st->elements[st->top];
}

static void outstack_push (SCC *st, Index element, Index component) {
  SCC_Component *top;
  assert( st->top < st->size );
  top = &(st->elements[st->top++]);
  top->vnum = element;
  top->cnum = component;
}

/* FUTURE: This linear search makes the overall algorithm quadratic.
   Instead, we should build a hash table that maps a vertex to its
   index in the original input (the array 'base').
*/
static Index index_of (char *vertex, Graph_t *G) { 
  Index index;
  for (index = 0; index < G->size; index++) {
    if (G->compare(G->context, G->base[index], vertex) == 0) {
      assert( index < INDEX_MAX );
      return (Index) index;
    }
  }
  if (G->width == 8) /* Guess a representation for the error message */
    LOGf("index_of", "vertex not found: %p", (void *) vertex);
  else
    LOGf("index_of", "vertex not found: %.*s", (int) G->width, vertex);
  return INDEX_UNDEFINED;
}

/* We use an adjacency matrix for the graph edges.  As a result, the
   input graph can have redundant vertices in a single edge list.
   Also, we hash each vertex and store its index (where it appears in
   the original vertex list, 'base').  By doing this, the Tarjan
   algorithm can return Strongly Connected Components containing
   vertex indicies.
*/
static Graph_t *make_graph (void **base, Index nel, Index width, void *null,
			    int (*compare)(void *, const void *, const void *),
			    void *context,
			    int *err) {
  Index i;
  Graph_t *G;
  Vertex_t *v;
  char *neighbor;
  void *edgelist;

  (*err) = SCC_ERR_OOM;
  
  assert( nel > 0 );
  G = malloc(sizeof(Graph_t));
  if (!G) return NULL;
  G->size = nel;
  G->vertices = malloc(nel * sizeof(Vertex_t));
  if (!G->vertices) goto free1;
  G->neighbors = malloc(nel * nel); /* for now, bytes not bits */
  if (!G->neighbors) goto free2;
  memset(G->neighbors, 0, nel * nel);

  /* We store these in the graph struct for easy parameter passing */
  G->base = base;
  G->width = width;
  G->compare = compare;
  G->context = context;

  /* Build the adjacency matrix */
  for (i = 0; i < nel; i++) {
    v = &(G->vertices[i]);
    v->index = INDEX_UNDEFINED;
    v->lowlink = INDEX_UNDEFINED;
    v->on_stack = FALSE;
    edgelist = base[i];			 /* Get edgelist i */
    neighbor = width + (char *)edgelist; /* Skip past the vertex to its neighbors */
    while (compare(context, neighbor, null) != 0) {
      if (set_adjacency(G, i, index_of(neighbor, G))) goto badgraph;
      neighbor += width;
    }
  } /* for each vertex */
  return G;

 badgraph:
  (*err) = SCC_ERR_GRAPH;
  free(G->neighbors);
 free2:
  free(G->vertices);
 free1:
  free(G);
  if (*err == SCC_ERR_OOM) LOG("make_graph", "out of memory");
  if (*err == SCC_ERR_GRAPH) LOG("make_graph", "invalid vertex in adjacency list");
  return NULL;
}

static void free_graph (Graph_t *G) {
  if (!G) return;
  if (G->vertices) free(G->vertices);
  if (G->neighbors) free(G->neighbors);
  free(G);
}

static Index next_edge(Graph_t *G, Index vnum, Index wnum) {
  assert( vnum >= 0 );
  if (wnum == INDEX_UNDEFINED)
    wnum = 0; /* start iterating */
  else
    wnum++;
  assert( wnum >= 0 );
  while ((wnum < G->size) && (adjacency(G, vnum, wnum) == 0)) wnum++;
  if (wnum == G->size) return INDEX_UNDEFINED;
  return wnum;
}

static void connect (Graph_t *G, Index vnum, Index *index, 
		     Index_Stack_t *stack, SCC *outstack) {
  int singleton;
  Index wnum, SCCnum;
  Vertex_t *meta = G->vertices;
#if SCC_LOGGING
  fprintf(stderr, "Entering connect: vnum = %d, index = %d\n", vnum, *index); 
#endif
  assert( *index > 0 );
  meta[vnum].index = *index;
  meta[vnum].lowlink = *index;
  (*index)++;
  stack_push(stack, vnum);
  meta[vnum].on_stack = TRUE;
  /* Explore each edge out of v */
  wnum = INDEX_UNDEFINED;
  while ((wnum = next_edge(G, vnum, wnum)) != INDEX_UNDEFINED) {
    if (meta[wnum].index == INDEX_UNDEFINED) {
      /* Index 'undefined' means we have not explored vertex w */
      connect(G, wnum, index, stack, outstack);
      meta[vnum].lowlink = MIN(meta[vnum].lowlink, meta[wnum].lowlink);
    } else if (meta[wnum].on_stack) {
      /* Destination w of edge v->w is on stack, so w is in same SCC as v */
      meta[vnum].lowlink = MIN(meta[vnum].lowlink, meta[wnum].index);
    } /* Else w not on stack, so we ignore edge v->w because w must be
	 part of some SCC we have already seen */
  } /* while */
  /* If v is the root of a SCC, pop stack and generate the SCC  */
  if (meta[vnum].lowlink == meta[vnum].index) {
#if SCC_LOGGING
    fprintf(stderr, "SCC #%u.  Vertex numbers are:\n", meta[vnum].index); 
#endif
    singleton = TRUE;		/* Enhancement to Tarjan's algorithm */
    do {
      wnum = stack_pop(stack);
      meta[wnum].on_stack = FALSE;
#if SCC_LOGGING
      fprintf(stderr, "  %u\n", wnum); 
#endif
      /* For an SCC to have no loops, it must be a singleton (one
	 vertex) and also lack a self-loop.  These are easy conditions
	 to check.  When met, we assign a negative component number to
	 signal the lack of loops in this SCC.  For speed (really?) we
	 write the test for loops as an arithmetic expression. */
      singleton &= (wnum == vnum);
      SCCnum = meta[vnum].index * (-2 * (singleton & !adjacency(G, vnum, vnum)) + 1);
      outstack_push(outstack, wnum, SCCnum);
    } while (wnum != vnum);
  }
#if SCC_LOGGING
  fprintf(stderr, "Leaving connect\n"); 
#endif
}

void SCC_free (SCC *output) {
  if (!output) return;
  if (output->elements) free(output->elements);
  free(output);
}

/* N.B. Caller must free 'output' when done with it, using SCC_free(). */
int SCC_run (void  **base,	    /* array of vertices, each with its edge list */
	     size_t  nel,	    /* number of vertices */
	     size_t  width,	    /* size of each vertex object (often a pointer) */
	     void   *null,
	     int   (*compare)(void *, const void *, const void *),
	     void   *context,   /* context argument passed to compare() */
	     /* output */
	     SCC **outstack) {

  Index i;
  Index index;
  Graph_t *G;
  Index_Stack_t *stack;	     /* intermediate data during processing */
  int err;

  if ((nel > INDEX_MAX) || (width > INDEX_MAX)) return SCC_ERR_SIZE;

  G = make_graph(base, (Index) nel, (Index) width, null, compare, context, &err);
  if (!G) {
    assert( err < 0 );
    return err;
  }

#if SCC_LOGGING
  if (G->size < 20)
    print_graph(G);
  else
    LOGf("SCC", "Not printing graph (too large at %d vertices)", G->size);
#endif

  stack = malloc(sizeof(Index_Stack_t));
  if (!stack) return SCC_ERR_OOM;
  stack->elements = malloc(nel * sizeof(Index));
  if (!stack->elements) return SCC_ERR_OOM;
  stack->size = (Index) nel;
  stack->top = 0;		/* top element at elements[top-1] */

  (*outstack) = malloc(sizeof(SCC));
  if (!(*outstack)) return SCC_ERR_OOM;
  (*outstack)->elements = malloc(nel * sizeof(SCC_Component));
  if (!(*outstack)->elements) return SCC_ERR_OOM;
  (*outstack)->size = (Index) nel;
  (*outstack)->top = 0;	   /* top element at elements[top-1] */

  index = 1;		   /* first component number (reserve 0 for "invalid") */
  for (i = 0; i < (Index) nel; i++) {
    if (G->vertices[i].index == INDEX_UNDEFINED) connect(G, i, &index, stack, *outstack);
  }
  free(stack->elements);
  free(stack);
  free_graph(G);
  return SCC_OK;
}
  

