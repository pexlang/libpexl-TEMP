/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  compile.h      Compile expressions and environments                      */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#if !defined(compile_h)
#define compile_h

#include "common.h"
#include "pattern.h"
#include "env.h"

/* ----------------------------------------------------------------------------- */
/* Compiler (internal API)                                                       */
/* ----------------------------------------------------------------------------- */

int env_in_context (Env *env, Context *C);

int compile_expression (Context *C, Expression *E, Env *env, Value *retval);
int compile_pattern (Context *C, Pattern *pat, Env *env, Value *retval);
int compile_environment (Context *C, Env *env, Index *errbinding);

int compile_init (Context *C);
int add_pattern (Context *C, Pattern *pat);
int compile_now (Context *C);

int link_pattern (Context *C, Pattern *P, Package **retval, short optim_mask);
int link_environment (Context *C, Env *env, Index *errbinding, Package **retval, short optim_mask);

/* ----------------------------------------------------------------------------- */
/* Interface (external API)                                                      */
/* ----------------------------------------------------------------------------- */

typedef struct Error {
  int     value;
  Index   position;
} Error;

Env     *pexlc_make_env (Env *parent);
Env     *pexlc_make_recursive_env (Env *parent);
void     pexlc_env_free (Env *env);

Context *pexlc_make_context (Env *env);

Ref      pexlc_make_binding (Context *C, Env *env, const char *name);
void     pexlc_bind_expression (Context *C, Expression *exp, Ref ref, Error *err);

Package *pexlc_compile_expression (Context *C, Expression *exp, Env *env, Error *err, short optim_mask);
Package *pexlc_compile_bound_expression (Context *C, Ref ref, Error *err, short optim_mask);
Package *pexlc_compile_environment (Context *C, Env *env, Error *err, short optim_mask);

#endif

