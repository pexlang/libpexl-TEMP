# Libpexl

# libpexl is a library for PEG-like languages

My long term goal is to move software developers away from regular expressions,
which are proven sources of a great variety of bugs.  To do that requires a
viable alternative.  The [Rosie Pattern Language](https://rosie-lang.org)
embodies many of my ideas of what is important in a regex alternative.

As that project has evolved, its C code (which began as a modified
[lpeg](http://www.inf.puc-rio.br/~roberto/lpeg)) needed restructuring.  To
support the features we want to implement, it really needs to be redesigned and
reimplemented.  That is what `libpexl` is.

`libpexl` will become the basis for a "Rosie 2.0" at some point.  Right now it
is a platform for experimenting with language features, i.e. for PEG-like
pattern matching languages.  In our academic research, we are interested in
language design, usability, and implementation.

## License

This project is licensed under the
[BSD 3-clause license](https://opensource.org/licenses/BSD-3-Clause).

