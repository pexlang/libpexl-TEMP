# Architecture

This document describes the architecture of the PEXL project.  At the moment, it
is more a collection of notes, but it will evolve with the project.

## Raison d'être

The Rosie Pattern Language project was implemented in Lua (command line
interface, RPL compiler front-end) and C (compiler back-end, "matching virtual
machine").  The C code was originally based on the Lua `lpeg` project, but more
and more of the `lpeg` code was rewritten or removed with each Rosie release.

Around the time of Rosie v1.2, we decided that while the design of the `lpeg`
"matching vm" remained compelling, we needed to redesign the compiler in order
to achieve our goals.  A rewrite of the back end of the RPL compiler would bring
with it more changes to vm, differentiating it further from its `lpeg` roots.

This project represents the results of the redesign effort.  It contains a new
compiler for a PEG-like language and a new run-time system (vm plus other
components).

Building the project produces `libpexl`, a pure C library with an API for
constructing expressions in a PEG-like language, compiling them, and executing
them -- i.e. matching patterns against a text input.

The project also builds `libpexl_runtime`, which contains only the capabilities
needed at run time to load pre-compiled patterns and execute them.  This library
is smaller than `libpexl` because it lacks support for constructing new patterns
and compiling them.  In production, those capabilities are not needed at run
time, so using `libpexl_runtime` there has the security benefit of reducing the
attack surface.

The new compiler was designed to replace the RPL compiler in the Rosie project,
but could be used to implement other PEG-like languages.


## High level design

TODO:

- Diagram
- Expression construction API
- Expression analysis API (static analysis)
- Compile and link API
- Persistence API
- Runtime API


## Notes on major components

